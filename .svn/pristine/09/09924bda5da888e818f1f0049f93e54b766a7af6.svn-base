package com.econetwireless.payment.gateway.core.executor.populator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.core.executor.TransactionInvocationContextBuilder;
import com.econetwireless.payment.gateway.entities.Application;
import com.econetwireless.payment.gateway.service.application.ApplicationService;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;

public class ApplicationTransactionInvocationContextPopulator implements TransactionInvocationContextPopulator {

	private ApplicationService applicationService;

	private static final Logger logger = LoggerFactory
			.getLogger(ApplicationTransactionInvocationContextPopulator.class);

	public void setApplicationService(ApplicationService applicationService) {
		this.applicationService = applicationService;
	}

	@Override
	public void populate(TransactionInvocationContextBuilder invocationContextBuilder) {
		String applicationCode = invocationContextBuilder.getTransactionRequest().getApplicationCode();
		if (StringUtils.isEmpty(applicationCode)) {
			logger.debug("Application code not supplied for request {}.Returning required parameter error message",
					invocationContextBuilder.getTransactionRequest());
			throw new GatewayApplicationException("Application code is required", "400");
		}
		Application application = applicationService.findApplicationByCode(applicationCode);
		if (application == null) {
			logger.debug("Application not found for request {}.Returning invalid application error message",
					invocationContextBuilder.getTransactionRequest());
			throw new GatewayApplicationException("Unknown application code " + applicationCode, "407");
		}
		if (!application.getActive()) {
			logger.debug("Requested application {} deactivated {}", invocationContextBuilder.getTransactionRequest(),
					application);
			throw new GatewayApplicationException("Application " + applicationCode + " is inactive", "405");
		}
		invocationContextBuilder.application(application);
	}

}
