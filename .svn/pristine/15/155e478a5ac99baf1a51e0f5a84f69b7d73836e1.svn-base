package com.econetwireless.payment.gateway.dao.applicationStatusCode;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.ApplicationStatusCode;
import com.econetwireless.payment.gateway.entities.Direction;

@Repository
public class ApplicationStatusCodeDaoImpl implements ApplicationStatusCodeDao {

	private EntityManager entityManager;

	public ApplicationStatusCode save(final ApplicationStatusCode applicationStatusCode) {
		return entityManager.merge(applicationStatusCode);
	}

	public ApplicationStatusCode findById(final long applicationStatusCodeId) {
		return entityManager.find(ApplicationStatusCode.class, applicationStatusCodeId);
	}

	public ApplicationStatusCode findByApplicationIdCodeAndDirection(final long applicationId, final String code,
			final Direction direction) {
		try {
			return entityManager
					.createNamedQuery("ApplicationStatusCode:findByApplicationCodeAndDirection",
							ApplicationStatusCode.class)
					.setParameter("applicationId", applicationId).setParameter("code", code)
					.setParameter("direction", direction).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public List<ApplicationStatusCode> findByApplicationId(long applicationId) {
		return entityManager.createNamedQuery("ApplicationStatusCode:findByApplication", ApplicationStatusCode.class)
				.setParameter("applicationId", applicationId).getResultList();
	}

	public List<ApplicationStatusCode> findByApplicationIdAndDirection(final long applicationId,
			final Direction direction) {
		return entityManager
				.createNamedQuery("ApplicationStatusCode:findByApplicationAndDirection", ApplicationStatusCode.class)
				.setParameter("applicationId", applicationId).setParameter("direction", direction).getResultList();

	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
