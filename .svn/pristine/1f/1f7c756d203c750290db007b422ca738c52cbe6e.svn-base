package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

import org.springframework.stereotype.Component;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

import noNamespace.COMMANDType;

@Component
public class CustomerLookupTransactionHandler extends AbstractEcocashInboundTransactionHandler {

	@Override
	protected TransactionResponse setAndValidateCommandParameters(final TransactionRequest request,
			final COMMANDType command) {

		final TransactionResponse msidsnValidationResponse = validateEcocashMsisdn(request.getMsisdn(),
				"Customer msisdn required", "Invalid customer msisdn : " + request.getMsisdn());
		if (msidsnValidationResponse != null)
			return msidsnValidationResponse;
		final TransactionResponse currencyValidationResponse = validateCurrency(request.getCurrencyCode(),
				"Currency code required", "Invalid currency code : " + request.getCurrencyCode());
		if (currencyValidationResponse != null)
			return currencyValidationResponse;
		final TransactionResponse sourceValidationResponse = validateSource(request.getSource(),
				"Remittance source required", "Invalid remittance source : " + request.getSource());
		if (sourceValidationResponse != null)
			return sourceValidationResponse;
		command.setMSISDN(formatEcocashMsisdn(request.getMsisdn()));
		command.setPROVIDER("101");
		command.setPAYID("12");
		command.setLANGUAGE1(1);
		command.setTYPE(EcocashInboundHandlerConstants.CommandParameters.CUSTOMER_LOOOKUP_TYPE_CODE);
		command.setSOURCE(request.getSource());
		command.setCHECKSUM(generateCheckSum(command.getTYPE() + command.getMSISDN() + command.getPROVIDER()
				+ command.getLANGUAGE1() + command.getEXTTRANSACTIONID() + command.getSOURCE()));
		return null;
	}

	@Override
	protected TransactionResponse generateTransactionResponse(final COMMANDType responseCommandType) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setStatusCode(responseCommandType.getTXNSTATUS());
		transactionResponse.setDestinationReference(responseCommandType.getTXNID());
		transactionResponse.setStatusReason(responseCommandType.getMESSAGE());
		transactionResponse.setSuccess(EcocashInboundHandlerConstants.ResponseCodes.SUCCESS
				.equalsIgnoreCase(responseCommandType.getTXNSTATUS()));
		transactionResponse.setAccountType(responseCommandType.getUSERTYPE());
		transactionResponse.setAccountName(responseCommandType.getMESSAGE());
		transactionResponse.setAccountStatus(responseCommandType.getCUSTOMERSTATUS());
		transactionResponse.setCurrencyCode(responseCommandType.getDEFAULTCURRENCY());
		return transactionResponse;
	}

	@Override
	protected RequestType getRequestType() {
		return RequestType.REMITTANCE;
	}
}
