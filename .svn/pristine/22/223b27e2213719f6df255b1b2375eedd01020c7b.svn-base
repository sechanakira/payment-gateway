package com.econetwireless.payment.gateway.dao.currency;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.Currency;

@Repository
public class CurrencyDaoImpl implements CurrencyDao {

	private EntityManager entityManager;

	public Currency save(final Currency currency) {
		return entityManager.merge(currency);
	}

	public Currency findById(final long currencyId) {
		return entityManager.find(Currency.class, currencyId);
	}

	public Currency findByNumericCode(final String numericCode) {
		try {
			return entityManager.createNamedQuery("Currency:findByNumericCode", Currency.class)
					.setParameter("numericCode", numericCode).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public Currency findByAlphaCode(final String alphaCode) {
		try {
			return entityManager.createNamedQuery("Currency:findByAlphaCode", Currency.class)
					.setParameter("alphaCode", alphaCode).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public List<Currency> findAll() {
		return entityManager.createNamedQuery("Currency:findAll", Currency.class).getResultList();
	}

	public List<Currency> findByStatus(final boolean active) {
		return entityManager.createNamedQuery("Currency:findByStatus", Currency.class).setParameter("active", active)
				.getResultList();
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
