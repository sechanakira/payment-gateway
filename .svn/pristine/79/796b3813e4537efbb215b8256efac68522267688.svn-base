package com.econetwireless.payment.gateway.core.config;

import java.util.Collection;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.econetwireless.common.strategies.service.ApplicationContextServiceLookup;
import com.econetwireless.common.strategies.service.ServiceLookup;
import com.econetwireless.payment.gateway.core.api.gateway.InvocationContextValidatorsConfig;
import com.econetwireless.payment.gateway.core.executor.DefaultTransactionExecutor;
import com.econetwireless.payment.gateway.core.executor.TransactionExecutor;
import com.econetwireless.payment.gateway.core.executor.TransactionHandlerInvoker;
import com.econetwireless.payment.gateway.core.executor.TransactionHandlerInvokerImpl;
import com.econetwireless.payment.gateway.core.executor.populator.TransactionInvocationContextPopulator;
import com.econetwireless.payment.gateway.core.executor.validator.TransactionInvocationContextValidator;
import com.econetwireless.payment.gateway.service.currency.CurrencyService;
import com.econetwireless.payment.gateway.service.transaction.TransactionService;

@Configuration
@EnableWebMvc
@Import({ InvocationContextPopulatorsConfig.class, InvocationContextValidatorsConfig.class })
@ComponentScan(basePackages = { "com.econetwireless.payment.gateway.core.api",
		"com.econetwireless.payment.gateway.application.interceptor", "com.econetwireless.payment.gateway.aggregator" })
@PropertySources({ @PropertySource("classpath:key-store.properties") })
public class GatewayConfig extends WebMvcConfigurerAdapter {

	@Bean
	public TransactionExecutor transactionExecutor(ServiceLookup serviceLookup, CurrencyService currencyService,
			TransactionHandlerInvoker transactionHandlerInvoker, TransactionService transactionService,
			List<TransactionInvocationContextPopulator> populators,
			Collection<TransactionInvocationContextValidator> validators) {
		final DefaultTransactionExecutor executor = new DefaultTransactionExecutor();
		executor.setServiceLookup(serviceLookup);
		executor.setContextPopulators(populators);
		executor.setContextValidators(validators);
		executor.setCurrencyService(currencyService);
		executor.setTransactionService(transactionService);
		executor.setTransactionHandlerInvoker(transactionHandlerInvoker);
		return executor;
	}

	@Bean
	public ServiceLookup serviceLookup(ApplicationContext applicationContext) {
		return new ApplicationContextServiceLookup(applicationContext);
	}

	@Bean
	public TransactionHandlerInvoker transactionHandlerInvoker(ServiceLookup serviceLookup) {
		final TransactionHandlerInvokerImpl invoker = new TransactionHandlerInvokerImpl();
		invoker.setServiceLookup(serviceLookup);
		return invoker;
	}
}