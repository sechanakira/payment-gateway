package com.econetwireless.payment.gateway.dao.transactionType;

import java.util.List;

import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.entities.TransactionType;

public interface TransactionTypeDao {

	TransactionType save(TransactionType transactionType);
	
	TransactionType findById(long transactionTypeId);
	
	List<TransactionType> findByApplicationId(long applicationId);
	
	TransactionType findByApplicationIdAndCode(long applicationId , String transactionTypeCode);
	
	TransactionType findByApplicationIdAndName(long applicationId , String transactionTypeName);
	
	List<TransactionType> findByStatus(boolean active);
	
	List<TransactionType> findByApplicationIdAndStatus(long applicationId , boolean active);
	
	List<TransactionType> findByIds(long... transactionTypeIds);
	
	List<TransactionType> findByApplicationIdAndDirection(long applicationId , Direction direction);
	
	List<TransactionType> findByApplicationIdDirectionAndStatus(long applicationId , Direction direction , boolean active);
}
