package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

import noNamespace.COMMANDType;
import noNamespace.COMMANDType.SECURITYMODE;

/**
 * Created by shingirai on 5/27/15.
 */
@Component
public class CustomerRegistrationTransactionHandler extends AbstractEcocashInboundTransactionHandler {

	@Override
	protected TransactionResponse setAndValidateCommandParameters(final TransactionRequest request,
			final COMMANDType command) {
		final TransactionResponse validateMsisdnResponse = validateEcocashMsisdn(request.getMsisdn(),
				"Agent msisdn required", "Invalid agent msisdn : " + request.getMsisdn());
		if (validateMsisdnResponse != null) {
			return validateMsisdnResponse;
		}
		final TransactionResponse validateMsisdn2Response = validateEcocashMsisdn(request.getMsisdn2(),
				"Customer msisdn required", "Invalid customer msisdn " + request.getMsisdn2());
		if (validateMsisdn2Response != null) {
			return validateMsisdn2Response;
		}
		if (StringUtils.isEmpty(request.getFirstName())) {
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.MISSING_PARAMETER,
					"Customer first name required", false);
		}
		if (StringUtils.isEmpty(request.getLastName())) {
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.MISSING_PARAMETER,
					"Customer last name required", false);
		}
		if (StringUtils.isEmpty(request.getGender())) {
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.MISSING_PARAMETER,
					"Customer gender required", false);
		}
		if (StringUtils.isEmpty(request.getDob())) {
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.MISSING_PARAMETER,
					"Customer dob required", false);
		}
		if (StringUtils.isEmpty(request.getIdNumber())) {
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.MISSING_PARAMETER,
					"Customer id number required", false);
		}
		if (!request.getIdNumber().matches(EcocashInboundHandlerConstants.ID_NUMBER_REGEX)) {
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.INVALID_PARAMETER,
					"Invalid customer id number : " + request.getIdNumber(), false);
		}
		if (!request.getGender().matches(EcocashInboundHandlerConstants.GENDER_REGEX)) {
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.INVALID_PARAMETER,
					"Invalid customer gender : " + request.getGender(), false);

		}
		try {
			final DateFormat dateFormat = new SimpleDateFormat(EcocashInboundHandlerConstants.REGISTRATION_DATE_FORMAT);
			final Date dob = dateFormat.parse(request.getDob());
			if (dob.after(new Date())) {
				return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.INVALID_PARAMETER,
						"Customer date of birth cannot be in the future : " + request.getDob(), false);
			}
		} catch (final Exception ex) {
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.INVALID_PARAMETER,
					"Invalid customer date of birth : " + request.getDob(), false);
		}
		command.setMSISDN(formatEcocashMsisdn(request.getMsisdn()));
		command.setMSISDN2(formatEcocashMsisdn(request.getMsisdn2()));
		command.setTYPE(EcocashInboundHandlerConstants.CommandParameters.CUSTOMER_REG_TYPE);
		command.setFNAME(request.getFirstName());
		command.setLNAME(request.getLastName());
		command.setDOB(request.getDob());
		command.setIDNUMBER(request.getIdNumber());
		command.setGENDER(request.getGender());
		command.setSOURCE(EcocashInboundHandlerConstants.CommandParameters.TRANSACTION_SOURCE);
		command.setEXTTRANSACTIONID(request.getSourceReference());
		command.setISPINCHECKREQ(StringUtils.isEmpty(request.getPin()) ? "N" : "Y");
		if (StringUtils.hasText(request.getPin())){
			command.setSECURITYMODE(SECURITYMODE.PINFULL);
			command.setPIN(request.getPin());
		}
		else
			command.setSECURITYMODE(SECURITYMODE.PINLESS);
		command.setCHECKSUM(generateCheckSum(command.getTYPE() + command.getPIN() + command.getMSISDN()
				+ command.getSOURCE() + command.getMSISDN2() + command.getIDNUMBER()));
		return null;
	}

	@Override
	protected TransactionResponse generateTransactionResponse(final COMMANDType responseCommand) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setStatusCode(responseCommand.getTXNSTATUS());
		transactionResponse.setDestinationReference(responseCommand.getTXNID());
		transactionResponse.setStatusReason(responseCommand.getMESSAGE());
		transactionResponse.setSuccess(true);
		return transactionResponse;
	}
}
