package com.econetwireless.payment.gateway.aggregator.api;

public interface TransactionHandler {

	TransactionResponse handleTransaction(TransactionRequest request , String gatewayReference);
	
}
