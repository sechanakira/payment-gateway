package com.econetwireless.payment.gateway.aggregator.api;

import java.io.Serializable;

import com.econetwireless.payment.gateway.entities.ApiType;
import com.econetwireless.payment.gateway.entities.Direction;

public class TransactionRequest implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String transactionId;
	private String vendorCode;
	private String vendorApiKey;
	private String applicationCode;
	private String transactionTypeCode;
	private Direction direction;
	private ApiType channel;
	private String currencyCode;
	private String amount;
	private String sourceReference;
	private String accountNumber1;
	private String accountNumber2;
	private String msisdn;
	private String msisdn2;
	private String firstName;
	private String lastName;
	private String dob;
	private String idNumber;
	private String gender;
	private String pin;
	private String accountName;
	private String checksum;
	private String transactionDate;
	private String transactionTime;
	private String serviceType;
	private String terminalId;
	private String terminalName;
	private String countryCode;
	private String source;
	private String destination;
	private String merchantName;
	private String additionalInfo1;
	private String additionalInfo2;
	private String receiver;
	private String description;
	private String paymentType;

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(final String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(final String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public String getTransactionTypeCode() {
		return transactionTypeCode;
	}

	public void setTransactionTypeCode(final String transactionTypeCode) {
		this.transactionTypeCode = transactionTypeCode;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(final Direction direction) {
		this.direction = direction;
	}

	public ApiType getChannel() {
		return channel;
	}

	public void setChannel(final ApiType channel) {
		this.channel = channel;
	}

	public String getSourceReference() {
		return sourceReference;
	}

	public void setSourceReference(final String sourceReference) {
		this.sourceReference = sourceReference;
	}

	public String getAccountNumber1() {
		return accountNumber1;
	}

	public void setAccountNumber1(final String accountNumber1) {
		this.accountNumber1 = accountNumber1;
	}

	public String getAccountNumber2() {
		return accountNumber2;
	}

	public void setAccountNumber2(final String accountNumber2) {
		this.accountNumber2 = accountNumber2;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(final String msisdn) {
		this.msisdn = msisdn;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(final String accountName) {
		this.accountName = accountName;
	}

	public String getVendorApiKey() {
		return vendorApiKey;
	}

	public void setVendorApiKey(final String vendorApiKey) {
		this.vendorApiKey = vendorApiKey;
	}

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(final String checksum) {
		this.checksum = checksum;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(final String amount) {
		this.amount = amount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(final String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getMsisdn2() {
		return msisdn2;
	}

	public void setMsisdn2(final String msisdn2) {
		this.msisdn2 = msisdn2;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(final String pin) {
		this.pin = pin;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(final String dob) {
		this.dob = dob;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(final String idNumber) {
		this.idNumber = idNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(final String gender) {
		this.gender = gender;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(final String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(final String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(final String transactionTime) {
		this.transactionTime = transactionTime;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(final String serviceType) {
		this.serviceType = serviceType;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(final String terminalId) {
		this.terminalId = terminalId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(final String countryCode) {
		this.countryCode = countryCode;
	}

	public String getSource() {
		return source;
	}

	public void setSource(final String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getAdditionalInfo1() {
		return additionalInfo1;
	}

	public void setAdditionalInfo1(String additionalInfo1) {
		this.additionalInfo1 = additionalInfo1;
	}

	public String getAdditionalInfo2() {
		return additionalInfo2;
	}

	public void setAdditionalInfo2(String additionalInfo2) {
		this.additionalInfo2 = additionalInfo2;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getDescription() {
		return description;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTerminalName() {
		return terminalName;
	}

	public void setTerminalName(String terminalName) {
		this.terminalName = terminalName;
	}

	@Override
	public String toString() {
		return "TransactionRequest [transactionId=" + transactionId + ", vendorCode=" + vendorCode + ", vendorApiKey="
				+ vendorApiKey + ", applicationCode=" + applicationCode + ", transactionTypeCode=" + transactionTypeCode
				+ ", direction=" + direction + ", channel=" + channel + ", currencyCode=" + currencyCode + ", amount="
				+ amount + ", sourceReference=" + sourceReference + ", accountNumber1=" + accountNumber1
				+ ", accountNumber2=" + accountNumber2 + ", msisdn=" + msisdn + ", msisdn2=" + msisdn2 + ", firstName="
				+ firstName + ", lastName=" + lastName + ", dob=" + dob + ", idNumber=" + idNumber + ", gender="
				+ gender + ", pin=" + pin + ", accountName=" + accountName + ", checksum=" + checksum
				+ ", transactionDate=" + transactionDate + ", transactionTime=" + transactionTime + ", serviceType="
				+ serviceType + ", terminalId=" + terminalId + ", terminalName=" + terminalName + ", countryCode="
				+ countryCode + ", source=" + source + ", destination=" + destination + ", merchantName=" + merchantName
				+ ", additionalInfo1=" + additionalInfo1 + ", additionalInfo2=" + additionalInfo2 + ", receiver="
				+ receiver + ", description=" + description + ", paymentType=" + paymentType + "]";
	}

}
