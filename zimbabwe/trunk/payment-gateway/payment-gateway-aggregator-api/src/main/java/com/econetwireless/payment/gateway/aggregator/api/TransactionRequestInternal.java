package com.econetwireless.payment.gateway.aggregator.api;

import java.io.Serializable;

public class TransactionRequestInternal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private TransactionRequest transactionRequest;

	private String gatewayReference;

	public TransactionRequestInternal(final TransactionRequest transactionRequest, final String gatewayReference) {
		this.gatewayReference = gatewayReference;
		this.transactionRequest = transactionRequest;
	}

	public TransactionRequest getTransactionRequest() {
		return transactionRequest;
	}

	public void setTransactionRequest(final TransactionRequest transactionRequest) {
		this.transactionRequest = transactionRequest;
	}

	public String getGatewayReference() {
		return gatewayReference;
	}

	public void setGatewayReference(final String gatewayReference) {
		this.gatewayReference = gatewayReference;
	}

}
