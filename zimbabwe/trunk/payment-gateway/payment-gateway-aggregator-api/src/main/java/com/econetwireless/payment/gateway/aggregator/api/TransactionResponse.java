package com.econetwireless.payment.gateway.aggregator.api;

import java.io.Serializable;

import com.econetwireless.payment.gateway.entities.TransactionStatus;

public class TransactionResponse implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String destinationReference;
	private String statusCode;
	private String mappedStatusCode;
	private Boolean success;
	private String statusReason;
	private String balance;
	private String amount;
	private String currencyCode;
	private String accountType;
	private String accountName;
	private String accountStatus;
	private String additionalInfo5;
	private String additionalInfo1;
	private String additionalInfo2;
	private String additionalInfo3;
	private String additionalInfo4;
	private String terminalName;
	private TransactionStatus transactionStatus;

	public String getDestinationReference() {
		return destinationReference;
	}

	public void setDestinationReference(final String destinationReference) {
		this.destinationReference = destinationReference;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(final String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(final String statusReason) {
		this.statusReason = statusReason;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(final String balance) {
		this.balance = balance;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(final Boolean success) {
		this.success = success;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(final String amount) {
		this.amount = amount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(final String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(final String accountType) {
		this.accountType = accountType;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(final String accountName) {
		this.accountName = accountName;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(final String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public TransactionStatus getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(TransactionStatus transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getMappedStatusCode() {
		return mappedStatusCode;
	}

	public void setMappedStatusCode(String mappedStatusCode) {
		this.mappedStatusCode = mappedStatusCode;
	}

	public String getAdditionalInfo() {
		return additionalInfo5;
	}

	public void setAdditionalInfo5(String additionalInfo5) {
		this.additionalInfo5 = additionalInfo5;
	}

	public String getAdditionalInfo1() {
		return additionalInfo1;
	}

	public void setAdditionalInfo1(String additionalInfo1) {
		this.additionalInfo1 = additionalInfo1;
	}

	public String getAdditionalInfo2() {
		return additionalInfo2;
	}

	public void setAdditionalInfo2(String additionalInfo2) {
		this.additionalInfo2 = additionalInfo2;
	}

	public String getAdditionalInfo3() {
		return additionalInfo3;
	}

	public void setAdditionalInfo3(String additionalInfo3) {
		this.additionalInfo3 = additionalInfo3;
	}

	public String getAdditionalInfo4() {
		return additionalInfo4;
	}

	public void setAdditionalInfo4(String additionalInfo4) {
		this.additionalInfo4 = additionalInfo4;
	}

	public String getAdditionalInfo5() {
		return additionalInfo5;
	}

	public String getTerminalName() {
		return terminalName;
	}

	public void setTerminalName(String terminalName) {
		this.terminalName = terminalName;
	}

	@Override
	public String toString() {
		return "TransactionResponse [destinationReference=" + destinationReference + ", statusCode=" + statusCode
				+ ", mappedStatusCode=" + mappedStatusCode + ", success=" + success + ", statusReason=" + statusReason
				+ ", balance=" + balance + ", amount=" + amount + ", currencyCode=" + currencyCode + ", accountType="
				+ accountType + ", accountName=" + accountName + ", accountStatus=" + accountStatus
				+ ", additionalInfo5=" + additionalInfo5 + ", additionalInfo1=" + additionalInfo1 + ", additionalInfo2="
				+ additionalInfo2 + ", additionalInfo3=" + additionalInfo3 + ", additionalInfo4=" + additionalInfo4
				+ ", terminalName=" + terminalName + ", transactionStatus=" + transactionStatus + "]";
	}

}
