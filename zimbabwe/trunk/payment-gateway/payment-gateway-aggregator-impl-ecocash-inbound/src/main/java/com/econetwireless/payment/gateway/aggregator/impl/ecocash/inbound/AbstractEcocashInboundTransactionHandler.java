package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.aggregator.api.TransactionHandler;
import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.entities.Currency;
import com.econetwireless.payment.gateway.service.currency.CurrencyService;
import com.mmoney.security.HmacSha1Signature;

import noNamespace.COMMANDDocument;
import noNamespace.COMMANDType;

public abstract class AbstractEcocashInboundTransactionHandler implements TransactionHandler {

	protected static final Logger logger = LoggerFactory.getLogger(AbstractEcocashInboundTransactionHandler.class);

	@Autowired
	private HttpClient client;

	@Autowired
	private CurrencyService currencyService;

	public final TransactionResponse handleTransaction(final TransactionRequest request,
			final String gatewayReference) {
		if (request == null)
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.INVALID_REQUEST,
					EcocashInboundHandlerConstants.StatusMessages.INVALID_REQUEST, false);
		if (logger.isDebugEnabled())
			logger.debug("Handling Ecocash inbound transaction {}", request);
		final COMMANDDocument commandDocument = COMMANDDocument.Factory.newInstance();
		final COMMANDType command = commandDocument.addNewCOMMAND();
		command.setEXTTRANSACTIONID(gatewayReference);
		final TransactionResponse transactionResponse = setAndValidateCommandParameters(request, command);
		if (transactionResponse != null)
			return transactionResponse;
		return postTransactionToEcocash(commandDocument);

	}

	protected abstract TransactionResponse setAndValidateCommandParameters(final TransactionRequest transactionRequest,
			COMMANDType command);

	private TransactionResponse postTransactionToEcocash(final COMMANDDocument commandDocument) {
		try {
			if (logger.isInfoEnabled())
				logger.info("Posting Ecocash transaction {}", commandDocument);
			final URIBuilder builder = new URIBuilder();
			builder.setScheme(EcocashInboundHandlerConstants.PostParameters.PROTOCOL);
			builder.setHost(EcocashInboundHandlerConstants.PostParameters.ECOCASH_HOST);
			builder.setPort(EcocashInboundHandlerConstants.PostParameters.ECOCASH_PORT);
			builder.setPath(determineResourcePath());
			builder.setParameter("LOGIN", determineLogin());
			builder.setParameter("PASSWORD", determinePassword());
			builder.setParameter(determineRequestGatewayCodeParamName(), determineRequestGatewayCode());
			builder.setParameter(determineRequestGatewayTypeParamName(), determineRequestGatewayType());
			builder.setParameter(determinePayloadParamName(), commandDocument.toString());
			if (logger.isInfoEnabled())
				logger.info("Request : {}", builder.build().toString());
			final HttpPost post = new HttpPost(builder.build().toString());
			final StringEntity requestBody = new StringEntity("");
			requestBody.setContentType(MediaType.APPLICATION_XML_VALUE);
			post.setEntity(requestBody);
			final HttpResponse response = client.execute(post);
			String responseString = EntityUtils.toString(response.getEntity());
			if (logger.isInfoEnabled())
				logger.info("Got Ecocash response {} , headers : {} ,  statusLine : {}", responseString,
						Arrays.toString(response.getAllHeaders()), response.getStatusLine());
			responseString = responseString.replaceAll("\\s&\\s", " &amp; ");
			final COMMANDDocument responseDocument = COMMANDDocument.Factory.parse(responseString);
			final COMMANDType responseCommand = responseDocument.getCOMMAND();
			return generateTransactionResponse(responseCommand);
		} catch (final Exception ex) {
			if (logger.isErrorEnabled())
				logger.error("Failed to post Ecocash transaction.Reason {}", ex);
			throw new RuntimeException(ex);
		}
	}

	private String determineRequestGatewayType() {
		if (getRequestType() == RequestType.REMITTANCE)
			return EcocashInboundHandlerConstants.PostParameters.REMITTANCE_REQUEST_GATEWAY_TYPE;
		return EcocashInboundHandlerConstants.PostParameters.REQUEST_GATEWAY_TYPE;
	}

	private String determineRequestGatewayTypeParamName() {
		if (getRequestType() == RequestType.REMITTANCE)
			return "REQUEST_GW_TYPE";
		return "REQUEST_GATEWAY_TYPE";
	}

	private String determineRequestGatewayCodeParamName() {
		if (getRequestType() == RequestType.REMITTANCE)
			return "REQUEST_GW_CODE";
		return "REQUEST_GATEWAY_CODE";
	}

	private String determineRequestGatewayCode() {
		if (getRequestType() == RequestType.REMITTANCE)
			return EcocashInboundHandlerConstants.PostParameters.REMITTANCE_REQUEST_GATEWAY_CODE;
		return EcocashInboundHandlerConstants.PostParameters.REQUEST_GATEWAY_CODE;
	}

	private String determinePassword() {
		if (getRequestType() == RequestType.REMITTANCE)
			return EcocashInboundHandlerConstants.PostParameters.REMITTANCE_PASSWORD;
		return EcocashInboundHandlerConstants.PostParameters.PASSWORD;
	}

	private String determineLogin() {
		if (getRequestType() == RequestType.REMITTANCE)
			return EcocashInboundHandlerConstants.PostParameters.REMITTANCE_LOGIN;
		return EcocashInboundHandlerConstants.PostParameters.LOGIN;
	}

	private String determineResourcePath() {
		if (getRequestType() == RequestType.REMITTANCE)
			return EcocashInboundHandlerConstants.PostParameters.REMITTANCE_PATH;
		return EcocashInboundHandlerConstants.PostParameters.PATH;
	}

	private String determinePayloadParamName() {
		if (getRequestType() == RequestType.REMITTANCE)
			return "MESSAGE";
		return "requestText";
	}

	protected String generateCheckSum(final String str) {
		return HmacSha1Signature.prepareCheckSum(str, getChecksumKey());
	}

	private String getChecksumKey() {
		if (getRequestType() == RequestType.REMITTANCE)
			return EcocashInboundHandlerConstants.PostParameters.REMITTANCE_CHECKSUM;
		return EcocashInboundHandlerConstants.PostParameters.CHECKSUM;
	}

	protected abstract TransactionResponse generateTransactionResponse(final COMMANDType responseCommandType);

	protected final NumberFormat getAmountFormat() {
		return new DecimalFormat(EcocashInboundHandlerConstants.Formats.AMOUNT_FORMAT);
	}

	protected final String formatEcocashMsisdn(final String msisdn) {
		if (msisdn.length() >= 9)
			return msisdn.substring(msisdn.length() - 9);
		return msisdn;
	}

	protected final TransactionResponse validateEcocashMsisdn(final String msisdn, final String requiredMessage,
			final String invalidMesage) {
		if (StringUtils.isEmpty(msisdn))
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.MISSING_PARAMETER,
					requiredMessage, false);
		if (!EcocashUtil.isValidMsisdn(msisdn))
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.INVALID_PARAMETER,
					invalidMesage, false);
		return null;
	}

	protected TransactionResponse buildResponseWithStatusCode(final String statusCodeKey, final String statusMessage,
			final boolean success) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setStatusCode(statusCodeKey);
		transactionResponse.setStatusReason(statusMessage);
		transactionResponse.setSuccess(success);
		return transactionResponse;
	}

	protected TransactionResponse validateAmount(final TransactionRequest transactionRequest,
			final String requiredMessage, final String invalidMessage, final String negativeAmountMsg) {
		if (StringUtils.isEmpty(transactionRequest.getAmount()))
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.MISSING_PARAMETER,
					requiredMessage, false);
		try {
			final double amount = Double.parseDouble(transactionRequest.getAmount());
			if (amount <= 0)
				return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.INVALID_PARAMETER,
						negativeAmountMsg, false);
		} catch (final NumberFormatException ex) {
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.INVALID_PARAMETER,
					invalidMessage, false);
		}
		return null;
	}

	protected TransactionResponse validateCurrency(final String currencyCode, final String requiredMessage,
			final String invalidMessage) {
		if (StringUtils.isEmpty(currencyCode))
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.MISSING_PARAMETER,
					requiredMessage, false);
		final Currency currency = currencyService.findCurrencyByAlphaCode(currencyCode);
		if (currency == null)
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.INVALID_PARAMETER,
					invalidMessage, false);
		return null;
	}

	protected TransactionResponse validateAgentCode(final TransactionRequest transactionRequest,
			final String requiredMessage, final String invalidMessage) {
		if (StringUtils.isEmpty(transactionRequest.getAccountNumber1()))
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.MISSING_PARAMETER,
					requiredMessage, false);
		return null;
	}

	protected TransactionResponse validateRequiredParameter(final Object parameterValue, final String errorMessage) {
		if (StringUtils.isEmpty(parameterValue))
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.MISSING_PARAMETER,
					errorMessage, false);
		return null;
	}

	protected TransactionResponse validateSource(final String source, final String requiredMessage,
			final String invalidMessage) {
		final TransactionResponse requiredValidationResponse = validateRequiredParameter(source, requiredMessage);
		if (requiredValidationResponse != null)
			return requiredValidationResponse;
		// validate value
		return null;
	}

	protected TransactionResponse validateAccountNumber1(final TransactionRequest transactionRequest,
			final String requiredMessage, final String invalidMessage) {
		if (StringUtils.isEmpty(transactionRequest.getAccountNumber1()))
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.MISSING_PARAMETER,
					requiredMessage, false);
		return null;
	}

	protected TransactionResponse validateBankId(final TransactionRequest transactionRequest, final String string,
			final String string2) {
		// TODO Auto-generated method stub
		return null;
	}

	protected RequestType getRequestType() {
		return RequestType.DEFAULT;
	}
}
