package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

import noNamespace.COMMANDType;
import noNamespace.COMMANDType.SECURITYMODE;

/**
 * Created by shingirai on 5/26/15.
 */
@Component
public class AgentAirtimePurchaseTransactionHandler extends AbstractEcocashInboundTransactionHandler {

	@Override
	protected TransactionResponse setAndValidateCommandParameters(final TransactionRequest request,
			final COMMANDType command) {

		final TransactionResponse msidsnValidationResponse = validateEcocashMsisdn(request.getMsisdn(),
				"Merchant msisdn required", "Invalid merchant msisdn : " + request.getMsisdn());
		if (msidsnValidationResponse != null) {
			return msidsnValidationResponse;
		}
		final TransactionResponse msisdn2ValidationResponse = validateEcocashMsisdn(request.getMsisdn2(),
				"Msisdn to credit airtime required", "Invalid msisdn to credit airtime");
		if (msisdn2ValidationResponse != null) {
			return msisdn2ValidationResponse;
		}
		final TransactionResponse amountValidationResponse = validateAmount(request, "Amount required",
				"Invalid amount : " + request.getAmount(),
				"Amount cannot be zero or negative : " + request.getAmount());
		if (amountValidationResponse != null) {
			return amountValidationResponse;
		}
		final TransactionResponse currencyValidationResponse = validateCurrency(request.getCurrencyCode(),
				"Currency code required", "Invalid currency code : " + request.getCurrencyCode());
		if (currencyValidationResponse != null) {
			return currencyValidationResponse;
		}
		command.setOPERATORID(EcocashInboundHandlerConstants.CommandParameters.AIRTIME_TOPUP_CODE);
		command.setAMOUNT(request.getAmount());
		if (StringUtils.hasText(request.getPin())) {
			command.setPIN(request.getPin());
		}
		command.setMSISDN2(formatEcocashMsisdn(request.getMsisdn2()));
		command.setMSISDN(formatEcocashMsisdn(request.getMsisdn()));
		command.setRECHARGETYPE(EcocashInboundHandlerConstants.CommandParameters.AIRTIME_PURCHASE_TYPE);
		command.setTYPE(EcocashInboundHandlerConstants.CommandParameters.AGENT_AIRTIME_RECHARGE_TYPE);
		command.setSOURCE(EcocashInboundHandlerConstants.CommandParameters.TRANSACTION_SOURCE);
		command.setEXTTRANSACTIONID(request.getSourceReference());
		command.setISPINCHECKREQ(StringUtils.isEmpty(request.getPin()) ? "N" : "Y");
		if (StringUtils.hasText(request.getPin()))
			command.setSECURITYMODE(SECURITYMODE.PINFULL);
		else
			command.setSECURITYMODE(SECURITYMODE.PINLESS);
		command.setCHECKSUM(generateCheckSum(command.getTYPE() + command.getAMOUNT() + command.getPIN()
				+ command.getMSISDN2() + command.getMSISDN() + command.getSOURCE() + command.getRECHARGETYPE()));
		return null;
	}

	@Override
	protected TransactionResponse generateTransactionResponse(final COMMANDType responseCommand) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setStatusCode(responseCommand.getTXNSTATUS());
		transactionResponse.setDestinationReference(responseCommand.getTXNID());
		transactionResponse.setStatusReason(responseCommand.getMESSAGE());
		transactionResponse.setSuccess(
				EcocashInboundHandlerConstants.ResponseCodes.SUCCESS.equalsIgnoreCase(responseCommand.getTXNSTATUS()));
		return transactionResponse;
	}
}