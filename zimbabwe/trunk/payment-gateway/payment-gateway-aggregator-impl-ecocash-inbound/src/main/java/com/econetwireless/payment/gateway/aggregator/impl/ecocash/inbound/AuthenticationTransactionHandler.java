package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

import noNamespace.COMMANDType;
import noNamespace.COMMANDType.SECURITYMODE;

@Component
public class AuthenticationTransactionHandler extends
		AbstractEcocashInboundTransactionHandler {

	@Override
	protected TransactionResponse setAndValidateCommandParameters(
			final TransactionRequest transactionRequest, final COMMANDType command) {
		final TransactionResponse validateMsisdnResponse = validateEcocashMsisdn(transactionRequest.getMsisdn(), "Subscriber msisdn required", "Invalid subscriber msisdn : " + transactionRequest.getMsisdn());
        if(validateMsisdnResponse != null){
        	return validateMsisdnResponse;
        }
		command.setTYPE(EcocashInboundHandlerConstants.CommandParameters.AUTHENTICATION_TYPE_CODE);
        command.setMSISDN(formatEcocashMsisdn(transactionRequest.getMsisdn()));
        command.setSOURCE(EcocashInboundHandlerConstants.CommandParameters.TRANSACTION_SOURCE);
        command.setEXTTRANSACTIONID(transactionRequest.getSourceReference());
        command.setISPINCHECKREQ(StringUtils.isEmpty(transactionRequest.getPin())  ? "N" : "Y");
        if (StringUtils.hasText(transactionRequest.getPin())){
			command.setSECURITYMODE(SECURITYMODE.PINFULL);
			command.setPIN(transactionRequest.getPin());
        }
		else
			command.setSECURITYMODE(SECURITYMODE.PINLESS);
        command.setCHECKSUM(generateCheckSum(command.getTYPE() + command.getPIN() + command.getMSISDN() + command.getSOURCE()));
        return null;
	}

	@Override
	protected TransactionResponse generateTransactionResponse(
			final COMMANDType responseCommand) {
		final TransactionResponse transactionResponse = new TransactionResponse();
        transactionResponse.setStatusCode(responseCommand.getTXNSTATUS());
        transactionResponse.setDestinationReference(responseCommand.getTXNID());
        transactionResponse.setStatusReason(responseCommand.getMESSAGE());
        transactionResponse.setSuccess(EcocashInboundHandlerConstants.ResponseCodes.SUCCESS.equalsIgnoreCase(responseCommand.getTXNSTATUS()));
        return transactionResponse;
	}

}
