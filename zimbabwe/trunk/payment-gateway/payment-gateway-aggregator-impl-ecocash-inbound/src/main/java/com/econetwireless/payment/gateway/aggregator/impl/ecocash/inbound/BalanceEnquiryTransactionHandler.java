package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

import noNamespace.COMMANDType;
import noNamespace.COMMANDType.SECURITYMODE;

@Component
public class BalanceEnquiryTransactionHandler extends AbstractEcocashInboundTransactionHandler {

	@Override
	protected TransactionResponse setAndValidateCommandParameters(final TransactionRequest request,
			final COMMANDType command) {
		final TransactionResponse transactionResponse = validateEcocashMsisdn(request.getMsisdn(),
				EcocashInboundHandlerConstants.StatusMessages.BalanceEnquiry.MSISDN_REQUIRED,
				EcocashInboundHandlerConstants.StatusMessages.BalanceEnquiry.INVALID_MSISDN);
		if (transactionResponse == null) {
			command.setMSISDN(formatEcocashMsisdn(request.getMsisdn()));
			command.setTYPE(EcocashInboundHandlerConstants.CommandParameters.BALANCE_EQNQUIRY_CODE);
			command.setSOURCE(EcocashInboundHandlerConstants.CommandParameters.TRANSACTION_SOURCE);
			command.setPAYID(EcocashInboundHandlerConstants.CommandParameters.NORMAL_WALLET_PAY_ID);
			command.setISPINCHECKREQ(StringUtils.isEmpty(request.getPin()) ? "N" : "Y");
			command.setEXTTRANSACTIONID(request.getSourceReference());
			if (StringUtils.hasText(request.getPin())) {
				command.setSECURITYMODE(SECURITYMODE.PINFULL);
				command.setPIN(request.getPin());
			} else
				command.setSECURITYMODE(SECURITYMODE.PINLESS);
			command.setCHECKSUM(
					generateCheckSum(command.getTYPE() + command.getPIN() + command.getMSISDN() + command.getSOURCE()));
			return null;
		}
		return transactionResponse;

	}

	@Override
	protected TransactionResponse generateTransactionResponse(final COMMANDType responseCommand) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setStatusCode(responseCommand.getTXNSTATUS());
		transactionResponse.setDestinationReference(responseCommand.getTXNID());
		transactionResponse.setStatusReason(responseCommand.getMESSAGE());
		transactionResponse.setBalance(responseCommand.getBALANCE());
		transactionResponse.setSuccess(
				EcocashInboundHandlerConstants.ResponseCodes.SUCCESS.equalsIgnoreCase(responseCommand.getTXNSTATUS()));
		return transactionResponse;
	}

}
