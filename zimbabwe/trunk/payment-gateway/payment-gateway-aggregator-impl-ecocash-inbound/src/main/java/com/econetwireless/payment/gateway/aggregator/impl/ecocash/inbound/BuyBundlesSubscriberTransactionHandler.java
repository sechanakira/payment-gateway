package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

import noNamespace.COMMANDType;
import noNamespace.COMMANDType.SECURITYMODE;

/**
 * Created by shingirai on 5/28/15.
 */
@Component
public class BuyBundlesSubscriberTransactionHandler extends AbstractEcocashInboundTransactionHandler {

	@Override
	protected TransactionResponse setAndValidateCommandParameters(final TransactionRequest request,
			final COMMANDType command) {
		final TransactionResponse validateMsisdnResponse = validateEcocashMsisdn(request.getMsisdn(),
				"Subscriber msisdn required", "Invalid subscriber msisdn : " + request.getMsisdn());
		if (validateMsisdnResponse != null) {
			return validateMsisdnResponse;
		}
		final TransactionResponse validateMsisdn2Response = validateEcocashMsisdn(request.getMsisdn2(),
				"Msisdn to credit bundles required", "Invalid msisdn to credit bundles");
		if (validateMsisdn2Response != null) {
			return validateMsisdn2Response;
		}
		final TransactionResponse amountValidationResponse = validateAmount(request, "Amount required",
				"Invalid amount : " + request.getAmount(),
				"Amount cannot be zero or negative : " + request.getAmount());
		if (amountValidationResponse != null) {
			return amountValidationResponse;
		}
		final TransactionResponse currencyValidationResponse = validateCurrency(request.getCurrencyCode(),
				"Currency code required", "Invalid currency code : " + request.getCurrencyCode());
		if (currencyValidationResponse != null) {
			return currencyValidationResponse;
		}

		// final TransactionResponse rechargeTypeValidationResponse =
		// validateRechargeType(request.getServiceType());
		// if(rechargeTypeValidationResponse != null){
		// return rechargeTypeValidationResponse;
		// }
		command.setOPERATORID(EcocashInboundHandlerConstants.CommandParameters.AIRTIME_TOPUP_CODE);
		command.setAMOUNT(request.getAmount());
		if (StringUtils.hasText(request.getPin()))
			command.setPIN(request.getPin());
		command.setMSISDN2(formatEcocashMsisdn(request.getMsisdn2()));
		command.setMSISDN(formatEcocashMsisdn(request.getMsisdn()));
		command.setTYPE(EcocashInboundHandlerConstants.CommandParameters.BUY_SUBSCRIBER_BUNDLES_TYPE);
		command.setRECHARGETYPE(request.getServiceType());
		command.setSOURCE(EcocashInboundHandlerConstants.CommandParameters.TRANSACTION_SOURCE);
		command.setEXTTRANSACTIONID(request.getSourceReference());
		command.setISPINCHECKREQ(StringUtils.isEmpty(request.getPin()) ? "N" : "Y");
		if (StringUtils.hasText(request.getPin()))
			command.setSECURITYMODE(SECURITYMODE.PINFULL);
		else
			command.setSECURITYMODE(SECURITYMODE.PINLESS);
		command.setCHECKSUM(generateCheckSum(command.getTYPE() + command.getAMOUNT() + command.getPIN()
				+ command.getMSISDN2() + command.getMSISDN() + command.getSOURCE() + command.getRECHARGETYPE()));
		return null;
	}

	private TransactionResponse validateRechargeType(final String serviceType) {
		if (StringUtils.isEmpty(serviceType)) {
			return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.MISSING_PARAMETER,
					"Recharge type required", false);
		}
		for (final String rechargeType : EcocashInboundHandlerConstants.ALLOWED_BUNDLE_RECHARGE_TYPES) {
			if (rechargeType.equalsIgnoreCase(serviceType)) {
				return null;
			}
		}
		return buildResponseWithStatusCode(EcocashInboundHandlerConstants.StatusCodes.INVALID_PARAMETER,
				"Invalid recharge type : " + serviceType, false);
	}

	@Override
	protected TransactionResponse generateTransactionResponse(final COMMANDType responseCommand) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setStatusCode(responseCommand.getTXNSTATUS());
		transactionResponse.setDestinationReference(responseCommand.getTXNID());
		transactionResponse.setStatusReason(responseCommand.getMESSAGE());
		transactionResponse.setSuccess(true);
		return transactionResponse;
	}
}
