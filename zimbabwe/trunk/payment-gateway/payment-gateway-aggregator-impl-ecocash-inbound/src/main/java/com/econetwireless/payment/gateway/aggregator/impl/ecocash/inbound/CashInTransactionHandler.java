package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

import noNamespace.COMMANDType;
import noNamespace.COMMANDType.SECURITYMODE;

/**
 * Created by shingirai on 5/28/15.
 */
@Component
public class CashInTransactionHandler extends AbstractEcocashInboundTransactionHandler {

	@Override
	protected TransactionResponse setAndValidateCommandParameters(final TransactionRequest request,
			final COMMANDType command) {
		final TransactionResponse validateMsisdnResponse = validateEcocashMsisdn(request.getMsisdn(),
				"Agent msisdn required", "Invalid agent msisdn : " + request.getMsisdn());
		if (validateMsisdnResponse != null) {
			return validateMsisdnResponse;
		}
		final TransactionResponse validateMsisdn2Response = validateEcocashMsisdn(request.getMsisdn(),
				"Subscriber msisdn required", "Invalid subscriber msisdn : " + request.getMsisdn2());
		if (validateMsisdn2Response != null) {
			return validateMsisdn2Response;
		}
		final TransactionResponse amountValidationResponse = validateAmount(request, "Amount required",
				"Invalid amount : " + request.getAmount(),
				"Amount cannot be zero or negative : " + request.getAmount());
		if (amountValidationResponse != null) {
			return amountValidationResponse;
		}
		final TransactionResponse currencyValidationResponse = validateCurrency(request.getCurrencyCode(),
				"Currency code required", "Invalid currency code : " + request.getCurrencyCode());
		if (currencyValidationResponse != null) {
			return currencyValidationResponse;
		}
		final TransactionResponse agentCodeValidationResponse = validateAgentCode(request, "Agent code required",
				"Invalid agent code : " + request.getAccountNumber1());
		if (agentCodeValidationResponse != null) {
			return agentCodeValidationResponse;
		}
		command.setMSISDN(formatEcocashMsisdn(formatEcocashMsisdn(request.getMsisdn())));
		command.setMSISDN2(formatEcocashMsisdn(formatEcocashMsisdn(request.getMsisdn2())));
		command.setTYPE(EcocashInboundHandlerConstants.CommandParameters.CASH_IN_CODE);
		command.setSOURCE(EcocashInboundHandlerConstants.CommandParameters.TRANSACTION_SOURCE);
		command.setEXTTRANSACTIONID(request.getSourceReference());
		command.setAMOUNT(request.getAmount());
		command.setAGNTCODE(request.getAccountNumber1().trim());
		command.setISPINCHECKREQ(StringUtils.isEmpty(request.getPin()) ? "N" : "Y");
		if (StringUtils.hasText(request.getPin())) {
			command.setSECURITYMODE(SECURITYMODE.PINFULL);
			command.setPIN(request.getPin());
		} else
			command.setSECURITYMODE(SECURITYMODE.PINLESS);
		command.setCHECKSUM(generateCheckSum(command.getTYPE() + command.getAMOUNT() + command.getPIN()
				+ command.getMSISDN() + command.getSOURCE() + command.getMSISDN2()));
		return null;
	}

	@Override
	protected TransactionResponse generateTransactionResponse(final COMMANDType responseCommand) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setStatusCode(responseCommand.getTXNSTATUS());
		transactionResponse.setDestinationReference(responseCommand.getTXNID());
		transactionResponse.setStatusReason(responseCommand.getMESSAGE());
		transactionResponse.setSuccess(
				EcocashInboundHandlerConstants.ResponseCodes.SUCCESS.equalsIgnoreCase(responseCommand.getTXNSTATUS()));
		return transactionResponse;
	}
}
