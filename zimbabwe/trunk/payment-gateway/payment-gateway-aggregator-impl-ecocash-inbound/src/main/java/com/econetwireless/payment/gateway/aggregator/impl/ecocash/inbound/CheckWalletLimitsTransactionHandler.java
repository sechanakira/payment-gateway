package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

import noNamespace.COMMANDType;
import noNamespace.COMMANDType.SECURITYMODE;

/**
 * Created by shingirai on 5/27/15.
 */
@Component
public class CheckWalletLimitsTransactionHandler extends AbstractEcocashInboundTransactionHandler {

	@Override
	protected TransactionResponse setAndValidateCommandParameters(TransactionRequest request, COMMANDType command) {
		final TransactionResponse validateMsisdnResponse = validateEcocashMsisdn(request.getMsisdn(),
				"Subscriber msisdn required", "Invalid subscriber msisdn : " + request.getMsisdn());
		if (validateMsisdnResponse != null) {
			return validateMsisdnResponse;
		}
		command.setTYPE(EcocashInboundHandlerConstants.CommandParameters.WALLET_LIMITS_TYPE);
		command.setMSISDN(formatEcocashMsisdn(request.getMsisdn()));
		command.setSOURCE(EcocashInboundHandlerConstants.CommandParameters.TRANSACTION_SOURCE);
		command.setEXTTRANSACTIONID(request.getSourceReference());
		if (StringUtils.hasText(request.getPin())) {
			command.setSECURITYMODE(SECURITYMODE.PINFULL);
			command.setPIN(request.getPin());
		} else
			command.setSECURITYMODE(SECURITYMODE.PINLESS);
		command.setISPINCHECKREQ(StringUtils.isEmpty(request.getPin()) ? "N" : "Y");
		command.setCHECKSUM(
				generateCheckSum(command.getTYPE() + command.getPIN() + command.getMSISDN() + command.getSOURCE()));
		return null;
	}

	@Override
	protected TransactionResponse generateTransactionResponse(final COMMANDType responseCommand) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setStatusCode(responseCommand.getTXNSTATUS());
		transactionResponse.setDestinationReference(responseCommand.getTXNID());
		transactionResponse.setStatusReason(responseCommand.getMESSAGE());
		transactionResponse.setSuccess(
				EcocashInboundHandlerConstants.ResponseCodes.SUCCESS.equalsIgnoreCase(responseCommand.getTXNSTATUS()));
		return transactionResponse;
	}
}
