package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

public interface EcocashInboundHandlerConstants {

	public interface PostParameters {

		String PROTOCOL = "http";
		String ECOCASH_HOST = "ecocashhost";
		int ECOCASH_PORT = 7890;
		String PATH = "/Txn/GenericAPIHandler";
		String REMITTANCE_PATH = "/Txn/OnlineCashInHandler";
		String LOGIN = "USSD_Bearer2"; // EPG USSD_Bearer2
		String PASSWORD = "cMLUHe2osdK6KlPLhUyaGa04FOs="; // EPGGATEWAY cMLUHe2osdK6KlPLhUyaGa04FOs=
		String REQUEST_GATEWAY_CODE = "USSD"; // EPGUSSD
		String REQUEST_GATEWAY_TYPE = "USSD";
		String CHECKSUM = "94F355RRRR65C8";
		String REMITTANCE_CHECKSUM = "ECOCASH";
		String REMITTANCE_LOGIN = "USSDMGHUB";
		String REMITTANCE_PASSWORD = "USSDMGHUBPASSWORDCOM123";
		String REMITTANCE_REQUEST_GATEWAY_CODE = "MGUSSD";
		String REMITTANCE_REQUEST_GATEWAY_TYPE = "USSD";
	}

	public interface CommandParameters {

		String PIN_CHECK_REQUIRED = "N";
		String BALANCE_EQNQUIRY_CODE = "CBEREQ";
		String AGENT_BALANCE_ENQUIRY_CODE = "RBEREQ";
		String SUBSCRIBER_TO_MERCHANT_CODE = "SUBMPREQ"; //SUBMPREQ;CMPREQ
		String CASH_IN_CODE = "RCIREQ";
		String TRANSACTION_SOURCE = "GENERICDEF"; // EPG
		String REMITTANCE_TRANSACTION_SOURCE = "MG";
		String SUBSCRIBER_AIRTIME_RECHARGE_TYPE = "CTMMREQ";
		String AGENT_AIRTIME_RECHARGE_TYPE = "RTMMREQ";
		String AIRTIME_TOPUP_CODE = "TOPUP4ECONET";
		String BUY_BUNDLES_TYPE = "RTMMREQ";
		String ACC_HISTORY_TYPE = "CLTREQ";
		String CUSTOMER_REG_TYPE = "RSUBREG";
		String WALLET_LIMITS_TYPE = "LMTENQ";
		String BUY_SUBSCRIBER_BUNDLES_TYPE = "CTMMREQ";
		String AIRTIME_PURCHASE_TYPE = "1";
		String DEFAULT_PIN = null;
		String NORMAL_WALLET_PAY_ID = "12";
		String MERCHANT_TO_SUBSCRIBER_CODE = "MPSUBREQ";
		String MERCHANT_TO_MERCHANT_CODE = "RTMREQ";
		String AUTHENTICATION_TYPE_CODE = "CLOGINVAL";
		String MERCHANT_WALLET_TO_BANK_CODE = "RWBREQ";
		String CUSTOMER_LOOOKUP_TYPE_CODE = "CLOOKUPREQ";
		String REMITTANCE_CASH_IN_TYPE_CODE = "CASHIN";
		String REMITTANCE_TRANSACTION_LOOKUP_CODE = "TLOOKUPREQ";
	}

	public interface Formats {

		String AMOUNT_FORMAT = "0.00";
	}

	public interface StatusCodes {

		String INVALID_PARAMETER = "408";
		String INVALID_REQUEST = "409";
		String MISSING_PARAMETER = "400";
	}

	public interface ResponseCodes {
		String SUCCESS = "200";
	}

	public interface StatusMessages {

		public interface BalanceEnquiry {

			String MSISDN_REQUIRED = "Subscriber mobile number required";
			String INVALID_MSISDN = "Invalid subscriber mobile number";
		}

		String INVALID_REQUEST = "Invalid request";
	}

	String ID_NUMBER_REGEX = "[-\\w]{5,}";
	String GENDER_REGEX = "(?i)MALE|FEMALE";
	String REGISTRATION_DATE_FORMAT = "ddMMyyyy";
	String[] ALLOWED_BUNDLE_RECHARGE_TYPES = { "2", "3", "4", "5.1", "5.2", "6" };
}
