package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

import org.springframework.util.StringUtils;

public class EcocashUtil {

	private static final String MSISDN_REGEX = "(((\\+|00)?263)|0)?7(7|8)\\d{7}";

	public static boolean isValidMsisdn(final String msisdn) {
		return StringUtils.hasText(msisdn) && msisdn.matches(MSISDN_REGEX);
	}

	public static String formatMsisdn(final String msisdn) {
		if (StringUtils.isEmpty(msisdn))
			return msisdn;
		final String msisdnToUse = msisdn.trim();
		if (msisdnToUse.length() > 9)
			return msisdnToUse.substring(msisdnToUse.length() - 9);
		return msisdnToUse;
	}
}
