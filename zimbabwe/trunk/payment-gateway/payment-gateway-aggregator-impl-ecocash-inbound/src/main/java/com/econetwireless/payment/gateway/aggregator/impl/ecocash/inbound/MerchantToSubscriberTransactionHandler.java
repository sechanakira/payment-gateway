package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

import noNamespace.COMMANDType;
import noNamespace.COMMANDType.SECURITYMODE;

@Component
public class MerchantToSubscriberTransactionHandler extends AbstractEcocashInboundTransactionHandler {

	@Override
	protected TransactionResponse setAndValidateCommandParameters(final TransactionRequest transactionRequest,
			final COMMANDType command) {
		final TransactionResponse subscriberMsisdnValidationResponse = validateEcocashMsisdn(
				transactionRequest.getMsisdn(), "Merchant msisdn required",
				"Invalid merchant msisdn : " + transactionRequest.getMsisdn());
		if (subscriberMsisdnValidationResponse != null) {
			return subscriberMsisdnValidationResponse;
		}
		final TransactionResponse merchantMsisdnValidationResponse = validateEcocashMsisdn(
				transactionRequest.getMsisdn2(), "Subscriber msisdn required",
				"Invalid subscriber msisdn : " + transactionRequest.getMsisdn2());
		if (merchantMsisdnValidationResponse != null) {
			return merchantMsisdnValidationResponse;
		}
		final TransactionResponse amountValidationResponse = validateAmount(transactionRequest, "Amount required",
				"Invalid amount : " + transactionRequest.getAmount(),
				"Amount cannot be negative or zero : " + transactionRequest.getAmount());
		if (amountValidationResponse != null) {
			return amountValidationResponse;
		}
		final TransactionResponse currencyValidationResponse = validateCurrency(transactionRequest.getCurrencyCode(),
				"Currency code required", "Invalid currency code : " + transactionRequest.getCurrencyCode());
		if (currencyValidationResponse != null) {
			return currencyValidationResponse;
		}
		command.setTYPE(EcocashInboundHandlerConstants.CommandParameters.MERCHANT_TO_SUBSCRIBER_CODE);
		command.setMERCODE(formatEcocashMsisdn(transactionRequest.getMsisdn()));
		command.setAMOUNT(transactionRequest.getAmount());
		if (StringUtils.hasText(transactionRequest.getPin())) {
			command.setPIN(transactionRequest.getPin());
		}
		command.setISPINCHECKREQ(StringUtils.isEmpty(transactionRequest.getPin()) ? "N" : "Y");
		command.setAMOUNT(getAmountFormat().format(new BigDecimal(transactionRequest.getAmount())));
		command.setSOURCE(EcocashInboundHandlerConstants.CommandParameters.TRANSACTION_SOURCE);
		command.setMSISDN2(formatEcocashMsisdn(transactionRequest.getMsisdn2()));
		command.setEXTTRANSACTIONID(transactionRequest.getSourceReference());
		if (StringUtils.hasText(transactionRequest.getPin()))
			command.setSECURITYMODE(SECURITYMODE.PINFULL);
		else
			command.setSECURITYMODE(SECURITYMODE.PINLESS);
		command.setCHECKSUM(generateCheckSum(command.getTYPE() + command.getAMOUNT() + command.getMERCODE()
				+ command.getPIN() + command.getMSISDN2() + command.getSOURCE()));
		return null;

	}

	@Override
	protected TransactionResponse generateTransactionResponse(final COMMANDType responseCommandType) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setStatusCode(responseCommandType.getTXNSTATUS());
		transactionResponse.setStatusReason(responseCommandType.getMESSAGE());
		transactionResponse.setDestinationReference(responseCommandType.getTXNID());
		transactionResponse.setSuccess(EcocashInboundHandlerConstants.ResponseCodes.SUCCESS
				.equalsIgnoreCase(responseCommandType.getTXNSTATUS()));
		return transactionResponse;
	}

}
