package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

import noNamespace.COMMANDType;
import noNamespace.COMMANDType.SECURITYMODE;

@Component
public class MerchantWalletToBankTransactionHandler extends AbstractEcocashInboundTransactionHandler {

	@Override
	protected TransactionResponse setAndValidateCommandParameters(final TransactionRequest transactionRequest,
			final COMMANDType command) {
		final TransactionResponse amountValidationResponse = validateAmount(transactionRequest, "Amount required",
				"Invalid amount : " + transactionRequest.getAmount(),
				"Amount cannot be negative or zero : " + transactionRequest.getAmount());
		if (amountValidationResponse != null) {
			return amountValidationResponse;
		}
		final TransactionResponse merchantMsisdnValidationResponse = validateEcocashMsisdn(
				transactionRequest.getMsisdn(), "Merchant msisdn required",
				"Invalid merchant msisdn : " + transactionRequest.getMsisdn());
		if (merchantMsisdnValidationResponse != null) {
			return merchantMsisdnValidationResponse;
		}
		final TransactionResponse receiverMsisdnValidationResponse = validateEcocashMsisdn(
				transactionRequest.getMsisdn2(), "Receiver msisdn is required",
				"Invalid receiver msisdn : " + transactionRequest.getMsisdn2());
		if (receiverMsisdnValidationResponse != null) {
			return receiverMsisdnValidationResponse;
		}
		final TransactionResponse currencyValidationResponse = validateCurrency(transactionRequest.getCurrencyCode(),
				"Currency code required", "Invalid currency code : " + transactionRequest.getCurrencyCode());
		if (currencyValidationResponse != null) {
			return currencyValidationResponse;
		}
		final TransactionResponse receiverAccountNoValidationResponse = validateAccountNumber1(transactionRequest,
				"Receiver account number is required", "Invalid receiver account number");
		if (receiverAccountNoValidationResponse != null) {
			return receiverAccountNoValidationResponse;
		}
		final TransactionResponse bankIdValidationResponse = validateBankId(transactionRequest, "Bank id is required",
				"Invalid bank id : " + transactionRequest.getTerminalId());
		if (bankIdValidationResponse != null) {
			return bankIdValidationResponse;
		}
		command.setTYPE(EcocashInboundHandlerConstants.CommandParameters.MERCHANT_WALLET_TO_BANK_CODE);
		command.setMSISDN(formatEcocashMsisdn(transactionRequest.getMsisdn()));
		command.setACCNO2(formatEcocashMsisdn(transactionRequest.getAccountNumber1().trim()));
		command.setAMOUNT(transactionRequest.getAmount());
		command.setPIN(StringUtils.isEmpty(transactionRequest.getPin()) ? "" : transactionRequest.getPin());
		command.setAMOUNT(getAmountFormat().format(new BigDecimal(transactionRequest.getAmount())));
		command.setSOURCE(EcocashInboundHandlerConstants.CommandParameters.TRANSACTION_SOURCE);
		command.setMSISDN2(formatEcocashMsisdn(transactionRequest.getMsisdn2()));
		command.setFLAG("FALSE");
		command.setUSERTYPE("CHANNEL");
		command.setREQUESTSTATE("FC");
		command.setLANGUAGE(1);
		command.setLANGUAGE1(1);
		command.setPROVIDER("101");
		command.setPROVIDER2("101");
		command.setBANKID(transactionRequest.getTerminalId());
		command.setISPINCHECKREQ(StringUtils.isEmpty(transactionRequest.getPin()) ? "N" : "Y");
		command.setCHECKSUM(generateCheckSum(command.getTYPE() + command.getPIN() + command.getACCNO2()
				+ command.getBANKID() + command.getMSISDN() + command.getMSISDN2() + command.getSOURCE()));
		if (StringUtils.hasText(transactionRequest.getPin()))
			command.setSECURITYMODE(SECURITYMODE.PINFULL);
		else
			command.setSECURITYMODE(SECURITYMODE.PINLESS);
		return null;
	}

	@Override
	protected TransactionResponse generateTransactionResponse(final COMMANDType responseCommandType) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setDestinationReference(responseCommandType.getEXTTRANSACTIONID());
		transactionResponse.setStatusCode(responseCommandType.getTXNSTATUS());
		transactionResponse.setStatusReason(responseCommandType.getMESSAGE());
		transactionResponse.setAmount(responseCommandType.getAMOUNT());
		transactionResponse.setSuccess(EcocashInboundHandlerConstants.ResponseCodes.SUCCESS
				.equalsIgnoreCase(responseCommandType.getTXNSTATUS()));
		return transactionResponse;
	}

}
