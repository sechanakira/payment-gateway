package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

import noNamespace.COMMANDType;

@Component
public class RemittanceCashInTransactionHandler extends AbstractEcocashInboundTransactionHandler {

	@Override
	protected TransactionResponse setAndValidateCommandParameters(final TransactionRequest request,
			final COMMANDType command) {
		final TransactionResponse validationResponse = validateRequest(request);
		if (validationResponse != null) {
			return validationResponse;
		}
		command.setTYPE(EcocashInboundHandlerConstants.CommandParameters.REMITTANCE_CASH_IN_TYPE_CODE);
		command.setFNAME(request.getFirstName());
		command.setLNAME(request.getLastName());
		command.setCOUNTRYCODE(request.getCountryCode());
		command.setMSISDN(formatEcocashMsisdn(request.getMsisdn()));
		command.setMSISDN2(formatEcocashMsisdn(request.getMsisdn2()));
		command.setAMOUNT(request.getAmount());
		command.setCURRENCY(request.getCurrencyCode());
		command.setPIN(request.getPin());
		command.setSOURCE(request.getSource());
		command.setDATE(generateCurrentDate());
		command.setTIME(generateCurrentTime());
		command.setCHECKSUM(generateCheckSum(command.getTYPE() + command.getAMOUNT() + command.getPIN()
				+ command.getMSISDN2() + command.getMSISDN() + command.getSOURCE()));
		return null;
	}

	private TransactionResponse validateRequest(final TransactionRequest request) {
		final TransactionResponse firstNameValidationResponse = validateRequiredParameter(request.getFirstName(),
				"Sender`s first name is required");
		if (firstNameValidationResponse != null) {
			return firstNameValidationResponse;
		}
		final TransactionResponse lastNameValidationResponse = validateRequiredParameter(request.getLastName(),
				"Sender`s last name is required");
		if (lastNameValidationResponse != null) {
			return lastNameValidationResponse;
		}
		final TransactionResponse countryCodeValidationResponse = validateCountryCode(request.getCountryCode(),
				"Country code is required", "Invalid country code : " + request.getCountryCode());
		if (countryCodeValidationResponse != null) {
			return countryCodeValidationResponse;
		}
		final TransactionResponse agentMsisdnValidationResponse = validateEcocashMsisdn(request.getMsisdn(),
				"Agent msisdn is required", "Invalid agent msisdn : " + request.getMsisdn());
		if (agentMsisdnValidationResponse != null) {
			return agentMsisdnValidationResponse;
		}
		final TransactionResponse customerMsidsnValidationResponse = validateEcocashMsisdn(request.getMsisdn2(),
				"Customer msisdn required", "Invalid customer msisdn : " + request.getMsisdn2());
		if (customerMsidsnValidationResponse != null) {
			return customerMsidsnValidationResponse;
		}
		final TransactionResponse currencyValidationResponse = validateCurrency(request.getCurrencyCode(),
				"Currency code required", "Invalid currency code : " + request.getCurrencyCode());
		if (currencyValidationResponse != null) {
			return currencyValidationResponse;
		}
		final TransactionResponse amountValidationResponse = validateAmount(request, "Cash in amount is required",
				"Invalid cash in amount : " + request.getAmount(),
				"Cash in amount cannot be negative or zero : " + request.getAmount());
		if (amountValidationResponse != null) {
			return amountValidationResponse;
		}
		return null;
	}

	private String generateCurrentTime() {
		return new SimpleDateFormat("ddMMyyyy").format(new Date());
	}

	private String generateCurrentDate() {
		return new SimpleDateFormat("HHmmss").format(new Date());
	}

	private TransactionResponse validateCountryCode(final String countryCode, final String requiredMessage,
			final String invalidMessage) {
		final TransactionResponse requiredParameterResponse = validateRequiredParameter(countryCode, requiredMessage);
		if (requiredParameterResponse != null) {
			return requiredParameterResponse;
		}
		// TODO validate value
		return null;
	}

	@Override
	protected TransactionResponse generateTransactionResponse(final COMMANDType responseCommandType) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setStatusCode(responseCommandType.getTXNSTATUS());
		transactionResponse.setDestinationReference(responseCommandType.getTXNID());
		transactionResponse.setStatusReason(responseCommandType.getMESSAGE());
		transactionResponse.setSuccess(EcocashInboundHandlerConstants.ResponseCodes.SUCCESS
				.equalsIgnoreCase(responseCommandType.getTXNSTATUS()));
		return transactionResponse;
	}

	@Override
	protected RequestType getRequestType() {
		return RequestType.REMITTANCE;
	}

}
