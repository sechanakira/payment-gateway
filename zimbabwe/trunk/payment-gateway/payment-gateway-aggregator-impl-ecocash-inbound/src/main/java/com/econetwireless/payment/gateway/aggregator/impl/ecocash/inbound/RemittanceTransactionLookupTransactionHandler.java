package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound.EcocashInboundHandlerConstants.StatusCodes;
import com.econetwireless.payment.gateway.dao.transaction.TransactionDao;
import com.econetwireless.payment.gateway.entities.Transaction;

import noNamespace.COMMANDType;

@Component
public class RemittanceTransactionLookupTransactionHandler extends AbstractEcocashInboundTransactionHandler {

	@Autowired
	private TransactionDao transactionDao;

	@Override
	protected TransactionResponse setAndValidateCommandParameters(final TransactionRequest request,
			final COMMANDType command) {
		final TransactionResponse msidsnValidationResponse = validateEcocashMsisdn(request.getMsisdn(),
				"Agent msisdn is required", "Invalid agent msisdn : " + request.getMsisdn());
		if (msidsnValidationResponse != null) {
			return msidsnValidationResponse;
		}
		final TransactionResponse sourceValidationResponse = validateSource(request.getSource(),
				"Remittance source required", "Invalid remittance source : " + request.getSource());
		if (sourceValidationResponse != null) {
			return sourceValidationResponse;
		}
		command.setMSISDN(formatEcocashMsisdn(request.getMsisdn()));
		command.setPROVIDER("101");
		command.setPAYID("12");
		command.setLANGUAGE1(1);
		command.setPIN(request.getPin());
		command.setTYPE(EcocashInboundHandlerConstants.CommandParameters.REMITTANCE_TRANSACTION_LOOKUP_CODE);
		command.setSOURCE(request.getSource());
		final Transaction cashInTransaction = lookupTransactionBySourceRef(request);
		if (cashInTransaction == null) {
			return buildResponseWithStatusCode(StatusCodes.INVALID_PARAMETER,
					"Transaction with reference " + request.getSourceReference() + " not found", false);
		}
		command.setTRANSACTIONID(cashInTransaction.getDestinationReference());
		command.setEXTTRANSACTIONID(cashInTransaction.getGatewayReference());
		command.setCHECKSUM(generateCheckSum(command.getTYPE() + command.getMSISDN() + command.getTRANSACTIONID()
				+ command.getLANGUAGE1() + command.getEXTTRANSACTIONID() + command.getSOURCE()));
		return null;
	}

	@Override
	protected TransactionResponse generateTransactionResponse(final COMMANDType responseCommandType) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setStatusCode(responseCommandType.getTXNSTATUS());
		transactionResponse.setDestinationReference(responseCommandType.getTXNID());
		transactionResponse.setStatusReason(responseCommandType.getMESSAGE());
		transactionResponse.setSuccess(EcocashInboundHandlerConstants.ResponseCodes.SUCCESS
				.equalsIgnoreCase(responseCommandType.getTXNSTATUS()));
		return transactionResponse;
	}

	@Override
	protected RequestType getRequestType() {
		return RequestType.REMITTANCE;
	}

	private Transaction lookupTransactionBySourceRef(final TransactionRequest request) {
		final List<Transaction> transactionsBySourceRef = transactionDao
				.findBySourceReference(request.getTransactionId());
		for (final Transaction transaction : transactionsBySourceRef) {
			if (transaction.getVendor().getCode().equalsIgnoreCase(request.getVendorCode())) {
				return transaction;
			}
		}
		return null;
	}
}
