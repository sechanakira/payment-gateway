package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound.config;

import javax.sql.DataSource;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class EcocashInboundConfig {

	@Bean
	public HttpClient httpClient(){
		return HttpClientBuilder.create().build();
	}
	
	@Bean
	public DataSource ecocashDataSource(){
		return null;
	}
	
	//@Bean
	public JdbcTemplate jdbcTemplate(){
		return new JdbcTemplate(ecocashDataSource());
	}
}
