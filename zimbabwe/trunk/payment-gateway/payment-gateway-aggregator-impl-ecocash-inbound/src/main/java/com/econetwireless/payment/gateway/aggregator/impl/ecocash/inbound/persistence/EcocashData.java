package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by fidelis on 9/29/2014.
 */
@Entity
@Table(name ="ecolife_view")
public class EcocashData implements Serializable
{
    @Column(name = "FIRST_NAME" , unique = false , length = 30)
    private String FIRST_NAME;
    @Column(name = "LAST_NAME" , unique = false , length = 30)
    private String LAST_NAME;
    @Id
    @Column(name = "MSISDN" , unique = true , length = 30)
    private String MSISDN;
    @Column(name = "NAT_ID" , unique = false , length = 30)
    private String NAT_ID ;
    @Column(name = "STATUS" , unique = false , length = 30)
    private String STATUS;
    @Column(name = "DOB" , unique = false , length = 30)
    private String DOB;
    @Column(name = "GENDER" , unique = false , length = 30)
    private String GENDER;
    @Column(name = "ADDRESS1" , unique = false , length = 30)
    private String ADDRESS1;
    @Column(name = "ADDRESS2" , unique = false , length = 30)
    private String ADDRESS2;
    @Column(name = "CITY" , unique = false , length = 30)
    private String CITY;

    public String getFIRST_NAME() {
        return FIRST_NAME;
    }

    public void setFIRST_NAME(String FIRST_NAME) {
        this.FIRST_NAME = FIRST_NAME;
    }

    public String getLAST_NAME() {
        return LAST_NAME;
    }

    public void setLAST_NAME(String LAST_NAME) {
        this.LAST_NAME = LAST_NAME;
    }

    public String getMSISDN() {
        return MSISDN;
    }

    public void setMSISDN(String MSISDN) {
        this.MSISDN = MSISDN;
    }

    public String getNAT_ID() {
        return NAT_ID;
    }

    public void setNAT_ID(String NAT_ID) {
        this.NAT_ID = NAT_ID;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getGENDER() {
        return GENDER;
    }

    public void setGENDER(String GENDER) {
        this.GENDER = GENDER;
    }

    public String getADDRESS1() {
        return ADDRESS1;
    }

    public void setADDRESS1(String ADDRESS1) {
        this.ADDRESS1 = ADDRESS1;
    }

    public String getADDRESS2() {
        return ADDRESS2;
    }

    public void setADDRESS2(String ADDRESS2) {
        this.ADDRESS2 = ADDRESS2;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }
}
