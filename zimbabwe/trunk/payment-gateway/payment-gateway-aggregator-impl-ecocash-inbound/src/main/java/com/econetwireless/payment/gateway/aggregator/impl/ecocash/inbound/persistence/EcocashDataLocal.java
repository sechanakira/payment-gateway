package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound.persistence;

/**
 * Created by admin on 09/06/2015.
 */
public interface EcocashDataLocal {
    public EcocashData findByMobileNumber(String mobileNumber);
}
