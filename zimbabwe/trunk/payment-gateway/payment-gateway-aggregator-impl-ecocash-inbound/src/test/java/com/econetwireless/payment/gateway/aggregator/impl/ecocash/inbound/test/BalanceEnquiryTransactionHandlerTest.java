package com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound.test;

import java.io.File;
import java.net.URL;

import org.apache.http.ProtocolVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.message.BasicHttpResponse;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;

import static org.mockito.Matchers.any;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound.BalanceEnquiryTransactionHandler;
import com.econetwireless.payment.gateway.aggregator.impl.ecocash.inbound.EcocashInboundHandlerConstants;


@RunWith(MockitoJUnitRunner.class)
public class BalanceEnquiryTransactionHandlerTest {

	
	@Mock
	private HttpClient httpClient;
	
	@InjectMocks
	private BalanceEnquiryTransactionHandler transactionHandler;
	
	@Test
	public void shouldReturnErrorCodeWhenSubscriberMsisdnIsEmpty(){
		final TransactionRequest transactionRequest = new TransactionRequest();
		String gatewayReference="0101011201261158";
		final TransactionResponse transactionResponse = transactionHandler.handleTransaction(transactionRequest , gatewayReference);
		Assert.assertThat(transactionResponse, notNullValue());
		Assert.assertThat(transactionResponse.getStatusCode(), equalTo(EcocashInboundHandlerConstants.StatusCodes.MISSING_PARAMETER));
		Assert.assertThat(transactionResponse.getStatusReason(), equalTo(EcocashInboundHandlerConstants.StatusMessages.BalanceEnquiry.MSISDN_REQUIRED));
		Assert.assertThat(transactionResponse.getBalance(), nullValue());
		Assert.assertThat(transactionResponse.getDestinationReference(), nullValue());
		verifyZeroInteractions(httpClient);
	}
	
	@Test
	public void shouldReturnErrorCodeWhenSubscriberMsisdnIsInvalid(){
		String gatewayReference="0101011201261158";
		final TransactionRequest transactionRequest = new TransactionRequest();
		transactionRequest.setMsisdn("6789");
		final TransactionResponse transactionResponse = transactionHandler.handleTransaction(transactionRequest , gatewayReference);
		Assert.assertThat(transactionResponse, notNullValue());
		Assert.assertThat(transactionResponse.getStatusCode(), equalTo(EcocashInboundHandlerConstants.StatusCodes.INVALID_PARAMETER));
		Assert.assertThat(transactionResponse.getStatusReason(), equalTo(EcocashInboundHandlerConstants.StatusMessages.BalanceEnquiry.INVALID_MSISDN));
		Assert.assertThat(transactionResponse.getBalance(), nullValue());
		Assert.assertThat(transactionResponse.getDestinationReference(), nullValue());
		verifyZeroInteractions(httpClient);
	}
	
	@Test
	public void shouldReturnEcocashResponseCodeOnSuccess() throws Exception{
		final TransactionRequest transactionRequest = new TransactionRequest();
		String gatewayReference="0101011201261158";
		transactionRequest.setMsisdn("771222164");
		transactionRequest.setSourceReference("ABCDEFGHJK");
		final BasicHttpResponse httpResponse = new BasicHttpResponse(new ProtocolVersion("http", 1, 1) , 200 , "OK");
		final URL responseFile = getClass().getResource("/balance-enq-success-response.txt");
	    final FileEntity entity = new FileEntity(new File(responseFile.getFile()), ContentType.APPLICATION_XML);
		httpResponse.setEntity(entity);
		when(httpClient.execute(any(HttpPost.class))).thenReturn(httpResponse);
		final TransactionResponse transactionResponse = transactionHandler.handleTransaction(transactionRequest , gatewayReference);
		Assert.assertThat(transactionResponse, notNullValue());
		Assert.assertThat(transactionResponse.getBalance(), equalTo("987.48"));
		Assert.assertThat(transactionResponse.getDestinationReference(), equalTo("BE150529.1107.A00005"));
		Assert.assertThat(transactionResponse.getStatusReason(), equalTo("Balance is USD:987.48. Enjoy Free access to world class courses on EcoSchool Academy from 24-26 April Dial *148# to subscribe. To opt-out SMS STOP to 34440."));
	}
}
