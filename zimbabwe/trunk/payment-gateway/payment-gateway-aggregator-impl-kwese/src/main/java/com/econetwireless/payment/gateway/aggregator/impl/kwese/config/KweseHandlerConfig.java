package com.econetwireless.payment.gateway.aggregator.impl.kwese.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.econetwireless.payment.gateway.aggregator.impl.kwese.handler.CustomerLookupTransactionHandler;

@Configuration
public class KweseHandlerConfig {
	
@Bean
public CustomerLookupTransactionHandler customerLookupTransactionHandler(){

	return new CustomerLookupTransactionHandler();
}

}
