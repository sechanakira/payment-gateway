package com.econetwireless.payment.gateway.aggregator.impl.kwese.handler;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.econetwireless.payment.gateway.aggregator.api.TransactionHandler;
import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

public class CustomerLookupTransactionHandler  implements TransactionHandler{
private static final Logger logger = LoggerFactory.getLogger(CustomerLookupTransactionHandler.class);
	
	public TransactionResponse handleTransaction(TransactionRequest request, String gatewayReference) {
		TransactionResponse transactionResponse = new TransactionResponse();
		logger.info("** KWESE****CustomerLookupTransactionHandler kwese");


		transactionResponse.setAccountName("Acc-name");
		transactionResponse.setTerminalName("terminal-name");
		transactionResponse.setAmount("12");
		transactionResponse.setDestinationReference("dest-ref");
		transactionResponse.setSuccess(Boolean.TRUE);

		return transactionResponse;
	}

}
