package com.econetwireless.payment.gateway.aggregator.impl.esolutions;

import java.util.Date;

public interface DateUtils {
	public String formatTransmissionDate(Date date);
}
