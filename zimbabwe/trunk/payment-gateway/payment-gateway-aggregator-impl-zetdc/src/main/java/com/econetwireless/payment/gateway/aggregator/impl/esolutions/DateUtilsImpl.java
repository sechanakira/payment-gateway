package com.econetwireless.payment.gateway.aggregator.impl.esolutions;

import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.TRANSACTION_DATE_FORMAT;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtilsImpl implements DateUtils {

	@Override
	public String formatTransmissionDate(Date date) {
		SimpleDateFormat transmissionDateFormat = new SimpleDateFormat(TRANSACTION_DATE_FORMAT);
		return transmissionDateFormat.format(date);
	}

}
