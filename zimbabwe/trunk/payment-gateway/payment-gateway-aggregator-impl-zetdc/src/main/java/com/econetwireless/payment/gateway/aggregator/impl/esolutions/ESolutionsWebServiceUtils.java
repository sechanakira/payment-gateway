package com.econetwireless.payment.gateway.aggregator.impl.esolutions;

import vending.mcommerce.esolutions.co.zw.ISOMsg;

public interface ESolutionsWebServiceUtils {
	public ISOMsg postRequest(final ISOMsg request);
}
