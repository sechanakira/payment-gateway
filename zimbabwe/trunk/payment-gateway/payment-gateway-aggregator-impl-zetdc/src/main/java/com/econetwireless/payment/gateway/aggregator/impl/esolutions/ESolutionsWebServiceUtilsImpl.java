package com.econetwireless.payment.gateway.aggregator.impl.esolutions;

import vending.mcommerce.esolutions.co.zw.ISOMsg;
import vending.mcommerce.esolutions.co.zw.VendingService;

public class ESolutionsWebServiceUtilsImpl implements ESolutionsWebServiceUtils {

	private VendingService vendingService;

	public void setVendingService(VendingService vendingService) {
		this.vendingService = vendingService;
	}

	@Override
	public ISOMsg postRequest(ISOMsg request) {
		return vendingService.sendVendingRequest(request);
	}

}
