package com.econetwireless.payment.gateway.aggregator.impl.esolutions;

import java.math.BigDecimal;
import java.util.Date;

import vending.mcommerce.esolutions.co.zw.ISOMsgField;

public interface ISOMessageFieldUtils {
	public ISOMsgField createProcessingCodeISOMsgField(final String processingCode);

	public ISOMsgField createTransmissionDateField(final Date transmissionDate);

	public ISOMsgField createVendorNumberField(final String vendorNumber);

	public ISOMsgField createTransactionReferenceField(final String transactionReference);

	public ISOMsgField createTerminalIDField(final String vendorTerminalID);

	public ISOMsgField createMerchantNameField(final String merchantName);

	public ISOMsgField createCustomerAccountNumberField(final String accountNumber);

	public ISOMsgField createCustomerDataField(final String customerData);

	public ISOMsgField createCurrencyCodeField(String currencyCode);

	public ISOMsgField createAggregatorField(final String aggregator);

	public ISOMsgField createPaymentTypeField(final String paymentType);

	public ISOMsgField creteTransactionAmountField(final BigDecimal transactionAmount);

	public ISOMsgField createCardAcceptorName(final String accepterName);
}
