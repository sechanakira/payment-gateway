package com.econetwireless.payment.gateway.aggregator.impl.esolutions;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import vending.mcommerce.esolutions.co.zw.ISOMsgField;

import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.*;

public class ISOMessageFieldUtilsImpl implements ISOMessageFieldUtils {

	@Autowired
	private DateUtils dateUtils;

	@Autowired
	private MoneyFormatUtils moneyFormatUtils;

	@Override
	public ISOMsgField createProcessingCodeISOMsgField(String processingCode) {
		final ISOMsgField processingCodeISOMsgField = new ISOMsgField();
		processingCodeISOMsgField.setNumber(PROCESSING_CODE_FIELD);
		processingCodeISOMsgField.setValue(processingCode);
		return processingCodeISOMsgField;
	}

	@Override
	public ISOMsgField createTransmissionDateField(Date transmissionDate) {
		final ISOMsgField transmissionDateField = new ISOMsgField();
		transmissionDateField.setNumber(TRANSMISSION_DATE_FIELD);
		transmissionDateField.setValue(dateUtils.formatTransmissionDate(transmissionDate));
		return transmissionDateField;
	}

	@Override
	public ISOMsgField createVendorNumberField(String vendorNumber) {
		final ISOMsgField vendorNumberField = new ISOMsgField();
		vendorNumberField.setNumber(VENDOR_NUMBER_FIELD);
		vendorNumberField.setValue(vendorNumber);
		return vendorNumberField;
	}

	@Override
	public ISOMsgField createTransactionReferenceField(String transactionReference) {
		final ISOMsgField transactionReferenceField = new ISOMsgField();
		transactionReferenceField.setNumber(TRANSACTION_REFERENCE_FIELD);
		transactionReferenceField.setValue(transactionReference);
		return transactionReferenceField;
	}

	@Override
	public ISOMsgField createTerminalIDField(String vendorTerminalID) {
		final ISOMsgField terminalIDField = new ISOMsgField();
		terminalIDField.setNumber(VENDOR_TERMINAL_ID_FIELD);
		terminalIDField.setValue(vendorTerminalID);
		return terminalIDField;
	}

	@Override
	public ISOMsgField createMerchantNameField(String merchantName) {
		final ISOMsgField merchantNameField = new ISOMsgField();
		merchantNameField.setNumber(MERCHANT_NAME_FIELD);
		merchantNameField.setValue(merchantName);
		return merchantNameField;
	}

	@Override
	public ISOMsgField createCustomerAccountNumberField(String accountNumber) {
		final ISOMsgField customerAccountNumberField = new ISOMsgField();
		customerAccountNumberField.setNumber(CUSTOMER_ACCOUNT_NUMBER_FIELD);
		customerAccountNumberField.setValue(accountNumber);
		return customerAccountNumberField;
	}

	@Override
	public ISOMsgField createCustomerDataField(String customerData) {
		final ISOMsgField customerDataField = new ISOMsgField();
		customerDataField.setNumber(CUSTOMER_DATA_FIELD);
		customerDataField.setValue(customerData);
		return customerDataField;
	}

	@Override
	public ISOMsgField createCurrencyCodeField(String currencyCode) {
		final ISOMsgField currencyCodeField = new ISOMsgField();
		currencyCodeField.setNumber(CURRENCY_CODE_FIELD);
		currencyCodeField.setValue(currencyCode);
		return currencyCodeField;
	}

	@Override
	public ISOMsgField createAggregatorField(String aggregator) {
		final ISOMsgField aggregatorField = new ISOMsgField();
		aggregatorField.setNumber(AGGREGATOR_FIELD);
		aggregatorField.setValue(aggregator);
		return aggregatorField;
	}

	@Override
	public ISOMsgField createPaymentTypeField(String paymentType) {
		final ISOMsgField paymentTypeField = new ISOMsgField();
		paymentTypeField.setNumber(PAYMENT_TYPE_FIELD);
		paymentTypeField.setValue(paymentType);
		return paymentTypeField;
	}

	@Override
	public ISOMsgField creteTransactionAmountField(BigDecimal transactionAmount) {
		final ISOMsgField transactionAmountField = new ISOMsgField();
		transactionAmountField.setNumber(TRANSACTION_AMOUNT_FIELD);
		transactionAmountField.setValue(moneyFormatUtils.getAmountInCents(transactionAmount));
		return transactionAmountField;
	}

	@Override
	public ISOMsgField createCardAcceptorName(String accepterName) {
		final ISOMsgField cardAcceptorName = new ISOMsgField();
		cardAcceptorName.setNumber(CARD_ACCEPTOR_NAME_FIELD);
		cardAcceptorName.setValue(accepterName);
		return cardAcceptorName;
	}

}
