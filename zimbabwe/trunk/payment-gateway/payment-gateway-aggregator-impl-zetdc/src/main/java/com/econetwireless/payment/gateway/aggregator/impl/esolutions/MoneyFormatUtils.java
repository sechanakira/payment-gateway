package com.econetwireless.payment.gateway.aggregator.impl.esolutions;

import java.math.BigDecimal;

public interface MoneyFormatUtils {
	
	public String getAmountInCents(final BigDecimal inputAmount);
	public BigDecimal getAmountInDollars(final String amountInCents);
	
}
