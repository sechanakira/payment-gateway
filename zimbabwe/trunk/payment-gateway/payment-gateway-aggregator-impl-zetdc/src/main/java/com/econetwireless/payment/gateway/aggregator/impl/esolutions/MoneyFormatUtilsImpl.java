package com.econetwireless.payment.gateway.aggregator.impl.esolutions;

import java.math.BigDecimal;

public class MoneyFormatUtilsImpl implements MoneyFormatUtils {

	@Override
	public String getAmountInCents(BigDecimal inputAmount){
		return Integer.toString(inputAmount.movePointRight(2).intValue());
	}

	@Override
	public BigDecimal getAmountInDollars(String amountInCents){
		BigDecimal amountInDollars = new BigDecimal(amountInCents);
		return amountInDollars.movePointLeft(2);
	}

}
