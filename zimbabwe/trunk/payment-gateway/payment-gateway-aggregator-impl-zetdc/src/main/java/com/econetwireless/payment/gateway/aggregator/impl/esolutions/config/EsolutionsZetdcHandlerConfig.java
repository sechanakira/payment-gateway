package com.econetwireless.payment.gateway.aggregator.impl.esolutions.config;

import java.io.IOException;
import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

import com.econetwireless.payment.gateway.aggregator.impl.esolutions.DateUtils;
import com.econetwireless.payment.gateway.aggregator.impl.esolutions.DateUtilsImpl;
import com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsWebServiceUtils;
import com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsWebServiceUtilsImpl;
import com.econetwireless.payment.gateway.aggregator.impl.esolutions.ISOMessageFieldUtils;
import com.econetwireless.payment.gateway.aggregator.impl.esolutions.ISOMessageFieldUtilsImpl;
import com.econetwireless.payment.gateway.aggregator.impl.esolutions.MoneyFormatUtils;
import com.econetwireless.payment.gateway.aggregator.impl.esolutions.MoneyFormatUtilsImpl;
import com.econetwireless.payment.gateway.aggregator.impl.esolutions.handler.CustomerInfoTransactionHandler;
import com.econetwireless.payment.gateway.aggregator.impl.esolutions.handler.LastTokenRetrievalTransactionHandler;
import com.econetwireless.payment.gateway.aggregator.impl.esolutions.handler.TokenPurchaseTransactionHandler;
import com.econetwireless.payment.gateway.aggregator.impl.esolutions.handler.VendorBalanceEnquiryTransactionHandler;
import com.econetwireless.payment.gateway.service.currency.CurrencyService;
import com.econetwireless.payment.gateway.service.parameter.ParameterService;

import vending.mcommerce.esolutions.co.zw.VendingService;

@Configuration
@PropertySource({ "classpath:esolutions-service.properties" })
public class EsolutionsZetdcHandlerConfig {

	@Bean
	public DateUtils dateUtils() {
		return new DateUtilsImpl();
	}

	@Bean
	public MoneyFormatUtils moneyFormatUtils() {
		return new MoneyFormatUtilsImpl();
	}

	@Bean
	public ISOMessageFieldUtils isoMessageFieldUtils() {
		return new ISOMessageFieldUtilsImpl();
	}

	@Bean
	public ESolutionsWebServiceUtils eSolutionsWebServiceUtils(VendingService vendingService) {
		final ESolutionsWebServiceUtilsImpl util = new ESolutionsWebServiceUtilsImpl();
		util.setVendingService(vendingService);
		return util;
	}

	@Bean
	public JaxWsPortProxyFactoryBean esolutionsServicePort(ResourceLoader resourceLoader, Environment environment)
			throws IOException {
		final JaxWsPortProxyFactoryBean factory = new JaxWsPortProxyFactoryBean();
		factory.setServiceName(environment.getRequiredProperty("esolutions.service.name"));
		factory.setEndpointAddress(environment.getRequiredProperty("esolutions.service.endpoint.url"));
		factory.setPortName(environment.getRequiredProperty("esolutions.service.port.name"));
		factory.setNamespaceUri(environment.getRequiredProperty("esolutions.service.namespaceuri"));
		factory.setWsdlDocumentResource(
				resourceLoader.getResource(environment.getRequiredProperty("esolutions.service.wsdl.location")));
		factory.setLookupServiceOnStartup(false);
		factory.addCustomProperty("com.sun.xml.ws.request.timeout", 360000L);
		factory.addCustomProperty("com.sun.xml.ws.connect.timeout", 360000L);
		factory.setServiceInterface(VendingService.class);
		return factory;
	}

	@Bean
	public CustomerInfoTransactionHandler customerInfoTransactionHandler(
			ESolutionsWebServiceUtils eSolutionsWebServiceUtils, ISOMessageFieldUtils isoMessageFieldUtils,
			ParameterService parameterService) {
		final CustomerInfoTransactionHandler handler = new CustomerInfoTransactionHandler();
		handler.setESolutionsWebServiceUtils(eSolutionsWebServiceUtils);
		handler.setIsoMessageFieldUtils(isoMessageFieldUtils);
		handler.setParameterService(parameterService);
		handler.setPaymentTypeMapping(paymentTypeMappings());
		return handler;
	}

	@Bean
	public TokenPurchaseTransactionHandler tokenPurchaseTransactionHandler(
			ESolutionsWebServiceUtils eSolutionsWebServiceUtils, ISOMessageFieldUtils isoMessageFieldUtils,
			ParameterService parameterService, CurrencyService currencyService) {
		final TokenPurchaseTransactionHandler handler = new TokenPurchaseTransactionHandler();
		handler.setESolutionsWebServiceUtils(eSolutionsWebServiceUtils);
		handler.setIsoMessageFieldUtils(isoMessageFieldUtils);
		handler.setParameterService(parameterService);
		handler.setPaymentTypeMapping(paymentTypeMappings());
		handler.setCurrencyService(currencyService);
		return handler;
	}

	@Bean
	public Properties paymentTypeMappings() {
		final Properties mappings = new Properties();
		mappings.setProperty("12345", "ZETDC_PREPAID_TYPE");
		return mappings;
	}

	@Bean
	public LastTokenRetrievalTransactionHandler lastTokenRetrievalTransactionHandler(
			ESolutionsWebServiceUtils eSolutionsWebServiceUtils, ISOMessageFieldUtils isoMessageFieldUtils,
			ParameterService parameterService,CurrencyService currencyService) {
		final LastTokenRetrievalTransactionHandler handler = new LastTokenRetrievalTransactionHandler();
		handler.setESolutionsWebServiceUtils(eSolutionsWebServiceUtils);
		handler.setIsoMessageFieldUtils(isoMessageFieldUtils);
		handler.setParameterService(parameterService);
		handler.setPaymentTypeMapping(paymentTypeMappings());
		handler.setCurrencyService(currencyService);
		return handler;
	}

	@Bean
	public VendorBalanceEnquiryTransactionHandler vendorBalanceEnquiryTransactionHandler(
			ESolutionsWebServiceUtils eSolutionsWebServiceUtils, ISOMessageFieldUtils isoMessageFieldUtils,
			ParameterService parameterService, CurrencyService currencyService) {
		final VendorBalanceEnquiryTransactionHandler handler = new VendorBalanceEnquiryTransactionHandler();
		handler.setESolutionsWebServiceUtils(eSolutionsWebServiceUtils);
		handler.setIsoMessageFieldUtils(isoMessageFieldUtils);
		handler.setParameterService(parameterService);
		handler.setPaymentTypeMapping(paymentTypeMappings());
		handler.setCurrencyService(currencyService);
		return handler;

	}
}
