package com.econetwireless.payment.gateway.aggregator.impl.esolutions.handler;

import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.ECOCASH_ZTEDC_AGGREGATOR_NAME_PARAM;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.ECOCASH_ZTEDC_MERCHANT_NAME_PARAM;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.ECOCASH_ZTEDC_VENDOR_CODE_PARAM;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.REQUEST_MESSAGE_TYPE_INDICATOR;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.econetwireless.payment.gateway.aggregator.api.TransactionHandler;
import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsWebServiceUtils;
import com.econetwireless.payment.gateway.aggregator.impl.esolutions.ISOMessageFieldUtils;
import com.econetwireless.payment.gateway.service.parameter.ParameterService;

import vending.mcommerce.esolutions.co.zw.ISOMsg;
import vending.mcommerce.esolutions.co.zw.ISOMsgField;

public abstract class AbstractZtedcTransactionHandler implements TransactionHandler {

	private static final Logger logger = LoggerFactory.getLogger(AbstractZtedcTransactionHandler.class);

	protected ISOMessageFieldUtils isoMessageFieldUtils;

	private ESolutionsWebServiceUtils eSolutionsWebServiceUtils;

	protected ParameterService parameterService;

	private Properties paymentTypeMapping;

	@Override
	public final TransactionResponse handleTransaction(TransactionRequest request, String gatewayReference) {
		logger.debug("Handling request {} , gateway reference {}", request, gatewayReference);
		final ISOMsg requestIsoMsg = new ISOMsg();
		addCommonFields(requestIsoMsg, request);
		addSpecialisedFields(requestIsoMsg, request);
		logger.info("Posting request {} to esolutions endpoint", generateIsoMsgDebugInfo(requestIsoMsg));
		final ISOMsg responseMsg = eSolutionsWebServiceUtils.postRequest(requestIsoMsg);
		final TransactionResponse transactionResponse = generateTransactionResponse(responseMsg, request);
		logger.info("Returning response  {}  for request {}", transactionResponse, request);
		return transactionResponse;
	}

	private static String generateIsoMsgDebugInfo(ISOMsg isoMsg) {
		final StringBuilder infoBuilder = new StringBuilder("ISO Msg Mti =");
		infoBuilder.append(isoMsg.getMti());
		infoBuilder.append(",Retrieval Reference = ");
		infoBuilder.append(isoMsg.getRetrievalReference());
		infoBuilder.append("\n");
		isoMsg.getField().stream().forEach(field -> generateIsoMsgFieldDebugInfo(field, infoBuilder));
		return infoBuilder.toString();
	}

	private static void generateIsoMsgFieldDebugInfo(ISOMsgField field, StringBuilder infoBuilder) {
		infoBuilder.append("Field ");
		infoBuilder.append(field.getNumber());
		infoBuilder.append(" = ");
		infoBuilder.append(field.getValue());
		infoBuilder.append("\n");
	}

	protected abstract TransactionResponse generateTransactionResponse(ISOMsg isoMsg, TransactionRequest request);

	protected abstract void addSpecialisedFields(ISOMsg isoMsg, TransactionRequest request);

	private void addCommonFields(ISOMsg isoMsg, TransactionRequest request) {
		isoMsg.setRetrievalReference(request.getSourceReference());
		isoMsg.setMti(REQUEST_MESSAGE_TYPE_INDICATOR);
		final List<ISOMsgField> isoMsgFields = isoMsg.getField();
		isoMsgFields.add(isoMessageFieldUtils.createTransmissionDateField(new Date()));
		final String vendorNumber = parameterService.findParameterValue(ECOCASH_ZTEDC_VENDOR_CODE_PARAM);
		isoMsgFields.add(isoMessageFieldUtils.createVendorNumberField(vendorNumber));
		isoMsgFields.add(isoMessageFieldUtils.createTerminalIDField(request.getTerminalId()));
		isoMsgFields.add(isoMessageFieldUtils.createCardAcceptorName(request.getTerminalName()));
		final String ztedcMerchantName = parameterService.findParameterValue(ECOCASH_ZTEDC_MERCHANT_NAME_PARAM);
		isoMsgFields.add(isoMessageFieldUtils.createMerchantNameField(ztedcMerchantName));
		final String ztedcAggregatorName = parameterService.findParameterValue(ECOCASH_ZTEDC_AGGREGATOR_NAME_PARAM);
		isoMsgFields.add(isoMessageFieldUtils.createAggregatorField(ztedcAggregatorName));
		final String paymentType = parameterService
				.findParameterValue(paymentTypeMapping.getProperty(request.getPaymentType()));
		isoMsgFields.add(isoMessageFieldUtils.createPaymentTypeField(paymentType));

	}

	public void setIsoMessageFieldUtils(ISOMessageFieldUtils isoMessageFieldUtils) {
		this.isoMessageFieldUtils = isoMessageFieldUtils;
	}

	public void setESolutionsWebServiceUtils(ESolutionsWebServiceUtils eSolutionsWebServiceUtils) {
		this.eSolutionsWebServiceUtils = eSolutionsWebServiceUtils;
	}

	public void setParameterService(ParameterService parameterService) {
		this.parameterService = parameterService;
	}

	public void setPaymentTypeMapping(Properties paymentTypeMapping) {
		this.paymentTypeMapping = paymentTypeMapping;
	}

}
