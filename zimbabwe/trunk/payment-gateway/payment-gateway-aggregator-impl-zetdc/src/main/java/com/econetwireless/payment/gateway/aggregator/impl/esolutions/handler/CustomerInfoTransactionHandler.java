package com.econetwireless.payment.gateway.aggregator.impl.esolutions.handler;

import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.CUSTOMER_ADDRESS_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.CUSTOMER_DATA_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.CUSTOMER_INFORMATION_PROCESSING_CODE;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.NARRATIVE_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.RESPONSE_CODE_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.SUCCESS_STATUS_CODE;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.TRANSACTION_REFERENCE_FIELD;

import java.util.List;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.entities.TransactionStatus;

import vending.mcommerce.esolutions.co.zw.ISOMsg;
import vending.mcommerce.esolutions.co.zw.ISOMsgField;

public class CustomerInfoTransactionHandler extends AbstractZtedcTransactionHandler {

	@Override
	protected TransactionResponse generateTransactionResponse(ISOMsg isoMsg, TransactionRequest request) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		for (final ISOMsgField isoMsgField : isoMsg.getField()) {
			if (isoMsgField.getNumber() == CUSTOMER_ADDRESS_FIELD) {
				transactionResponse.setAdditionalInfo5(isoMsgField.getValue());
			}
			if (isoMsgField.getNumber() == CUSTOMER_DATA_FIELD) {
				transactionResponse.setAdditionalInfo1(isoMsgField.getValue());
			}
			if (isoMsgField.getNumber() == RESPONSE_CODE_FIELD) {
				transactionResponse.setStatusCode(isoMsgField.getValue());
			}
			if (isoMsgField.getNumber() == TRANSACTION_REFERENCE_FIELD) {
				transactionResponse.setDestinationReference(isoMsgField.getValue());
			}
			if (isoMsgField.getNumber() == NARRATIVE_FIELD) {
				transactionResponse.setStatusReason(isoMsgField.getValue());
			}

		}
		transactionResponse
				.setTransactionStatus(SUCCESS_STATUS_CODE.equalsIgnoreCase(transactionResponse.getStatusCode())
						? TransactionStatus.SUCCESS : TransactionStatus.FAILED);
		return transactionResponse;
	}

	@Override
	protected void addSpecialisedFields(ISOMsg isoMsg, TransactionRequest request) {
		final List<ISOMsgField> isoMsgFields = isoMsg.getField();
		isoMsgFields.add(isoMessageFieldUtils.createProcessingCodeISOMsgField(CUSTOMER_INFORMATION_PROCESSING_CODE));
		isoMsgFields.add(isoMessageFieldUtils.createCustomerAccountNumberField(request.getAccountNumber2()));
	}

}
