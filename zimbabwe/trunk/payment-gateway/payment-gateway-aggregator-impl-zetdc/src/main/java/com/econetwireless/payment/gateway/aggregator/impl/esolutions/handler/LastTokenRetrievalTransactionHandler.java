package com.econetwireless.payment.gateway.aggregator.impl.esolutions.handler;

import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.LAST_CUSTOMER_TOKEN_PROCESSING_CODE;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.NARRATIVE_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.RESPONSE_CODE_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.TOKEN_VOUCHER_DATA_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.TRANSACTION_REFERENCE_FIELD;

import java.util.List;

import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.entities.Currency;
import com.econetwireless.payment.gateway.service.currency.CurrencyService;

import vending.mcommerce.esolutions.co.zw.ISOMsg;
import vending.mcommerce.esolutions.co.zw.ISOMsgField;

public class LastTokenRetrievalTransactionHandler extends AbstractZtedcTransactionHandler {

	private CurrencyService currencyService;

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	@Override
	protected TransactionResponse generateTransactionResponse(ISOMsg isoMsg, TransactionRequest request) {
		final TransactionResponse response = new TransactionResponse();
		for (final ISOMsgField isoMsgField : isoMsg.getField()) {
			if (isoMsgField.getNumber() == TOKEN_VOUCHER_DATA_FIELD) {
				final String token = extractToken(isoMsgField.getValue());
				response.setAdditionalInfo1(token);
			}
			if (isoMsgField.getNumber() == RESPONSE_CODE_FIELD) {
				response.setStatusCode(isoMsgField.getValue());
			}
			if (isoMsgField.getNumber() == TRANSACTION_REFERENCE_FIELD) {
				response.setDestinationReference(isoMsgField.getValue());
			}
			if (isoMsgField.getNumber() == NARRATIVE_FIELD) {
				response.setStatusReason(isoMsgField.getValue());
			}
		}
		return response;
	}

	@Override
	protected void addSpecialisedFields(ISOMsg isoMsg, TransactionRequest request) {
		final List<ISOMsgField> isoMsgFields = isoMsg.getField();
		isoMsgFields.add(isoMessageFieldUtils.createProcessingCodeISOMsgField(LAST_CUSTOMER_TOKEN_PROCESSING_CODE));
		isoMsgFields.add(
				isoMessageFieldUtils.createCustomerDataField(request.getAccountName() + "|" + request.getMsisdn()));
		final Currency currency = currencyService.findCurrencyByNumericCode(request.getCurrencyCode());
		isoMsgFields.add(isoMessageFieldUtils.createCurrencyCodeField(currency.getAlphaCode()));
		isoMsgFields.add(isoMessageFieldUtils.createCustomerAccountNumberField(request.getAccountNumber2()));
	}

	private String extractToken(String value) {
		if (StringUtils.hasText(value)) {
			final String[] valueItems = value.split("\\|");
			return valueItems.length > 0 ? valueItems[0] : "";
		}
		return "";
	}

}
