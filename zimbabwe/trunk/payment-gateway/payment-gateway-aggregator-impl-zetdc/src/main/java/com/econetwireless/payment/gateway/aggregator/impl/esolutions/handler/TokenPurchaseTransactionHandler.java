package com.econetwireless.payment.gateway.aggregator.impl.esolutions.handler;

import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.ARREARS_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.CUSTOMER_DATA_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.FIXED_CHARGES_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.NARRATIVE_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.RESPONSE_CODE_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.TOKEN_PURCHASE_PROCESSING_CODE;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.TOKEN_VOUCHER_DATA_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.TRANSACTION_REFERENCE_FIELD;

import java.math.BigDecimal;
import java.util.List;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.entities.Currency;
import com.econetwireless.payment.gateway.service.currency.CurrencyService;

import vending.mcommerce.esolutions.co.zw.ISOMsg;
import vending.mcommerce.esolutions.co.zw.ISOMsgField;

public class TokenPurchaseTransactionHandler extends AbstractZtedcTransactionHandler {

	private CurrencyService currencyService;

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	@Override
	protected TransactionResponse generateTransactionResponse(ISOMsg isoMsg, TransactionRequest request) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		String tokenData = "";
		String fixedCharges = "";
		String arrears = "";
		transactionResponse.setAdditionalInfo1(request.getTerminalName().trim());
		for (final ISOMsgField isoMsgField : isoMsg.getField()) {
			if (isoMsgField.getNumber() == TOKEN_VOUCHER_DATA_FIELD) {
				tokenData = transactionResponse.getStatusReason();
			}
			if (isoMsgField.getNumber() == RESPONSE_CODE_FIELD) {
				transactionResponse.setStatusCode(isoMsgField.getValue());
			}
			if (isoMsgField.getNumber() == ARREARS_FIELD) {
				arrears = isoMsgField.getValue();
			}
			if (isoMsgField.getNumber() == CUSTOMER_DATA_FIELD) {
				String customerDataValue = isoMsgField.getValue();
				if (customerDataValue.length() > 40) {
					customerDataValue = customerDataValue.substring(0, 40);
				}
				transactionResponse.setTerminalName(customerDataValue);
			}
			if (isoMsgField.getNumber() == FIXED_CHARGES_FIELD) {
				fixedCharges = isoMsgField.getValue();
			}
			if (isoMsgField.getNumber() == TRANSACTION_REFERENCE_FIELD) {
				transactionResponse.setDestinationReference(isoMsgField.getValue());
			}
			if (isoMsgField.getNumber() == NARRATIVE_FIELD) {
				transactionResponse.setStatusReason(isoMsgField.getValue());
			}
		}
		transactionResponse.setAdditionalInfo5(tokenData + "|" + arrears + "|" + fixedCharges);
		return transactionResponse;
	}

	@Override
	protected void addSpecialisedFields(ISOMsg isoMsg, TransactionRequest request) {
		final List<ISOMsgField> isoMsgFields = isoMsg.getField();
		isoMsgFields.add(isoMessageFieldUtils.createProcessingCodeISOMsgField(TOKEN_PURCHASE_PROCESSING_CODE));
		isoMsgFields.add(isoMessageFieldUtils
				.creteTransactionAmountField(new BigDecimal(request.getAmount()).multiply(new BigDecimal(100))));
		isoMsgFields.add(
				isoMessageFieldUtils.createCustomerDataField(request.getAccountName() + "|" + request.getMsisdn()));
		final Currency currency = currencyService.findCurrencyByNumericCode(request.getCurrencyCode());
		isoMsgFields.add(isoMessageFieldUtils.createCurrencyCodeField(currency.getAlphaCode()));
		isoMsgFields.add(isoMessageFieldUtils.createCustomerAccountNumberField(request.getAccountNumber2()));
	}

}
