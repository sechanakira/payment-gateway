package com.econetwireless.payment.gateway.aggregator.impl.esolutions.handler;

import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.CURRENCY_CODE_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.NARRATIVE_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.RESPONSE_CODE_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.TRANSACTION_REFERENCE_FIELD;
import static com.econetwireless.payment.gateway.aggregator.impl.esolutions.ESolutionsConstants.VENDOR_BALANCE_FIELD;

import java.util.List;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.entities.Currency;
import com.econetwireless.payment.gateway.service.currency.CurrencyService;

import vending.mcommerce.esolutions.co.zw.ISOMsg;
import vending.mcommerce.esolutions.co.zw.ISOMsgField;

public class VendorBalanceEnquiryTransactionHandler extends AbstractZtedcTransactionHandler {

	private CurrencyService currencyService;

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	@Override
	protected TransactionResponse generateTransactionResponse(ISOMsg isoMsg, TransactionRequest request) {
		final TransactionResponse response = new TransactionResponse();
		for (final ISOMsgField isoMsgField : isoMsg.getField()) {
			if (isoMsgField.getNumber() == TRANSACTION_REFERENCE_FIELD) {
				response.setDestinationReference(isoMsgField.getValue());
			}
			if (isoMsgField.getNumber() == RESPONSE_CODE_FIELD) {
				response.setStatusCode(isoMsgField.getValue());
			}
			if (isoMsgField.getNumber() == CURRENCY_CODE_FIELD) {
				final Currency currency = currencyService.findCurrencyByAlphaCode(isoMsgField.getValue());
				response.setCurrencyCode(currency.getNumericCode());
			}
			if (isoMsgField.getNumber() == VENDOR_BALANCE_FIELD) {
				response.setBalance(isoMsgField.getValue());
			}
			if (isoMsgField.getNumber() == NARRATIVE_FIELD) {
				response.setStatusReason(isoMsgField.getValue());
			}
		}
		return response;
	}

	@Override
	protected void addSpecialisedFields(ISOMsg isoMsg, TransactionRequest request) {
		final List<ISOMsgField> isoMsgFields = isoMsg.getField();
		isoMsgFields.add(isoMessageFieldUtils.createProcessingCodeISOMsgField(request.getTransactionTypeCode()));

	}

}
