package com.econetwireless.payment.gateway.application.interceptor.api;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;

public interface Interceptor {
	
	InterceptorResponse intercept(TransactionRequest transactionRequest);
}
