package com.econetwireless.payment.gateway.application.interceptor.api;

import java.io.Serializable;

import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.entities.TransactionStatus;

public class InterceptorResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TransactionResponse transactionResponse;
	private TransactionStatus transactionStatus;

	public TransactionResponse getTransactionResponse() {
		return transactionResponse;
	}

	public void setTransactionResponse(TransactionResponse transactionResponse) {
		this.transactionResponse = transactionResponse;
	}

	public TransactionStatus getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(TransactionStatus transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

}
