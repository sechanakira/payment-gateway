package com.econetwireless.payment.gateway.core.api.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.core.executor.TransactionExecutor;

@RestController
@RequestMapping("/transaction")
public class GatewayRestApi {

	@Autowired
	private TransactionExecutor transactionExecutor;
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public TransactionResponse postTransaction(@RequestBody TransactionRequest transactionRequest){
		return transactionExecutor.execute(transactionRequest);
	}
}
