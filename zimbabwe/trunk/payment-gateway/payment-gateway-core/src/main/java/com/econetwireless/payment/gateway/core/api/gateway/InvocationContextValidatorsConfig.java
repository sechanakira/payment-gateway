package com.econetwireless.payment.gateway.core.api.gateway;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;

import com.econetwireless.payment.gateway.core.executor.validator.ChecksumTransactionInvocationContextValidator;
import com.econetwireless.payment.gateway.core.executor.validator.DuplicateTransactionInvocationContextValidator;
import com.econetwireless.payment.gateway.core.executor.validator.TransactionInvocationContextValidator;
import com.econetwireless.payment.gateway.service.transaction.TransactionService;

@Configuration
public class InvocationContextValidatorsConfig {

	@Bean
	@Profile("checksum-verification")
	@Order(1)
	public TransactionInvocationContextValidator checksumValidator(Environment environment,
			ResourceLoader resourceLoader) {
		final ChecksumTransactionInvocationContextValidator validator = new ChecksumTransactionInvocationContextValidator();
		validator.setKeyStoreLocation(
				resourceLoader.getResource(environment.getRequiredProperty("checksum.verification.keystore.location")));
		validator.setKeyStorePassword(environment.getRequiredProperty("checksum.verification.keystore.password"));
		validator.setKeyStoreProvider(environment.getRequiredProperty("checksum.verification.keystore.provider"));
		return validator;
	}

	@Bean
	@Order(2)
	public TransactionInvocationContextValidator duplicateTransactionValidator(TransactionService transactionService) {
		final DuplicateTransactionInvocationContextValidator validator = new DuplicateTransactionInvocationContextValidator();
		validator.setTransactionService(transactionService);
		return validator;
	}
}
