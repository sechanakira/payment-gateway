package com.econetwireless.payment.gateway.core.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.econetwireless.payment.gateway.config.dao.DaoConfiguration;
import com.econetwireless.payment.gateway.config.service.ServiceConfiguration;

public class GatewayApplicationInitializer extends
		AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[]{DaoConfiguration.class,ServiceConfiguration.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[]{GatewayConfig.class};
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/restapi/*" };
	}

}
