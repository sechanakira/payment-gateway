package com.econetwireless.payment.gateway.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import com.econetwireless.payment.gateway.core.executor.populator.ApplicationTransactionInvocationContextPopulator;
import com.econetwireless.payment.gateway.core.executor.populator.TransactionInvocationContextPopulator;
import com.econetwireless.payment.gateway.core.executor.populator.TransactionTypeTransactionInvocationContextPopulator;
import com.econetwireless.payment.gateway.core.executor.populator.VendorAggregatorMappingTransactionInvocationContextPopulator;
import com.econetwireless.payment.gateway.core.executor.populator.VendorApiAssignmentTransactionInvocationContextPopulator;
import com.econetwireless.payment.gateway.core.executor.populator.VendorTranTypeMappingTransactionInvocationContextPopulator;
import com.econetwireless.payment.gateway.core.executor.populator.VendorTransactionInvocationContextPopulator;
import com.econetwireless.payment.gateway.service.application.ApplicationService;
import com.econetwireless.payment.gateway.service.transationType.TransactionTypeService;
import com.econetwireless.payment.gateway.service.vendor.VendorService;
import com.econetwireless.payment.gateway.service.vendorAggregatorMapping.VendorAggregatorMappingService;
import com.econetwireless.payment.gateway.service.vendorTranTypeMapping.VendorTranTypeMappingService;

@Configuration
public class InvocationContextPopulatorsConfig {

	@Bean
	@Order(1)
	public TransactionInvocationContextPopulator applicationPopulator(ApplicationService applicationService) {
		ApplicationTransactionInvocationContextPopulator populator = new ApplicationTransactionInvocationContextPopulator();
		populator.setApplicationService(applicationService);
		return populator;
	}

	@Bean
	@Order(2)
	public TransactionInvocationContextPopulator vendorPopulator(VendorService vendorService) {
		VendorTransactionInvocationContextPopulator populator = new VendorTransactionInvocationContextPopulator();
		populator.setVendorService(vendorService);
		return populator;
	}

	@Bean
	@Order(3)
	public TransactionInvocationContextPopulator transactionTypePopulator(
			TransactionTypeService transationTypeService) {
		TransactionTypeTransactionInvocationContextPopulator populator = new TransactionTypeTransactionInvocationContextPopulator();
		populator.setTransationTypeService(transationTypeService);
		return populator;
	}

	@Bean
	@Order(4)
	public TransactionInvocationContextPopulator aggregatorPopulator(
			VendorAggregatorMappingService vendorAggregatorMappingService) {
		VendorAggregatorMappingTransactionInvocationContextPopulator populator = new VendorAggregatorMappingTransactionInvocationContextPopulator();
		populator.setVendorAggregatorMappingService(vendorAggregatorMappingService);
		return populator;
	}

	@Bean
	@Order(5)
	public TransactionInvocationContextPopulator apiAssignmentPopulator(VendorService vendorService) {
		VendorApiAssignmentTransactionInvocationContextPopulator populator = new VendorApiAssignmentTransactionInvocationContextPopulator();
		populator.setVendorService(vendorService);
		return populator;
	}

	@Bean
	@Order(6)
	public TransactionInvocationContextPopulator tranTypeMappingPopulator(
			VendorTranTypeMappingService vendorTranTypeMappingService) {
		VendorTranTypeMappingTransactionInvocationContextPopulator populator = new VendorTranTypeMappingTransactionInvocationContextPopulator();
		populator.setVendorTranTypeMappingService(vendorTranTypeMappingService);
		return populator;
	}

}
