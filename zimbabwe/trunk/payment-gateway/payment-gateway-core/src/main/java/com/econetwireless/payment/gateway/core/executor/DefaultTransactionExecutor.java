package com.econetwireless.payment.gateway.core.executor;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.econetwireless.common.strategies.service.ServiceLookup;
import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.application.interceptor.api.Interceptor;
import com.econetwireless.payment.gateway.application.interceptor.api.InterceptorResponse;
import com.econetwireless.payment.gateway.core.executor.populator.TransactionInvocationContextPopulator;
import com.econetwireless.payment.gateway.core.executor.validator.TransactionInvocationContextValidator;
import com.econetwireless.payment.gateway.entities.ApplicationInterceptor;
import com.econetwireless.payment.gateway.entities.Currency;
import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.entities.MonetaryAmount;
import com.econetwireless.payment.gateway.entities.StatusCodeMapping;
import com.econetwireless.payment.gateway.entities.Transaction;
import com.econetwireless.payment.gateway.service.currency.CurrencyService;
import com.econetwireless.payment.gateway.service.exception.ExceptionUtil;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;
import com.econetwireless.payment.gateway.service.transaction.TransactionService;

public class DefaultTransactionExecutor implements TransactionExecutor {

	private static final Logger logger = LoggerFactory.getLogger(DefaultTransactionExecutor.class);

	private Collection<TransactionInvocationContextPopulator> contextPopulators;

	private Collection<TransactionInvocationContextValidator> contextValidators;

	private TransactionHandlerInvoker transactionHandlerInvoker;

	private ServiceLookup serviceLookup;

	private TransactionService transactionService;

	private CurrencyService currencyService;

	public void setContextPopulators(Collection<TransactionInvocationContextPopulator> contextPopulators) {
		this.contextPopulators = contextPopulators;
	}

	public void setContextValidators(Collection<TransactionInvocationContextValidator> contextValidators) {
		this.contextValidators = contextValidators;
	}

	public void setServiceLookup(ServiceLookup serviceLookup) {
		this.serviceLookup = serviceLookup;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public void setCurrencyService(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	public void setTransactionHandlerInvoker(TransactionHandlerInvoker transactionHandlerInvoker) {
		this.transactionHandlerInvoker = transactionHandlerInvoker;
	}

	@Override
	public TransactionResponse execute(final TransactionRequest transactionRequest) {
		TransactionInvocationContext context = null;
		TransactionResponse transactionResponse = null;
		try {
			logger.debug("Received transaction request {}", transactionRequest);
			final TransactionInvocationContextBuilder contextBuilder = new TransactionInvocationContextBuilder(
					transactionRequest);
			contextBuilder.transactionDate(new Date());
			contextBuilder.statusDate(contextBuilder.getTransactionDate());
			contextPopulators.stream().forEachOrdered(populator -> populator.populate(contextBuilder));
			context = contextBuilder.build();
			final TransactionInvocationContext contextToValidate = context;
			contextValidators.stream().forEachOrdered(validator -> validator.validate(contextToValidate));
			final TransactionResponse interceptorResponse = invokeApplicationInterceptors(context);
			if (interceptorResponse != null) {
				logger.debug("Got an interceptor response {}.Using this as the transaction response");
				transactionResponse = interceptorResponse;
			} else {
				logger.debug("No interceptor response received.Invoking the target handler");
				transactionResponse = transactionHandlerInvoker.invoke(context);
			}
			context.setTransactionResponse(transactionResponse);
			mapStatusCodes(context);
			generateTransactionRecord(context);
			return transactionResponse;
		} catch (final GatewayApplicationException gwe) {
			logger.error("Encountered gateway error {}", ExceptionUtil.generateStackTrace(gwe));
			transactionResponse = buildErrorResponse(gwe.getStatusCode(), gwe.getMessage());

		} catch (final Exception ex) {
			logger.error("Encountered unxpected error {}", ExceptionUtil.generateStackTrace(ex));
			transactionResponse = buildErrorResponse("500", "Error processing your request.Please try again later");
		} finally {
			if (context != null) {
				context.setTransactionResponse(transactionResponse);
			}
		}
		return transactionResponse;
	}

	private void generateTransactionRecord(TransactionInvocationContext context) {
		logger.debug("Generating transaction record for request {}", context.getTransactionRequest());
		final Transaction transaction = new Transaction();
		transaction.setAggregator(context.getAggregator());
		final TransactionResponse transactionResponse = context.getTransactionResponse();
		transaction.setDestinationReference(transactionResponse.getDestinationReference());
		transaction.setMonetaryAmount(context.getMonetaryAmount());
		final TransactionRequest transactionRequest = context.getTransactionRequest();
		transaction.setMsisdn(transactionRequest.getMsisdn());
		transaction.setSourceReference(transactionRequest.getSourceReference());
		transaction.setGatewayReference(context.getGatewayReference());
		transaction.setStatusDate(context.getStatusDate());
		transaction.setTransactionDate(context.getTransactionDate());
		transaction.setTransactionStatus(transactionResponse.getTransactionStatus());
		transaction.setTransactionType(context.getTransactionType());
		transaction.setVendor(context.getVendor());
		transaction.setApplication(context.getApplication());
		transaction.setDestinationResponseCode(transactionResponse.getMappedStatusCode());
		transaction.setSourceResponseCode(transactionResponse.getStatusCode());
		transaction.setDestinationResponseMessage(transactionResponse.getStatusReason());
		transaction.setMsisdn2(context.getTransactionRequest().getMsisdn2());
		setMonetaryAmount(transaction, context);
		final Transaction savedTransaction = transactionService.addTransaction(transaction);
		logger.info("Successfully saved transaction {} for request {}", savedTransaction,
				context.getTransactionRequest());
		context.setTransaction(savedTransaction);
	}

	private void setMonetaryAmount(Transaction transaction, TransactionInvocationContext context) {
		transaction.setMonetaryAmount(new MonetaryAmount());
		if (!StringUtils.isEmpty(context.getTransactionRequest().getAmount())) {
			try {
				final BigDecimal amount = new BigDecimal(context.getTransactionRequest().getAmount());
				transaction.getMonetaryAmount().setAmount(amount);
			} catch (final NumberFormatException ex) {
				// ignore
			}
		}
		if (!StringUtils.isEmpty(context.getTransactionRequest().getCurrencyCode())) {
			final Currency currency = currencyService
					.findCurrencyByAlphaCode(context.getTransactionRequest().getCurrencyCode());
			transaction.getMonetaryAmount().setCurrency(currency);
		}
	}

	private void mapStatusCodes(TransactionInvocationContext context) {
		logger.debug("Mapping transaction response status code {}", context);
		final Collection<StatusCodeMapping> statusCodeMappings = context.getVendorTranTypeMapping()
				.getStatusCodeMappings();
		final TransactionResponse transactionResponse = context.getTransactionResponse();
		final String vendorStatusCode = transactionResponse.getStatusCode();
		final StatusCodeMapping relevantMapping = statusCodeMappings.stream()
				.filter(mapping -> mapping.getMappedCode().equalsIgnoreCase(vendorStatusCode)).findFirst().orElse(null);
		if (relevantMapping != null) {
			logger.debug("Found response code mapping {} for {}", relevantMapping, context);
			transactionResponse.setMappedStatusCode(relevantMapping.getApplicationStatusCode().getCode());
		} else {
			logger.debug("No mapping found for response code {}.Falling back to the returned code", context);
			transactionResponse.setMappedStatusCode(transactionResponse.getStatusCode());
		}
		logger.debug("Mapped response code {} to {} for request {}", context.getTransactionResponse().getStatusCode(),
				context.getTransactionResponse().getMappedStatusCode(), context.getTransactionRequest());
	}

	private TransactionResponse buildErrorResponse(String statusCode, String statusMessage) {
		final TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setStatusCode(statusCode);
		transactionResponse.setStatusReason(statusMessage);
		transactionResponse.setSuccess(false);
		return transactionResponse;
	}

	private TransactionResponse invokeApplicationInterceptors(TransactionInvocationContext context) {
		final Collection<ApplicationInterceptor> applicableInterceptors = context.getApplication().getInterceptors();
		return applicableInterceptors.stream()
				.filter(interceptor -> isApplicableInterceptor(context.getTransactionRequest().getDirection(),
						interceptor.getDirection()))
				.map(interceptor -> invokeApplicationInterceptor(interceptor, context.getTransactionRequest()))
				.filter(transactionResponse -> transactionResponse != null).findFirst().orElse(null);

	}

	@SuppressWarnings("unchecked")
	private TransactionResponse invokeApplicationInterceptor(ApplicationInterceptor applicationInterceptor,
			TransactionRequest transactionRequest) {
		final Interceptor interceptor = serviceLookup
				.lookup((Class<? extends Interceptor>) applicationInterceptor.getInterceptorClass(), true);
		final InterceptorResponse interceptorResponse = interceptor.intercept(transactionRequest);
		if (interceptorResponse != null) {
			final TransactionResponse transactionResponse = interceptorResponse.getTransactionResponse();
			transactionResponse.setTransactionStatus(interceptorResponse.getTransactionStatus());
			return transactionResponse;
		}
		return null;
	}

	private static boolean isApplicableInterceptor(Direction requestDirection, Direction interceptorDirection) {
		return requestDirection == interceptorDirection || interceptorDirection == Direction.BIDIRECTIONAL;
	}
}
