package com.econetwireless.payment.gateway.core.executor;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

public interface TransactionExecutor {

	public TransactionResponse execute(TransactionRequest transactionRequest);
	
}
