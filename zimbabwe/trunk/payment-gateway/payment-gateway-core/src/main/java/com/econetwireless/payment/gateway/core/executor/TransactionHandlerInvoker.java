package com.econetwireless.payment.gateway.core.executor;

import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

public interface TransactionHandlerInvoker {

	TransactionResponse invoke(TransactionInvocationContext invocationContext);

}