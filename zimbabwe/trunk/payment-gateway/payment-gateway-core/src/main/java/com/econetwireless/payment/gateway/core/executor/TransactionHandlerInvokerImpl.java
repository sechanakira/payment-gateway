package com.econetwireless.payment.gateway.core.executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.econetwireless.common.strategies.service.ServiceLookup;
import com.econetwireless.payment.gateway.aggregator.api.TransactionHandler;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

public class TransactionHandlerInvokerImpl implements TransactionHandlerInvoker {

	private ServiceLookup serviceLookup;

	public void setServiceLookup(ServiceLookup serviceLookup) {
		this.serviceLookup = serviceLookup;
	}

	private static final Logger logger = LoggerFactory.getLogger(TransactionHandlerInvokerImpl.class);

	@Override
	@SuppressWarnings("unchecked")
	public TransactionResponse invoke(final TransactionInvocationContext invocationContext) {
		logger.debug("Invoking handler for context {}", invocationContext);
		final TransactionHandler transactionHandler = serviceLookup.lookup(
				(Class<? extends TransactionHandler>) invocationContext.getVendorTranTypeMapping().getHandlerClass(),
				true);
		final TransactionResponse transactionResponse = transactionHandler
				.handleTransaction(invocationContext.getTransactionRequest(), invocationContext.getGatewayReference());
		logger.info("Transaction handler response for request {} is {}", invocationContext, transactionResponse);
		return transactionResponse;
	}

}
