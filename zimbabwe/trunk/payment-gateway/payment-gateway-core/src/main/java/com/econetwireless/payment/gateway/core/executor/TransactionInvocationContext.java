package com.econetwireless.payment.gateway.core.executor;

import java.io.Serializable;
import java.util.Date;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.entities.Aggregator;
import com.econetwireless.payment.gateway.entities.Application;
import com.econetwireless.payment.gateway.entities.MonetaryAmount;
import com.econetwireless.payment.gateway.entities.Transaction;
import com.econetwireless.payment.gateway.entities.TransactionType;
import com.econetwireless.payment.gateway.entities.Vendor;
import com.econetwireless.payment.gateway.entities.VendorApiAssignment;
import com.econetwireless.payment.gateway.entities.VendorTranTypeMapping;

public class TransactionInvocationContext implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private final TransactionRequest transactionRequest;
	private String gatewayReference;
	private TransactionType transactionType;
	private Vendor vendor;
	private Aggregator aggregator;
	private Application application;
	private VendorTranTypeMapping vendorTranTypeMapping;
	private VendorApiAssignment vendorApiAssignment;
	private MonetaryAmount monetaryAmount;
	private Date statusDate;
	private Date transactionDate;
	private TransactionResponse transactionResponse;
	private Transaction transaction;

	public TransactionInvocationContext(final TransactionRequest transactionRequest) {
		this.transactionRequest = transactionRequest;
	}

	public TransactionRequest getTransactionRequest() {
		return transactionRequest;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(final TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(final Vendor vendor) {
		this.vendor = vendor;
	}

	public Aggregator getAggregator() {
		return aggregator;
	}

	public void setAggregator(final Aggregator aggregator) {
		this.aggregator = aggregator;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(final Application application) {
		this.application = application;
	}

	public VendorTranTypeMapping getVendorTranTypeMapping() {
		return vendorTranTypeMapping;
	}

	public void setVendorTranTypeMapping(final VendorTranTypeMapping vendorTranTypeMapping) {
		this.vendorTranTypeMapping = vendorTranTypeMapping;
	}

	public MonetaryAmount getMonetaryAmount() {
		return monetaryAmount;
	}

	public void setMonetaryAmount(final MonetaryAmount monetaryAmount) {
		this.monetaryAmount = monetaryAmount;
	}

	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(final Date statusDate) {
		this.statusDate = statusDate;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(final Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getGatewayReference() {
		return gatewayReference;
	}

	public void setGatewayReference(final String gatewayReference) {
		this.gatewayReference = gatewayReference;
	}

	public VendorApiAssignment getVendorApiAssignment() {
		return vendorApiAssignment;
	}

	public void setVendorApiAssignment(VendorApiAssignment vendorApiAssignment) {
		this.vendorApiAssignment = vendorApiAssignment;
	}

	public TransactionResponse getTransactionResponse() {
		return transactionResponse;
	}

	public void setTransactionResponse(TransactionResponse transactionResponse) {
		this.transactionResponse = transactionResponse;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	@Override
	public String toString() {
		return "TransactionInvocationContext [transactionRequest=" + transactionRequest + ", gatewayReference="
				+ gatewayReference + ", transactionType=" + transactionType + ", vendor=" + vendor + ", aggregator="
				+ aggregator + ", application=" + application + ", vendorTranTypeMapping=" + vendorTranTypeMapping
				+ ", vendorApiAssignment=" + vendorApiAssignment + ", monetaryAmount=" + monetaryAmount
				+ ", statusDate=" + statusDate + ", transactionDate=" + transactionDate + ", transactionResponse="
				+ transactionResponse + ", transaction=" + transaction + "]";
	}

}
