package com.econetwireless.payment.gateway.core.executor;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.entities.Aggregator;
import com.econetwireless.payment.gateway.entities.Application;
import com.econetwireless.payment.gateway.entities.MonetaryAmount;
import com.econetwireless.payment.gateway.entities.TransactionType;
import com.econetwireless.payment.gateway.entities.Vendor;
import com.econetwireless.payment.gateway.entities.VendorApiAssignment;
import com.econetwireless.payment.gateway.entities.VendorTranTypeMapping;

public class TransactionInvocationContextBuilder {

	private final TransactionInvocationContext invocationContext;

	public TransactionInvocationContextBuilder(TransactionRequest transactionRequest) {
		invocationContext = new TransactionInvocationContext(transactionRequest);
	}

	public TransactionRequest getTransactionRequest() {
		return invocationContext.getTransactionRequest();
	}

	public TransactionInvocationContextBuilder transactionType(TransactionType transactionType) {
		invocationContext.setTransactionType(transactionType);
		return this;
	}

	public TransactionType getTransactionType() {
		return invocationContext.getTransactionType();
	}

	public TransactionInvocationContextBuilder vendor(Vendor vendor) {
		invocationContext.setVendor(vendor);
		return this;
	}

	public Vendor getVendor() {
		return invocationContext.getVendor();
	}

	public TransactionInvocationContextBuilder vendorTranTypeMapping(VendorTranTypeMapping vendorTranTypeMapping) {
		invocationContext.setVendorTranTypeMapping(vendorTranTypeMapping);
		return this;
	}

	public TransactionInvocationContextBuilder transactionDate(Date transactionDate) {
		invocationContext.setTransactionDate(transactionDate);
		return this;
	}

	public TransactionInvocationContextBuilder statusDate(Date statusDate) {
		invocationContext.setStatusDate(statusDate);
		return this;
	}

	public TransactionInvocationContextBuilder monetaryAmount(MonetaryAmount amount) {
		invocationContext.setMonetaryAmount(amount);
		return this;
	}

	public TransactionInvocationContextBuilder application(Application application) {
		invocationContext.setApplication(application);
		return this;
	}

	public Application getApplication() {
		return invocationContext.getApplication();
	}

	public TransactionInvocationContextBuilder aggregator(Aggregator aggregator) {
		invocationContext.setAggregator(aggregator);
		return this;
	}

	public Aggregator getAggregator() {
		return invocationContext.getAggregator();
	}

	public TransactionInvocationContextBuilder vendorApiAssignment(VendorApiAssignment vendorApiAssignment) {
		invocationContext.setVendorApiAssignment(vendorApiAssignment);
		return this;
	}

	public TransactionInvocationContext build() {
		invocationContext.setGatewayReference(generateGatewayReference());
		return invocationContext;
	}

	private String generateGatewayReference() {
		final StringBuilder gatewayReferenceBuilder = new StringBuilder();
		gatewayReferenceBuilder.append(String.format("%02d", invocationContext.getApplication().getId()));
		gatewayReferenceBuilder.append(String.format("%04d", invocationContext.getVendor().getId()));
		gatewayReferenceBuilder.append(String.format("%02d", invocationContext.getAggregator().getId()));
		gatewayReferenceBuilder.append(String.format("%02d", invocationContext.getTransactionType().getId()));
		gatewayReferenceBuilder
				.append(new SimpleDateFormat("ddMMyyHHmmssSSS").format(invocationContext.getTransactionDate()));
		return gatewayReferenceBuilder.toString();
	}

	public Date getTransactionDate() {
		return invocationContext.getTransactionDate();
	}
}
