package com.econetwireless.payment.gateway.core.executor.populator;

import com.econetwireless.payment.gateway.core.executor.TransactionInvocationContextBuilder;

public interface TransactionInvocationContextPopulator {

	void populate(TransactionInvocationContextBuilder invocationContextBuilder);;

}
