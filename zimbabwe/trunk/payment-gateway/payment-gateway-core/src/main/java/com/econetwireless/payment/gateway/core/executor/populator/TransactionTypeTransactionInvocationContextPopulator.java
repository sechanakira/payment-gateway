package com.econetwireless.payment.gateway.core.executor.populator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.core.executor.TransactionInvocationContextBuilder;
import com.econetwireless.payment.gateway.entities.TransactionType;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;
import com.econetwireless.payment.gateway.service.transationType.TransactionTypeService;

public class TransactionTypeTransactionInvocationContextPopulator implements TransactionInvocationContextPopulator {

	private TransactionTypeService transationTypeService;

	private static final Logger logger = LoggerFactory
			.getLogger(TransactionTypeTransactionInvocationContextPopulator.class);

	public void setTransationTypeService(TransactionTypeService transationTypeService) {
		this.transationTypeService = transationTypeService;
	}

	@Override
	public void populate(TransactionInvocationContextBuilder invocationContextBuilder) {
		Assert.state(invocationContextBuilder.getApplication() != null,
				"Appliction not found in transaction invocation context");
		final String transactionTypeCode = invocationContextBuilder.getTransactionRequest().getTransactionTypeCode();
		if (StringUtils.isEmpty(transactionTypeCode)) {
			logger.debug("Application code not provided for request {}",
					invocationContextBuilder.getTransactionRequest());
			throw new GatewayApplicationException("Transaction type code required", "400");
		}
		final TransactionType transactionType = transationTypeService.findTransactionTypeByApplicationIdAndCode(
				invocationContextBuilder.getApplication().getId(), transactionTypeCode);
		if (transactionType == null) {
			logger.debug("Transaction type not found for request {}", invocationContextBuilder.getTransactionRequest());
			throw new GatewayApplicationException("Unknown transaction type " + transactionTypeCode, "404");
		}
		if (!transactionType.getActive()) {
			logger.debug("Requested transaction type {} inactive {}", invocationContextBuilder.getTransactionRequest(),
					transactionType);
			throw new GatewayApplicationException("Transaction type " + transactionTypeCode + " is not active", "406");
		}
		invocationContextBuilder.transactionType(transactionType);
	}

}
