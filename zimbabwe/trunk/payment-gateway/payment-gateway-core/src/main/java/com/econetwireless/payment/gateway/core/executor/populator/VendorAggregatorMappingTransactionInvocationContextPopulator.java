package com.econetwireless.payment.gateway.core.executor.populator;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.core.executor.TransactionInvocationContextBuilder;
import com.econetwireless.payment.gateway.entities.VendorAggregatorMapping;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;
import com.econetwireless.payment.gateway.service.vendorAggregatorMapping.VendorAggregatorMappingService;

public class VendorAggregatorMappingTransactionInvocationContextPopulator
		implements TransactionInvocationContextPopulator {

	private VendorAggregatorMappingService vendorAggregatorMappingService;

	private static final Logger logger = LoggerFactory
			.getLogger(VendorAggregatorMappingTransactionInvocationContextPopulator.class);

	@Override
	public void populate(TransactionInvocationContextBuilder invocationContextBuilder) {
		Assert.state(invocationContextBuilder.getVendor() != null,
				"Vendor not found in transaction invocation context");
		final List<VendorAggregatorMapping> vendorAggregatorMappings = vendorAggregatorMappingService
				.findVendorAggregatorMappingByVendorAndStatus(invocationContextBuilder.getVendor().getId(), true);
		if (vendorAggregatorMappings.isEmpty()) {
			logger.debug("No active vendor aggregator mappings found for request {}",
					invocationContextBuilder.getTransactionRequest());
			throw new GatewayApplicationException("Invalid vendor profile", "401");
		}
		invocationContextBuilder.aggregator(vendorAggregatorMappings.get(0).getAggregator());
	}

	public void setVendorAggregatorMappingService(VendorAggregatorMappingService vendorAggregatorMappingService) {
		this.vendorAggregatorMappingService = vendorAggregatorMappingService;
	}

}
