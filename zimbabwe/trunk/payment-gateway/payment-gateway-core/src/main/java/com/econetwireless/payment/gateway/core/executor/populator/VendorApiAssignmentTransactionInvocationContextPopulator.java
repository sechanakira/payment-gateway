package com.econetwireless.payment.gateway.core.executor.populator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.core.executor.TransactionInvocationContextBuilder;
import com.econetwireless.payment.gateway.entities.Application;
import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.entities.Vendor;
import com.econetwireless.payment.gateway.entities.VendorApiAssignment;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;
import com.econetwireless.payment.gateway.service.vendor.VendorService;

public class VendorApiAssignmentTransactionInvocationContextPopulator implements TransactionInvocationContextPopulator {

	private VendorService vendorService;

	private static final Logger logger = LoggerFactory
			.getLogger(VendorApiAssignmentTransactionInvocationContextPopulator.class);

	public void setVendorService(VendorService vendorService) {
		this.vendorService = vendorService;
	}

	@Override
	public void populate(TransactionInvocationContextBuilder invocationContextBuilder) {
		if (invocationContextBuilder.getTransactionRequest().getDirection() == Direction.INBOUND) {
			final Vendor vendor = invocationContextBuilder.getVendor();
			Assert.state(vendor != null, "Vendor not found in transcation invocation context");
			final Application application = invocationContextBuilder.getApplication();
			Assert.state(application != null, "Application not found in invocation context");
			final VendorApiAssignment vendorApiAssignment = vendorService
					.findVendorApiAssignmentByVendorApplicationAndApiType(vendor.getId(), application.getId(),
							invocationContextBuilder.getTransactionRequest().getChannel());
			final boolean hasPermissions = vendorApiAssignment != null && vendorApiAssignment.getActive()
					&& vendorApiAssignment.getApiDefinition().getActive();
			if (!hasPermissions) {
				logger.debug("Vendor has no rights to requested API {}",
						invocationContextBuilder.getTransactionRequest());
				throw new GatewayApplicationException("You do not have permissions to use this API", "403");
			}
			invocationContextBuilder.vendorApiAssignment(vendorApiAssignment);
		}
	}

}
