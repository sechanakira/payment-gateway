package com.econetwireless.payment.gateway.core.executor.populator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.core.executor.TransactionInvocationContextBuilder;
import com.econetwireless.payment.gateway.entities.VendorTranTypeMapping;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;
import com.econetwireless.payment.gateway.service.vendorTranTypeMapping.VendorTranTypeMappingService;

public class VendorTranTypeMappingTransactionInvocationContextPopulator
		implements TransactionInvocationContextPopulator {

	private VendorTranTypeMappingService vendorTranTypeMappingService;

	private static final Logger logger = LoggerFactory
			.getLogger(VendorTranTypeMappingTransactionInvocationContextPopulator.class);

	public void setVendorTranTypeMappingService(VendorTranTypeMappingService vendorTranTypeMappingService) {
		this.vendorTranTypeMappingService = vendorTranTypeMappingService;
	}

	@Override
	public void populate(TransactionInvocationContextBuilder invocationContextBuilder) {
		Assert.state(invocationContextBuilder.getAggregator() != null,
				"Aggregator not found in transaction invocation context");
		Assert.state(invocationContextBuilder.getTransactionType() != null,
				"Transaction type not found in transaction invocation context");
		Assert.state(invocationContextBuilder.getVendor() != null,
				"Vendor not found in transaction invocation context");
		final VendorTranTypeMapping vendorTranTypeMapping = vendorTranTypeMappingService
				.findVendorTranTypeMappingByVendorAggregatorAndTranType(invocationContextBuilder.getVendor().getId(),
						invocationContextBuilder.getAggregator().getId(),
						invocationContextBuilder.getTransactionType().getId());
		if (vendorTranTypeMapping == null || !vendorTranTypeMapping.getActive()) {
			logger.debug("Active vendor transaction type mapping not found for request {}",
					invocationContextBuilder.getTransactionRequest());
			throw new GatewayApplicationException("You do not have permissions to access requested service", "");
		}
		invocationContextBuilder.vendorTranTypeMapping(vendorTranTypeMapping);

	}

}
