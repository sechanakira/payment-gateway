package com.econetwireless.payment.gateway.core.executor.populator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.core.executor.TransactionInvocationContextBuilder;
import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.entities.Vendor;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;
import com.econetwireless.payment.gateway.service.vendor.VendorService;

public class VendorTransactionInvocationContextPopulator implements TransactionInvocationContextPopulator {

	private VendorService vendorService;

	private static final Logger logger = LoggerFactory.getLogger(VendorTransactionInvocationContextPopulator.class);

	public void setVendorService(VendorService vendorService) {
		this.vendorService = vendorService;
	}

	@Override
	public void populate(TransactionInvocationContextBuilder invocationContextBuilder) {
		final TransactionRequest transactionRequest = invocationContextBuilder.getTransactionRequest();
		final String vendorCode = transactionRequest.getVendorCode();
		if (StringUtils.isEmpty(vendorCode)) {
			logger.debug("Application code not supplied for request {}", transactionRequest);
			throw new GatewayApplicationException("Vendor code is required", "400");
		}
		final Vendor vendor = vendorService.findVendorByVendorCode(vendorCode);
		if (vendor == null) {
			logger.debug("Vendor not found for request {}", transactionRequest);
			throw new GatewayApplicationException("Invalid vendor details", "401");
		}
		if (!vendor.getActive()) {
			logger.debug("Requesting vendor {} not active {}", transactionRequest, vendor);
			throw new GatewayApplicationException("Your vendor profile was deactivated", "402");
		}
		if (transactionRequest.getDirection() == Direction.INBOUND
				&& !vendor.getApiKey().equalsIgnoreCase(transactionRequest.getVendorApiKey())) {
			logger.debug("Authentication failure for vendor {}", transactionRequest);
			throw new GatewayApplicationException("Invalid vendor details", "401");
		}
		invocationContextBuilder.vendor(vendor);
	}

}
