package com.econetwireless.payment.gateway.core.executor.validator;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.Signature;
import java.security.cert.X509Certificate;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.core.executor.TransactionInvocationContext;
import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.entities.Vendor;
import com.econetwireless.payment.gateway.service.exception.ExceptionUtil;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;
import com.econetwireless.payment.gateway.service.exception.GatewayRuntimeException;

public class ChecksumTransactionInvocationContextValidator implements TransactionInvocationContextValidator {

	private String keyStoreProvider;

	private Resource keyStoreLocation;

	private String keyStorePassword;

	private static final Logger logger = LoggerFactory.getLogger(ChecksumTransactionInvocationContextValidator.class);

	public void setKeyStoreProvider(String keyStoreProvider) {
		this.keyStoreProvider = keyStoreProvider;
	}

	public void setKeyStoreLocation(Resource keyStoreLocation) {
		this.keyStoreLocation = keyStoreLocation;
	}

	public void setKeyStorePassword(String keyStorePassword) {
		this.keyStorePassword = keyStorePassword;
	}

	@Override
	public void validate(TransactionInvocationContext transactionInvocationContext) {
		if (transactionInvocationContext.getTransactionRequest().getDirection() == Direction.INBOUND) {
			Assert.state(transactionInvocationContext.getVendor() != null,
					"Vendor not found in transaction invocation context");
			final Vendor vendor = transactionInvocationContext.getVendor();
			try {
				logger.debug("Validating checksum for request {} using keystoreProvider {} , keyStoreLocation {}",
						transactionInvocationContext.getTransactionRequest(), keyStorePassword, keyStoreLocation);
				final KeyStore keyStore = KeyStore.getInstance(keyStoreProvider);
				final InputStream keyStoreInputStream = keyStoreLocation.getInputStream();
				keyStore.load(keyStoreInputStream, keyStorePassword.toCharArray());
				keyStoreInputStream.close();
				final X509Certificate cert = (X509Certificate) keyStore.getCertificate(vendor.getCode().toLowerCase());
				final Signature signature = Signature.getInstance(cert.getSigAlgName());
				signature.initVerify(cert.getPublicKey());
				final StringBuilder data = new StringBuilder();
				final TransactionRequest transactionRequest = transactionInvocationContext.getTransactionRequest();
				data.append(transactionRequest.getVendorCode());
				data.append(transactionRequest.getVendorApiKey());
				data.append(transactionRequest.getTransactionTypeCode());
				data.append(transactionRequest.getSourceReference());
				signature.update(data.toString().getBytes());
				try {
					final boolean matches = signature.verify(Base64.decodeBase64(transactionRequest.getChecksum()));
					if (!matches) {
						logger.info("Checksum verification failed for vendor {} using signature algorithm {}",
								vendor.getCode(), cert.getSigAlgName());
						throw new GatewayApplicationException("Invalid Checksum.Data corrupted", "408");
					}
				} catch (final Exception ex) {
					logger.info("Failed to verify checksum {} probably due to checksum data format.Error {}",
							vendor.getCode(), ExceptionUtil.generateStackTrace(ex));
					throw new GatewayApplicationException("Invalid Checksum.Data corrupted", "408");
				}

				logger.info("Successfully verified checksum for vendor {} using signature algorithm {}",
						vendor.getCode(), cert.getSigAlgName());
			} catch (final GatewayApplicationException gwe) {
				throw gwe;
			} catch (final Exception ex) {
				logger.error(
						"Failed to verify vendor checksum {} probably due to keystore configuration problem.Error {}",
						vendor.getCode(), ExceptionUtil.generateStackTrace(ex));
				throw new GatewayRuntimeException(ex);
			}
		}
	}

}
