package com.econetwireless.payment.gateway.core.executor.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.core.executor.TransactionInvocationContext;
import com.econetwireless.payment.gateway.entities.Transaction;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;
import com.econetwireless.payment.gateway.service.transaction.TransactionService;

public class DuplicateTransactionInvocationContextValidator implements TransactionInvocationContextValidator {

	private TransactionService transactionService;

	private static final Logger logger = LoggerFactory.getLogger(DuplicateTransactionInvocationContextValidator.class);

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	@Override
	public void validate(TransactionInvocationContext transactionInvocationContext) {
		TransactionRequest transactionRequest = transactionInvocationContext.getTransactionRequest();
		logger.debug("Checking for duplicate transaction for request {}" , transactionRequest);
		Assert.state(transactionInvocationContext.getVendor() != null,
				"Vendor not found in transaction invocation context");
		Assert.state(transactionInvocationContext.getAggregator() != null,
				"Aggregator not found in transaction invocation context");
		final Transaction transactionBySourceRef = transactionService.findTransactionByVendorAggregatorAndReference(
				transactionInvocationContext.getVendor().getId(), transactionInvocationContext.getAggregator().getId(),
				transactionRequest.getSourceReference(), true);
		if (transactionBySourceRef != null) {
			logger.info("Found a duplicate transaction {} for request {}" , transactionBySourceRef , transactionRequest);
			throw new GatewayApplicationException("Duplicate transaction reference : " + transactionRequest.getSourceReference(), "410");
		}
	}

}
