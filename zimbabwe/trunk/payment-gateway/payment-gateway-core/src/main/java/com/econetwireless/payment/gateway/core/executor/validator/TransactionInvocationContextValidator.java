package com.econetwireless.payment.gateway.core.executor.validator;

import com.econetwireless.payment.gateway.core.executor.TransactionInvocationContext;

public interface TransactionInvocationContextValidator {

	void validate(TransactionInvocationContext transactionInvocationContext);
	
}
