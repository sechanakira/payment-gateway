package com.econetwireless.payment.gateway.core.executor;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.entities.*;
import com.econetwireless.payment.gateway.service.application.ApplicationService;
import com.econetwireless.payment.gateway.service.statusCode.GatewayConstants;
import com.econetwireless.payment.gateway.service.statusCode.GatewayStatusCodeResolver;
import com.econetwireless.payment.gateway.service.transationType.TransactionTypeService;
import com.econetwireless.payment.gateway.service.vendor.VendorService;
import com.econetwireless.payment.gateway.service.vendorAggregatorMapping.VendorAggregatorMappingService;
import com.econetwireless.payment.gateway.service.vendorTranTypeMapping.VendorTranTypeMappingService;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@Ignore
public class DefaultTransactionExecutorTest {

	@InjectMocks
	private DefaultTransactionExecutor transactionExecutor = new  DefaultTransactionExecutor();
	
	@Mock
	private GatewayStatusCodeResolver statusCodesResolver;
	
	@Mock
	private VendorService vendorService;
	
	@Mock
	private ApplicationService applicationService;
	
	@Mock
	private VendorAggregatorMappingService vendorAggregatorMappingService;
	
	@Mock
	private TransactionTypeService transactionTypeService;
	
	@Mock
	private VendorTranTypeMappingService vendorTranTypeMappingService;
	
	@Mock
	private TransactionHandlerInvoker transactionHandlerInvoker;
	
    @Test
    @Ignore
    public void shouldReturnErrorCodeWhenVendorCodeIsEmpty(){
		final TransactionRequest transactionRequest = new TransactionRequest();
		final GatewayStatusCode gatewayStatusCode = new GatewayStatusCode();
		final String statusMessage = "Vendor code required";
		gatewayStatusCode.setCode("400");
		gatewayStatusCode.setMessage(statusMessage);
		when(statusCodesResolver.resolve(GatewayConstants.StatusCodeKeys.MISSING_PARAMETER, statusMessage)).thenReturn(gatewayStatusCode);
		final TransactionResponse transactionResponse = transactionExecutor.execute(transactionRequest);
		verify(statusCodesResolver , times(1)).resolve(GatewayConstants.StatusCodeKeys.MISSING_PARAMETER, statusMessage);
		Assert.assertEquals(gatewayStatusCode.getCode(), transactionResponse.getStatusCode());
		Assert.assertEquals(gatewayStatusCode.getMessage(), transactionResponse.getStatusReason());
		Assert.assertNull(transactionResponse.getDestinationReference());
	}
	
	@Test
	public void shouldReturnErrorCodeWhenTransactionTypeCodeIsEmpty(){
		final TransactionRequest transactionRequest = new TransactionRequest();
		transactionRequest.setVendorCode("ver123");
		final GatewayStatusCode gatewayStatusCode = new GatewayStatusCode();
		String statusMessage = "Transaction type code required";
		String statusCode = "400";
		gatewayStatusCode.setCode(statusCode);
		gatewayStatusCode.setMessage(statusMessage);
		when(statusCodesResolver.resolve(GatewayConstants.StatusCodeKeys.MISSING_PARAMETER , statusMessage)).thenReturn(gatewayStatusCode);
		final TransactionResponse transactionResponse = transactionExecutor.execute(transactionRequest);
		verify(statusCodesResolver,times(1)).resolve(GatewayConstants.StatusCodeKeys.MISSING_PARAMETER, statusMessage);
		Assert.assertEquals(gatewayStatusCode.getCode(), transactionResponse.getStatusCode());
		Assert.assertEquals(gatewayStatusCode.getMessage(), transactionResponse.getStatusReason());
		Assert.assertNull(transactionResponse.getDestinationReference());
	}
	
	@Test
	public void shouldReturnErrorCodeWhenDirectionIsNull(){
		final TransactionRequest transactionRequest = new TransactionRequest();
		transactionRequest.setVendorCode("ver123");
		transactionRequest.setTransactionTypeCode("txn123");
		transactionRequest.setApplicationCode("ecocash");
		transactionRequest.setVendorApiKey("abcdefghijk");
		final GatewayStatusCode gatewayStatusCode = new GatewayStatusCode();
		String statusMessage = "Transaction direction required";
		gatewayStatusCode.setCode("400");
		gatewayStatusCode.setMessage(statusMessage);
		when(statusCodesResolver.resolve(GatewayConstants.StatusCodeKeys.MISSING_PARAMETER, statusMessage)).thenReturn(gatewayStatusCode);
		final TransactionResponse transactionResponse = transactionExecutor.execute(transactionRequest);
		verify(statusCodesResolver,times(1)).resolve(GatewayConstants.StatusCodeKeys.MISSING_PARAMETER, statusMessage);
		Assert.assertEquals(gatewayStatusCode.getCode(), transactionResponse.getStatusCode());
		Assert.assertEquals(gatewayStatusCode.getMessage(), transactionResponse.getStatusReason());
		Assert.assertNull(transactionResponse.getDestinationReference());
	}
	
	@Test
	public void shouldReturnErrorCodeWhenVendorProfileDoesNotExist(){
	   final TransactionRequest transactionRequest = new TransactionRequest();
	   transactionRequest.setVendorCode("ver123");
	   transactionRequest.setVendorApiKey("abcdefghijklmnopqrs");
	   transactionRequest.setTransactionTypeCode("txn123");
	   transactionRequest.setDirection(Direction.INBOUND);
	   transactionRequest.setApplicationCode("ecocash");
	   final GatewayStatusCode gatewayStatusCode = new GatewayStatusCode();
	   gatewayStatusCode.setCode("401");
	   final String statusMessage = "Invalid vendor details";
	   gatewayStatusCode.setMessage(statusMessage);
	   when(vendorService.findVendorByVendorCode(transactionRequest.getVendorCode())).thenReturn(null);
	   when(statusCodesResolver.resolve(GatewayConstants.StatusCodeKeys.INVALID_VENDOR_PROFILE, statusMessage)).thenReturn(gatewayStatusCode);
	   final TransactionResponse transactionResponse = transactionExecutor.execute(transactionRequest);
	   verify(vendorService,times(1)).findVendorByVendorCode(transactionRequest.getVendorCode());
	   verify(statusCodesResolver , times(1)).resolve(GatewayConstants.StatusCodeKeys.INVALID_VENDOR_PROFILE, statusMessage);
	   Assert.assertEquals(gatewayStatusCode.getCode(), transactionResponse.getStatusCode());
	   Assert.assertEquals(gatewayStatusCode.getMessage(), transactionResponse.getStatusReason());
	   Assert.assertNull(transactionResponse.getDestinationReference());
	}
	
	
	@Test
	public void shouldReturnErrorCodeWhenVendorProfileIsInActive(){
		final TransactionRequest transactionRequest = new TransactionRequest();
		transactionRequest.setVendorCode("ver123");
		transactionRequest.setVendorApiKey("abcdefghijklmnopqrs");
		transactionRequest.setTransactionTypeCode("txn123");
		transactionRequest.setDirection(Direction.INBOUND);
		transactionRequest.setApplicationCode("ecocash");
		transactionRequest.setVendorApiKey("abcdefghijks");
		final GatewayStatusCode gatewayStatusCode = new GatewayStatusCode();
		gatewayStatusCode.setCode("402");
		final String statusMessage = "Your vendor profile was deactivated";
		gatewayStatusCode.setMessage(statusMessage);
		Vendor vendorResult = new Vendor();
		vendorResult.setActive(false);
		vendorResult.setApiKey(transactionRequest.getVendorApiKey());
		when(vendorService.findVendorByVendorCode(transactionRequest.getVendorCode())).thenReturn(vendorResult);
		when(statusCodesResolver.resolve(GatewayConstants.StatusCodeKeys.VENDOR_DEACTIVATED, statusMessage)).thenReturn(gatewayStatusCode);
		final TransactionResponse transactionResponse = transactionExecutor.execute(transactionRequest);
		verify(vendorService).findVendorByVendorCode(transactionRequest.getVendorCode());
		verify(statusCodesResolver).resolve(GatewayConstants.StatusCodeKeys.VENDOR_DEACTIVATED, statusMessage);
		Assert.assertEquals(gatewayStatusCode.getCode(), transactionResponse.getStatusCode());
		Assert.assertEquals(gatewayStatusCode.getMessage(), transactionResponse.getStatusReason());
		Assert.assertNull(transactionResponse.getDestinationReference());
	}
	
	@Test
	public void shouldReturnErrorCodeWhenApplicationCodeIsEmpty(){
		final TransactionRequest transactionRequest = new TransactionRequest();
		transactionRequest.setVendorCode("ver123");
		transactionRequest.setVendorApiKey("abcdefghijklmnopqrs");
		transactionRequest.setTransactionTypeCode("txn123");
		transactionRequest.setDirection(Direction.INBOUND);
		final GatewayStatusCode gatewayStatusCode = new GatewayStatusCode();
		gatewayStatusCode.setCode("400");
		final String statusMessage = "Application code required";
		gatewayStatusCode.setMessage(statusMessage);
		when(statusCodesResolver.resolve(GatewayConstants.StatusCodeKeys.MISSING_PARAMETER, statusMessage)).thenReturn(gatewayStatusCode);
		final TransactionResponse transactionResponse = transactionExecutor.execute(transactionRequest);
		verify(statusCodesResolver).resolve(GatewayConstants.StatusCodeKeys.MISSING_PARAMETER, statusMessage);
		Assert.assertEquals(gatewayStatusCode.getCode(), transactionResponse.getStatusCode());
		Assert.assertEquals(gatewayStatusCode.getMessage(), transactionResponse.getStatusReason());
		Assert.assertNull(transactionResponse.getDestinationReference());
	}
	
	@Test
	public void shouldReturnErrorCodeWhenApplicationDoesNotExist(){
		final TransactionRequest transactionRequest = new TransactionRequest();
		transactionRequest.setVendorCode("ver123");
		transactionRequest.setTransactionTypeCode("txn123");
		transactionRequest.setDirection(Direction.INBOUND);
		transactionRequest.setApplicationCode("ecocash");
		transactionRequest.setVendorApiKey("abcdefghijklmnopqrs");
		final Vendor vendorResult = new Vendor();
		vendorResult.setCode(transactionRequest.getVendorCode());
		vendorResult.setActive(true);
		vendorResult.setApiKey(transactionRequest.getVendorApiKey());
		when(vendorService.findVendorByVendorCode(transactionRequest.getVendorCode())).thenReturn(vendorResult);
		when(applicationService.findApplicationByCode(transactionRequest.getApplicationCode())).thenReturn(null);
		final GatewayStatusCode gatewayStatusCode = new GatewayStatusCode();
		gatewayStatusCode.setCode("407");
		final String statusMessage = "Unknown application";
		gatewayStatusCode.setMessage(statusMessage);
		when(statusCodesResolver.resolve(GatewayConstants.StatusCodeKeys.UNKNOWN_APPLICATION, statusMessage)).thenReturn(gatewayStatusCode);
		final TransactionResponse transactionResponse = transactionExecutor.execute(transactionRequest);
		verify(statusCodesResolver).resolve(GatewayConstants.StatusCodeKeys.UNKNOWN_APPLICATION, statusMessage);
		Assert.assertEquals(gatewayStatusCode.getCode(), transactionResponse.getStatusCode());
		Assert.assertEquals(gatewayStatusCode.getMessage(), transactionResponse.getStatusReason());
		Assert.assertNull(transactionResponse.getDestinationReference());
	}
	
	@Test
	public void shouldReturnErrorCodeWhenApplicationIsDeactivated(){
		final TransactionRequest transactionRequest = new TransactionRequest();
		transactionRequest.setVendorCode("ver123");
		transactionRequest.setTransactionTypeCode("txn123");
		transactionRequest.setDirection(Direction.INBOUND);
		transactionRequest.setApplicationCode("ecocash");
		transactionRequest.setVendorApiKey("abcdefghijklmnopqrs");
		final Vendor vendorResult = new Vendor();
		vendorResult.setCode(transactionRequest.getVendorCode());
		vendorResult.setActive(true);
		vendorResult.setApiKey(transactionRequest.getVendorApiKey());
		when(vendorService.findVendorByVendorCode(transactionRequest.getVendorCode())).thenReturn(vendorResult);
		final Application applicationResult = new Application();
		applicationResult.setActive(false);
		when(applicationService.findApplicationByCode(transactionRequest.getApplicationCode())).thenReturn(applicationResult);
		final GatewayStatusCode gatewayStatusCode = new GatewayStatusCode();
		gatewayStatusCode.setCode("407");
		final String statusMessage = "Application deactivated";
		when(statusCodesResolver.resolve(GatewayConstants.StatusCodeKeys.APPLICATION_DEACTIVATED, statusMessage)).thenReturn(gatewayStatusCode);
		final TransactionResponse transactionResponse = transactionExecutor.execute(transactionRequest);
		verify(statusCodesResolver).resolve(GatewayConstants.StatusCodeKeys.APPLICATION_DEACTIVATED, statusMessage);
		Assert.assertEquals(gatewayStatusCode.getCode(), transactionResponse.getStatusCode());
		Assert.assertEquals(gatewayStatusCode.getMessage(), transactionResponse.getStatusReason());
		Assert.assertNull(transactionResponse.getDestinationReference());
	}
	
	@Test
	public void shouldReturnErrorCodeWhenThereIsNoActiveAggregatorVendorMapping(){
		final TransactionRequest transactionRequest = new TransactionRequest();
		transactionRequest.setVendorCode("ver123");
		transactionRequest.setTransactionTypeCode("txn123");
		transactionRequest.setDirection(Direction.INBOUND);
		transactionRequest.setApplicationCode("ecocash");
		final Vendor vendorResult = new Vendor();
		vendorResult.setCode(transactionRequest.getVendorCode());
		vendorResult.setActive(true);
		when(vendorService.findVendorByVendorCode(transactionRequest.getVendorCode())).thenReturn(vendorResult);
		final Application applicationResult = new Application();
		applicationResult.setActive(false);
		when(applicationService.findApplicationByCode(transactionRequest.getApplicationCode())).thenReturn(applicationResult);
		final GatewayStatusCode gatewayStatusCode = new GatewayStatusCode();
		gatewayStatusCode.setCode("407");
		final String statusMessage = "Invalid vendor profile";
		when(statusCodesResolver.resolve(GatewayConstants.StatusCodeKeys.APPLICATION_DEACTIVATED, statusMessage)).thenReturn(gatewayStatusCode);
	}
	
	@Test
	//@Ignore
	public void shouldReturnSuccessCodeForInboundRequest(){
		final TransactionRequest transactionRequest = new TransactionRequest();
		transactionRequest.setVendorCode("ver123");
		transactionRequest.setTransactionTypeCode("txn123");
		transactionRequest.setDirection(Direction.INBOUND);
		transactionRequest.setApplicationCode("ecocash");
		transactionRequest.setVendorApiKey("abcdefghijklmnopqrs");
		final Vendor vendorResult = new Vendor();
		vendorResult.setId(1L);
		vendorResult.setCode(transactionRequest.getVendorCode());
		vendorResult.setActive(true);
		VendorClass vendorClass = new VendorClass();
		vendorClass.setName("A");
	    vendorResult.setVendorClass(vendorClass);
	    vendorResult.setApiKey(transactionRequest.getVendorApiKey());
		when(vendorService.findVendorByVendorCode(transactionRequest.getVendorCode())).thenReturn(vendorResult);
		final Application applicationResult = new Application();
		applicationResult.setActive(true);
		applicationResult.setId(1L);
		when(applicationService.findApplicationByCode(transactionRequest.getApplicationCode())).thenReturn(applicationResult);
		final VendorAggregatorMapping vendorAggregatorMapping = new VendorAggregatorMapping();
		vendorAggregatorMapping.setActive(true);
		final Aggregator aggregator = new Aggregator();
		aggregator.setActive(true);
		aggregator.setName("econet");
		aggregator.setId(1L);
		vendorAggregatorMapping.setAggregator(aggregator);
		vendorAggregatorMapping.setVendor(vendorResult);
		when(vendorAggregatorMappingService.findVendorAggregatorMappingByVendorAndStatus(1L, true)).thenReturn(Collections.singletonList(vendorAggregatorMapping));
		final TransactionType transactionTypeResult = new TransactionType();
		when(transactionTypeService.findTransactionTypeByApplicationIdAndCode(applicationResult.getId() , transactionRequest.getTransactionTypeCode())).thenReturn(transactionTypeResult);
		final VendorTranTypeMapping vendorTranTypeMapping = new VendorTranTypeMapping();
		vendorTranTypeMapping.setTransactionType(transactionTypeResult);
		vendorTranTypeMapping.setActive(true);
		vendorTranTypeMapping.setAggregator(vendorAggregatorMapping.getAggregator());
		vendorTranTypeMapping.setVendor(vendorAggregatorMapping.getVendor());
	    when(vendorTranTypeMappingService.findVendorTranTypeMappingByVendorAggregatorAndTranType(vendorAggregatorMapping.getVendor().getId() , vendorAggregatorMapping.getAggregator().getId() , vendorTranTypeMapping.getTransactionType().getId())).thenReturn(vendorTranTypeMapping);
	    final TransactionResponse transactionResponse = new TransactionResponse();
	    when(transactionHandlerInvoker.invoke(any(TransactionInvocationContext.class))).thenReturn(transactionResponse);
	    transactionExecutor.execute(transactionRequest);
	}
}
