package com.econetwireless.payment.gateway.config.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.econetwireless.payment.gateway.dao.aggregator.AggregatorDao;
import com.econetwireless.payment.gateway.dao.aggregator.AggregatorDaoImpl;
import com.econetwireless.payment.gateway.dao.aggregatorStatusCodeMapping.AggregatorStatusCodeMappingDao;
import com.econetwireless.payment.gateway.dao.aggregatorStatusCodeMapping.AggregatorStatusCodeMappingDaoImpl;
import com.econetwireless.payment.gateway.dao.apiDefinition.ApiDefinitionDao;
import com.econetwireless.payment.gateway.dao.apiDefinition.ApiDefinitionDaoImpl;
import com.econetwireless.payment.gateway.dao.application.ApplicationDao;
import com.econetwireless.payment.gateway.dao.application.ApplicationDaoImpl;
import com.econetwireless.payment.gateway.dao.applicationStatusCode.ApplicationStatusCodeDao;
import com.econetwireless.payment.gateway.dao.applicationStatusCode.ApplicationStatusCodeDaoImpl;
import com.econetwireless.payment.gateway.dao.currency.CurrencyDao;
import com.econetwireless.payment.gateway.dao.currency.CurrencyDaoImpl;
import com.econetwireless.payment.gateway.dao.parameter.ParameterDao;
import com.econetwireless.payment.gateway.dao.parameter.ParameterDaoImpl;
import com.econetwireless.payment.gateway.dao.transaction.TransactionDao;
import com.econetwireless.payment.gateway.dao.transaction.TransactionDaoImpl;
import com.econetwireless.payment.gateway.dao.transactionType.TransactionTypeDao;
import com.econetwireless.payment.gateway.dao.transactionType.TransactionTypeDaoImpl;
import com.econetwireless.payment.gateway.dao.vendor.VendorDao;
import com.econetwireless.payment.gateway.dao.vendor.VendorDaoImpl;
import com.econetwireless.payment.gateway.dao.vendorAggregatorMapping.VendorAggregatorMappingDao;
import com.econetwireless.payment.gateway.dao.vendorAggregatorMapping.VendorAggregatorMappingDaoImpl;
import com.econetwireless.payment.gateway.dao.vendorApiAssignment.VendorApiAssignmentDao;
import com.econetwireless.payment.gateway.dao.vendorApiAssignment.VendorApiAssignmentDaoImpl;
import com.econetwireless.payment.gateway.dao.vendorProfileRequest.VendorProfileRequestDao;
import com.econetwireless.payment.gateway.dao.vendorProfileRequest.VendorProfileRequestDaoImp;
import com.econetwireless.payment.gateway.dao.vendorRole.VendorRoleDao;
import com.econetwireless.payment.gateway.dao.vendorRole.VendorRoleDaoImpl;
import com.econetwireless.payment.gateway.dao.vendorRoleAssignment.VendorRoleAssignmentDao;
import com.econetwireless.payment.gateway.dao.vendorRoleAssignment.VendorRoleAssignmentDaoImpl;
import com.econetwireless.payment.gateway.dao.vendorRoleTranTypePermission.VendorRoleTranTypePermissionDao;
import com.econetwireless.payment.gateway.dao.vendorRoleTranTypePermission.VendorRoleTranTypePermissionDaoImpl;
import com.econetwireless.payment.gateway.dao.vendorTranTypeMapping.VendorTranTypeMappingDao;
import com.econetwireless.payment.gateway.dao.vendorTranTypeMapping.VendorTranTypeMappingDaoImpl;

@Configuration
public class DaoConfiguration {

	@PersistenceContext
	private EntityManager entityManager;

	@Bean
	public AggregatorDao aggregatorDao() {
		final AggregatorDaoImpl instance = new AggregatorDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public AggregatorStatusCodeMappingDao aggregatorStatusCodeMappingDao() {
		final AggregatorStatusCodeMappingDaoImpl instance = new AggregatorStatusCodeMappingDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public ApiDefinitionDao apiDefinitionDao() {
		final ApiDefinitionDaoImpl instance = new ApiDefinitionDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public ApplicationDao applicationDao() {
		final ApplicationDaoImpl instance = new ApplicationDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public ApplicationStatusCodeDao applicationStatusCodeDao() {
		final ApplicationStatusCodeDaoImpl instance = new ApplicationStatusCodeDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public CurrencyDao currencyDao() {
		final CurrencyDaoImpl instance = new CurrencyDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public ParameterDao parameterDao() {
		final ParameterDaoImpl instance = new ParameterDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public TransactionDao transactionDao() {
		final TransactionDaoImpl instance = new TransactionDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public TransactionTypeDao transactionTypeDao() {
		final TransactionTypeDaoImpl instance = new TransactionTypeDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public VendorDao vendorDao() {
		final VendorDaoImpl instance = new VendorDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public VendorAggregatorMappingDao vendorAggregatorMappingDao() {
		final VendorAggregatorMappingDaoImpl instance = new VendorAggregatorMappingDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public VendorApiAssignmentDao vendorApiAssignmentDao() {
		final VendorApiAssignmentDaoImpl instance = new VendorApiAssignmentDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public VendorProfileRequestDao vendorProfileRequestDao() {
		final VendorProfileRequestDaoImp instance = new VendorProfileRequestDaoImp();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public VendorRoleDao vendorRoleDao() {
		final VendorRoleDaoImpl instance = new VendorRoleDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public VendorRoleAssignmentDao vendorRoleAssignmentDao() {
		final VendorRoleAssignmentDaoImpl instance = new VendorRoleAssignmentDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public VendorRoleTranTypePermissionDao vendorRoleTranTypePermissionDao() {
		final VendorRoleTranTypePermissionDaoImpl instance = new VendorRoleTranTypePermissionDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

	@Bean
	public VendorTranTypeMappingDao vendorTranTypeMappingDao() {
		final VendorTranTypeMappingDaoImpl instance = new VendorTranTypeMappingDaoImpl();
		instance.setEntityManager(entityManager);
		return instance;
	}

}
