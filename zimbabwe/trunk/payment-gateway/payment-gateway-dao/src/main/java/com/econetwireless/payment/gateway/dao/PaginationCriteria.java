package com.econetwireless.payment.gateway.dao;

import java.io.Serializable;

public class PaginationCriteria implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final int FIRST_RESULT_INDEX = 0;
	
	private static final int DEFAULT_MAX_RESULTS = 100;
	
	public static final PaginationCriteria DEFAULT_CRITERIA = new PaginationCriteria(FIRST_RESULT_INDEX, DEFAULT_MAX_RESULTS);
	
	private int firstResult;
	
	private int maxResults;
	
	public PaginationCriteria(final int firstResult , final int maxResults){
		this.firstResult = firstResult;
		this.maxResults = maxResults;
	}

	public int getFirstResult() {
		return firstResult;
	}

	public void setFirstResult(int firstResult) {
		this.firstResult = firstResult;
	}

	public int getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}
	
	
}
