package com.econetwireless.payment.gateway.dao.aggregator;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.Aggregator;

@Repository
public class AggregatorDaoImpl implements AggregatorDao {

	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Aggregator save(final Aggregator aggregator) {
		return entityManager.merge(aggregator);
	}

	public Aggregator findById(final long aggregatorId) {
		return entityManager.find(Aggregator.class, aggregatorId);
	}

	public Aggregator findByName(final String aggregatorName) {
		try {
			return entityManager.createNamedQuery("Aggregator:findByName", Aggregator.class)
					.setParameter("name", aggregatorName).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public List<Aggregator> findByStatus(boolean active) {
		return entityManager.createNamedQuery("Aggregator:findByStatus", Aggregator.class)
				.setParameter("active", active).getResultList();
	}

	public List<Aggregator> findAll() {
		return entityManager.createNamedQuery("Aggregator:findAll", Aggregator.class).getResultList();
	}

	public List<Aggregator> findByDefault(final boolean _defualt) {
		return entityManager.createNamedQuery("Aggregator:findByDefault", Aggregator.class)
				.setParameter("default", _defualt).getResultList();
	}

	public List<Aggregator> findByDefaultAndStatus(final boolean _default, final boolean active) {
		return entityManager.createNamedQuery("Aggregator:findByDefaultAndStatus", Aggregator.class)
				.setParameter("default", _default).setParameter("active", active).getResultList();
	}

}
