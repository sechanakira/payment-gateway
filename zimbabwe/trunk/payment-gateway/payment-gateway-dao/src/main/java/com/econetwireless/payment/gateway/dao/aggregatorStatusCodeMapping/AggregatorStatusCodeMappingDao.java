package com.econetwireless.payment.gateway.dao.aggregatorStatusCodeMapping;

import java.util.List;

import com.econetwireless.payment.gateway.entities.AggregatorStatusCodeMapping;
import com.econetwireless.payment.gateway.entities.Direction;

public interface AggregatorStatusCodeMappingDao {

	AggregatorStatusCodeMapping save(AggregatorStatusCodeMapping aggregatorStatusCodeMapping);
	
	AggregatorStatusCodeMapping findById(long aggregatorStatusCodeMappingId);
	
	AggregatorStatusCodeMapping findByAggregatorApplicationMappedCodeAndDirection(long aggregatorId , long applicationId , String mappedCode , Direction direction);
	
	AggregatorStatusCodeMapping findByAggregatorAndApplicationStatusCode(long aggregatorId , long applicationStatusCodeId);
	
	List<AggregatorStatusCodeMapping> findByAggregatorAndApplication(long aggregatorId , long applicationId);
	
}
