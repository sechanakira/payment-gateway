package com.econetwireless.payment.gateway.dao.aggregatorStatusCodeMapping;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.AggregatorStatusCodeMapping;
import com.econetwireless.payment.gateway.entities.Direction;

@Repository
public class AggregatorStatusCodeMappingDaoImpl implements AggregatorStatusCodeMappingDao {

	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public AggregatorStatusCodeMapping save(final AggregatorStatusCodeMapping aggregatorStatusCodeMapping) {
		return entityManager.merge(aggregatorStatusCodeMapping);
	}

	public AggregatorStatusCodeMapping findById(final long aggregatorStatusCodeMappingId) {
		return entityManager.find(AggregatorStatusCodeMapping.class, aggregatorStatusCodeMappingId);
	}

	public AggregatorStatusCodeMapping findByAggregatorApplicationMappedCodeAndDirection(final long aggregatorId,
			final long applicationId, final String mappedCode, final Direction direction) {
		try {
			return entityManager
					.createNamedQuery("AggregatorStatusCodeMapping:findByAggregatorApplicationMappedCodeAndDirection",
							AggregatorStatusCodeMapping.class)
					.setParameter("aggregatorId", aggregatorId).setParameter("applicationId", applicationId)
					.setParameter("mappedCode", mappedCode).setParameter("direction", direction).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public List<AggregatorStatusCodeMapping> findByAggregatorAndApplication(final long aggregatorId,
			final long applicationId) {
		return entityManager
				.createNamedQuery("AggregatorStatusCodeMapping:findByAggregatorAndApplication",
						AggregatorStatusCodeMapping.class)
				.setParameter("aggregatorId", aggregatorId).setParameter("applicationId", applicationId)
				.getResultList();
	}

	public AggregatorStatusCodeMapping findByAggregatorAndApplicationStatusCode(final long aggregatorId,
			final long applicationStatusCodeId) {
		try {
			return entityManager
					.createNamedQuery("AggregatorStatusCodeMapping:findByAggregatorAndApplicationStatusCode",
							AggregatorStatusCodeMapping.class)
					.setParameter("aggregatorId", aggregatorId)
					.setParameter("applicationStatusCodeId", applicationStatusCodeId).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

}
