package com.econetwireless.payment.gateway.dao.apiDefinition;

import java.util.List;

import com.econetwireless.payment.gateway.entities.ApiDefinition;
import com.econetwireless.payment.gateway.entities.ApiType;

public interface ApiDefinitionDao {

	ApiDefinition save(ApiDefinition apiDefinition);
	
	ApiDefinition findById(long apiDefinitionId);
	
	ApiDefinition findByApplicationAndType(long applicationId , ApiType apiType);
	
	List<ApiDefinition> findByApplication(long applicationId);
	
}
