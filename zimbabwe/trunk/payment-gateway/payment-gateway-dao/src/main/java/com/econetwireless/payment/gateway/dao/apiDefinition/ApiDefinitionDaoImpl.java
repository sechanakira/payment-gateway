package com.econetwireless.payment.gateway.dao.apiDefinition;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.ApiDefinition;
import com.econetwireless.payment.gateway.entities.ApiType;

@Repository
public class ApiDefinitionDaoImpl implements ApiDefinitionDao {

	private EntityManager entityManager;

	@Override
	public ApiDefinition save(final ApiDefinition apiDefinition) {
		return entityManager.merge(apiDefinition);
	}

	@Override
	public ApiDefinition findById(final long apiDefinitionId) {
		return entityManager.find(ApiDefinition.class, apiDefinitionId);
	}

	@Override
	public ApiDefinition findByApplicationAndType(final long applicationId, final ApiType apiType) {
		try {
			return entityManager.createNamedQuery("ApiDefinition:findByApplicationAndType", ApiDefinition.class)
					.setParameter("applicationId", applicationId).setParameter("apiType", apiType).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	@Override
	public List<ApiDefinition> findByApplication(final long applicationId) {
		return entityManager.createNamedQuery("ApiDefinition:findByApplication", ApiDefinition.class)
				.setParameter("applicationId", applicationId).getResultList();
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
