package com.econetwireless.payment.gateway.dao.application;

import java.util.List;

import com.econetwireless.payment.gateway.entities.Application;

public interface ApplicationDao {

	Application save(Application application);
	
	Application findById(long applicationId);
	
	Application findByCode(String applicationCode);
	
	Application findByName(String applicationName);
	
	List<Application> findByStatus(boolean active);
	
	List<Application> findAll();
	
}
