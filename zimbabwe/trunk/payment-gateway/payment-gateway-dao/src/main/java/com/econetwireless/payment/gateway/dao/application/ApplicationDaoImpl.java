package com.econetwireless.payment.gateway.dao.application;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.Application;

@Repository
public class ApplicationDaoImpl implements ApplicationDao {

	private EntityManager entityManager;

	public Application save(final Application application) {
		return entityManager.merge(application);
	}

	public Application findById(final long applicationId) {
		return entityManager.find(Application.class, applicationId);
	}

	public Application findByCode(final String applicationCode) {
		try {
			return entityManager.createNamedQuery("Application:findByCode", Application.class)
					.setParameter("code", applicationCode).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public List<Application> findByStatus(final boolean active) {
		return entityManager.createNamedQuery("Application:findByStatus", Application.class)
				.setParameter("active", active).getResultList();
	}

	public List<Application> findAll() {
		return entityManager.createNamedQuery("Application:findAll", Application.class).getResultList();
	}

	public Application findByName(final String applicationName) {
		try {
			return entityManager.createNamedQuery("Application:findByName", Application.class)
					.setParameter("name", applicationName).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
