package com.econetwireless.payment.gateway.dao.applicationStatusCode;

import java.util.List;

import com.econetwireless.payment.gateway.entities.ApplicationStatusCode;
import com.econetwireless.payment.gateway.entities.Direction;

public interface ApplicationStatusCodeDao {

	ApplicationStatusCode save(ApplicationStatusCode applicationStatusCode);
	
	ApplicationStatusCode findById(long applicationStatusCodeId);
	
	ApplicationStatusCode findByApplicationIdCodeAndDirection(long applicationId , String code , Direction direction);
	
	List<ApplicationStatusCode> findByApplicationId(long applicationId);
	
	List<ApplicationStatusCode> findByApplicationIdAndDirection(long applicationId , Direction direction);
	
}
