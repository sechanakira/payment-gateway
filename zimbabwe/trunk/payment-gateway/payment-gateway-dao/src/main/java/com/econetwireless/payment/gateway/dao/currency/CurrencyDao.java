package com.econetwireless.payment.gateway.dao.currency;

import java.util.List;

import com.econetwireless.payment.gateway.entities.Currency;

public interface CurrencyDao {

	Currency save(Currency currency);
	
	Currency findById(long currencyId);
	
	Currency findByNumericCode(String numericCode);
	
	Currency findByAlphaCode(String alphaCode);
	
	List<Currency> findAll();
	
	List<Currency> findByStatus(boolean active);
}
