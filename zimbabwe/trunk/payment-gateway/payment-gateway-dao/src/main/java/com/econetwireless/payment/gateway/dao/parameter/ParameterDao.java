package com.econetwireless.payment.gateway.dao.parameter;

import java.util.List;

import com.econetwireless.payment.gateway.entities.Parameter;

public interface ParameterDao {

	Parameter save(Parameter parameter);
	
	Parameter findById(long parameterId);
	
	Parameter findByName(String parameterName);
	
	List<Parameter> findAll();
	
	List<Parameter> findParametersByNames(String... parameterNames);
}
