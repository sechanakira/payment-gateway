package com.econetwireless.payment.gateway.dao.parameter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.entities.Parameter;

@Repository
public class ParameterDaoImpl implements ParameterDao {

	private EntityManager entityManager;

	public Parameter save(final Parameter parameter) {
		return entityManager.merge(parameter);
	}

	public Parameter findById(final long parameterId) {
		return entityManager.find(Parameter.class, parameterId);
	}

	public Parameter findByName(final String parameterName) {
		try {
			return entityManager.createNamedQuery("Parameter:findByName", Parameter.class)
					.setParameter("name", parameterName).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public List<Parameter> findAll() {
		return entityManager.createNamedQuery("Parameter:findAll", Parameter.class).getResultList();
	}

	public List<Parameter> findParametersByNames(final String... parameterNames) {
		if (parameterNames != null && parameterNames.length > 0) {
			final List<String> namesList = new ArrayList<>(parameterNames.length);
			for (final String parameterName : parameterNames) {
				if (StringUtils.hasText(parameterName)) {
					namesList.add(parameterName.toLowerCase());
				}
			}
			if (!namesList.isEmpty()) {
				return entityManager.createNamedQuery("Parameter:findByNames", Parameter.class)
						.setParameter("names", namesList).getResultList();
			}
		}
		return Collections.<Parameter> emptyList();
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
