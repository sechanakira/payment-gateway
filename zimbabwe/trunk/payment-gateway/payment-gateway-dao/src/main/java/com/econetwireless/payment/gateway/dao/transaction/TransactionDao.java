package com.econetwireless.payment.gateway.dao.transaction;

import java.util.List;

import com.econetwireless.payment.gateway.dao.PaginationCriteria;
import com.econetwireless.payment.gateway.entities.Transaction;
import com.econetwireless.payment.gateway.entities.TransactionStatus;

public interface TransactionDao {

	Transaction save(Transaction transaction);

	Transaction findById(long transactionId);

	Transaction findByVendorAggregatorAndSourceRef(long vendorId, long aggregatorId, String sourceReference);

	Transaction findByVendorAggregatorAndDestRef(long vendorId, long aggregatorId, String destRef);

	List<Transaction> findTransactionsByMsisdn(String msisdn, PaginationCriteria paginationCriteria);

	List<Transaction> findBySourceReference(String sourceReference);
	
	Transaction findByGatewayReference(String gatewayReference);
	
	List<Transaction> findTransactionsByVendorTranTypeAndStatus(String vendorCode, String transactionTypeCode, TransactionStatus transactionStatus, int maxRecords);
	

}
