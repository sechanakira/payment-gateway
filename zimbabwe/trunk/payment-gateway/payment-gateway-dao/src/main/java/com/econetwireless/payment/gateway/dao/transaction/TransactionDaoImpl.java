package com.econetwireless.payment.gateway.dao.transaction;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.dao.PaginationCriteria;
import com.econetwireless.payment.gateway.entities.Transaction;
import com.econetwireless.payment.gateway.entities.TransactionStatus;

@Repository
public class TransactionDaoImpl implements TransactionDao {

	private EntityManager entityManager;

	public Transaction save(final Transaction transaction) {
		return entityManager.merge(transaction);
	}

	public Transaction findById(final long transactionId) {
		return entityManager.find(Transaction.class, transactionId);
	}

	public Transaction findByVendorAggregatorAndSourceRef(final long vendorId, final long aggregatorId,
			final String sourceReference) {
		Assert.hasText(sourceReference, "Transaction source reference required");
		try {
			return entityManager.createNamedQuery("Transaction:findByVendorAggregatorAndSourceRef", Transaction.class)
					.setParameter("vendorId", vendorId).setParameter("aggregatorId", aggregatorId)
					.setParameter("sourceReference", sourceReference).setMaxResults(1).setFirstResult(0)
					.getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public Transaction findByVendorAggregatorAndDestRef(final long vendorId, final long aggregatorId,
			final String destRef) {
		Assert.hasText(destRef, "Transaction destination reference required");
		try {
			return entityManager.createNamedQuery("Transaction:findByVendorAggregatorAndDestRef", Transaction.class)
					.setParameter("vendorId", vendorId).setParameter("aggregatorId", aggregatorId)
					.setParameter("destinationReference", destRef).setFirstResult(0).setMaxResults(1).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public List<Transaction> findTransactionsByMsisdn(final String msisdn,
			final PaginationCriteria paginationCriteria) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Transaction> findBySourceReference(final String sourceReference) {
		return entityManager.createNamedQuery("Transaction:findBySourceRef", Transaction.class)
				.setParameter("sourceReference", sourceReference).getResultList();
	}

	public Transaction findByGatewayReference(String gatewayReference) {
		try {
			return entityManager.createNamedQuery("Transaction:findByGatewayRef", Transaction.class)
					.setParameter("gatewayReference", gatewayReference).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public List<Transaction> findTransactionsByVendorTranTypeAndStatus(String vendorCode, String transactionTypeCode,
			TransactionStatus transactionStatus, int maxRecords) {
		return entityManager.createNamedQuery("Transaction:findByVendorTranTypeAndStatus", Transaction.class)
				.setParameter("vendorCode", vendorCode).setParameter("tranTypeCode", transactionTypeCode)
				.setParameter("status", transactionStatus).setMaxResults(maxRecords).getResultList();
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
