package com.econetwireless.payment.gateway.dao.transactionType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.entities.TransactionType;

@Repository
public class TransactionTypeDaoImpl implements TransactionTypeDao {

	private EntityManager entityManager;

	public TransactionType save(final TransactionType transactionType) {
		return entityManager.merge(transactionType);
	}

	public TransactionType findById(final long transactionTypeId) {
		return entityManager.find(TransactionType.class, transactionTypeId);
	}

	public List<TransactionType> findByApplicationId(final long applicationId) {
		return entityManager.createNamedQuery("TransactionType:findByApplication", TransactionType.class)
				.setParameter("applicationId", applicationId).getResultList();
	}

	public TransactionType findByApplicationIdAndCode(final long applicationId, final String transactionTypeCode) {
		try {
			return entityManager.createNamedQuery("TransactionType:findByApplicationAndCode", TransactionType.class)
					.setParameter("applicationId", applicationId).setParameter("code", transactionTypeCode)
					.getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public TransactionType findByApplicationIdAndName(final long applicationId, final String transactionTypeName) {
		try {
			return entityManager.createNamedQuery("TransactionType:findByApplicationAndName", TransactionType.class)
					.setParameter("applicationId", applicationId).setParameter("name", transactionTypeName)
					.getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public List<TransactionType> findByStatus(final boolean active) {
		return entityManager.createNamedQuery("TransactionType:findByStatus", TransactionType.class)
				.setParameter("active", active).getResultList();
	}

	public List<TransactionType> findByApplicationIdAndStatus(final long applicationId, final boolean active) {
		return entityManager.createNamedQuery("TransactionType:findByApplicationAndStatus", TransactionType.class)
				.setParameter("applicationId", applicationId).setParameter("active", active).getResultList();
	}

	public List<TransactionType> findByIds(final long... transactionTypeIds) {
		if (transactionTypeIds.length > 0) {
			final List<Long> transactionTypeIdList = new ArrayList<Long>(transactionTypeIds.length);
			for (final long transactionTypeId : transactionTypeIds) {
				transactionTypeIdList.add(transactionTypeId);
			}
			return entityManager.createNamedQuery("TransactionType:findByIds", TransactionType.class)
					.setParameter("transactionTypeIds", transactionTypeIdList).getResultList();
		}
		return Collections.<TransactionType> emptyList();
	}

	public List<TransactionType> findByApplicationIdAndDirection(final long applicationId, final Direction direction) {
		return entityManager.createNamedQuery("TransactionType:findByApplicationAndDirection", TransactionType.class)
				.setParameter("applicationId", applicationId).setParameter("direction", direction).getResultList();
	}

	public List<TransactionType> findByApplicationIdDirectionAndStatus(final long applicationId,
			final Direction direction, final boolean active) {
		return entityManager
				.createNamedQuery("TransactionType:findByApplicationDirectionAndStatus", TransactionType.class)
				.setParameter("applicationId", applicationId).setParameter("direction", direction)
				.setParameter("active", active).getResultList();
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
