package com.econetwireless.payment.gateway.dao.vendor;

import java.util.List;

import com.econetwireless.payment.gateway.entities.Vendor;

public interface VendorDao {
	
	Vendor save(Vendor vendor);
	
	Vendor findById(long vendorId);
	
	Vendor findByName(String vendorName);
	
	Vendor findByCode(String vendorCode);
	
	List<Vendor> findAll();
	
	List<Vendor> findByStatus(boolean active);
}
