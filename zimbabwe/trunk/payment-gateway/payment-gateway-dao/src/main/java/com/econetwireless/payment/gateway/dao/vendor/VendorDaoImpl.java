package com.econetwireless.payment.gateway.dao.vendor;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.Vendor;

@Repository
public class VendorDaoImpl implements VendorDao {

	private EntityManager entityManager;

	public Vendor save(final Vendor vendor) {
		return entityManager.merge(vendor);
	}

	public Vendor findById(final long vendorId) {
		return entityManager.find(Vendor.class, vendorId);
	}

	public Vendor findByName(final String vendorName) {
		try {
			return entityManager.createNamedQuery("Vendor:findByName", Vendor.class).setParameter("name", vendorName)
					.getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public Vendor findByCode(final String vendorCode) {
		try {
			return entityManager.createNamedQuery("Vendor:findByCode", Vendor.class).setParameter("code", vendorCode)
					.getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public List<Vendor> findAll() {
		return entityManager.createNamedQuery("Vendor:findAll", Vendor.class).getResultList();
	}

	public List<Vendor> findByStatus(final boolean active) {
		return entityManager.createNamedQuery("Vendor:findByStatus", Vendor.class).setParameter("active", active)
				.getResultList();
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
