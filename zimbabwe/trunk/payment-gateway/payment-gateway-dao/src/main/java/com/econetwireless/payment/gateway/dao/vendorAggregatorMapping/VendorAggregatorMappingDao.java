package com.econetwireless.payment.gateway.dao.vendorAggregatorMapping;

import java.util.List;

import com.econetwireless.payment.gateway.entities.VendorAggregatorMapping;

public interface VendorAggregatorMappingDao {

	VendorAggregatorMapping save(VendorAggregatorMapping vendorAggregatorMapping);
	
	VendorAggregatorMapping findById(long vendorAggregatorMappingId);
	
	VendorAggregatorMapping findByVendorAndAggregator(long vendorId , long aggregatorId);
	
	VendorAggregatorMapping findByVendorAndAggregatorAndStatus(long vendorId , long aggregatorId , boolean active);
	
	List<VendorAggregatorMapping> findByVendorAndStatus(long vendorId , boolean active);
	
	List<VendorAggregatorMapping> findByVendor(long vendorId);
	
	List<VendorAggregatorMapping> findByStatus(boolean active);
}
