package com.econetwireless.payment.gateway.dao.vendorAggregatorMapping;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.VendorAggregatorMapping;

@Repository
public class VendorAggregatorMappingDaoImpl implements VendorAggregatorMappingDao {

	private EntityManager entityManager;

	public VendorAggregatorMapping save(final VendorAggregatorMapping vendorAggregatorMapping) {
		return entityManager.merge(vendorAggregatorMapping);
	}

	public VendorAggregatorMapping findById(final long vendorAggregatorMappingId) {
		return entityManager.find(VendorAggregatorMapping.class, vendorAggregatorMappingId);
	}

	public VendorAggregatorMapping findByVendorAndAggregator(final long vendorId, final long aggregatorId) {
		try {
			return entityManager
					.createNamedQuery("VendorAggregatorMapping:findByVendorAndAggregator",
							VendorAggregatorMapping.class)
					.setParameter("vendorId", vendorId).setParameter("aggregatorId", aggregatorId).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public VendorAggregatorMapping findByVendorAndAggregatorAndStatus(final long vendorId, final long aggregatorId,
			final boolean active) {
		try {
			return entityManager
					.createNamedQuery("VendorAggregatorMapping:findByVendorAggregatorAndStatus",
							VendorAggregatorMapping.class)
					.setParameter("vendorId", vendorId).setParameter("aggregatorId", aggregatorId)
					.setParameter("active", active).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public List<VendorAggregatorMapping> findByVendorAndStatus(final long vendorId, final boolean active) {
		return entityManager
				.createNamedQuery("VendorAggregatorMapping:findByVendorAndStatus", VendorAggregatorMapping.class)
				.setParameter("vendorId", vendorId).setParameter("active", active).getResultList();

	}

	public List<VendorAggregatorMapping> findByVendor(final long vendorId) {
		return entityManager.createNamedQuery("VendorAggregatorMapping:findByVendor", VendorAggregatorMapping.class)
				.setParameter("vendorId", vendorId).getResultList();
	}

	public List<VendorAggregatorMapping> findByStatus(final boolean active) {
		return entityManager.createNamedQuery("VendorAggregatorMapping:findByStatus", VendorAggregatorMapping.class)
				.setParameter("active", active).getResultList();
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
