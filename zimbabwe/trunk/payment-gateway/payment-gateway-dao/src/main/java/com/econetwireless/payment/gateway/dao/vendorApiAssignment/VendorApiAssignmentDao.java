package com.econetwireless.payment.gateway.dao.vendorApiAssignment;

import java.util.List;

import com.econetwireless.payment.gateway.entities.ApiType;
import com.econetwireless.payment.gateway.entities.VendorApiAssignment;

public interface VendorApiAssignmentDao {

	VendorApiAssignment save(VendorApiAssignment apiAssignment);
	
	VendorApiAssignment findById(long apiAssignmentId);
	
	List<VendorApiAssignment> findByVendorApplicationAndStatus(long vendorId , long applicationId , boolean status);
	
	VendorApiAssignment findByVendorApplicationAndApiType(long vendorId , long applicationId , ApiType apiType);
	
	List<VendorApiAssignment> findByVendorAndApplication(long vendorId , long applicationId);
}
