package com.econetwireless.payment.gateway.dao.vendorApiAssignment;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.ApiType;
import com.econetwireless.payment.gateway.entities.VendorApiAssignment;

@Repository
public class VendorApiAssignmentDaoImpl implements VendorApiAssignmentDao {

	private EntityManager entityManager;

	@Override
	public VendorApiAssignment save(final VendorApiAssignment apiAssignment) {
		return entityManager.merge(apiAssignment);
	}

	@Override
	public VendorApiAssignment findById(final long apiAssignmentId) {
		return entityManager.find(VendorApiAssignment.class, apiAssignmentId);
	}

	@Override
	public List<VendorApiAssignment> findByVendorApplicationAndStatus(final long vendorId, final long applicationId,
			final boolean status) {
		return entityManager
				.createNamedQuery("VendorApiAssignment:findByVendorApplicationAndStatus", VendorApiAssignment.class)
				.setParameter("applicationId", vendorId).setParameter("vendorId", vendorId)
				.setParameter("active", status).getResultList();
	}

	@Override
	public VendorApiAssignment findByVendorApplicationAndApiType(final long vendorId, final long applicationId,
			final ApiType apiType) {
		try {
			return entityManager
					.createNamedQuery("VendorApiAssignment:findByVendorApplicationAndApiType",
							VendorApiAssignment.class)
					.setParameter("vendorId", vendorId).setParameter("applicationId", applicationId)
					.setParameter("apiType", apiType).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public List<VendorApiAssignment> findByVendorAndApplication(final long vendorId, final long applicationId) {
		return entityManager
				.createNamedQuery("VendorApiAssignment:findByVendorAndApplication", VendorApiAssignment.class)
				.setParameter("vendorId", vendorId).setParameter("applicationId", applicationId).getResultList();
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
