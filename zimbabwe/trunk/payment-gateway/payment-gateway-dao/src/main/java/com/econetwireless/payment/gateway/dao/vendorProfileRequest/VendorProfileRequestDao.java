package com.econetwireless.payment.gateway.dao.vendorProfileRequest;

import java.util.List;

import com.econetwireless.payment.gateway.entities.VendorProfileRequest;
import com.econetwireless.payment.gateway.entities.VendorProfileRequestStatus;

public interface VendorProfileRequestDao {

	VendorProfileRequest save(VendorProfileRequest vendorProfileRequest);
	
	VendorProfileRequest findById(long vendorProfileRequestId);
	
	List<VendorProfileRequest> findByApplicationAndStatus(long applicationId , VendorProfileRequestStatus status);
	
	List<VendorProfileRequest> findByApplicationAndName(long applicationId , String vendorName);
	
}
