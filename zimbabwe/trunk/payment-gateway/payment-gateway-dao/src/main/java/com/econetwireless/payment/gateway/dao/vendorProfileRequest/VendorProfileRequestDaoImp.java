package com.econetwireless.payment.gateway.dao.vendorProfileRequest;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.VendorProfileRequest;
import com.econetwireless.payment.gateway.entities.VendorProfileRequestStatus;

@Repository
public class VendorProfileRequestDaoImp implements VendorProfileRequestDao {

	private EntityManager entityManager;

	public VendorProfileRequest save(final VendorProfileRequest vendorProfileRequest) {
		return entityManager.merge(vendorProfileRequest);
	}

	public VendorProfileRequest findById(final long vendorProfileRequestId) {
		return entityManager.find(VendorProfileRequest.class, vendorProfileRequestId);
	}

	public List<VendorProfileRequest> findByApplicationAndStatus(final long applicationId,
			final VendorProfileRequestStatus status) {
		return entityManager
				.createNamedQuery("VendorProfileRequest:findByApplicationAndStatus", VendorProfileRequest.class)
				.setParameter("applicationId", applicationId).setParameter("status", status).getResultList();
	}

	public List<VendorProfileRequest> findByApplicationAndName(final long applicationId, final String vendorName) {
		return entityManager
				.createNamedQuery("VendorProfileRequest:findByApplicationAndName", VendorProfileRequest.class)
				.setParameter("applicationId", applicationId).setParameter("name", vendorName).getResultList();
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
