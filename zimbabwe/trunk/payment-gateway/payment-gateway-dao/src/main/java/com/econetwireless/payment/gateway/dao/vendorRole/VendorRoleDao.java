package com.econetwireless.payment.gateway.dao.vendorRole;

import java.util.List;

import com.econetwireless.payment.gateway.entities.VendorRole;

public interface VendorRoleDao {

	VendorRole save(VendorRole vendorRole);
	
	VendorRole findById(long vendorRoleId);
	
	List<VendorRole> findByApplication(long applicationId);
	
	List<VendorRole> findByApplicationAndStatus(long applicationId , boolean active);
	
	VendorRole findByApplicationAndName(long applicationId , String name);
	
}
