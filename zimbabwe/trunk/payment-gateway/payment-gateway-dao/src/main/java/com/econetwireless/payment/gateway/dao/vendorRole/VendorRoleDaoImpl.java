package com.econetwireless.payment.gateway.dao.vendorRole;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.VendorRole;

@Repository
public class VendorRoleDaoImpl implements VendorRoleDao {

	private EntityManager entityManager;

	@Override
	public VendorRole save(final VendorRole vendorRole) {
		return entityManager.merge(vendorRole);
	}

	@Override
	public VendorRole findById(final long vendorRoleId) {
		return entityManager.find(VendorRole.class, vendorRoleId);
	}

	@Override
	public List<VendorRole> findByApplication(final long applicationId) {
		return entityManager.createNamedQuery("VendorRole:findByApplication", VendorRole.class)
				.setParameter("applicationId", applicationId).getResultList();
	}

	@Override
	public List<VendorRole> findByApplicationAndStatus(final long applicationId, final boolean active) {
		return entityManager.createNamedQuery("VendorRole:findByApplicationAndStatus", VendorRole.class)
				.setParameter("applicationId", applicationId).setParameter("active", active).getResultList();
	}

	@Override
	public VendorRole findByApplicationAndName(final long applicationId, final String name) {
		try {
			return entityManager.createNamedQuery("VendorRole:findByApplicationAndName", VendorRole.class)
					.setParameter("applicationId", applicationId).setParameter("name", name).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
