package com.econetwireless.payment.gateway.dao.vendorRoleAssignment;

import java.util.List;

import com.econetwireless.payment.gateway.entities.VendorRoleAssignment;

public interface VendorRoleAssignmentDao {

	VendorRoleAssignment save(final VendorRoleAssignment vendorRoleAssignment);
	
	VendorRoleAssignment findById(final long vendorRoleAssignmentId);
	
	List<VendorRoleAssignment> findByVendorAndApplication(final long vendorId , final long applicationId);

	List<VendorRoleAssignment> findByVendorRoleAndStatus(final long vendorRoleId , boolean active);
	
}
