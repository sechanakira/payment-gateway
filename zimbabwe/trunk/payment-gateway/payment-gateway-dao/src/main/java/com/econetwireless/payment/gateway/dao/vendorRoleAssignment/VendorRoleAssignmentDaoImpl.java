package com.econetwireless.payment.gateway.dao.vendorRoleAssignment;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.VendorRoleAssignment;

@Repository
public class VendorRoleAssignmentDaoImpl implements VendorRoleAssignmentDao {

	private EntityManager entityManager;

	@Override
	public VendorRoleAssignment save(final VendorRoleAssignment vendorRoleAssignment) {
		return entityManager.merge(vendorRoleAssignment);
	}

	@Override
	public VendorRoleAssignment findById(final long vendorRoleAssignmentId) {
		return entityManager.find(VendorRoleAssignment.class, vendorRoleAssignmentId);
	}

	@Override
	public List<VendorRoleAssignment> findByVendorAndApplication(final long vendorId, final long applicationId) {
		return entityManager
				.createNamedQuery("VendorRoleAssignment:findByVendorAndApplication", VendorRoleAssignment.class)
				.setParameter("vendorId", vendorId).setParameter("applicationId", applicationId).getResultList();
	}

	@Override
	public List<VendorRoleAssignment> findByVendorRoleAndStatus(final long vendorRoleId, final boolean active) {
		return entityManager
				.createNamedQuery("VendorRoleAssignment:findByVendorRoleAndStatus", VendorRoleAssignment.class)
				.setParameter("vendorRoleId", vendorRoleId).setParameter("active", active).getResultList();
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
