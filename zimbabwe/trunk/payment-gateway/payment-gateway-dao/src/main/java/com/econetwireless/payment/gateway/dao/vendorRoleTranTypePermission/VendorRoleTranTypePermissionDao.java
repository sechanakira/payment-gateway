package com.econetwireless.payment.gateway.dao.vendorRoleTranTypePermission;

import java.util.List;

import com.econetwireless.payment.gateway.entities.VendorRoleTranTypePermission;

public interface VendorRoleTranTypePermissionDao {

	VendorRoleTranTypePermission save(VendorRoleTranTypePermission permission);
	
	VendorRoleTranTypePermission findById(long vendorRoleTranTypePermissionId);
	
	List<VendorRoleTranTypePermission> findByVendorRole(long vendorRoleId);
	
	List<VendorRoleTranTypePermission> findByVendorRoleAndStatus(long vendorRoleId , boolean active);
	
	VendorRoleTranTypePermission findByVendorRoleAndTranType(long vendorRoleId , long transactionTypeId);

}
