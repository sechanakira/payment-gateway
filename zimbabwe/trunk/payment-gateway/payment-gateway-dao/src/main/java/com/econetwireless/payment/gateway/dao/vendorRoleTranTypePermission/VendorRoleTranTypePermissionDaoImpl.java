package com.econetwireless.payment.gateway.dao.vendorRoleTranTypePermission;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.VendorRoleTranTypePermission;

@Repository
public class VendorRoleTranTypePermissionDaoImpl implements VendorRoleTranTypePermissionDao {

	private EntityManager entityManager;

	@Override
	public VendorRoleTranTypePermission save(final VendorRoleTranTypePermission permission) {
		return entityManager.merge(permission);
	}

	@Override
	public VendorRoleTranTypePermission findById(final long vendorRoleTranTypePermissionId) {
		return entityManager.find(VendorRoleTranTypePermission.class, vendorRoleTranTypePermissionId);
	}

	@Override
	public List<VendorRoleTranTypePermission> findByVendorRole(final long vendorRoleId) {
		return entityManager
				.createNamedQuery("VendorRoleTranTypePermission:findByVendorRole", VendorRoleTranTypePermission.class)
				.setParameter("vendorRoleId", vendorRoleId).getResultList();
	}

	@Override
	public List<VendorRoleTranTypePermission> findByVendorRoleAndStatus(final long vendorRoleId, final boolean active) {
		return entityManager
				.createNamedQuery("VendorRoleTranTypePermission:findByVendorRoleAndStatus",
						VendorRoleTranTypePermission.class)
				.setParameter("vendorRoleId", vendorRoleId).setParameter("active", active).getResultList();
	}

	@Override
	public VendorRoleTranTypePermission findByVendorRoleAndTranType(final long vendorRoleId,
			final long transactionTypeId) {
		try {
			return entityManager
					.createNamedQuery("VendorRoleTranTypePermission:findByVendorRoleAndTransactionType",
							VendorRoleTranTypePermission.class)
					.setParameter("vendorRoleId", vendorRoleId).setParameter("transactionTypeId", transactionTypeId)
					.getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
