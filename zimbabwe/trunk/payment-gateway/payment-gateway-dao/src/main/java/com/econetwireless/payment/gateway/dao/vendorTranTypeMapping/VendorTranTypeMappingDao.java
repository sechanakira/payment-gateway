package com.econetwireless.payment.gateway.dao.vendorTranTypeMapping;

import java.util.List;

import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.entities.VendorTranTypeMapping;

public interface VendorTranTypeMappingDao {

	VendorTranTypeMapping save(VendorTranTypeMapping vendorTranTypeMapping);

	VendorTranTypeMapping findById(long vendorTranTypeMappingId);

	VendorTranTypeMapping findByVendorAggregatorTranTypeAndStatus(long vendorId, long aggregatorId, long tranTypeId,
			boolean active);

	VendorTranTypeMapping findByVendorAggregatorAndTranType(long vendorId, long aggregatorId, long tranTypeId);

	List<VendorTranTypeMapping> findByVendorAndTranType(long vendorId, long tranTypeId);

	List<VendorTranTypeMapping> findByVendor(long vendorId);

	List<VendorTranTypeMapping> findAggregatorAndDirection(long aggregatorId, final Direction inbound);

	VendorTranTypeMapping findByMappedVendorCodeAggregatorTranTypeAndStatus(String mappedVendorCode, long aggregatorId,
			long tranTypeId, boolean active);

}
