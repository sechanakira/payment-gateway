package com.econetwireless.payment.gateway.dao.vendorTranTypeMapping;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.entities.VendorTranTypeMapping;

@Repository
public class VendorTranTypeMappingDaoImpl implements VendorTranTypeMappingDao {

	private EntityManager entityManager;

	@Override
	public VendorTranTypeMapping save(final VendorTranTypeMapping vendorTranTypeMapping) {
		return entityManager.merge(vendorTranTypeMapping);
	}

	@Override
	public VendorTranTypeMapping findById(final long vendorTranTypeMappingId) {
		return entityManager.find(VendorTranTypeMapping.class, vendorTranTypeMappingId);
	}

	@Override
	public VendorTranTypeMapping findByVendorAggregatorTranTypeAndStatus(final long vendorId, final long aggregatorId,
			final long tranTypeId, final boolean active) {
		try {
			return entityManager
					.createNamedQuery("VendorTranTypeMapping:findByVendorAggregatorTranTypeAndStatus",
							VendorTranTypeMapping.class)
					.setParameter("vendorId", vendorId).setParameter("aggregatorId", aggregatorId)
					.setParameter("active", active).setParameter("tranTypeId", tranTypeId).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	@Override
	public VendorTranTypeMapping findByVendorAggregatorAndTranType(final long vendorId, final long aggregatorId,
			final long tranTypeId) {
		try {
			return entityManager
					.createNamedQuery("VendorTranTypeMapping:findByVendorAggregatorAndTranType",
							VendorTranTypeMapping.class)
					.setParameter("vendorId", vendorId).setParameter("aggregatorId", aggregatorId)
					.setParameter("tranTypeId", tranTypeId).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

	@Override
	public List<VendorTranTypeMapping> findByVendorAndTranType(final long vendorId, final long tranTypeId) {
		return entityManager
				.createNamedQuery("VendorTranTypeMapping:findByVendorAndTranType", VendorTranTypeMapping.class)
				.setParameter("vendorId", vendorId).setParameter("tranTypeId", tranTypeId).getResultList();
	}

	@Override
	public List<VendorTranTypeMapping> findByVendor(final long vendorId) {
		return entityManager.createNamedQuery("VendorTranTypeMapping:findByVendor", VendorTranTypeMapping.class)
				.setParameter("vendorId", vendorId).getResultList();
	}

	@Override
	public List<VendorTranTypeMapping> findAggregatorAndDirection(final long aggregatorId, final Direction direction) {
		return entityManager
				.createNamedQuery("VendorTranTypeMapping:findByAggregatorAndDirection", VendorTranTypeMapping.class)
				.setParameter("aggregatorId", aggregatorId).setParameter("direction", direction).getResultList();
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public VendorTranTypeMapping findByMappedVendorCodeAggregatorTranTypeAndStatus(String mappedVendorCode,
			long aggregatorId, long tranTypeId, boolean active) {
		try {
			return entityManager
					.createNamedQuery("VendorTranTypeMapping:findByVendorAggregatorTranTypeAndStatus",
							VendorTranTypeMapping.class)
					.setParameter("mappedVendorCode", mappedVendorCode).setParameter("aggregatorId", aggregatorId)
					.setParameter("active", active).setParameter("tranTypeId", tranTypeId).getSingleResult();
		} catch (final NoResultException ex) {
			return null;
		}
	}

}
