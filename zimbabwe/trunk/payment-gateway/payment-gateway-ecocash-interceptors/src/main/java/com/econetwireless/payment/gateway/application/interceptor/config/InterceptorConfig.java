package com.econetwireless.payment.gateway.application.interceptor.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({"classpath:otp-service-context.xml"})
public class InterceptorConfig {

}
