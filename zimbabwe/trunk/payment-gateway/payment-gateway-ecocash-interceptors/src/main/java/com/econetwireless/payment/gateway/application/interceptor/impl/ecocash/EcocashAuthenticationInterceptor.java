package com.econetwireless.payment.gateway.application.interceptor.impl.ecocash;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.application.interceptor.api.Interceptor;
import com.econetwireless.payment.gateway.application.interceptor.api.InterceptorResponse;
import com.econetwireless.payment.gateway.entities.Application;
import com.econetwireless.payment.gateway.entities.GatewayStatusCode;
import com.econetwireless.payment.gateway.entities.TransactionStatus;
import com.econetwireless.payment.gateway.entities.TransactionType;
import com.econetwireless.payment.gateway.entities.Vendor;
import com.econetwireless.payment.gateway.entities.VendorTranTypeMapping;
import com.econetwireless.payment.gateway.service.application.ApplicationService;
import com.econetwireless.payment.gateway.service.statusCode.GatewayConstants;
import com.econetwireless.payment.gateway.service.statusCode.GatewayStatusCodeResolver;
import com.econetwireless.payment.gateway.service.transationType.TransactionTypeService;
import com.econetwireless.payment.gateway.service.vendor.VendorService;
import com.econetwireless.payment.gateway.service.vendorTranTypeMapping.VendorTranTypeMappingService;
import com.econetwireless.pin.management.client.AuthenticationRequest;
import com.econetwireless.pin.management.client.AuthenticationResponse;
import com.econetwireless.pin.management.client.OTPPort;

@Component
public class EcocashAuthenticationInterceptor implements Interceptor{

	@Autowired
	private OTPPort otpPort;
	
	@Autowired
	private GatewayStatusCodeResolver statusCodesResolver;
	
	@Autowired
	private VendorService vendorService;
	
	@Autowired
	private TransactionTypeService transactionTypeService;
	
	@Autowired
	private ApplicationService applicationService;
	
	@Autowired
	private VendorTranTypeMappingService vendorTranTypeMappingService;
	
	public InterceptorResponse intercept(final TransactionRequest transactionRequest) {
		final Vendor vendor = vendorService.findVendorByVendorCode(transactionRequest.getVendorCode());
		if(vendor == null){
			return buildResponseWithStatusCode(GatewayConstants.StatusCodeKeys.INVALID_VENDOR_PROFILE, "Invalid vendor details" , TransactionStatus.FAILED);
		}
		final Application application = applicationService.findApplicationByCode(transactionRequest.getApplicationCode());
		final TransactionType transactionType = transactionTypeService.findTransactionTypeByApplicationIdAndCode(application.getId(), transactionRequest.getTransactionTypeCode());
		if(!transactionType.getRequiresAuthentication()){
			transactionRequest.setPin(null);
			return null;
		}
		final List<VendorTranTypeMapping> vendorTranTypeMappings = vendorTranTypeMappingService.findVendorTranTypeMappingsByVendor(vendor.getId());
	    for(final VendorTranTypeMapping vendorTranTypeMapping : vendorTranTypeMappings){
			if(vendorTranTypeMapping.getTransactionType().getCode().equalsIgnoreCase(transactionRequest.getTransactionTypeCode())){
				if(vendorTranTypeMapping.getTrusted()){
					//ecocash pin
					return null;
				}
				break;
			}
		}
	    
		if(StringUtils.isEmpty(transactionRequest.getPin())){
			return buildResponseWithStatusCode(GatewayConstants.StatusCodeKeys.MISSING_PARAMETER, "Pin required" , TransactionStatus.FAILED);
		}
		if(StringUtils.isEmpty(transactionRequest.getMsisdn())){
			return buildResponseWithStatusCode(GatewayConstants.StatusCodeKeys.MISSING_PARAMETER, "Msisdn required" , TransactionStatus.FAILED);
		}
		if(!isValidMsisdn(transactionRequest.getMsisdn())){
			return buildResponseWithStatusCode(GatewayConstants.StatusCodeKeys.INVALID_PARAMETER, "Invalid msisdn : " + transactionRequest.getMsisdn(), TransactionStatus.FAILED);
		}
		if(vendor.getTrusted()){
			return null;
		}
		
		final AuthenticationRequest authenticationRequest = new AuthenticationRequest();
		authenticationRequest.setApplicationCode("ecocash");//parameterize
		authenticationRequest.setMsisdn(transactionRequest.getMsisdn());
		authenticationRequest.setPin(transactionRequest.getPin());
		final AuthenticationResponse response = otpPort.authenticate(authenticationRequest);
		if(!response.isSuccess()){
			return buildResponseWithStatusCode(GatewayConstants.StatusCodeKeys.NOT_PERMITTED, response.getReason() , TransactionStatus.FAILED);
		}
		transactionRequest.setPin(null);
		return null;
		
	}
	
	private boolean isValidMsisdn(final String msisdn) {
		return msisdn.matches(GatewayConstants.MSISDN_REGEX);
	}

	private InterceptorResponse buildResponseWithStatusCode(final String statusCodeKey , final String statusMessage , final TransactionStatus transactionStatus){
		final InterceptorResponse interceptorResponse = new InterceptorResponse();
		final TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setStatusReason(statusMessage);
		final GatewayStatusCode gatewayStatusCode = statusCodesResolver.resolve(statusCodeKey, statusMessage);
		if(gatewayStatusCode != null){
			transactionResponse.setStatusCode(gatewayStatusCode.getCode());
			transactionResponse.setStatusReason(gatewayStatusCode.getMessage());
		}
		interceptorResponse.setTransactionStatus(transactionStatus);
		interceptorResponse.setTransactionResponse(transactionResponse);
		return interceptorResponse;
	}

	
}
