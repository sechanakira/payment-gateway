package com.econetwireless.payment.gateway.endpoint.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

public class AggregatorTransactionExecutor {

	private String transactionProcessingEngineUrl;

	public void setTransactionProcessingEngineUrl(String processingEngineUrl) {
		transactionProcessingEngineUrl = processingEngineUrl;
	}

	public TransactionResponse execute(TransactionRequest transactionRequest) {
		final RestTemplate restTemplate = new RestTemplate();
		final List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		messageConverters.add(new MappingJackson2HttpMessageConverter());
		restTemplate.setMessageConverters(messageConverters);
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		final HttpEntity<TransactionRequest> entity = new HttpEntity<>(transactionRequest, httpHeaders);
		final ResponseEntity<TransactionResponse> responseEntity = restTemplate
				.postForEntity(transactionProcessingEngineUrl, entity, TransactionResponse.class);
		return responseEntity.getBody();
	}
}
