package com.econetwireless.payment.gateway.cpg.api.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@ImportResource("classpath:soap-api-context.xml")
@PropertySources({@PropertySource("classpath:cpg-inbound-api.properties")})
public class CpgInboundConfig {

}
