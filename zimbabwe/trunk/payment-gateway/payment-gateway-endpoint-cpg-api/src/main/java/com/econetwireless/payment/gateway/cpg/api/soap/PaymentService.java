package com.econetwireless.payment.gateway.cpg.api.soap;

import com.econetwireless.payment.gateway.cpg.api.model.Request;
import com.econetwireless.payment.gateway.cpg.api.model.Response;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Created by tinashehondo on 11/4/16.
 */

@WebService(name = "PaymentPort" , targetNamespace="http://www.econetwiress/cpg/api/soap")
public interface PaymentService {
    @WebMethod
    Response postTransaction(@WebParam(name = "transactionRequest")Request request);

}
