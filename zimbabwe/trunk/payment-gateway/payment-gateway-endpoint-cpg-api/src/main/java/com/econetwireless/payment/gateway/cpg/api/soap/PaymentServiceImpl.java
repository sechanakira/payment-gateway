package com.econetwireless.payment.gateway.cpg.api.soap;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.cpg.api.model.Request;
import com.econetwireless.payment.gateway.cpg.api.model.Response;
import com.econetwireless.payment.gateway.endpoint.api.AggregatorTransactionExecutor;
import com.econetwireless.payment.gateway.entities.ApiType;
import com.econetwireless.payment.gateway.entities.Direction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Created by tinashehondo on 11/4/16.
 */
@WebService(serviceName = "PaymentService", endpointInterface = "com.econetwireless.payment.gateway.cpg.api.soap.PaymentService", portName = "PaymentPort", targetNamespace = "http://www.econetwiress/cpg/api/soap")

public class PaymentServiceImpl extends SpringBeanAutowiringSupport implements PaymentService{
    private AggregatorTransactionExecutor aggregatorTransactionExecutor;

    private static final Logger logger = LoggerFactory.getLogger(PaymentServiceImpl.class);

    @Autowired
    private Environment environment;

    private RestTemplate restTemplate;

    public PaymentServiceImpl() {
        super();
    }

    @PostConstruct
    private void initializeRestTemplate() {
        restTemplate = new RestTemplate();
        final List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new Jaxb2RootElementHttpMessageConverter());
        messageConverters.add(new StringHttpMessageConverter());
        restTemplate.setMessageConverters(messageConverters);
        aggregatorTransactionExecutor = new AggregatorTransactionExecutor();
        aggregatorTransactionExecutor
                .setTransactionProcessingEngineUrl(environment.getRequiredProperty("core.transaction.processing.url"));
    }

    public Response postTransaction(final Request request) {
        if (logger.isInfoEnabled()) {
            logger.info("Post transaction request {}", request);
        }
        final TransactionRequest aggregatorRequest = adaptRequest(request);
        final TransactionResponse aggregatorRespponse = aggregatorTransactionExecutor.execute(aggregatorRequest);
        final Response response = adaptResponse(aggregatorRespponse);
        if (logger.isInfoEnabled()) {
            logger.info("Post response {}", response);
        }
        return response;
    }

    private Response adaptResponse(final TransactionResponse aggregatorResponse) {
        final Response response = new Response();
        response.setField1(aggregatorResponse.getStatusCode());
        response.setField2(aggregatorResponse.getStatusReason());
        response.setField3(aggregatorResponse.getDestinationReference());
        response.setField4(aggregatorResponse.getBalance());
        response.setField5(aggregatorResponse.getAccountType());
        response.setField6(aggregatorResponse.getAccountName());
        response.setField7(aggregatorResponse.getAccountStatus());
        response.setField8(aggregatorResponse.getCurrencyCode());
        return response;
    }

    private TransactionRequest adaptRequest(final Request request) {
        final TransactionRequest aggregatorTransactionRequest = new TransactionRequest();
        aggregatorTransactionRequest
                .setApplicationCode(environment.getProperty("payment.gateway.ecocash.application.code")); // parameterize
        aggregatorTransactionRequest.setDirection(Direction.INBOUND);
        aggregatorTransactionRequest.setChannel(ApiType.SOAP);
        aggregatorTransactionRequest.setSourceReference(request.getField10());
        aggregatorTransactionRequest.setTransactionTypeCode(request.getField7());
        aggregatorTransactionRequest.setVendorCode(request.getField1());
        aggregatorTransactionRequest.setVendorApiKey(request.getField2());
        aggregatorTransactionRequest.setMsisdn(request.getField3());
        aggregatorTransactionRequest.setAccountNumber1(request.getField4());
        aggregatorTransactionRequest.setCurrencyCode(request.getField13());
        aggregatorTransactionRequest.setAmount(request.getField12());
        aggregatorTransactionRequest.setPin(request.getField5());
        aggregatorTransactionRequest.setMsisdn2(request.getField11());
        aggregatorTransactionRequest.setFirstName(request.getField15());
        aggregatorTransactionRequest.setLastName(request.getField16());
        aggregatorTransactionRequest.setDob(request.getField17());
        aggregatorTransactionRequest.setGender(request.getField18());
        aggregatorTransactionRequest.setIdNumber(request.getField19());
        aggregatorTransactionRequest.setChecksum(request.getField6());
        aggregatorTransactionRequest.setServiceType(request.getField14());
        aggregatorTransactionRequest.setTerminalId(request.getField21());
        aggregatorTransactionRequest.setSource(request.getField22());
        aggregatorTransactionRequest.setCountryCode(request.getField23());
        aggregatorTransactionRequest.setTransactionId(request.getField24());
        aggregatorTransactionRequest.setDestination(request.getField25());
        return aggregatorTransactionRequest;
    }


}
