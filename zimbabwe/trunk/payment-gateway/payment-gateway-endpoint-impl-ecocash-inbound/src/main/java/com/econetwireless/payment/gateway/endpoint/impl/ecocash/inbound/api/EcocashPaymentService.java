package com.econetwireless.payment.gateway.endpoint.impl.ecocash.inbound.api;
import javax.jws.WebMethod;
import javax.jws.WebParam;

import javax.jws.WebService;


@WebService(name = "EcocashPaymentPort" , targetNamespace="http://www.econetwiress/ecocash/api/soap")
public interface EcocashPaymentService {

	@WebMethod
	Response postTransaction(@WebParam(name = "transactionRequest")Request request);
		
}
