package com.econetwireless.payment.gateway.endpoint.impl.ecocash.inbound.api;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.endpoint.api.AggregatorTransactionExecutor;
import com.econetwireless.payment.gateway.entities.ApiType;
import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.pin.management.client.DateTime;
import com.econetwireless.pin.management.client.ObjectFactory;
import com.econetwireless.pin.management.client.OtpRequest;
import com.econetwireless.pin.management.client.OtpResponse;

import zw.co.econet.notification.apis.soap.DispatchInfo;
import zw.co.econet.notification.apis.soap.Language;
import zw.co.econet.notification.apis.soap.NotificationRequest;

@WebService(serviceName = "EcocashPaymentService", endpointInterface = "com.econetwireless.payment.gateway.endpoint.impl.ecocash.inbound.api.EcocashPaymentService", portName = "EcocashPaymentPort", targetNamespace = "http://www.econetwiress/ecocash/api/soap")
public class EcocashPaymentServiceImpl extends SpringBeanAutowiringSupport implements EcocashPaymentService {

	private AggregatorTransactionExecutor aggregatorTransactionExecutor;

	private static final Logger logger = LoggerFactory.getLogger(EcocashPaymentServiceImpl.class);

	@Autowired
	private Environment environment;

	private RestTemplate restTemplate;

	public EcocashPaymentServiceImpl() {
		super();
	}

	@PostConstruct
	private void initializeRestTemplate() {
		restTemplate = new RestTemplate();
		final List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(new Jaxb2RootElementHttpMessageConverter());
		messageConverters.add(new StringHttpMessageConverter());
		restTemplate.setMessageConverters(messageConverters);
		aggregatorTransactionExecutor = new AggregatorTransactionExecutor();
		aggregatorTransactionExecutor
				.setTransactionProcessingEngineUrl(environment.getRequiredProperty("core.transaction.processing.url"));
	}

	public Response postTransaction(final Request request) {
		if (logger.isInfoEnabled()) {
			logger.info("Post transaction request {}", request);
		}
		final TransactionRequest aggregatorRequest = adaptRequest(request);
		final TransactionResponse aggregatorRespponse = aggregatorTransactionExecutor.execute(aggregatorRequest);
		final Response response = adaptResponse(aggregatorRespponse);
		if (logger.isInfoEnabled()) {
			logger.info("Post response {}", response);
		}
		return response;
	}

	private Response adaptResponse(final TransactionResponse aggregatorResponse) {
		final Response response = new Response();
		response.setField1(aggregatorResponse.getStatusCode());
		response.setField2(aggregatorResponse.getStatusReason());
		response.setField3(aggregatorResponse.getDestinationReference());
		response.setField4(aggregatorResponse.getBalance());
		response.setField5(aggregatorResponse.getAccountType());
		response.setField6(aggregatorResponse.getAccountName());
		response.setField7(aggregatorResponse.getAccountStatus());
		response.setField8(aggregatorResponse.getCurrencyCode());
		return response;
	}

	private TransactionRequest adaptRequest(final Request request) {
		final TransactionRequest aggregatorTransactionRequest = new TransactionRequest();
		aggregatorTransactionRequest
				.setApplicationCode(environment.getProperty("payment.gateway.ecocash.application.code")); // parameterize
		aggregatorTransactionRequest.setDirection(Direction.INBOUND);
		aggregatorTransactionRequest.setChannel(ApiType.SOAP);
		aggregatorTransactionRequest.setSourceReference(request.getField10());
		aggregatorTransactionRequest.setTransactionTypeCode(request.getField7());
		aggregatorTransactionRequest.setVendorCode(request.getField1());
		aggregatorTransactionRequest.setVendorApiKey(request.getField2());
		aggregatorTransactionRequest.setMsisdn(request.getField3());
		aggregatorTransactionRequest.setAccountNumber1(request.getField4());
		aggregatorTransactionRequest.setCurrencyCode(request.getField13());
		aggregatorTransactionRequest.setAmount(request.getField12());
		aggregatorTransactionRequest.setPin(request.getField5());
		aggregatorTransactionRequest.setMsisdn2(request.getField11());
		aggregatorTransactionRequest.setFirstName(request.getField15());
		aggregatorTransactionRequest.setLastName(request.getField16());
		aggregatorTransactionRequest.setDob(request.getField17());
		aggregatorTransactionRequest.setGender(request.getField18());
		aggregatorTransactionRequest.setIdNumber(request.getField19());
		aggregatorTransactionRequest.setChecksum(request.getField6());
		aggregatorTransactionRequest.setServiceType(request.getField14());
		aggregatorTransactionRequest.setTerminalId(request.getField21());
		aggregatorTransactionRequest.setSource(request.getField22());
		aggregatorTransactionRequest.setCountryCode(request.getField23());
		aggregatorTransactionRequest.setTransactionId(request.getField24());
		aggregatorTransactionRequest.setDestination(request.getField25());
		return aggregatorTransactionRequest;
	}

	@WebMethod
	public Response requestPin(@WebParam(name = "pinRequest") final Request request) {
		final String pinManagementUrl = environment.getProperty("pin.management.pin.request.url");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));
		final ObjectFactory objectFactory = new ObjectFactory();
		final OtpRequest otpRequest = objectFactory.createOtpRequest();
		otpRequest.setApplicationCode(environment.getProperty("pin.management.ecocash.application.code"));
		otpRequest.setMsisdn(request.getField3());
		final HttpEntity<OtpRequest> entity = new HttpEntity<OtpRequest>(otpRequest, httpHeaders);
		final ResponseEntity<OtpResponse> otpResponseEntity = restTemplate.postForEntity(pinManagementUrl, entity,
				OtpResponse.class);
		final OtpResponse otpResponse = otpResponseEntity.getBody();
		final Response response = new Response();
		response.setField6(otpResponse.getReference());
		notifyClient(request, otpResponse);
		return response;
	}

	private void notifyClient(final Request request, final OtpResponse otpResponse) {
		final NotificationRequest notificationRequest = new NotificationRequest();
		notificationRequest.setApplicationCode(environment.getProperty("notification.management.application.code"));
		notificationRequest.setLanguage(Language.ENGLISH);
		notificationRequest.setReference(otpResponse.getReference());
		final String expiryTime = formatDateTime(otpResponse.getExpiresOn());
		notificationRequest.setStaticBody(MessageFormat.format("Your Ecocash one time pin is {0} and expires at {1}",
				otpResponse.getPin(), expiryTime));
		final DispatchInfo dispatchInfo = new DispatchInfo();
		dispatchInfo.getSmsMobileNumbers().add(request.getField3());
		notificationRequest.setDispatchInfo(dispatchInfo);
		final String notificationManagementUrl = environment
				.getProperty("notification.management.dispatch.notification.url");
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Collections.singletonList(MediaType.TEXT_PLAIN));
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		final HttpEntity<NotificationRequest> entity = new HttpEntity<NotificationRequest>(notificationRequest,
				httpHeaders);
		restTemplate.postForEntity(notificationManagementUrl, entity, String.class);
	}

	private String formatDateTime(final DateTime expiresOn) {
		final Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, expiresOn.getDay());
		calendar.set(Calendar.MONTH, expiresOn.getMonth() - 1);
		calendar.set(Calendar.YEAR, expiresOn.getYear());
		calendar.set(Calendar.HOUR_OF_DAY, expiresOn.getHour());
		calendar.set(Calendar.MINUTE, expiresOn.getMinute());
		calendar.set(Calendar.SECOND, expiresOn.getSecond());
		final DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		return dateFormat.format(calendar.getTime());
	}

}
