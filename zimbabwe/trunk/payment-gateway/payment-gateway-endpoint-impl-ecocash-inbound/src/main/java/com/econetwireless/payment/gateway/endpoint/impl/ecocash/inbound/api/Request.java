package com.econetwireless.payment.gateway.endpoint.impl.ecocash.inbound.api;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "request", namespace = "http://www.econetwiress/ecocash/api", propOrder = { "field1", "field2",
		"field3", "field4", "field5", "field6", "field7", "field8", "field9", "field10", "field11", "field12",
		"field13", "field14", "field15", "field16", "field17", "field18", "field19", "field20", "field21", "field22",
		"field23", "field24", "field25" })
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Request implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	private String field11;
	private String field12;
	private String field13;
	private String field14;
	private String field15;
	private String field16;
	private String field17;
	private String field18;
	private String field19;
	private String field20;
	private String field21;
	private String field22;
	private String field23;
	private String field24;
	private String field25;

	@XmlElement(name = "field1", required = false)
	public String getField1() {
		return field1;
	}

	public void setField1(final String field1) {
		this.field1 = field1;
	}

	@XmlElement(name = "field2", required = false)
	public String getField2() {
		return field2;
	}

	public void setField2(final String field2) {
		this.field2 = field2;
	}

	@XmlElement(name = "field3", required = false)
	public String getField3() {
		return field3;
	}

	public void setField3(final String field3) {
		this.field3 = field3;
	}

	@XmlElement(name = "field4", required = false)
	public String getField4() {
		return field4;
	}

	public void setField4(final String field4) {
		this.field4 = field4;
	}

	@XmlElement(name = "field5", required = false)
	public String getField5() {
		return field5;
	}

	public void setField5(final String field5) {
		this.field5 = field5;
	}

	@XmlElement(name = "field6", required = false)
	public String getField6() {
		return field6;
	}

	public void setField6(final String field6) {
		this.field6 = field6;
	}

	@XmlElement(name = "field7", required = false)
	public String getField7() {
		return field7;
	}

	public void setField7(final String field7) {
		this.field7 = field7;
	}

	@XmlElement(name = "field8", required = false)
	public String getField8() {
		return field8;
	}

	public void setField8(final String field8) {
		this.field8 = field8;
	}

	@XmlElement(name = "field9", required = false)
	public String getField9() {
		return field9;
	}

	public void setField9(final String field9) {
		this.field9 = field9;
	}

	@XmlElement(name = "field10", required = false)
	public String getField10() {
		return field10;
	}

	public void setField10(final String field10) {
		this.field10 = field10;
	}

	@XmlElement(name = "field11", required = false)
	public String getField11() {
		return field11;
	}

	public void setField11(final String field11) {
		this.field11 = field11;
	}

	@XmlElement(name = "field12", required = false)
	public String getField12() {
		return field12;
	}

	public void setField12(final String field12) {
		this.field12 = field12;
	}

	@XmlElement(name = "field13", required = false)
	public String getField13() {
		return field13;
	}

	public void setField13(final String field13) {
		this.field13 = field13;
	}

	@XmlElement(name = "field14", required = false)
	public String getField14() {
		return field14;
	}

	public void setField14(final String field14) {
		this.field14 = field14;
	}

	@XmlElement(name = "field15", required = false)
	public String getField15() {
		return field15;
	}

	public void setField15(final String field15) {
		this.field15 = field15;
	}

	@XmlElement(name = "field16", required = false)
	public String getField16() {
		return field16;
	}

	public void setField16(final String field16) {
		this.field16 = field16;
	}

	@XmlElement(name = "field17", required = false)
	public String getField17() {
		return field17;
	}

	public void setField17(final String field17) {
		this.field17 = field17;
	}

	@XmlElement(name = "field18", required = false)
	public String getField18() {
		return field18;
	}

	public void setField18(final String field18) {
		this.field18 = field18;
	}

	@XmlElement(name = "field19", required = false)
	public String getField19() {
		return field19;
	}

	public void setField19(final String field19) {
		this.field19 = field19;
	}

	@XmlElement(name = "field20", required = false)
	public String getField20() {
		return field20;
	}

	public void setField20(final String field20) {
		this.field20 = field20;
	}

	@XmlElement(name = "field21", required = false)
	public String getField21() {
		return field21;
	}

	public void setField21(final String field21) {
		this.field21 = field21;
	}

	@XmlElement(name = "field22", required = false)
	public String getField22() {
		return field22;
	}

	public void setField22(final String field22) {
		this.field22 = field22;
	}

	@XmlElement(name = "field23", required = false)
	public String getField23() {
		return field23;
	}

	public void setField23(final String field23) {
		this.field23 = field23;
	}

	@XmlElement(name = "field24", required = false)
	public String getField24() {
		return field24;
	}

	public void setField24(final String field24) {
		this.field24 = field24;
	}

	@XmlElement(name = "field25", required = false)
	public String getField25() {
		return field25;
	}

	public void setField25(final String field25) {
		this.field25 = field25;
	}

	@Override
	public String toString() {
		return "Request [field1=" + field1 + ", field2=" + field2 + ", field3=" + field3 + ", field4=" + field4
				+ ", field5=" + field5 + ", field6=" + field6 + ", field7=" + field7 + ", field8=" + field8
				+ ", field9=" + field9 + ", field10=" + field10 + ", field11=" + field11 + ", field12=" + field12
				+ ", field13=" + field13 + ", field14=" + field14 + ", field15=" + field15 + ", field16=" + field16
				+ ", field17=" + field17 + ", field18=" + field18 + ", field19=" + field19 + ", field20=" + field20
				+ ", field21=" + field21 + ", field22=" + field22 + ", field23=" + field23 + ", field24=" + field24
				+ ", field25=" + field25 + "]";
	}

}
