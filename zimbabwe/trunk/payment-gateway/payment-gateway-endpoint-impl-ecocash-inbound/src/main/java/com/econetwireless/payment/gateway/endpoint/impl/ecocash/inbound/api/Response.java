package com.econetwireless.payment.gateway.endpoint.impl.ecocash.inbound.api;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(namespace = "http://www.econetwiress/ecocash/api", propOrder = { "field1", "field2", "field3", "field4",
		"field5", "field6", "field7", "field8", "field9", "field10", "field11", "field12", "field13", "field14",
		"field15" })
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Response implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	private String field11;
	private String field12;
	private String field13;
	private String field14;
	private String field15;

	@XmlElement(name = "field1", required = false)
	public String getField1() {
		return field1;
	}

	public void setField1(final String field1) {
		this.field1 = field1;
	}

	@XmlElement(name = "field2", required = false)
	public String getField2() {
		return field2;
	}

	public void setField2(final String field2) {
		this.field2 = field2;
	}

	@XmlElement(name = "field3", required = false)
	public String getField3() {
		return field3;
	}

	public void setField3(final String field3) {
		this.field3 = field3;
	}

	@XmlElement(name = "field4", required = false)
	public String getField4() {
		return field4;
	}

	public void setField4(final String field4) {
		this.field4 = field4;
	}

	@XmlElement(name = "field5", required = false)
	public String getField5() {
		return field5;
	}

	public void setField5(final String field5) {
		this.field5 = field5;
	}

	@XmlElement(name = "field6", required = false)
	public String getField6() {
		return field6;
	}

	public void setField6(final String field6) {
		this.field6 = field6;
	}

	@XmlElement(name = "field7", required = false)
	public String getField7() {
		return field7;
	}

	public void setField7(final String field7) {
		this.field7 = field7;
	}

	@XmlElement(name = "field8", required = false)
	public String getField8() {
		return field8;
	}

	public void setField8(final String field8) {
		this.field8 = field8;
	}

	@XmlElement(name = "field9", required = false)
	public String getField9() {
		return field9;
	}

	public void setField9(final String field9) {
		this.field9 = field9;
	}

	@XmlElement(name = "field10", required = false)
	public String getField10() {
		return field10;
	}

	public void setField10(final String field10) {
		this.field10 = field10;
	}

	@XmlElement(name = "field11", required = false)
	public String getField11() {
		return field11;
	}

	public void setField11(final String field11) {
		this.field11 = field11;
	}

	@XmlElement(name = "field12", required = false)
	public String getField12() {
		return field12;
	}

	public void setField12(final String field12) {
		this.field12 = field12;
	}

	@XmlElement(name = "field13", required = false)
	public String getField13() {
		return field13;
	}

	public void setField13(final String field13) {
		this.field13 = field13;
	}

	@XmlElement(name = "field14", required = false)
	public String getField14() {
		return field14;
	}

	public void setField14(final String field14) {
		this.field14 = field14;
	}

	@XmlElement(name = "field15", required = false)
	public String getField15() {
		return field15;
	}

	public void setField15(final String field15) {
		this.field15 = field15;
	}

	@Override
	public String toString() {
		return "Response [field1=" + field1 + ", field2=" + field2 + ", field3=" + field3 + ", field4=" + field4
				+ ", field5=" + field5 + ", field6=" + field6 + ", field7=" + field7 + ", field8=" + field8
				+ ", field9=" + field9 + ", field10=" + field10 + ", field11=" + field11 + ", field12=" + field12
				+ ", field13=" + field13 + ", field14=" + field14 + ", field15=" + field15 + "]";
	}

}
