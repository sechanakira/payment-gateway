package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.config;

import java.io.IOException;

import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.econetwireless.payment.gateway.endpoint.api.AggregatorTransactionExecutor;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.DefaultMtiRequestResponseMapper;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.EcocashOutboundIsoEndpoint;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.IsoMessageProcessor;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.MtiRequestResponseMapper;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.marshaller.IsoMessageMarshaller;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.marshaller.MappingIsoMessageMarshaller;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.parser.IsoMessageParser;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.parser.MappingIsoMessageParser;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.transactionprocesser.DefaultTransactionProcessor;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.transactionprocesser.TransactionProcessor;

@Configuration
@PropertySource("classpath:endpoint.properties")
public class EcocashOutboundEndpointConfiguration {

	@Bean
	public EcocashOutboundIsoEndpoint isoEndpoint(@Value("${ecocash.outbound.endpoint.url}") String endpointUrl,
			Processor endpointProcessor) {
		final EcocashOutboundIsoEndpoint endpoint = new EcocashOutboundIsoEndpoint();
		endpoint.setEndpointProcessor(endpointProcessor);
		endpoint.setEndpointUrl(endpointUrl);
		return endpoint;
	}

	@Bean
	public Processor isoMessageProcessor(@Value("${iso-msg-config-file}") Resource isoMessageConfig,
			IsoMessageMarshaller messageMarshaller, IsoMessageParser messageParser,
			TransactionProcessor transactionProcessor, MtiRequestResponseMapper mtiRequestResponseMapper) {
		final IsoMessageProcessor messageProcessor = new IsoMessageProcessor();
		messageProcessor.setIsoMessageConfig(isoMessageConfig);
		messageProcessor.setMessageMarshaller(messageMarshaller);
		messageProcessor.setTransactionProcessor(transactionProcessor);
		messageProcessor.setMessageParser(messageParser);
		messageProcessor.setMtiRequestResponseMapper(mtiRequestResponseMapper);
		return messageProcessor;
	}

	@Bean
	public IsoMessageParser isoMessageParser(
			@Value("${iso-msg-request-field-mapping-file}") Resource isoRequestFieldMappingsConfig) throws IOException {
		final MappingIsoMessageParser parser = new MappingIsoMessageParser();
		parser.setFieldMappings(PropertiesLoaderUtils.loadProperties(isoRequestFieldMappingsConfig));
		return parser;
	}

	@Bean
	public IsoMessageMarshaller isoMessageMarshaller(
			@Value("${iso-msg-response-field-mapping-file}") Resource isoResponseFieldMappingsConfig)
			throws IOException {
		final MappingIsoMessageMarshaller marshaller = new MappingIsoMessageMarshaller();
		marshaller.setFieldMappings(PropertiesLoaderUtils.loadProperties(isoResponseFieldMappingsConfig));
		return marshaller;
	}

	@Bean
	public AggregatorTransactionExecutor transactionExecutor(
			@Value("${core.transaction.processing.url}") String transactionProcessingUrl) {
		final AggregatorTransactionExecutor executor = new AggregatorTransactionExecutor();
		executor.setTransactionProcessingEngineUrl(transactionProcessingUrl);
		return executor;
	}

	@Bean
	public TransactionProcessor transactionProcessor(AggregatorTransactionExecutor aggregatorTransactionExecutor) {
		final DefaultTransactionProcessor processor = new DefaultTransactionProcessor();
		processor.setTransactionExecutor(aggregatorTransactionExecutor);
		return processor;
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public MtiRequestResponseMapper mtiRequestResponseMapper() {
		return new DefaultMtiRequestResponseMapper();
	}

}
