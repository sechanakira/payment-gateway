package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.config;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.stream.IntStream;

import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;

public class Main {

	public static void main(String[] args) throws Exception {
		Socket socket = null;
		BufferedOutputStream osw;
		BufferedInputStream bis;
		try {
			socket = new Socket("192.168.132.209", 3000);
			socket.setSoTimeout(30 * 1000);
			osw = new BufferedOutputStream(socket.getOutputStream());
			bis = new BufferedInputStream(socket.getInputStream());
			final GenericPackager packager = new GenericPackager(Main.class.getResourceAsStream("/iso-msg-config.xml"));
			final ISOMsg isoMessage = new ISOMsg();
			isoMessage.setPackager(packager);
			isoMessage.setMTI("0200");
			isoMessage.set(3, "500000");
			isoMessage.set(7, "0729172007");
			isoMessage.set(4, "500");
			isoMessage.set(11, "000744");
			isoMessage.set(12, "172007");
			isoMessage.set(13, "0729");
			isoMessage.set(22, "000");
			isoMessage.set(25, "00");
			isoMessage.set(37, "000000078180");
			isoMessage.set(41, "POS101");
			isoMessage.set(42, "ZETDC");
			isoMessage.set(43, "ZESA_Zimbabwe_Payment");
			isoMessage.set(49, "840");
			isoMessage.set(32, "263106");
			isoMessage.set(102, "263772222091");
			isoMessage.set(103, "14101513589");
			isoMessage.set(123, "201101204024102");
			isoMessage.set(98, "12345");
			isoMessage.set(37, "1234abcdefgh");
			byte[] data = isoMessage.pack();
			final byte[] request = isoMessage.pack();
			final byte[] header = { (byte) (request.length / 256), (byte) (request.length % 256) };
			isoMessage.setHeader(header);
			data = isoMessage.pack();
			osw.write(data);
			osw.flush();
			// socket.close();
			final ISOMsg isoResponseMessage = new ISOMsg();
			isoResponseMessage.setPackager(packager);
			isoResponseMessage.unpack(bis);
			final IntStream fieldStream = IntStream.range(1, isoResponseMessage.getMaxField() + 1);
			fieldStream.forEachOrdered(
					fieldNo -> System.out.println(fieldNo + "->" + isoResponseMessage.getString(fieldNo)));
			// byte[] responseHeader = new byte[2];
			// bis.read(responseHeader);
			// byte[] response = new byte[responseHeader[0] * 256 +
			// responseHeader[1]];
			// bis.read(response);
			// ISOMsg isoResponseMessage = new ISOMsg();
			// isoResponseMessage.setPackager(packager);
			// isoResponseMessage.setHeader(responseHeader);
			// isoResponseMessage.unpack(b)
		} catch (final IOException e) {
			System.err.print(e);
		} finally {
			socket.close();
		}
	}
}
