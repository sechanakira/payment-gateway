package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultMtiRequestResponseMapper implements MtiRequestResponseMapper {

	private final Map<String, String> requestResponseCodes = new HashMap<>();

	private static final String FINANCIAL_REQUEST_CODE = "0200";

	private static final String FINANCIAL_RESPONSE_CODE = "0210";

	private static final Logger logger = LoggerFactory.getLogger(DefaultMtiRequestResponseMapper.class);

	public DefaultMtiRequestResponseMapper() {
		addDefaultMappings();
	}

	private void addDefaultMappings() {
		requestResponseCodes.put(FINANCIAL_REQUEST_CODE, FINANCIAL_RESPONSE_CODE);
		logger.debug("Added default mappings {}", requestResponseCodes);
	}

	@Override
	public String map(String requestMtiCode) {
		final String responseMtiCode = requestResponseCodes.get(requestMtiCode);
		logger.debug("Mapped request MTI code {} to response MTI code {}", requestMtiCode, responseMtiCode);
		return responseMtiCode;
	}

	public void addMapping(String requestMtiCode, String responseMtiCode) {
		requestResponseCodes.put(requestMtiCode, responseMtiCode);
		logger.debug("Added mapping for request MTI code {} to response MTI code {}", requestMtiCode, responseMtiCode);
	}

}
