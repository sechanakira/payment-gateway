package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.SimpleRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.SmartLifecycle;

public class EcocashOutboundIsoEndpoint implements SmartLifecycle {

	private String endpointUrl;

	private DefaultCamelContext camelContext;

	private Processor endpointProcessor;

	private static final Logger logger = LoggerFactory.getLogger(EcocashOutboundIsoEndpoint.class);

	public void setEndpointUrl(String endpointUrl) {
		this.endpointUrl = endpointUrl;
	}

	@PostConstruct
	private void init() {
		camelContext = new DefaultCamelContext();
		final IsoMsgProtocolCodecFactory codecFactory = new IsoMsgProtocolCodecFactory();
		final SimpleRegistry registry = new SimpleRegistry();
		registry.put("iso-codec", codecFactory);
		camelContext = new DefaultCamelContext(registry);
	}

	public void setEndpointProcessor(Processor endpointProcessor) {
		this.endpointProcessor = endpointProcessor;
	}

	@Override
	public boolean isRunning() {
		return camelContext.isStarted();
	}

	@Override
	public void start() {
		final RouteBuilder rootBuilder = new RouteBuilder() {

			@Override
			public void configure() throws Exception {
				from(endpointUrl).process(endpointProcessor);
			}
		};
		try {
			camelContext.addRoutes(rootBuilder);
			camelContext.start();
			logger.info("Successfully started Ecocash ISO outbound processor @ {}", new Date());
		} catch (final Exception ex) {
			logger.error("Failed to start Ecocash ISO outbound processor due to {}", ex);
		}
	}

	@Override
	public void stop() {
		try {
			camelContext.stop();
			logger.info("Successfully started Ecocash ISO outbound processor @ {}", new Date());
		} catch (final Exception ex) {
			logger.debug("Failed to stop Ecocash ISO outbound processor due to {}", ex);
		}
	}

	@Override
	public int getPhase() {
		return Integer.MAX_VALUE;
	}

	@Override
	public boolean isAutoStartup() {
		return true;
	}

	@Override
	public void stop(Runnable callback) {
		stop();
	}

}
