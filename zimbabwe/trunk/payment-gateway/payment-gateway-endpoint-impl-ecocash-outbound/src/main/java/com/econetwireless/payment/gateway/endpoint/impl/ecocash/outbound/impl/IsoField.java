package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl;

import java.text.DecimalFormat;
import java.util.Arrays;

public enum IsoField {

	PROCESSING_CODE(3),

	AMOUNT(4) {
		@Override
		public String getRawValue(Object processedValue) {
			if (processedValue != null) {
				final Number amount = (Number) processedValue;
				final Number amountInSmallerUnit = amount.doubleValue() * 100.0;
				return new DecimalFormat("0").format(amountInSmallerUnit);
			}
			return "";
		}

		@Override
		public Object getProcessedValue(Object rawValue) {
			if (rawValue != null) {
				final double amountInSmallerUnit = Double.parseDouble(rawValue.toString());
				return amountInSmallerUnit / 100.0;
			}
			return null;
		}

	},

	TRANSMISSION_DATE(7), TRACE_NUMBER(11), LOCAL_TRANSACTION_TIME(12), LOCAL_TRANSACTION_DATE(13), VENDOR(
			32), RECEIVER(100), REFERENCE_NO(61), CURRENCY_CODE(49), VENDOR_TERMINAL_ID(41), MERCHANT_NAME(
					42), ADDITIONAL_INFO_1(44), ADDITIONAL_INFO_2(46), ADDITIONAL_INFO_5(48), TRANSACTION_DESC(
							104), ACCOUNT_NUMBER(102), REFERENCE(37), ACCOUNT_NUMBER2(103), PAYMENT_TYPE(
									98), RESPONSE_CODE(39), ADDITIONAL_AMOUNT(54), VENDOR_TERMINAL_NAME(43);

	private int fieldNo;

	private IsoField(int fieldNo) {
		this.fieldNo = fieldNo;
	}

	public int getFieldNo() {
		return fieldNo;
	}

	public static IsoField fromFieldNumber(int fieldNo) {
		return Arrays.stream(values()).filter(f -> f.getFieldNo() == fieldNo).findFirst().orElse(null);
	}

	public Object getProcessedValue(Object rawValue) {
		if (rawValue instanceof String) {
			return ((String) rawValue).trim();
		}
		return rawValue;
	}

	public String getRawValue(Object processedValue) {
		if (processedValue == null) {
			return null;
		}
		return processedValue.toString();
	}
}
