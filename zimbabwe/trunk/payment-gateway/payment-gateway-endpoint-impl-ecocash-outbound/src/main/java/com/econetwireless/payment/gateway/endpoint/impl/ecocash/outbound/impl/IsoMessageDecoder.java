package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl;

import java.io.ByteArrayOutputStream;

import org.apache.mina.common.ByteBuffer;
import org.apache.mina.common.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IsoMessageDecoder extends CumulativeProtocolDecoder {

	private static final String CUMULATIVE_DATA_TO_WRITE_ATTR = "DATA_TO_WRITE";

	private static final int MAX_MANDATORY_ISO_FIELD = 102;

	private static final Logger logger = LoggerFactory.getLogger(IsoMessageDecoder.class);

	@Override
	protected boolean doDecode(final IoSession session, final ByteBuffer in, final ProtocolDecoderOutput out)
			throws Exception {
		final int remaining = in.remaining();
		final byte[] buf = new byte[remaining];
		in.get(buf);

		if (logger.isDebugEnabled()) {
			logger.debug("Received data packet : {}", new String(buf));
		}
		ByteArrayOutputStream cachedData = (ByteArrayOutputStream) session.getAttribute("DATA_TO_WRITE");
		if (cachedData == null) {
			cachedData = new ByteArrayOutputStream(150);
			session.setAttribute(CUMULATIVE_DATA_TO_WRITE_ATTR, cachedData);
		}
		cachedData.write(buf);
		cachedData.flush();
		final GenericPackager packager = new GenericPackager(getClass().getResourceAsStream("/iso-msg-config.xml"));
		final ISOMsg msg = new ISOMsg();
		msg.setPackager(packager);
		try {
			msg.unpack(cachedData.toByteArray());

			if (!msg.hasField(MAX_MANDATORY_ISO_FIELD)) {
				if (logger.isDebugEnabled()) {
					logger.debug("Field {} not found.Message incomplete..", MAX_MANDATORY_ISO_FIELD);
				}
				return false;
			}
		} catch (final Exception ex) {
			if (logger.isDebugEnabled()) {
				logger.debug(
						"Invalid format message...Probably incomplete.Waiting for more packets....." + ex.getMessage());
			}
			return false;
		}

		if (logger.isInfoEnabled()) {
			final ByteArrayOutputStream os = new ByteArrayOutputStream();
			msg.pack(os);
			logger.info("Decoded ISO Msg : {}", new String(os.toByteArray()));
		}
		out.write(msg.pack());
		cachedData.close();
		return true;
	}
}
