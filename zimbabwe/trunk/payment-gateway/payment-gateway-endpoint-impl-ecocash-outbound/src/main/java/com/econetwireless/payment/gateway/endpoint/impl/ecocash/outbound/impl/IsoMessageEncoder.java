package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl;

import org.apache.mina.common.ByteBuffer;
import org.apache.mina.common.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

public class IsoMessageEncoder extends ProtocolEncoderAdapter {

	@Override
	public void encode(final IoSession session, final Object message, final ProtocolEncoderOutput out)
			throws Exception {
		final byte[] msg = (byte[]) message;
		final ByteBuffer buffer = ByteBuffer.wrap(msg);
		out.write(buffer);
		out.flush();
	}
}
