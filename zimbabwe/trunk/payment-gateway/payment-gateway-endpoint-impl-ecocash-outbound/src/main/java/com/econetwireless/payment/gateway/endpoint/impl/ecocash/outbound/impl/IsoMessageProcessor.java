package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.marshaller.IsoMessageMarshaller;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.parser.IsoMessageParser;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.transactionprocesser.TransactionProcessor;

public class IsoMessageProcessor implements Processor {

	private IsoMessageParser messageParser;

	private Resource isoMessageConfig;

	private IsoMessageMarshaller messageMarshaller;

	private TransactionProcessor transactionProcessor;

	private MtiRequestResponseMapper mtiRequestResponseMapper;

	private static final Logger logger = LoggerFactory.getLogger(IsoMessageProcessor.class);

	public void setMessageParser(IsoMessageParser messageParser) {
		this.messageParser = messageParser;
	}

	public void setIsoMessageConfig(Resource isoMessageConfig) {
		this.isoMessageConfig = isoMessageConfig;
	}

	public void setMessageMarshaller(IsoMessageMarshaller messageMarshaller) {
		this.messageMarshaller = messageMarshaller;
	}

	public void setTransactionProcessor(TransactionProcessor transactionProcessor) {
		this.transactionProcessor = transactionProcessor;
	}

	public void setMtiRequestResponseMapper(MtiRequestResponseMapper mtiRequestResponseMapper) {
		this.mtiRequestResponseMapper = mtiRequestResponseMapper;
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		final long startTime = System.currentTimeMillis();
		logger.debug("Received ISO request {}", exchange);
		final GenericPackager packager = new GenericPackager(isoMessageConfig.getInputStream());
		final ISOMsg isoMessage = new ISOMsg();
		isoMessage.setPackager(packager);
		isoMessage.unpack(exchange.getIn().getBody(byte[].class));
		final TransactionRequest transactionRequest = messageParser.parse(isoMessage);
		final ISOMsg responseMsg = createResponseMessage(isoMessage);
		final TransactionResponse transactionResponse = transactionProcessor.process(transactionRequest);
		messageMarshaller.marshall(responseMsg, transactionResponse);
		postProcessMessage(responseMsg);
		final byte[] response = responseMsg.pack();
		logger.info("Sending ISO message {} for request {}", new String(response));
		exchange.getOut().setBody(response, byte[].class);
		final long endTime = System.currentTimeMillis();
		logger.info("Request processing completed in {} ms", endTime - startTime);
	}

	private ISOMsg createResponseMessage(ISOMsg requestMessage) throws Exception {
		final ISOMsg isoMsg = requestMessage;
		isoMsg.setMTI(mtiRequestResponseMapper.map(requestMessage.getMTI()));
		isoMsg.set(100,"263106");
		isoMsg.set(43,"ZESA_Zimbabwe_Payment");
		//isoMsg.set(44,"ZESA_Zimbabwe_Payment");
		isoMsg.set(15,"1008");
		isoMsg.set(28,"C00000001");
		isoMsg.set(30,"C00000001");


		return isoMsg;
	}

	private void postProcessMessage(ISOMsg isoMessage) throws Exception {
		final byte[] response = isoMessage.pack();
		final byte[] header = { (byte) (response.length / 256), (byte) (response.length % 256) };
		isoMessage.setHeader(header);
	}

}
