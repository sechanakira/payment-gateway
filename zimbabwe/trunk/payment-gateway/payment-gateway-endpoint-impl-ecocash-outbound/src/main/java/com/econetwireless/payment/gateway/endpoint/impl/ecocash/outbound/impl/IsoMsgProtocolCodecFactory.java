package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl;

import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

public class IsoMsgProtocolCodecFactory implements ProtocolCodecFactory {

	public ProtocolDecoder getDecoder() throws Exception {
		return new IsoMessageDecoder();
	}

	public ProtocolEncoder getEncoder() throws Exception {
		return new IsoMessageEncoder();
	}
}
