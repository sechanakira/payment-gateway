package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl;

public interface MtiRequestResponseMapper {

	String map(String requestMtiCode);

}
