package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.marshaller;

import org.jpos.iso.ISOMsg;

import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

public interface IsoMessageMarshaller {

	void marshall(ISOMsg isoMsg, TransactionResponse transactionResponse);

}
