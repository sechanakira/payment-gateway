package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.marshaller;

import java.util.Properties;

import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.IsoField;

public class MappingIsoMessageMarshaller implements IsoMessageMarshaller {

	private Properties fieldMappings;

	private static final Logger logger = LoggerFactory.getLogger(MappingIsoMessageMarshaller.class);

	public void setFieldMappings(Properties fieldMappings) {
		this.fieldMappings = fieldMappings;
	}

	@Override
	public void marshall(ISOMsg isoMsg, TransactionResponse transactionResponse) {
		logger.debug("Marshalling iso message {} using field mappings {}", isoMsg, fieldMappings);
		final BeanWrapper beanWrapper = new BeanWrapperImpl(transactionResponse);
		fieldMappings.forEach((propertyName, fieldNumber) -> setField(isoMsg, beanWrapper, (String) propertyName,
				(String) fieldNumber));
		logger.info("Marshalling result for iso msg {} is {}", isoMsg.toString(), transactionResponse);
	}

	private void setField(ISOMsg isoMsg, BeanWrapper beanWrapper, String propertyName, String fieldNumber) {
		logger.debug("Attempting to set iso field {} from property {}", fieldNumber, propertyName);
		try {
			final int fieldNo = Integer.parseInt(fieldNumber);
			final IsoField isoField = IsoField.fromFieldNumber(fieldNo);
			final Object propertyValue = beanWrapper.getPropertyValue(propertyName);
			String valueToSet;
			if (isoField == null) {
				valueToSet = propertyValue == null ? "" : propertyValue.toString();
			} else {
				valueToSet = isoField.getRawValue(propertyValue);
			}
			isoMsg.set(fieldNo, valueToSet);
			logger.debug("Set iso field {} number {} property {} to value {}", isoField, fieldNo, propertyName,
					propertyValue);
		} catch (final Exception ex) {
			logger.debug("Failed to set iso field {} from property {} due to {}", fieldNumber, propertyName, ex);
		}
	}

}
