package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.parser;

import org.jpos.iso.ISOMsg;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;

public interface IsoMessageParser {

	TransactionRequest parse(ISOMsg isoMsg);

}
