package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.parser;

import java.util.Properties;

import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.IsoField;
import com.econetwireless.payment.gateway.entities.Direction;

public class MappingIsoMessageParser implements IsoMessageParser {

	private Properties fieldMappings;

	private static final Logger logger = LoggerFactory.getLogger(MappingIsoMessageParser.class);

	public void setFieldMappings(Properties fieldMappings) {
		this.fieldMappings = fieldMappings;
	}

	@Override
	public TransactionRequest parse(ISOMsg isoMsg) {
		logger.debug("Parsing ISO msg {} using field mappings {}", isoMsg, fieldMappings);
		final TransactionRequest transactionRequest = new TransactionRequest();
		transactionRequest.setApplicationCode("ecocash"); // parameterize
		transactionRequest.setDirection(Direction.OUTBOUND);
		final BeanWrapper beanWrapper = new BeanWrapperImpl(transactionRequest);
		fieldMappings.forEach(
				(property, fieldNumber) -> setField(beanWrapper, isoMsg, (String) property, (String) fieldNumber));
		logger.info("Parsed ISO msg {} to {}", isoMsg, transactionRequest);
		return transactionRequest;
	}

	private void setField(BeanWrapper transactionRequestWrapper, ISOMsg isoMsg, String property, String fieldNumber) {
		try {
			final int isoFieldPosition = Integer.parseInt(fieldNumber);
			final IsoField isoField = IsoField.fromFieldNumber(isoFieldPosition);
			final Object rawFieldValue = isoMsg.getValue(isoFieldPosition);
			final Object valueToSet;
			if (isoField != null) {
				valueToSet = isoField.getProcessedValue(rawFieldValue);
				transactionRequestWrapper.setPropertyValue(property, valueToSet);
			} else {
				valueToSet = rawFieldValue;
				transactionRequestWrapper.setPropertyValue(property, rawFieldValue);
			}
			logger.debug("Parsed ISO field {} from property {} with value {}", fieldNumber, property, valueToSet);
		} catch (final Exception ex) {
			logger.error("Failed to parse ISO field {} from property {} for ISO message {} due to {}", fieldNumber,
					property, isoMsg, ex);
		}
	}

}
