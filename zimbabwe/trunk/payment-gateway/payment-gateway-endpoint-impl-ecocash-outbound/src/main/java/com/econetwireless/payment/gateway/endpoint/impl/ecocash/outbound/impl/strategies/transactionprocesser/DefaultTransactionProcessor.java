package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.transactionprocesser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;
import com.econetwireless.payment.gateway.endpoint.api.AggregatorTransactionExecutor;

public class DefaultTransactionProcessor implements TransactionProcessor {

	private AggregatorTransactionExecutor transactionExecutor;

	private static final Logger logger = LoggerFactory.getLogger(DefaultTransactionProcessor.class);

	public void setTransactionExecutor(AggregatorTransactionExecutor transactionExecutor) {
		this.transactionExecutor = transactionExecutor;
	}

	@Override
	public TransactionResponse process(TransactionRequest request) {
		final TransactionResponse response = transactionExecutor.execute(request);
		logger.info("Processing result for request {} is {}", request, response);
		return response;

	}

}
