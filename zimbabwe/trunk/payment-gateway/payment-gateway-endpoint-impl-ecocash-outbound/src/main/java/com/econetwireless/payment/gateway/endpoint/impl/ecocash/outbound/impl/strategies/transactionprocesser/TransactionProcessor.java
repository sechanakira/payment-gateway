package com.econetwireless.payment.gateway.endpoint.impl.ecocash.outbound.impl.strategies.transactionprocesser;

import com.econetwireless.payment.gateway.aggregator.api.TransactionRequest;
import com.econetwireless.payment.gateway.aggregator.api.TransactionResponse;

public interface TransactionProcessor {

	TransactionResponse process(TransactionRequest request);

}
