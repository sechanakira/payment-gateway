package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "aggregator")
@NamedQueries({
		@NamedQuery(name = "Aggregator:findByName", query = "Select a from Aggregator a where lower(a.name) = lower(:name)"),
		@NamedQuery(name = "Aggregator:findByStatus", query = "Select a from Aggregator a where a.active = :active"),
		@NamedQuery(name = "Aggregator:findAll", query = "Select a from Aggregator a"),
		@NamedQuery(name = "Aggregator:findByDefault", query = "Select a from Aggregator a where a.default = :default"),
		@NamedQuery(name = "Aggregator:findByDefaultAndStatus", query = "Select a from Aggregator a where a.default = :default and a.active = :active") })
public class Aggregator implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private Boolean active;
	private Boolean _default;
	private Long version;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 100, unique = true)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "active", nullable = false)
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Version
	@Column(name = "version")
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Column(name = "is_default", nullable = false)
	public Boolean getDefault() {
		return _default;
	}

	public void setDefault(Boolean _default) {
		this._default = _default;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Aggregator other = (Aggregator) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

}
