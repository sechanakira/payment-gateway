package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "aggregator_status_code_mapping")
@NamedQueries({
		@NamedQuery(name = "AggregatorStatusCodeMapping:findByAggregatorApplicationMappedCodeAndDirection", query = "Select m from AggregatorStatusCodeMapping m where m.aggregator.id = :aggregatorId and m.applicationStatusCode.application.id = :applicationId and lower(m.mappedCode) = lower(:mappedCode) and m.applicationStatusCode.direction = :direction"),
		@NamedQuery(name = "AggregatorStatusCodeMapping:findByAggregatorAndApplication", query = "Select m from AggregatorStatusCodeMapping m where m.aggregator.id = :aggregatorId and m.applicationStatusCode.application.id = :applicationId"),
		@NamedQuery(name = "AggregatorStatusCodeMapping:findByAggregatorAndApplicationStatusCode", query = "Select m from AggregatorStatusCodeMapping m where m.aggregator.id = :aggregatorId and m.applicationStatusCode.id = :applicationStatusCodeId") })
public class AggregatorStatusCodeMapping implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Aggregator aggregator;
	private ApplicationStatusCode applicationStatusCode;
	private String mappedCode;
	private Long version;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "aggregator_id", nullable = false)
	public Aggregator getAggregator() {
		return aggregator;
	}

	public void setAggregator(Aggregator aggregator) {
		this.aggregator = aggregator;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "application_status_code_id", nullable = false)
	public ApplicationStatusCode getApplicationStatusCode() {
		return applicationStatusCode;
	}

	public void setApplicationStatusCode(ApplicationStatusCode applicationStatusCode) {
		this.applicationStatusCode = applicationStatusCode;
	}

	@Column(name = "mapped_code", nullable = false, length = 15)
	public String getMappedCode() {
		return mappedCode;
	}

	public void setMappedCode(String mappedCode) {
		this.mappedCode = mappedCode;
	}

	@Version
	@Column(name = "version", nullable = false)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

}
