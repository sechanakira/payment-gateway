package com.econetwireless.payment.gateway.entities;

public enum ApiType {
	SOAP, REST, ISO;
}
