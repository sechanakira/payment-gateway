package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "application")
@NamedQueries({
		@NamedQuery(name = "Application:findByCode", query = "Select a from Application a where lower(a.code) = lower(:code)"),
		@NamedQuery(name = "Application:findByStatus", query = "Select a from Application a where a.active = :active"),
		@NamedQuery(name = "Application:findByName", query = "Select a from Application a where lower(a.name) = lower(:name)"),
		@NamedQuery(name = "Application:findAll", query = "Select a from Application a") })
public class Application implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private String code;
	private Boolean active;
	private Long version;
	private List<ApplicationInterceptor> interceptors = new ArrayList<ApplicationInterceptor>();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 50, unique = true)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "code", nullable = false, length = 15, unique = true)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "active", nullable = false)
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Version
	@Column(name = "version", nullable = false)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "application_interceptors", joinColumns = @JoinColumn(name = "application_id"))
	@OrderBy("position")
	@Embedded
	public List<ApplicationInterceptor> getInterceptors() {
		return interceptors;
	}

	public void setInterceptors(List<ApplicationInterceptor> interceptors) {
		this.interceptors = interceptors;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Application other = (Application) obj;
		if (code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!code.equals(other.code)) {
			return false;
		}
		return true;
	}

}
