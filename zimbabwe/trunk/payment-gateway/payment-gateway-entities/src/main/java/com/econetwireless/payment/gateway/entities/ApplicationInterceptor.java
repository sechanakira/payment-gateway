package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.econetwireless.common.jpa.conveters.ClassConverter;

@Embeddable
public class ApplicationInterceptor implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private Class interceptorClass;
	private Direction direction;
	private Integer position;

	@Column(name = "name", length = 30, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Convert(converter = ClassConverter.class)
	@Column(name = "interceptor_class", length = 200, nullable = false)
	public Class getInterceptorClass() {
		return interceptorClass;
	}

	public void setInterceptorClass(Class interceptorClass) {
		this.interceptorClass = interceptorClass;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "direction", nullable = false, length = 30)
	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	@Column(name = "position", nullable = false)
	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

}
