package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "currency")
@NamedQueries({
		@NamedQuery(name = "Currency:findByNumericCode", query = "Select c from Currency c where c.numericCode = :numericCode"),
		@NamedQuery(name = "Currency:findByAlphaCode", query = "Select c from Currency c where lower(c.alphaCode) = lower(:alphaCode)"),
		@NamedQuery(name = "Currency:findAll", query = "Select c from Currency c"),
		@NamedQuery(name = "Currency:findByStatus", query = "Select c from Currency c where c.active = :active") })
public class Currency implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String numericCode;
	private String alphaCode;
	private String country;
	private Boolean active;
	private Long version;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "numeric_code", nullable = false, length = 5, unique = true)
	public String getNumericCode() {
		return numericCode;
	}

	public void setNumericCode(String numericCode) {
		this.numericCode = numericCode;
	}

	@Column(name = "alpha_code", nullable = false, unique = true, length = 5)
	public String getAlphaCode() {
		return alphaCode;
	}

	public void setAlphaCode(String alphaCode) {
		this.alphaCode = alphaCode;
	}

	@Column(name = "country", nullable = false, length = 100)
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "active", nullable = false)
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Version
	@Column(name = "version", nullable = false)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alphaCode == null) ? 0 : alphaCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Currency other = (Currency) obj;
		if (alphaCode == null) {
			if (other.alphaCode != null) {
				return false;
			}
		} else if (!alphaCode.equals(other.alphaCode)) {
			return false;
		}
		return true;
	}

}
