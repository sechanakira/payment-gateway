package com.econetwireless.payment.gateway.entities;

public enum Direction {

	INBOUND, OUTBOUND, BIDIRECTIONAL;
}
