package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;

public class GatewayStatusCode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String key;
	private String code;
	private String message;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "GatewayStatusCode [key=" + key + ", code=" + code
				+ ", message=" + message + "]";
	}

	
}
