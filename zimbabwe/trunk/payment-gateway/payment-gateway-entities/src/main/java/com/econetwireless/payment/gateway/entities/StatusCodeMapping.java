package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class StatusCodeMapping implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ApplicationStatusCode applicationStatusCode;
	private String mappedCode;

	@ManyToOne(optional = false)
	@JoinColumn(name = "application_status_code_id")
	public ApplicationStatusCode getApplicationStatusCode() {
		return applicationStatusCode;
	}

	public void setApplicationStatusCode(ApplicationStatusCode applicationStatusCode) {
		this.applicationStatusCode = applicationStatusCode;
	}

	@Column(name = "mapped_code", nullable = false, length = 30)
	public String getMappedCode() {
		return mappedCode;
	}

	public void setMappedCode(String mappedCode) {
		this.mappedCode = mappedCode;
	}

	@Override
	public String toString() {
		return "StatusCodeMapping [applicationStatusCode=" + applicationStatusCode + ", mappedCode=" + mappedCode + "]";
	}
	
	

}
