package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name = "transaction")
@NamedQueries({
		@NamedQuery(name = "Transaction:findByVendorAggregatorAndSourceRef", query = "Select t from Transaction t where t.vendor.id = :vendorId and t.aggregator.id = :aggregatorId and lower(t.sourceReference) = lower(:sourceReference)"),
		@NamedQuery(name = "Transaction:findByVendorAggregatorAndDestRef", query = "Select t from Transaction t where t.vendor.id = :vendorId and t.aggregator.id = :aggregatorId and lower(t.destinationReference) = lower(:destinationReference)"),
		@NamedQuery(name = "Transaction:findBySourceRef", query = "Select t from Transaction t where lower(t.sourceReference) = lower(:sourceReference)"),
		@NamedQuery(name = "Transaction:findByGatewayRef", query = "Select t from Transaction t where lower(t.gatewayReference) = lower(:gatewayReference)"),
		@NamedQuery(name = "Transaction:findByVendorTranTypeAndStatus", query = "Select t from Transaction t where lower(t.vendor.code) = lower(:vendorCode) and lower(t.transactionType.code) = lower(:tranTypeCode) and t.transactionStatus = :status order by transactionDate") })
public class Transaction implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String sourceReference;
	private String gatewayReference;
	private String destinationReference;
	private String msisdn;
	private String msisdn2;
	private Application application;
	private TransactionType transactionType;
	private Vendor vendor;
	private Aggregator aggregator;
	private Date transactionDate;
	private TransactionStatus transactionStatus;
	private Date statusDate;
	private Long version;
	private MonetaryAmount monetaryAmount = new MonetaryAmount();
	private String sourceResponseCode;
	private String destinationResponseCode;
	private String destinationResponseMessage;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "tran_type_id", nullable = false)
	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(final TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "vendor_id", nullable = false)
	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(final Vendor vendor) {
		this.vendor = vendor;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "aggregator_id", nullable = false)
	public Aggregator getAggregator() {
		return aggregator;
	}

	public void setAggregator(final Aggregator aggregator) {
		this.aggregator = aggregator;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "transaction_date", nullable = false)
	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(final Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "transaction_status", length = 15)
	public TransactionStatus getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(final TransactionStatus transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	@Column(name = "status_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(final Date statusDate) {
		this.statusDate = statusDate;
	}

	@Version
	@Column(name = "version", nullable = false)
	public Long getVersion() {
		return version;
	}

	public void setVersion(final Long version) {
		this.version = version;
	}

	@Embedded
	public MonetaryAmount getMonetaryAmount() {
		return monetaryAmount;
	}

	public void setMonetaryAmount(final MonetaryAmount monetaryAmount) {
		this.monetaryAmount = monetaryAmount;
	}

	@Column(name = "source_ref", length = 100)
	public String getSourceReference() {
		return sourceReference;
	}

	public void setSourceReference(final String sourceReference) {
		this.sourceReference = sourceReference;
	}

	@Column(name = "dest_ref", length = 100)
	public String getDestinationReference() {
		return destinationReference;
	}

	public void setDestinationReference(final String destinationReference) {
		this.destinationReference = destinationReference;
	}

	@Column(name = "msisdn", length = 20)
	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(final String msisdn) {
		this.msisdn = msisdn;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "application_id", nullable = false)
	public Application getApplication() {
		return application;
	}

	public void setApplication(final Application application) {
		this.application = application;
	}

	@Column(name = "gateway_reference", nullable = false, length = 100, unique = true)
	public String getGatewayReference() {
		return gatewayReference;
	}

	public void setGatewayReference(final String gatewayReference) {
		this.gatewayReference = gatewayReference;
	}

	@Column(name = "dest_response_code", length = 30)
	public String getDestinationResponseCode() {
		return destinationResponseCode;
	}

	public void setDestinationResponseCode(final String destinationResponseCode) {
		this.destinationResponseCode = destinationResponseCode;
	}

	@Column(name = "dest_response_msg", length = 200)
	public String getDestinationResponseMessage() {
		return destinationResponseMessage;
	}

	public void setDestinationResponseMessage(final String destinationResponseMessage) {
		this.destinationResponseMessage = destinationResponseMessage;
	}

	@Column(name = "msisdn2", length = 20)
	public String getMsisdn2() {
		return msisdn2;
	}

	public void setMsisdn2(String msisdn2) {
		this.msisdn2 = msisdn2;
	}

	@Column(name = "source_response_code", nullable = false, length = 30)
	public String getSourceResponseCode() {
		return sourceResponseCode;
	}

	public void setSourceResponseCode(String sourceResponseCode) {
		this.sourceResponseCode = sourceResponseCode;
	}

}
