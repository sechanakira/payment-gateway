package com.econetwireless.payment.gateway.entities;

public enum TransactionStatus {
	SUCCESS, FAILED, REVERSED, PENDING;
}
