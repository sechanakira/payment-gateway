package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlElement;

import com.econetwireless.common.jpa.conveters.ClassConverter;

@Entity
@Table(name = "transaction_type")
@NamedQueries({
		@NamedQuery(name = "TransactionType:findByApplication", query = "Select t from TransactionType t where t.application.id = :applicationId"),
		@NamedQuery(name = "TransactionType:findByIds", query = "Select t from TransactionType t where t.id in :transactionTypeIds"),
		@NamedQuery(name = "TransactionType:findByApplicationAndCode", query = "Select t from TransactionType t where t.application.id  = :applicationId and lower(t.code) = lower(:code)"),
		@NamedQuery(name = "TransactionType:findByApplicationAndName", query = "Select t from TransactionType t where t.application.id = :applicationId and lower(t.name) = lower(:name)"),
		@NamedQuery(name = "TransactionType:findByStatus", query = "Select t from TransactionType t where t.active = :active"),
		@NamedQuery(name = "TransactionType:findByApplicationAndStatus", query = "Select t from TransactionType t where t.application.id = :applicationId and t.active = :active"),
		@NamedQuery(name = "TransactionType:findByApplicationAndDirection", query = "Select t from TransactionType t where t.application.id = :applicationId and t.direction = :direction"),
		@NamedQuery(name = "TransactionType:findByApplicationDirectionAndStatus", query = "Select t from TransactionType t where t.application.id = :applicationId and t.direction = :direction and t.active = :active") })
public class TransactionType implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private String code;
	private Application application;
	private Boolean active;
	private Boolean requiresAuthentication;
	private Direction direction;
	private Class handlerClass;
	private Long version;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 100, unique = true)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "code", nullable = false, length = 20, unique = true)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "active", nullable = false)
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "application_id", nullable = false)
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	@Version
	@Column(name = "version", nullable = false)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@XmlElement(name = "requires-authentication", required = true)
	@Column(name = "requires_authentication", nullable = false)
	public Boolean getRequiresAuthentication() {
		return requiresAuthentication;
	}

	public void setRequiresAuthentication(Boolean requiresAuthentication) {
		this.requiresAuthentication = requiresAuthentication;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "direction", nullable = false)
	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	@Convert(converter = ClassConverter.class)
	@Column(name = "handler_class", length = 200)
	public Class getHandlerClass() {
		return handlerClass;
	}

	public void setHandlerClass(Class handlerClass) {
		this.handlerClass = handlerClass;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((application == null) ? 0 : application.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final TransactionType other = (TransactionType) obj;
		if (application == null) {
			if (other.application != null) {
				return false;
			}
		} else if (!application.equals(other.application)) {
			return false;
		}
		if (code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!code.equals(other.code)) {
			return false;
		}
		return true;
	}

}
