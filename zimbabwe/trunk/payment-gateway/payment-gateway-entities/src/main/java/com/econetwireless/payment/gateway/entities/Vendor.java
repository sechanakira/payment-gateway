package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "vendor")
@NamedQueries({
		@NamedQuery(name = "Vendor:findByCode", query = "Select v from Vendor v where lower(v.code) = lower(:code)"),
		@NamedQuery(name = "Vendor:findByName", query = "Select v from Vendor v where lower(v.name)= lower(:name)"),
		@NamedQuery(name = "Vendor:findByStatus", query = "Select v from Vendor v where v.active = :active"),
		@NamedQuery(name = "Vendor:findAll", query = "Select v from Vendor v") })
public class Vendor implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private String code;
	private String apiKey;
	private Boolean active;
	private VendorClass vendorClass;
	private Boolean trusted;
	private String physicalAddress;
	private String contactPerson;
	private String contactNumber;
	private Date dateAdded;
	private Date statusDate;
	private String statusUser;
	private String addedBy;
	private List<VendorRoleAssignment> roleAssignments = new ArrayList<VendorRoleAssignment>();
	private List<VendorApiAssignment> apiAssignments = new ArrayList<VendorApiAssignment>();
	private Long version;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", length = 100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "code", length = 15, nullable = false, unique = true)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "api_key", length = 100, nullable = false)
	@XmlElement(name = "api-key")
	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	@Column(name = "active", nullable = false)
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@ManyToOne(optional = true)
	@JoinColumn(name = "vendor_class_id", nullable = true)
	@XmlElement(name = "vendor-class")
	public VendorClass getVendorClass() {
		return vendorClass;
	}

	public void setVendorClass(VendorClass vendorClass) {
		this.vendorClass = vendorClass;
	}

	@XmlElement(name = "trusted", required = true)
	@Column(name = "trusted", nullable = false)
	public Boolean getTrusted() {
		return trusted;
	}

	public void setTrusted(Boolean trusted) {
		this.trusted = trusted;
	}

	@OneToMany(mappedBy = "vendor")
	@XmlTransient
	public List<VendorRoleAssignment> getRoleAssignments() {
		return roleAssignments;
	}

	public void setRoleAssignments(List<VendorRoleAssignment> roleAssignments) {
		this.roleAssignments = roleAssignments;
	}

	@OneToMany(mappedBy = "vendor")
	@XmlTransient
	public List<VendorApiAssignment> getApiAssignments() {
		return apiAssignments;
	}

	public void setApiAssignments(List<VendorApiAssignment> apiAssignments) {
		this.apiAssignments = apiAssignments;
	}

	@XmlElement(name = "physical-address")
	@Column(name = "physical_address", length = 100)
	public String getPhysicalAddress() {
		return physicalAddress;
	}

	public void setPhysicalAddress(String physicalAddress) {
		this.physicalAddress = physicalAddress;
	}

	@XmlElement(name = "contact-person")
	@Column(name = "contact_person", length = 50)
	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	@XmlElement(name = "contact-number")
	@Column(name = "contact_number", length = 30)
	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	@XmlElement(name = "date-added")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_added", nullable = false)
	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	@XmlElement(name = "status-date")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "status_date", nullable = false)
	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	@XmlElement(name = "status-user")
	@Column(name = "status_user", length = 30, nullable = false)
	public String getStatusUser() {
		return statusUser;
	}

	public void setStatusUser(String statusUser) {
		this.statusUser = statusUser;
	}

	@XmlElement(name = "added-by")
	@Column(name = "added_by", length = 30, nullable = false)
	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	@Version
	@Column(name = "version", nullable = false)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Vendor other = (Vendor) obj;
		if (code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!code.equals(other.code)) {
			return false;
		}
		return true;
	}

}
