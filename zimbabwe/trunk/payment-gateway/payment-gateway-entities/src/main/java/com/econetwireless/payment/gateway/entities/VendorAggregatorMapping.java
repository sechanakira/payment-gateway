package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "vendor_aggregator_mapping")
@NamedQueries({
		@NamedQuery(name = "VendorAggregatorMapping:findByVendorAndAggregator", query = "Select m from VendorAggregatorMapping m where m.vendor.id = :vendorId and m.aggregator.id = :aggregatorId"),
		@NamedQuery(name = "VendorAggregatorMapping:findByVendorAggregatorAndStatus", query = "Select m from VendorAggregatorMapping m where m.vendor.id = :vendorId and m.aggregator.id = :aggregatorId and m.active = :active"),
		@NamedQuery(name = "VendorAggregatorMapping:findByVendor", query = "Select m from VendorAggregatorMapping m where m.vendor.id = :vendorId"),
		@NamedQuery(name = "VendorAggregatorMapping:findByStatus", query = "Select m from VendorAggregatorMapping m where m.active = :active"),
		@NamedQuery(name = "VendorAggregatorMapping:findByVendorAndStatus", query = "Select m from VendorAggregatorMapping m where m.vendor.id = :vendorId and m.active = :active") })
public class VendorAggregatorMapping implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Vendor vendor;
	private Aggregator aggregator;
	private Boolean active;
	private Long version;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "vendor_id", nullable = false)
	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "aggregator_id", nullable = false)
	public Aggregator getAggregator() {
		return aggregator;
	}

	public void setAggregator(Aggregator aggregator) {
		this.aggregator = aggregator;
	}

	@Column(name = "active", nullable = false)
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Version
	@Column(name = "version", nullable = false)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aggregator == null) ? 0 : aggregator.hashCode());
		result = prime * result + ((vendor == null) ? 0 : vendor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final VendorAggregatorMapping other = (VendorAggregatorMapping) obj;
		if (aggregator == null) {
			if (other.aggregator != null) {
				return false;
			}
		} else if (!aggregator.equals(other.aggregator)) {
			return false;
		}
		if (vendor == null) {
			if (other.vendor != null) {
				return false;
			}
		} else if (!vendor.equals(other.vendor)) {
			return false;
		}
		return true;
	}

}
