package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "vendor_api_assignment")
@NamedQueries({
		@NamedQuery(name = "VendorApiAssignment:findByVendorApplicationAndStatus", query = "Select i from VendorApiAssignment i where i.apiDefinition.application.id = :applicationId and i.vendor.id = :vendorId and i.active = :active"),
		@NamedQuery(name = "VendorApiAssignment:findByVendorApplicationAndApiType", query = "Select i from VendorApiAssignment i where i.vendor.id = :vendorId and i.apiDefinition.application.id = :applicationId and i.apiDefinition.type = :apiType"),
		@NamedQuery(name = "VendorApiAssignment:findByVendorAndApplication", query = "Select i from VendorApiAssignment i where i.vendor.id = :vendorId and i.apiDefinition.application.id = :applicationId") })
public class VendorApiAssignment implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Vendor vendor;
	private ApiDefinition apiDefinition;
	private Boolean active;
	private Long version;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "vendor_id", nullable = false)
	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "api_definition_id", nullable = false)
	public ApiDefinition getApiDefinition() {
		return apiDefinition;
	}

	public void setApiDefinition(ApiDefinition apiDefinition) {
		this.apiDefinition = apiDefinition;
	}

	@Column(name = "active", nullable = false)
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Version
	@Column(name = "version", nullable = false)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

}
