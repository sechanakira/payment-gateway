
package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name = "vendor_profile_request")
@NamedQueries({
		@NamedQuery(name = "VendorProfileRequest:findByApplicationAndStatus", query = "Select distinct v from VendorProfileRequest v left join fetch v.requestedApis where v.application.id = :applicationId and v.status = :status"),
		@NamedQuery(name = "VendorProfileRequest:findByApplicationAndName", query = "Select v from VendorProfileRequest v where v.application.id = :applicationId and lower(v.name) = :name") })
public class VendorProfileRequest implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private Application application;
	private List<ApiDefinition> requestedApis = new ArrayList<ApiDefinition>();
	private VendorProfileRequestStatus status;
	private String physicalAddress;
	private String purpose;
	private String contactPerson;
	private String contactNumber;
	private String applicationName;
	private Date dateAdded;
	private Date statusDate;
	private String statusUser;
	private String addedBy;
	private Boolean existing;
	private Long version;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 50)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToMany
	@JoinTable(name = "requested_apis", joinColumns = @JoinColumn(name = "vendor_profile_request_id"), inverseJoinColumns = @JoinColumn(name = "api_definition_id"))
	public List<ApiDefinition> getRequestedApis() {
		return requestedApis;
	}

	public void setRequestedApis(List<ApiDefinition> requestedApis) {
		this.requestedApis = requestedApis;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "application_id", nullable = false)
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "status", nullable = false)
	public VendorProfileRequestStatus getStatus() {
		return status;
	}

	public void setStatus(VendorProfileRequestStatus status) {
		this.status = status;
	}

	@Version
	@Column(name = "version")
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Column(name = "purpose", length = 200, nullable = false)
	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	@Column(name = "contact_person", length = 50)
	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	@Column(name = "contact_number", length = 100)
	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	@Column(name = "application_name", length = 100)
	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_added", nullable = false)
	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "status_date", nullable = false)
	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	@Column(name = "status_user", length = 30, nullable = false)
	public String getStatusUser() {
		return statusUser;
	}

	public void setStatusUser(String statusUser) {
		this.statusUser = statusUser;
	}

	@Column(name = "added_by", length = 30, nullable = false)
	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	@Column(name = "physical_address", length = 100)
	public String getPhysicalAddress() {
		return physicalAddress;
	}

	public void setPhysicalAddress(String physicalAddress) {
		this.physicalAddress = physicalAddress;
	}

	@Column(name = "is_existing", nullable = false)
	public Boolean getExisting() {
		return existing;
	}

	public void setExisting(Boolean existing) {
		this.existing = existing;
	}

}
