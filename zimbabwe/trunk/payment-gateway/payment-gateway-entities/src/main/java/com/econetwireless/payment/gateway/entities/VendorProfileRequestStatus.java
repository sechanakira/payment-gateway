package com.econetwireless.payment.gateway.entities;

public enum VendorProfileRequestStatus {

	PENDING, INITIATED, REJECTED, COMPLETED;
}
