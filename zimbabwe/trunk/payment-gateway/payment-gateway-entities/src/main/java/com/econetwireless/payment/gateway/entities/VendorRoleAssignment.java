package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "vendor_role_assignment")
@NamedQueries({
		@NamedQuery(name = "VendorRoleAssignment:findByVendorAndApplication", query = "Select a from VendorRoleAssignment a where a.vendor.id  = :vendorId and a.role.application.id = :applicationId"),
		@NamedQuery(name = "VendorRoleAssignment:findByVendorRoleAndStatus", query = "Select a from VendorRoleAssignment a where a.role.id = :vendorRoleId and a.active = :active") })
public class VendorRoleAssignment implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Vendor vendor;
	private VendorRole role;
	private Boolean active;
	private Long version;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "vendor_id", nullable = false)
	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "role_id", nullable = false)
	public VendorRole getRole() {
		return role;
	}

	public void setRole(VendorRole role) {
		this.role = role;
	}

	@Column(name = "active", nullable = false)
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Version
	@Column(name = "version", nullable = false)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

}
