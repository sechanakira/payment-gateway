package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name = "vendor_role_tran_type_permission")
@NamedQueries({
		@NamedQuery(name = "VendorRoleTranTypePermission:findByVendorRole", query = "Select p from VendorRoleTranTypePermission p where p.vendorRole.id = :vendorRoleId"),
		@NamedQuery(name = "VendorRoleTranTypePermission:findByVendorRoleAndTransactionType", query = "Select p from VendorRoleTranTypePermission p where p.vendorRole.id = :vendorRoleId and p.transactionType.id = :transactionTypeId") })
public class VendorRoleTranTypePermission implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private VendorRole vendorRole;
	private TransactionType transactionType;
	private Boolean active;
	private Date dateAdded;
	private Long version;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "vendor_role_id", nullable = false)
	public VendorRole getVendorRole() {
		return vendorRole;
	}

	public void setVendorRole(VendorRole vendorRole) {
		this.vendorRole = vendorRole;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "transaction_type_id", nullable = false)
	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	@Column(name = "active", nullable = false)
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_added", nullable = false)
	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	@Version
	@Column(name = "version", nullable = false)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

}
