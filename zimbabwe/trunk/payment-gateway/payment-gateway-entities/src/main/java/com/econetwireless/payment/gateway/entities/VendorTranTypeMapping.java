package com.econetwireless.payment.gateway.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.econetwireless.common.jpa.conveters.ClassConverter;

@Entity
@Table(name = "vendor_tran_type_mapping")
@NamedQueries({
		@NamedQuery(name = "VendorTranTypeMapping:findByVendorAggregatorTranTypeAndStatus", query = "Select m from VendorTranTypeMapping m where m.vendor.id = :vendorId and m.aggregator.id = :aggregatorId and m.transactionType.id = :tranTypeId and m.active = :active"),
		@NamedQuery(name = "VendorTranTypeMapping:findByVendorAndTranType", query = "Select m from VendorTranTypeMapping m where m.vendor.id = :vendorId and m.transactionType.id = :tranTypeId"),
		@NamedQuery(name = "VendorTranTypeMapping:findByVendorAggregatorAndTranType", query = "Select m from VendorTranTypeMapping m where m.vendor.id = :vendorId and m.aggregator.id = :aggregatorId and m.transactionType.id = :tranTypeId"),
		@NamedQuery(name = "VendorTranTypeMapping:findByVendor", query = "Select m from VendorTranTypeMapping m where m.vendor.id = :vendorId"),
		@NamedQuery(name = "VendorTranTypeMapping:findByAggregatorAndDirection", query = "Select m from VendorTranTypeMapping m where m.aggregator.id = :aggregatorId and m.transactionType.direction = :direction"),
		@NamedQuery(name = "VendorTranTypeMapping:findByMappedCodeAggregatorTranTypeAndStatus", query = "Select m from VendorTranTypeMapping m where lower(m.mappedVendorCode) = lower(:mappedVendorCode) and m.aggregator.id = :aggregatorId and m.transactionType.id = :tranTypeId and m.active = :active"), })
public class VendorTranTypeMapping implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Vendor vendor;
	private Aggregator aggregator;
	private TransactionType transactionType;
	private Boolean active;
	private Class handlerClass;
	private Boolean trusted;
	private String mappedVendorCode;
	private Collection<StatusCodeMapping> statusCodeMappings;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "vendor_id", nullable = false)
	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "aggregator_id", nullable = false)
	public Aggregator getAggregator() {
		return aggregator;
	}

	public void setAggregator(Aggregator aggregator) {
		this.aggregator = aggregator;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "transaction_type_id", nullable = false)
	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	@Column(name = "active", nullable = false)
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Convert(converter = ClassConverter.class)
	@Column(name = "handler_class", nullable = false, length = 150)
	public Class getHandlerClass() {
		return handlerClass;
	}

	public void setHandlerClass(Class handlerClass) {
		this.handlerClass = handlerClass;
	}

	@Column(name = "trusted", nullable = false)
	public Boolean getTrusted() {
		return trusted;
	}

	public void setTrusted(Boolean trusted) {
		this.trusted = trusted;
	}

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "vendor_tran_status_code_mapping", joinColumns = @JoinColumn(name = "vendor_tran_type_id"))
	public Collection<StatusCodeMapping> getStatusCodeMappings() {
		return statusCodeMappings;
	}

	public void setStatusCodeMappings(Collection<StatusCodeMapping> statusCodeMappings) {
		this.statusCodeMappings = statusCodeMappings;
	}

	@Column(name = "mapped_vendor_code", length = 15)
	public String getMappedVendorCode() {
		return mappedVendorCode;
	}

	public void setMappedVendorCode(String mappedVendorCode) {
		this.mappedVendorCode = mappedVendorCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aggregator == null) ? 0 : aggregator.hashCode());
		result = prime * result + ((transactionType == null) ? 0 : transactionType.hashCode());
		result = prime * result + ((vendor == null) ? 0 : vendor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final VendorTranTypeMapping other = (VendorTranTypeMapping) obj;
		if (aggregator == null) {
			if (other.aggregator != null) {
				return false;
			}
		} else if (!aggregator.equals(other.aggregator)) {
			return false;
		}
		if (transactionType == null) {
			if (other.transactionType != null) {
				return false;
			}
		} else if (!transactionType.equals(other.transactionType)) {
			return false;
		}
		if (vendor == null) {
			if (other.vendor != null) {
				return false;
			}
		} else if (!vendor.equals(other.vendor)) {
			return false;
		}
		return true;
	}

}
