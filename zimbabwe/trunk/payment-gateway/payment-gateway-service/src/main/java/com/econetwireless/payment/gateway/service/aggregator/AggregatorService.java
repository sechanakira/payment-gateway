package com.econetwireless.payment.gateway.service.aggregator;

import java.util.List;

import com.econetwireless.payment.gateway.entities.Aggregator;

public interface AggregatorService {

	Aggregator addAggregator(Aggregator aggregator);
	
	Aggregator updateAggregator(Aggregator aggregator);
	
	Aggregator findAggregatorByAggregatorId(long aggregatorId);
	
	Aggregator findAggregatorByName(String aggregatorName);
	
	List<Aggregator> findAggregatorsByStatus(boolean active);
	
	List<Aggregator> findAllAggregators();
	
	Aggregator findDefaultAggregator();
	
	
}
