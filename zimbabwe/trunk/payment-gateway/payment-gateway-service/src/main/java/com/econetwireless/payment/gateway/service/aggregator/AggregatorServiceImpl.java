package com.econetwireless.payment.gateway.service.aggregator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.dao.aggregator.AggregatorDao;
import com.econetwireless.payment.gateway.dao.vendorTranTypeMapping.VendorTranTypeMappingDao;
import com.econetwireless.payment.gateway.entities.Aggregator;
import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.entities.VendorTranTypeMapping;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;

@Service
@Transactional
public class AggregatorServiceImpl implements AggregatorService {

	@Autowired
	private AggregatorDao aggregatorDao;

	@Autowired
	private VendorTranTypeMappingDao vendorTranTypeMappingDao;
	
	@Override
	public Aggregator addAggregator(final Aggregator aggregator) {
		validateAggregator(aggregator);
		aggregator.setActive(true);
		if (aggregator.getDefault()) {
			final List<Aggregator> defaultAggregators = aggregatorDao.findByDefault(true);
			for (Aggregator defaultAggregator : defaultAggregators) {
				defaultAggregator.setDefault(false);
				final List<VendorTranTypeMapping> tranTypeMappingsByAggregator = vendorTranTypeMappingDao.findAggregatorAndDirection(defaultAggregator.getId() , Direction.INBOUND);
				for(VendorTranTypeMapping vendorTranTypeMapping : tranTypeMappingsByAggregator){
					vendorTranTypeMapping.setAggregator(aggregator);
				}
			}
		}
		return aggregatorDao.save(aggregator);
	}

	private void validateAggregator(final Aggregator aggregator) {
		Assert.notNull(aggregator, "Aggregator required");
		Assert.hasText(aggregator.getName(), "Aggregator name required");
		Assert.notNull(aggregator.getDefault(), "Aggregator default indicator required");
		if (aggregator.getId() != null) {
			Assert.notNull(aggregator.getActive(), "Aggregator status required");
		}
		final Aggregator aggregatorByName = findAggregatorByName(aggregator.getName());
		if (aggregatorByName != null && !aggregatorByName.getId().equals(aggregator.getId())) {
			throw new GatewayApplicationException("Aggregator with name " + aggregator.getName() + " already exists");
		}
	}

	@Override
	public Aggregator updateAggregator(final Aggregator aggregator) {
		validateAggregator(aggregator);
		if (aggregator.getDefault()) {
			final List<Aggregator> defaultAggregators = aggregatorDao.findByDefault(true);
			for (Aggregator defaultAggregator : defaultAggregators) {
				if (defaultAggregator.getId().equals(aggregator.getId())) {
					defaultAggregator.setDefault(false);
					final List<VendorTranTypeMapping> tranTypeMappingsByAggregator = vendorTranTypeMappingDao.findAggregatorAndDirection(defaultAggregator.getId() , Direction.INBOUND);
					for(VendorTranTypeMapping vendorTranTypeMapping : tranTypeMappingsByAggregator){
						vendorTranTypeMapping.setAggregator(aggregator);
					}
				}
			}
		}
		return aggregatorDao.save(aggregator);
	}

	@Override
	@Transactional(readOnly = true)
	public Aggregator findAggregatorByName(final String aggregatorName) {
		Assert.hasText(aggregatorName, "Aggregator name required");
		return aggregatorDao.findByName(aggregatorName);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Aggregator> findAggregatorsByStatus(final boolean active) {
		return aggregatorDao.findByStatus(active);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Aggregator> findAllAggregators() {
		return aggregatorDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Aggregator findAggregatorByAggregatorId(final long aggregatorId) {
		return aggregatorDao.findById(aggregatorId);
	}

	@Override
	@Transactional(readOnly = true)
	public Aggregator findDefaultAggregator() {
		final List<Aggregator> defaultAggregators = aggregatorDao.findByDefaultAndStatus(true, true);
		if (!defaultAggregators.isEmpty()) {
			return defaultAggregators.get(0);
		}
		return null;
	}

}
