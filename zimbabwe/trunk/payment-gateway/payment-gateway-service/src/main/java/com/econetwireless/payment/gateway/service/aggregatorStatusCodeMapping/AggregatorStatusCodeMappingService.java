package com.econetwireless.payment.gateway.service.aggregatorStatusCodeMapping;

import java.util.List;

import com.econetwireless.payment.gateway.entities.AggregatorStatusCodeMapping;
import com.econetwireless.payment.gateway.entities.Direction;

public interface AggregatorStatusCodeMappingService {

	AggregatorStatusCodeMapping addAggregatorStatusCodeMapping(AggregatorStatusCodeMapping aggregatorStatusCodeMapping);
	
	AggregatorStatusCodeMapping updateAggregatorStatusCodeMapping(AggregatorStatusCodeMapping aggregatorStatusCodeMapping);
	
	AggregatorStatusCodeMapping findAggregatorStatusCodeMappingById(long aggregatorStatusCodeMappingId);
	
	List<AggregatorStatusCodeMapping> findAggregatorStatusCodesByAggregatorAndApplication(long aggregatorId , long applicationId);
	
	AggregatorStatusCodeMapping findAggregatorStatusCodeByAggregatorApplicationMappedCodeAndDirection(long aggregatorId , long applicationId , String mappedCode , Direction direction);
	
	AggregatorStatusCodeMapping findAggregatorStatusCodeByAggregatorStatusCodeId(long aggregatorStatusCodeMappingId);
	
	AggregatorStatusCodeMapping findAggregatorStatusCodeByAggregatorAndApplicationStatusCode(long aggregatorId , long applicationStatusCodeId);
}
