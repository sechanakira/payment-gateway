package com.econetwireless.payment.gateway.service.aggregatorStatusCodeMapping;

import com.econetwireless.payment.gateway.dao.aggregatorStatusCodeMapping.AggregatorStatusCodeMappingDao;
import com.econetwireless.payment.gateway.entities.AggregatorStatusCodeMapping;
import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

@Service
@Transactional
public class AggregatorStatusCodeMappingServiceImpl implements AggregatorStatusCodeMappingService{

	@Autowired
	private AggregatorStatusCodeMappingDao aggregatorStatusCodeMappingDao;
	
	public AggregatorStatusCodeMapping addAggregatorStatusCodeMapping(
		final AggregatorStatusCodeMapping aggregatorStatusCodeMapping) {
		validateAggregatorStatusCodeMapping(aggregatorStatusCodeMapping);
		return aggregatorStatusCodeMappingDao.save(aggregatorStatusCodeMapping);
	}

	private void validateAggregatorStatusCodeMapping(
			final AggregatorStatusCodeMapping aggregatorStatusCodeMapping) {
		Assert.notNull(aggregatorStatusCodeMapping);
		Assert.hasText(aggregatorStatusCodeMapping.getMappedCode() , "Aggregator status code mapped code required");
		Assert.notNull(aggregatorStatusCodeMapping.getAggregator() , "Aggregator status code aggregator required");
		Assert.notNull(aggregatorStatusCodeMapping.getApplicationStatusCode() , "Aggregator status code application status code required");
		final AggregatorStatusCodeMapping aggregatorStatusCodeMappingByAggregatorAndApplicationStatusCode = findAggregatorStatusCodeByAggregatorAndApplicationStatusCode(aggregatorStatusCodeMapping.getAggregator().getId(), aggregatorStatusCodeMapping.getApplicationStatusCode().getId());
		if(aggregatorStatusCodeMappingByAggregatorAndApplicationStatusCode != null && !aggregatorStatusCodeMappingByAggregatorAndApplicationStatusCode.getId().equals(aggregatorStatusCodeMapping.getId())){
			throw new GatewayApplicationException("Aggregator status code already defined for aggregator " + aggregatorStatusCodeMapping.getAggregator().getName() + " , application " + aggregatorStatusCodeMapping.getApplicationStatusCode().getApplication().getName() + " , code " + aggregatorStatusCodeMapping.getApplicationStatusCode().getCode() + " and direction " + aggregatorStatusCodeMapping.getApplicationStatusCode().getDirection());
		}
		final AggregatorStatusCodeMapping aggregatorStatusCodeMappingByAggregatorApplicationAndCode = findAggregatorStatusCodeByAggregatorApplicationMappedCodeAndDirection(aggregatorStatusCodeMapping.getAggregator().getId(), aggregatorStatusCodeMapping.getApplicationStatusCode().getApplication().getId(), aggregatorStatusCodeMapping.getMappedCode() , aggregatorStatusCodeMapping.getApplicationStatusCode().getDirection());
		if(aggregatorStatusCodeMappingByAggregatorApplicationAndCode != null && !aggregatorStatusCodeMappingByAggregatorApplicationAndCode.getId().equals(aggregatorStatusCodeMapping.getId())){
			throw new GatewayApplicationException("Aggregator status code " + aggregatorStatusCodeMapping.getMappedCode() + " already mapped for aggregator " + aggregatorStatusCodeMapping.getAggregator().getName() + " and application " + aggregatorStatusCodeMapping.getApplicationStatusCode().getApplication().getName());
		}
	}

	public AggregatorStatusCodeMapping updateAggregatorStatusCodeMapping(
			final AggregatorStatusCodeMapping aggregatorStatusCodeMapping) {
		validateAggregatorStatusCodeMapping(aggregatorStatusCodeMapping);
		return aggregatorStatusCodeMappingDao.save(aggregatorStatusCodeMapping);
	}

	@Transactional(readOnly = true)
	public AggregatorStatusCodeMapping findAggregatorStatusCodeMappingById(
			final long aggregatorStatusCodeMappingId) {
		return aggregatorStatusCodeMappingDao.findById(aggregatorStatusCodeMappingId);
	}

	@Transactional(readOnly = true)
	public List<AggregatorStatusCodeMapping> findAggregatorStatusCodesByAggregatorAndApplication(
			final long aggregatorId, final long applicationId) {
		return aggregatorStatusCodeMappingDao.findByAggregatorAndApplication(aggregatorId, applicationId);
	}

	@Transactional(readOnly = true)
	public AggregatorStatusCodeMapping findAggregatorStatusCodeByAggregatorApplicationMappedCodeAndDirection(
			final long aggregatorId, final long applicationId, final String mappedCode , final Direction direction) {
		Assert.hasText(mappedCode , "Aggregator status code mapped code required");
		return aggregatorStatusCodeMappingDao.findByAggregatorApplicationMappedCodeAndDirection(aggregatorId, applicationId, mappedCode , direction);
	}

	@Transactional(readOnly = true)
	public AggregatorStatusCodeMapping findAggregatorStatusCodeByAggregatorStatusCodeId(
			final long aggregatorStatusCodeMappingId) {
		return aggregatorStatusCodeMappingDao.findById(aggregatorStatusCodeMappingId);
	}

	@Transactional(readOnly = true)
	public AggregatorStatusCodeMapping findAggregatorStatusCodeByAggregatorAndApplicationStatusCode(
			final long aggregatorId, final long applicationStatusCodeId) {
		return aggregatorStatusCodeMappingDao.findByAggregatorAndApplicationStatusCode(aggregatorId, applicationStatusCodeId);
	}

}
