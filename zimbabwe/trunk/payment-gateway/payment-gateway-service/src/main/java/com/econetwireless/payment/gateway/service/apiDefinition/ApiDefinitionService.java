package com.econetwireless.payment.gateway.service.apiDefinition;

import java.util.List;

import com.econetwireless.payment.gateway.entities.ApiDefinition;
import com.econetwireless.payment.gateway.entities.ApiType;

public interface ApiDefinitionService {

	ApiDefinition addApiDefinition(ApiDefinition apiDefinition);
	
	ApiDefinition findApiDefinitionById(long apiDefinitionId);
	
	List<ApiDefinition> findApiDefinitionsByApplication(long applicationId);
	
	ApiDefinition findApiDefinitionsByApplicationAndType(long applicationId , ApiType apiType);

	ApiDefinition updateApiDefinition(ApiDefinition apiDefinition);
}
