package com.econetwireless.payment.gateway.service.apiDefinition;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.dao.apiDefinition.ApiDefinitionDao;
import com.econetwireless.payment.gateway.entities.ApiDefinition;
import com.econetwireless.payment.gateway.entities.ApiType;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;

@Service
@Transactional(readOnly = true)
public class ApiDefinitionServiceImpl implements ApiDefinitionService{

	@Autowired
	private ApiDefinitionDao apiDefinitionDao;
	
	@Override
	@Transactional(readOnly = false)
	public ApiDefinition addApiDefinition(final ApiDefinition apiDefinition) {
		validate(apiDefinition);
		apiDefinition.setActive(true);
		return apiDefinitionDao.save(apiDefinition);
	}

	@Override
	@Transactional(readOnly = false)
	public ApiDefinition updateApiDefinition(final ApiDefinition apiDefinition) {
		validate(apiDefinition);
		return apiDefinitionDao.save(apiDefinition);
	}
	
	private void validate(final ApiDefinition apiDefinition) {
		Assert.notNull(apiDefinition , "Api definition required");
		Assert.notNull(apiDefinition.getType() , "Api type required");
		Assert.notNull(apiDefinition.getApplication() , "Api definition application required");
		if(apiDefinition.getId() != null){
			Assert.notNull(apiDefinition.getActive() , "Api definition status required");
		}
		final ApiDefinition apiDefinitionByAppAndType = apiDefinitionDao.findByApplicationAndType(apiDefinition.getApplication().getId(), apiDefinition.getType());
		if(apiDefinitionByAppAndType != null && !apiDefinitionByAppAndType.getId().equals(apiDefinition.getId())){
			throw new GatewayApplicationException("Api definition of type " + apiDefinition.getType() + " already exists for application " + apiDefinition.getApplication().getName());
		}
	}

	@Override
	public ApiDefinition findApiDefinitionById(final long apiDefinitionId) {
		return apiDefinitionDao.findById(apiDefinitionId);
	}

	@Override
	public List<ApiDefinition> findApiDefinitionsByApplication(
			final long applicationId) {
		return apiDefinitionDao.findByApplication(applicationId);
	}

	@Override
	public ApiDefinition findApiDefinitionsByApplicationAndType(
			final long applicationId, final ApiType apiType) {
		Assert.notNull(apiType , "Api type required");
		return apiDefinitionDao.findByApplicationAndType(applicationId, apiType);
	}

}
