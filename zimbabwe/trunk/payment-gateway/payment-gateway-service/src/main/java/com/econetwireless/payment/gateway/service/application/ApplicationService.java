package com.econetwireless.payment.gateway.service.application;

import java.util.List;

import com.econetwireless.payment.gateway.entities.Application;

public interface ApplicationService {

	Application addAplication(Application application);
	
	Application updateApplication(Application application);
	
	Application findApplicationById(long applicationId);
	
	Application findApplicationByCode(String applicationCode);
	
    Application findApplicationByName(String applicationName);
	
	List<Application> findApplicationsByStatus(boolean active);
	
	List<Application> findAllApplications();
}
