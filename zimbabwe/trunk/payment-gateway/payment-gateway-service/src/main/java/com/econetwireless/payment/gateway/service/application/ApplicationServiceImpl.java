package com.econetwireless.payment.gateway.service.application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.dao.application.ApplicationDao;
import com.econetwireless.payment.gateway.entities.Application;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;

@Service
@Transactional
public class ApplicationServiceImpl implements ApplicationService {

	@Autowired
	private ApplicationDao applicationDao;

	public Application addAplication(final Application application) {
		validateApplication(application);
		application.setActive(true);
		return applicationDao.save(application);
	}

	private void validateApplication(final Application application) {
		Assert.hasText(application.getCode(), "Application code required");
		Assert.hasText(application.getName(), "Application name required");
		if (application.getId() != null) {
			Assert.notNull(application, "Application status required");
		}
		final Application applicationByCode = findApplicationByCode(application.getCode());
		if (applicationByCode != null && !applicationByCode.getId().equals(application.getId())) {
			throw new GatewayApplicationException("Application with code " + application.getCode() + " already exists", "");
		}
		final Application applicationByName = findApplicationByName(application.getName());
		if (applicationByName != null && !applicationByName.getId().equals(application.getId())) {
			throw new GatewayApplicationException("Application with name " + application.getName() + " already exists", "");
		}
	}

	public Application updateApplication(final Application application) {
		validateApplication(application);
		return applicationDao.save(application);
	}

	@Transactional(readOnly = true)
	public Application findApplicationById(final long applicationId) {
		return applicationDao.findById(applicationId);
	}

	@Transactional(readOnly = true)
	public Application findApplicationByCode(final String applicationCode) {
		Assert.hasText(applicationCode, "Application code required");
		return applicationDao.findByCode(applicationCode);
	}

	@Transactional(readOnly = true)
	public List<Application> findApplicationsByStatus(final boolean active) {
		return applicationDao.findByStatus(active);
	}

	@Transactional(readOnly = true)
	public List<Application> findAllApplications() {
		return applicationDao.findAll();
	}

	@Transactional(readOnly = true)
	public Application findApplicationByName(final String applicationName) {
		Assert.hasText(applicationName, "Application name required");
		return applicationDao.findByName(applicationName);
	}

}
