package com.econetwireless.payment.gateway.service.applicationStatusCode;

import java.util.List;

import com.econetwireless.payment.gateway.entities.ApplicationStatusCode;
import com.econetwireless.payment.gateway.entities.Direction;

public interface ApplicationStatusCodeService {

	ApplicationStatusCode addApplicationStatusCode(ApplicationStatusCode applicationStatusCode);
	
	ApplicationStatusCode updateApplicationStatusCode(ApplicationStatusCode applicationStatusCode);
	
	ApplicationStatusCode findApplicationStatusCodeById(long applicationStatusCodeId);
	
	ApplicationStatusCode findApplicationStatusCodeByApplicationIdDirectionAndCode(long applicationId , Direction direction , String code);
	
	List<ApplicationStatusCode> findApplicationStatusCodeByApplicationId(long applicationId);
}
