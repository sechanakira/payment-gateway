package com.econetwireless.payment.gateway.service.applicationStatusCode;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.dao.applicationStatusCode.ApplicationStatusCodeDao;
import com.econetwireless.payment.gateway.entities.ApplicationStatusCode;
import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;

@Service
@Transactional
public class ApplicationStatusCodeServiceImpl implements ApplicationStatusCodeService{

	@Autowired
	private ApplicationStatusCodeDao applicationStatusCodeDao;
	
	public ApplicationStatusCode addApplicationStatusCode(
			final ApplicationStatusCode applicationStatusCode) {
		validateApplicationStatusCode(applicationStatusCode);
		return applicationStatusCodeDao.save(applicationStatusCode);
	}

	private void validateApplicationStatusCode(
			final ApplicationStatusCode applicationStatusCode) {
		Assert.notNull(applicationStatusCode , "Application status code required");
		Assert.hasText(applicationStatusCode.getCode() , "Application status code code required");
		Assert.notNull(applicationStatusCode.getDirection() , "Application status code direction required");
		Assert.notNull(applicationStatusCode.getApplication() , "Application status code application required");
		Assert.hasText(applicationStatusCode.getDescription() , "Application status code description required");
		ApplicationStatusCode statusCodeByAppDirectionAndCode = findApplicationStatusCodeByApplicationIdDirectionAndCode(applicationStatusCode.getApplication().getId(), applicationStatusCode.getDirection(), applicationStatusCode.getCode());
		if(statusCodeByAppDirectionAndCode != null){
			if(!statusCodeByAppDirectionAndCode.getId().equals(applicationStatusCode.getId())){
				throw new GatewayApplicationException("Application status code " + applicationStatusCode.getCode() + " already exists for " + applicationStatusCode.getApplication().getCode() + " and direction " + applicationStatusCode.getDirection());
			}
		}
		else if(applicationStatusCode.getDirection() != Direction.BIDIRECTIONAL){
			statusCodeByAppDirectionAndCode = findApplicationStatusCodeByApplicationIdDirectionAndCode(applicationStatusCode.getApplication().getId(), Direction.BIDIRECTIONAL, applicationStatusCode.getCode());
			if(statusCodeByAppDirectionAndCode != null && !statusCodeByAppDirectionAndCode.getId().equals(applicationStatusCode.getId())){
				throw new GatewayApplicationException("Application status code " + applicationStatusCode.getCode() + " already exists for " + applicationStatusCode.getApplication().getCode() + " and direction " + applicationStatusCode.getDirection());
			}
		}
	}

	public ApplicationStatusCode updateApplicationStatusCode(
			final ApplicationStatusCode applicationStatusCode) {
		validateApplicationStatusCode(applicationStatusCode);
		return applicationStatusCodeDao.save(applicationStatusCode);
	}

	@Transactional(readOnly = true)
	public ApplicationStatusCode findApplicationStatusCodeById(
			long applicationStatusCodeId) {
		return applicationStatusCodeDao.findById(applicationStatusCodeId);
	}

	@Transactional(readOnly = true)
	public ApplicationStatusCode findApplicationStatusCodeByApplicationIdDirectionAndCode(
			final long applicationId, final Direction direction, final String code) {
		Assert.hasText(code , "Application status code code required");
		Assert.notNull(direction , "Application status code direction required");
		ApplicationStatusCode applicationStatusCode = applicationStatusCodeDao.findByApplicationIdCodeAndDirection(applicationId, code, direction);
		if(applicationStatusCode == null && direction != Direction.BIDIRECTIONAL){
			applicationStatusCode = applicationStatusCodeDao.findByApplicationIdCodeAndDirection(applicationId, code, Direction.BIDIRECTIONAL);
		}
		return applicationStatusCode;
	}

	@Transactional(readOnly = true)
	public List<ApplicationStatusCode> findApplicationStatusCodeByApplicationId(
			final long applicationId) {
		return applicationStatusCodeDao.findByApplicationId(applicationId);
	}

}
