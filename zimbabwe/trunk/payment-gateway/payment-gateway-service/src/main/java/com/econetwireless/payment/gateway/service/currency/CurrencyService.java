package com.econetwireless.payment.gateway.service.currency;

import java.util.List;

import com.econetwireless.payment.gateway.entities.Currency;

public interface CurrencyService {

	Currency addCurrency(Currency currency);
	
	Currency updateCurrency(Currency currency);
	
	Currency findCurrencyByCurrencyId(long currencyId);
	
	Currency findCurrencyByNumericCode(String numericCode);
	
	Currency findCurrencyByAlphaCode(String alphaCode);
	
	List<Currency> findAllCurrencies();
	
	List<Currency> findCurrenciesByStatus(boolean active);
}
