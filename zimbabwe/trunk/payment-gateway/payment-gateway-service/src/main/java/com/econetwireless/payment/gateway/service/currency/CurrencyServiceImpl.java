package com.econetwireless.payment.gateway.service.currency;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.dao.currency.CurrencyDao;
import com.econetwireless.payment.gateway.entities.Currency;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;

@Service
@Transactional
public class CurrencyServiceImpl implements CurrencyService {

	@Autowired
	private CurrencyDao currencyDao;
	
	public Currency addCurrency(final Currency currency) {
		validateCurrency(currency);
		currency.setActive(true);
		return currencyDao.save(currency);
	}

	private void validateCurrency(final Currency currency) {
		Assert.notNull(currency , "Currency required");
		Assert.hasText(currency.getNumericCode() , "Currency numeric code required");
		Assert.hasText(currency.getAlphaCode() , "Currency alpha code required");
		Assert.hasText(currency.getCountry() , "Currency country required");
		if(currency.getId() != null){
			Assert.notNull(currency.getActive() , "Currency status required");
		}
		final Currency currencyByNumericCode = findCurrencyByNumericCode(currency.getNumericCode());
		if(currencyByNumericCode != null && !currencyByNumericCode.getId().equals(currency.getId())){
			throw new GatewayApplicationException("Currency with numeric code " + currency.getNumericCode() + " already exists");
		}
		final Currency currencyByAlphaCode = findCurrencyByAlphaCode(currency.getAlphaCode());
		if(currencyByAlphaCode != null && !currencyByAlphaCode.getId().equals(currency.getId())){
			throw new GatewayApplicationException("Currency with alpha code " + currency.getAlphaCode() + " already exists");
		}
	}

	public Currency updateCurrency(final Currency currency) {
		validateCurrency(currency);
		return currencyDao.save(currency);
	}

	@Transactional(readOnly = true)
	public Currency findCurrencyByNumericCode(final String numericCode) {
		Assert.hasText(numericCode , "Currency numeric code required");
		return currencyDao.findByNumericCode(numericCode);
	}

	@Transactional(readOnly = true)
	public Currency findCurrencyByAlphaCode(String alphaCode) {
		Assert.hasText(alphaCode , "Currency alpha code required");
		return currencyDao.findByAlphaCode(alphaCode);
	}

	@Transactional(readOnly = true)
	public List<Currency> findAllCurrencies() {
		return currencyDao.findAll();
	}

	@Transactional(readOnly = true)
	public List<Currency> findCurrenciesByStatus(final boolean active) {
		return currencyDao.findByStatus(active);
	}

	@Transactional(readOnly = true)
	public Currency findCurrencyByCurrencyId(long currencyId) {
		return currencyDao.findById(currencyId);
	}

}
