package com.econetwireless.payment.gateway.service.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionUtil {

	private ExceptionUtil() {

	}

	public static String generateStackTrace(Exception exception) {
		final StringWriter sw = new StringWriter();
		final PrintWriter pw = new PrintWriter(sw);
		exception.printStackTrace(pw);
		pw.flush();
		return sw.toString();
	}
}
