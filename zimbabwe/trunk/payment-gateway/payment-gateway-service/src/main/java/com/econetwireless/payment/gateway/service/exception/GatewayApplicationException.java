package com.econetwireless.payment.gateway.service.exception;

public class GatewayApplicationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String statusCode;

	public GatewayApplicationException(String msg, String statusCode) {
		super(msg);
		this.statusCode = statusCode;
	}

	public GatewayApplicationException(String msg) {
		this(msg, "");
	}

	public GatewayApplicationException(Throwable cause, String statusCode) {
		super(cause);
		this.statusCode = statusCode;
	}

	public String getStatusCode() {
		return statusCode;
	}

}
