package com.econetwireless.payment.gateway.service.exception;

public class GatewayRuntimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GatewayRuntimeException(String message, Throwable cause) {
		super(message, cause);

	}

	public GatewayRuntimeException(String message) {
		super(message);
	}

	public GatewayRuntimeException(Throwable cause) {
		super(cause);
	}

}
