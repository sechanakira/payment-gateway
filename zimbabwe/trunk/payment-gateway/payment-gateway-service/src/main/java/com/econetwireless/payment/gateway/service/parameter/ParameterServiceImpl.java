package com.econetwireless.payment.gateway.service.parameter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.dao.parameter.ParameterDao;
import com.econetwireless.payment.gateway.entities.Parameter;

@Service
@Transactional
public class ParameterServiceImpl implements ParameterService{

	@Autowired
	private ParameterDao parameterDao;
	
	public Parameter addParameter(final Parameter parameter) {
		validateParameter(parameter);
		return parameterDao.save(parameter);
	}

	private void validateParameter(final Parameter parameter) {
		// TODO Auto-generated method stub
		
	}

	public Parameter updateParameter(final Parameter parameter) {
		validateParameter(parameter);
		return parameterDao.save(parameter);
	}

	@Transactional(readOnly = true)
	public Parameter findParameterById(final long parameterId) {
		return parameterDao.findById(parameterId);
	}

	@Transactional(readOnly = true)
	public Parameter findParameterByName(final String parameterName) {
		Assert.hasText(parameterName , "Parameter name required");
		return parameterDao.findByName(parameterName);
	}

	@Transactional(readOnly = true)
	public String findParameterValue(final String parameterName) {
		final Parameter parameter = parameterDao.findByName(parameterName);
		if(parameter != null){
			return parameter.getValue();
		}
		return null;
	}

	@Transactional(readOnly = true)
	public List<Parameter> findAllParameters() {
		return parameterDao.findAll();
	}

	@Transactional(readOnly = true)
	public Map<String, String> findParameterValues(String... parameterNames) {
		final List<Parameter> parameterResults = parameterDao.findParametersByNames(parameterNames);
		final Map<String,String> parameterValues = new LinkedHashMap<String, String>(parameterNames.length);
		for(Parameter parameter : parameterResults){
			parameterValues.put(parameter.getName(), parameter.getValue());
		}
		return parameterValues;
	}

}
