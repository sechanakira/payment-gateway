package com.econetwireless.payment.gateway.service.statusCode;

public interface GatewayConstants {

	public interface StatusCodeKeys{
		
		String MISSING_PARAMETER = "status.code.parameter.required";
		
		String SERVER_ERROR = "status.code.server.error";
		
		String INVALID_VENDOR_PROFILE  ="status.code.invalid.vendor";
		
		String VENDOR_DEACTIVATED = "status.code.vendor.deactivated";
		
		String UNKNOWN_APPLICATION = "status.code.unknown.application";
		
		String APPLICATION_DEACTIVATED = "status.code.application.deactivated";
		
		String INVALID_TRANSACTION_TYPE = "status.code.unknown.transaction.type";
		
		String DEACTIVATED_TRANSACTION_TYPE = "status.code.transaction.type.deactivated";
		
		String NOT_PERMITTED = "status.code.not.permitted";
		
		String INVALID_PARAMETER = "status.code.invalid.parameter";
		
		String SUBMITTED_FOR_PROCESSING = "status.code.submitted";
		
		String DUPLICATE = "status.code.duplicate";
		
		
	}

	String MSISDN_REGEX = "(((\\+|00)263)|0)?7(7|8)\\d{7}";
}
