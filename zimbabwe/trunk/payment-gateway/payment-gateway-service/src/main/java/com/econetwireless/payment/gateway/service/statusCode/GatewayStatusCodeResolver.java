package com.econetwireless.payment.gateway.service.statusCode;

import com.econetwireless.payment.gateway.entities.GatewayStatusCode;

public interface GatewayStatusCodeResolver {

	GatewayStatusCode resolve(String statusCodeKey , String message);
}
