package com.econetwireless.payment.gateway.service.statusCode;

import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.econetwireless.payment.gateway.entities.GatewayStatusCode;

@Component
public class ResourceBundleGatewayStatusCodeResolver implements
		GatewayStatusCodeResolver {

	private static final Logger logger = LoggerFactory.getLogger(ResourceBundleGatewayStatusCodeResolver.class);
	
	private ResourceBundle resourceBundle;
	
	private final String RESOURCE_BUNDLE_FILE = "gateway-status-codes";

	public ResourceBundleGatewayStatusCodeResolver() {
		resourceBundle = ResourceBundle.getBundle(RESOURCE_BUNDLE_FILE);
	}

	public GatewayStatusCode resolve(final String statusCodeKey,
			final String message) {
		if(logger.isDebugEnabled()){
			logger.debug("Resolving status code key : {} , message : {} using properties file {}",statusCodeKey,message,RESOURCE_BUNDLE_FILE);
		}
		Assert.hasText(statusCodeKey, "Status code key required");
		final String propertyValue = resourceBundle.getString(statusCodeKey);
		if(logger.isDebugEnabled()){
			logger.debug("Found property value {} for key {}",propertyValue,statusCodeKey);
		}
		if (StringUtils.hasText(propertyValue)) {
			final String[] statusCodeTokens = propertyValue.split("[,;]");
			if (!ObjectUtils.isEmpty(statusCodeTokens)) {
				final GatewayStatusCode gatewayStatusCode = new GatewayStatusCode();
				gatewayStatusCode.setKey(statusCodeKey);
				gatewayStatusCode.setCode(statusCodeTokens[0]);
				if (StringUtils.isEmpty(message)) {
					if (statusCodeTokens.length > 1) {
						gatewayStatusCode.setMessage(statusCodeTokens[1]);
					}
				} else {
					gatewayStatusCode.setMessage(message);
				}
				if(logger.isInfoEnabled()){
					logger.info("Resolved status code key to {}" , gatewayStatusCode);
				}
				return gatewayStatusCode;
			}

			return null;
		}
		return null;
	}

}
