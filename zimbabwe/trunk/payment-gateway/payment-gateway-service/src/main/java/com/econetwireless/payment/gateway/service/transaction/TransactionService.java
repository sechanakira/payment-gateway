package com.econetwireless.payment.gateway.service.transaction;

import java.util.List;

import com.econetwireless.payment.gateway.entities.Transaction;
import com.econetwireless.payment.gateway.entities.TransactionStatus;

public interface TransactionService {

	Transaction addTransaction(Transaction transaction);
	
	Transaction updateTransaction(Transaction transaction);
	
	Transaction findTransactionByTransactionId(long transactionId);
	
	Transaction findTransactionByVendorAggregatorAndReference(long vendorId , long aggregatorId , String reference , boolean isSource);
	
	List<Transaction> findTransactionsByMsisdn(String msisdn , int firstResult , int maxResults);
	
	Transaction findTransactionByUniqueReference(String reference);
	
	List<Transaction> findTransactionsBySourceRef(String sourceReference);

	List<Transaction> findTransactionsByVendorTranTypeAndStatus(String string, String string2,
			TransactionStatus pending, int maximumResults);
	
	
}
