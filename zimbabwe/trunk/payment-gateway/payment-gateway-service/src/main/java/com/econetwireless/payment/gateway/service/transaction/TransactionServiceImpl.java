package com.econetwireless.payment.gateway.service.transaction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.dao.transaction.TransactionDao;
import com.econetwireless.payment.gateway.entities.Transaction;
import com.econetwireless.payment.gateway.entities.TransactionStatus;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService{

	@Autowired
	private TransactionDao transactionDao;
	
	public Transaction addTransaction(final Transaction transaction) {
		validateTransaction(transaction);
		return transactionDao.save(transaction);
	}

	private void validateTransaction(final Transaction transaction) {
		
	}

	public Transaction updateTransaction(final Transaction transaction) {
		validateTransaction(transaction);
		return transactionDao.save(transaction);
	}

	@Transactional(readOnly = true)
	public Transaction findTransactionByTransactionId(final long transactionId) {
		return transactionDao.findById(transactionId);
	}

	@Transactional(readOnly = true)
	public Transaction findTransactionByVendorAggregatorAndReference(
			final long vendorId, final long aggregatorId, final String reference, final boolean isSourceReference) {
		Assert.hasText(reference, "Reference required");
		if(isSourceReference){
			return transactionDao.findByVendorAggregatorAndSourceRef(vendorId, aggregatorId, reference);
		}
		return transactionDao.findByVendorAggregatorAndDestRef(vendorId, aggregatorId, reference);
	}

	public List<Transaction> findTransactionsByMsisdn(final String msisdn,
			final int firstResult, final int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	public Transaction findTransactionByUniqueReference(String reference) {
		return transactionDao.findByGatewayReference(reference);
	}

	public List<Transaction> findTransactionsBySourceRef(String sourceReference) {
		return transactionDao.findBySourceReference(sourceReference);
	}

	public List<Transaction> findTransactionsByVendorTranTypeAndStatus(String vendorCode, String transactionTypeCode,
			TransactionStatus transactionStatus, int maximumResults) {
		return transactionDao.findTransactionsByVendorTranTypeAndStatus(vendorCode, transactionTypeCode, transactionStatus, maximumResults);
	}

}
