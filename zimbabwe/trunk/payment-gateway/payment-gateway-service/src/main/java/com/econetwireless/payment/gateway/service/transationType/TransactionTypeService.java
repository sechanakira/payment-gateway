package com.econetwireless.payment.gateway.service.transationType;

import java.util.List;

import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.entities.TransactionType;

public interface TransactionTypeService {

	TransactionType addTransactionType(TransactionType transactionType);
	
	TransactionType updateTransactionType(TransactionType transactionType);
	
	TransactionType findTransactionTypeByTransactionTypeId(long transactionTypeId);
	
	List<TransactionType> findTransactionTypesByApplicationId(long applicationId);
	
	TransactionType findTransactionTypeByApplicationIdAndCode(long applicationId , String transactionTypeCode);
	
	TransactionType findTransactionTypeByApplicationIdAndName(long applicationId , String transactionTypeName);
	
	List<TransactionType> findTransactionTypesByStatus(boolean active);
	
	List<TransactionType> findTransactionTypeByApplicationIdAndStatus(long applicationId , boolean active);
	
	List<TransactionType> findTransactionTypesByTransactionTypeIds(final long... transactionTypeIds);
	
	List<TransactionType> findTransactionTypesByApplicationAndDirection(final long applicationId , final Direction direction);
	
	List<TransactionType> findTransactionTypesByApplicationDirectionAndStatus(final long applicationId , final Direction direction , final boolean status);
}
