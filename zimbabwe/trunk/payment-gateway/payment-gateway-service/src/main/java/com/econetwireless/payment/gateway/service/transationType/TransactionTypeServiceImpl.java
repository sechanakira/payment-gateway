package com.econetwireless.payment.gateway.service.transationType;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.aggregator.api.TransactionHandler;
import com.econetwireless.payment.gateway.dao.transactionType.TransactionTypeDao;
import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.entities.TransactionType;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;

@Service
@Transactional
public class TransactionTypeServiceImpl implements TransactionTypeService {

	@Autowired
	private TransactionTypeDao transactionTypeDao;

	public TransactionType addTransactionType(final TransactionType transactionType) {
		validateTransactionType(transactionType);
		transactionType.setActive(true);
		return transactionTypeDao.save(transactionType);
	}

	private void validateTransactionType(final TransactionType transactionType) {
		Assert.notNull(transactionType, "Transaction type required");
		Assert.hasText(transactionType.getCode(), "Transaction type code required");
		Assert.hasText(transactionType.getName(), "Transaction type name required");
		Assert.notNull(transactionType.getDirection(), "Transaction type direction required");
		Assert.notNull(transactionType.getRequiresAuthentication(),
				"Transaction type requires authentication indicator required");
		if (transactionType.getDirection() == Direction.BIDIRECTIONAL) {
			throw new GatewayApplicationException("Transaction type direction can only be INBOUND or OUTBOUND");
		}
		if (transactionType.getDirection() == Direction.INBOUND) {
			Assert.notNull(transactionType.getHandlerClass(),
					"Transaction handler class required for inbound transaction types");
			try {
				if (!TransactionHandler.class.isAssignableFrom(transactionType.getHandlerClass())) {
					throw new GatewayApplicationException(
							"Invalid transaction type handler class : " + transactionType.getHandlerClass());
				}
			} catch (final Exception ex) {
				throw new GatewayApplicationException(
						"Invalid transaction type handler class : " + transactionType.getHandlerClass());
			}
		}
		if (transactionType.getId() != null) {
			Assert.notNull(transactionType.getActive(), "Transaction type status required");
		}
		Assert.notNull(transactionType.getApplication(), "Transaction type application required");
		final TransactionType transactionTypeByApplicationAndCode = findTransactionTypeByApplicationIdAndCode(
				transactionType.getApplication().getId(), transactionType.getCode());
		if (transactionTypeByApplicationAndCode != null
				&& !transactionTypeByApplicationAndCode.getId().equals(transactionType.getId())) {
			throw new GatewayApplicationException(
					"Transaction type with code " + transactionType.getCode() + " already exists");
		}
		final TransactionType transactionTypeByApplicationAndName = findTransactionTypeByApplicationIdAndName(
				transactionType.getApplication().getId(), transactionType.getName());
		if (transactionTypeByApplicationAndName != null
				&& !transactionTypeByApplicationAndName.getId().equals(transactionType.getId())) {
			throw new GatewayApplicationException(
					"Transaction type with name " + transactionType.getName() + " already exists");
		}
	}

	public TransactionType updateTransactionType(final TransactionType transactionType) {
		validateTransactionType(transactionType);
		return transactionTypeDao.save(transactionType);
	}

	@Transactional(readOnly = true)
	public TransactionType findTransactionTypeByTransactionTypeId(final long transactionTypeId) {
		return transactionTypeDao.findById(transactionTypeId);
	}

	@Transactional(readOnly = true)
	public List<TransactionType> findTransactionTypesByApplicationId(final long applicationId) {
		return transactionTypeDao.findByApplicationId(applicationId);
	}

	@Transactional(readOnly = true)
	public TransactionType findTransactionTypeByApplicationIdAndCode(final long applicationId,
			final String transactionTypeCode) {
		Assert.hasText(transactionTypeCode, "Transaction type code required");
		return transactionTypeDao.findByApplicationIdAndCode(applicationId, transactionTypeCode);
	}

	@Transactional(readOnly = true)
	public TransactionType findTransactionTypeByApplicationIdAndName(final long applicationId,
			final String transactionTypeName) {
		Assert.hasText(transactionTypeName, "Transaction type name required");
		return transactionTypeDao.findByApplicationIdAndName(applicationId, transactionTypeName);
	}

	@Transactional(readOnly = true)
	public List<TransactionType> findTransactionTypesByStatus(final boolean active) {
		return transactionTypeDao.findByStatus(active);
	}

	@Transactional(readOnly = true)
	public List<TransactionType> findTransactionTypeByApplicationIdAndStatus(final long applicationId,
			final boolean active) {
		return transactionTypeDao.findByApplicationIdAndStatus(applicationId, active);
	}

	@Transactional(readOnly = true)
	public List<TransactionType> findTransactionTypesByTransactionTypeIds(final long... transactionTypeIds) {
		return transactionTypeDao.findByIds(transactionTypeIds);
	}

	@Transactional(readOnly = true)
	public List<TransactionType> findTransactionTypesByApplicationAndDirection(final long applicationId,
			final Direction direction) {
		Assert.notNull(direction, "Direction required");
		return transactionTypeDao.findByApplicationIdAndDirection(applicationId, direction);
	}

	@Transactional(readOnly = true)
	public List<TransactionType> findTransactionTypesByApplicationDirectionAndStatus(final long applicationId,
			final Direction direction, final boolean status) {
		return transactionTypeDao.findByApplicationIdDirectionAndStatus(applicationId, direction, status);
	}

}
