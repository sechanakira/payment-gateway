package com.econetwireless.payment.gateway.service.vendor;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.econetwireless.payment.gateway.entities.Application;
import com.econetwireless.payment.gateway.entities.Vendor;

public class VendorApplicationConfiguration implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Vendor vendor;
	private Application application;
	private List<Long> assignedRoleIds;
	private List<Long> assignedApiDefinitionIds;

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	@XmlElement(name = "assigned-role-ids")
	public List<Long> getAssignedRoleIds() {
		return assignedRoleIds;
	}

	public void setAssignedRoleIds(List<Long> assignedRoleIds) {
		this.assignedRoleIds = assignedRoleIds;
	}

	@XmlElement(name = "assigned-api-definition-ids")
	public List<Long> getAssignedApiDefinitionIds() {
		return assignedApiDefinitionIds;
	}

	public void setAssignedApiDefinitionIds(List<Long> assignedApiDefinitionIds) {
		this.assignedApiDefinitionIds = assignedApiDefinitionIds;
	}

}
