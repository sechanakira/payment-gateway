package com.econetwireless.payment.gateway.service.vendor;

import java.util.List;

import com.econetwireless.payment.gateway.entities.ApiType;
import com.econetwireless.payment.gateway.entities.Vendor;
import com.econetwireless.payment.gateway.entities.VendorApiAssignment;

public interface VendorService {

	Vendor addVendor(Vendor vendor);
	
	Vendor updateVendor(Vendor vendor);
	
	Vendor findVendorByVendorId(long vendorId);
	
	Vendor findVendorByName(String vendorName);
	
	Vendor findVendorByVendorCode(String vendorCode);
	
	List<Vendor> findAllVendors();
	
	List<Vendor> findVendorsByStatus(boolean active);
	
	List<VendorApiAssignment> findActiveVendorApiAssigmentsByApplication(long vendorId , long applicationId);
	
	VendorApiAssignment findVendorApiAssignmentByVendorApplicationAndApiType(long vendorId , long applicationId , ApiType apiType);
	
	Vendor configureVendorForApplication(final VendorApplicationConfiguration vendorConfiguration);
}
