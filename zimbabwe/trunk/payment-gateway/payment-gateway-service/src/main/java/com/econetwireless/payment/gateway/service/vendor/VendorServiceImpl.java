package com.econetwireless.payment.gateway.service.vendor;

import java.security.MessageDigest;
import java.util.List;

import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.dao.apiDefinition.ApiDefinitionDao;
import com.econetwireless.payment.gateway.dao.vendor.VendorDao;
import com.econetwireless.payment.gateway.dao.vendorApiAssignment.VendorApiAssignmentDao;
import com.econetwireless.payment.gateway.dao.vendorRole.VendorRoleDao;
import com.econetwireless.payment.gateway.dao.vendorRoleAssignment.VendorRoleAssignmentDao;
import com.econetwireless.payment.gateway.dao.vendorRoleTranTypePermission.VendorRoleTranTypePermissionDao;
import com.econetwireless.payment.gateway.dao.vendorTranTypeMapping.VendorTranTypeMappingDao;
import com.econetwireless.payment.gateway.entities.Aggregator;
import com.econetwireless.payment.gateway.entities.ApiDefinition;
import com.econetwireless.payment.gateway.entities.ApiType;
import com.econetwireless.payment.gateway.entities.TransactionType;
import com.econetwireless.payment.gateway.entities.Vendor;
import com.econetwireless.payment.gateway.entities.VendorApiAssignment;
import com.econetwireless.payment.gateway.entities.VendorRole;
import com.econetwireless.payment.gateway.entities.VendorRoleAssignment;
import com.econetwireless.payment.gateway.entities.VendorRoleTranTypePermission;
import com.econetwireless.payment.gateway.entities.VendorTranTypeMapping;
import com.econetwireless.payment.gateway.service.aggregator.AggregatorService;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;

@Service
@Transactional
public class VendorServiceImpl implements VendorService {

	@Autowired
	private VendorDao vendorDao;

	@Autowired
	private VendorApiAssignmentDao vendorApiAssignmentDao;

	@Autowired
	private ApiDefinitionDao apiDefinitionDao;

	@Autowired
	private VendorRoleDao vendorRoleDao;

	@Autowired
	private VendorRoleAssignmentDao vendorRoleAssignmentDao;

	@Autowired
	private VendorRoleTranTypePermissionDao vendorRoleTranTypePermissionDao;

	@Autowired
	private VendorTranTypeMappingDao vendorTranTypeMappingDao;

	@Autowired
	private AggregatorService aggregatorService;

	@Override
	public Vendor addVendor(final Vendor vendor) {
		validateVendor(vendor);
		vendor.setActive(true);
		String apiKey = generateVendorApiKey(vendor);
		vendor.setApiKey(apiKey);
		return vendorDao.save(vendor);
	}

	private String generateVendorApiKey(final Vendor vendor) {
		try {
			final StringBuilder keyBuilder = new StringBuilder();
			keyBuilder.append(vendor.getName());
			keyBuilder.append("_");
			keyBuilder.append(vendor.getCode());
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
			byte[] digest = messageDigest.digest(keyBuilder.toString().getBytes());
			return Hex.encodeHexString(digest);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void validateVendor(final Vendor vendor) {
		Assert.notNull(vendor, "Vendor required");
		Assert.hasText(vendor.getCode(), "Vendor code required");
		Assert.hasText(vendor.getName(), "Vendor name required");
		if (vendor.getId() != null) {
			Assert.notNull(vendor.getActive(), "Vendor status required");
			Assert.hasText(vendor.getApiKey(), "Vendor api key required");
		}
		final Vendor vendorByCode = findVendorByVendorCode(vendor.getCode());
		if (vendorByCode != null && !vendorByCode.getId().equals(vendor.getId())) {
			throw new GatewayApplicationException("Vendor with code " + vendor.getCode() + " already exists");
		}
		final Vendor vendorByName = findVendorByName(vendor.getName());
		if (vendorByName != null && !vendorByName.getId().equals(vendor.getId())) {
			throw new GatewayApplicationException("Vendor with name " + vendor.getName() + " already exists");
		}
	}

	@Override
	public Vendor updateVendor(final Vendor vendor) {
		validateVendor(vendor);
		return vendorDao.save(vendor);
	}

	@Override
	@Transactional(readOnly = true)
	public Vendor findVendorByVendorId(final long vendorId) {
		return vendorDao.findById(vendorId);
	}

	@Override
	@Transactional(readOnly = true)
	public Vendor findVendorByName(final String vendorName) {
		Assert.hasText(vendorName, "Vendor name required");
		return vendorDao.findByName(vendorName);
	}

	@Override
	@Transactional(readOnly = true)
	public Vendor findVendorByVendorCode(final String vendorCode) {
		Assert.hasText(vendorCode, "Vendor code required");
		return vendorDao.findByCode(vendorCode);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Vendor> findAllVendors() {
		return vendorDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Vendor> findVendorsByStatus(boolean active) {
		return vendorDao.findByStatus(active);
	}

	@Override
	@Transactional(readOnly = true)
	public List<VendorApiAssignment> findActiveVendorApiAssigmentsByApplication(final long vendorId,
			final long applicationId) {
		return vendorApiAssignmentDao.findByVendorApplicationAndStatus(vendorId, applicationId, true);
	}

	@Override
	@Transactional(readOnly = true)
	public VendorApiAssignment findVendorApiAssignmentByVendorApplicationAndApiType(final long vendorId,
			final long applicationId, final ApiType apiType) {
		return vendorApiAssignmentDao.findByVendorApplicationAndApiType(vendorId, applicationId, apiType);

	}

	@Override
	public Vendor configureVendorForApplication(final VendorApplicationConfiguration vendorConfiguration) {
		validateVendorApplicationConfiguration(vendorConfiguration);
		Vendor vendor = vendorConfiguration.getVendor();
		Vendor updatedVendor;
		if (vendor.getId() == null) {
			updatedVendor = addVendor(vendor);
		} else {
			updatedVendor = updateVendor(vendor);
		}
		final List<VendorRoleAssignment> existingRoleAssignments = vendorRoleAssignmentDao
				.findByVendorAndApplication(updatedVendor.getId(), vendorConfiguration.getApplication().getId());
		for (long assignedRoleId : vendorConfiguration.getAssignedRoleIds()) {
			final VendorRole vendorRole = vendorRoleDao.findById(assignedRoleId);
			boolean isExisting = false;
			for (VendorRoleAssignment existingRoleAssignment : existingRoleAssignments) {
				if (existingRoleAssignment.getRole().getId().equals(vendorRole.getId())) {
					isExisting = true;
					if (!existingRoleAssignment.getActive()) {
						existingRoleAssignment.setActive(true);
						updateVendorTranTypeMapping(updatedVendor, vendorRole, existingRoleAssignments, true);
					}
				}
			}
			if (!isExisting) {
				final VendorRoleAssignment newRoleAssignment = new VendorRoleAssignment();
				newRoleAssignment.setActive(true);
				newRoleAssignment.setRole(vendorRole);
				newRoleAssignment.setVendor(updatedVendor);
				vendorRoleAssignmentDao.save(newRoleAssignment);
				updateVendorTranTypeMapping(updatedVendor, vendorRole, existingRoleAssignments,true);
			}
		}
		for (VendorRoleAssignment existingRoleAssignment : existingRoleAssignments) {
			boolean revoked = true;
			for (long assignedRoleId : vendorConfiguration.getAssignedRoleIds()) {
				if (existingRoleAssignment.getRole().getId().equals(assignedRoleId)) {
					revoked = false;
					break;
				}
			}
			if (revoked) {
				existingRoleAssignment.setActive(false);
				updateVendorTranTypeMapping(updatedVendor, existingRoleAssignment.getRole(),existingRoleAssignments, false);
			}
		}
		final List<VendorApiAssignment> existingApiAssignments = vendorApiAssignmentDao
				.findByVendorAndApplication(updatedVendor.getId(), vendorConfiguration.getApplication().getId());
		for (long assignedApiId : vendorConfiguration.getAssignedApiDefinitionIds()) {
			final ApiDefinition apiDefinition = apiDefinitionDao.findById(assignedApiId);
			boolean isExisting = false;
			for (VendorApiAssignment existingApiAssignment : existingApiAssignments) {
				if (existingApiAssignment.getApiDefinition().getId().equals(assignedApiId)) {
					isExisting = true;
					if (!existingApiAssignment.getActive()) {
						existingApiAssignment.setActive(false);
					}
				}
			}
			if (!isExisting) {
				final VendorApiAssignment vendorApiAssignment = new VendorApiAssignment();
				vendorApiAssignment.setActive(true);
				vendorApiAssignment.setApiDefinition(apiDefinition);
				vendorApiAssignment.setVendor(updatedVendor);
				vendorApiAssignmentDao.save(vendorApiAssignment);
			}
		}
		for (VendorApiAssignment existingApiAssignment : existingApiAssignments) {
			boolean revoked = true;
			for (long assignedApiId : vendorConfiguration.getAssignedApiDefinitionIds()) {
				if (existingApiAssignment.getApiDefinition().getId().equals(assignedApiId)) {
					revoked = false;
					break;
				}
			}
			if (revoked) {
				existingApiAssignment.setActive(false);
			}
		}
		return updatedVendor;
	}

	private void updateVendorTranTypeMapping(final Vendor vendor, final VendorRole role,
			List<VendorRoleAssignment> roleAssignments, final boolean assigned) {
		final List<VendorRoleTranTypePermission> tranTypePermissions = vendorRoleTranTypePermissionDao
				.findByVendorRole(role.getId());
		final Aggregator defaultAggregator = aggregatorService.findDefaultAggregator();
		Assert.notNull(defaultAggregator, "Default aggregator not set");
		for (final VendorRoleTranTypePermission vendorRoleTranTypePermission : tranTypePermissions) {
			final VendorTranTypeMapping vendorTranTypeMapping = vendorTranTypeMappingDao
					.findByVendorAggregatorAndTranType(vendor.getId(), defaultAggregator.getId(),
							vendorRoleTranTypePermission.getTransactionType().getId());
			boolean aggregateAllowedForTranType = determineAggregateTranTypePermission(
					vendorRoleTranTypePermission.getTransactionType(), role, roleAssignments, assigned);
			if (aggregateAllowedForTranType) {
				if (vendorTranTypeMapping != null) {
					if (!vendorTranTypeMapping.getActive()) {
						vendorTranTypeMapping.setActive(true);
					}
				} else {
					VendorTranTypeMapping newMapping = new VendorTranTypeMapping();
					newMapping.setActive(true);
					newMapping.setAggregator(defaultAggregator);
					newMapping.setTransactionType(vendorRoleTranTypePermission.getTransactionType());
					newMapping.setVendor(vendor);
					newMapping.setHandlerClass(vendorRoleTranTypePermission.getTransactionType().getHandlerClass());
					vendorTranTypeMappingDao.save(vendorTranTypeMapping);
				}
			} else {
				if (vendorTranTypeMapping != null) {
					vendorTranTypeMapping.setActive(false);
				}
			}
		}
	}

	private boolean determineAggregateTranTypePermission(final TransactionType transactionType, final VendorRole role,
			final List<VendorRoleAssignment> roleAssignments, final boolean assigned) {
		boolean allowed = false;
		if (!assigned) {
			for (VendorRoleAssignment roleAssignment : roleAssignments) {
				if (!roleAssignment.getRole().getId().equals(role.getId()) && roleAssignment.getActive()) {
					final VendorRoleTranTypePermission roleTranTypePermission = vendorRoleTranTypePermissionDao
							.findByVendorRoleAndTranType(roleAssignment.getVendor().getId(), transactionType.getId());
					if (roleTranTypePermission != null && roleTranTypePermission.getActive()) {
						allowed = true;
						break;
					}
				}
			}
			return allowed;
		}
		return true;
	}

	private void validateVendorApplicationConfiguration(final VendorApplicationConfiguration vendorConfiguration) {

	}

}
