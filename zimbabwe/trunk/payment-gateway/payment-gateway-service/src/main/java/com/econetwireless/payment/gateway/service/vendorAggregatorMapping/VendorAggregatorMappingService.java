package com.econetwireless.payment.gateway.service.vendorAggregatorMapping;

import java.util.List;

import com.econetwireless.payment.gateway.entities.VendorAggregatorMapping;

public interface VendorAggregatorMappingService {

	VendorAggregatorMapping addVendorAggregatorMapping(VendorAggregatorMapping vendorAggregatorMapping);
	
	VendorAggregatorMapping updateVendorAggregatorMapping(VendorAggregatorMapping vendorAggregatorMapping);
	
	VendorAggregatorMapping findVendorAggregatorMappingByVendorAggregatorMappingId(long vendorAggregatorMappingId);
	
	VendorAggregatorMapping findVendorAggregatorMappingByVendorAndAggregator(long vendorId , long aggregatorId);
	
	List<VendorAggregatorMapping> findVendorAggregatorMappingByVendor(long vendorId);
	
	List<VendorAggregatorMapping> findVendorAggregatorMappingByVendorAndStatus(long vendorId , boolean active);
}
