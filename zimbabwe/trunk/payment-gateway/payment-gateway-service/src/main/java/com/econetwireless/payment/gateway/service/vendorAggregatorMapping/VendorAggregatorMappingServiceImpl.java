package com.econetwireless.payment.gateway.service.vendorAggregatorMapping;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.dao.vendorAggregatorMapping.VendorAggregatorMappingDao;
import com.econetwireless.payment.gateway.entities.VendorAggregatorMapping;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;

@Service
@Transactional
public class VendorAggregatorMappingServiceImpl implements VendorAggregatorMappingService{

	@Autowired
	private VendorAggregatorMappingDao vendorAggregatorMappingDao;
	
	public VendorAggregatorMapping addVendorAggregatorMapping(
			final VendorAggregatorMapping vendorAggregatorMapping) {
		validateVendorAggregatorMapping(vendorAggregatorMapping);
		vendorAggregatorMapping.setActive(true);
		disableExistingActiveMapping(vendorAggregatorMapping.getVendor().getId());
		return vendorAggregatorMappingDao.save(vendorAggregatorMapping);
	}

	private void disableExistingActiveMapping(final long vendorId) {
		final List<VendorAggregatorMapping> activeVendorAggregatorMappings = findVendorAggregatorMappingByVendorAndStatus(vendorId, true);
		if(!activeVendorAggregatorMappings.isEmpty()){
			final VendorAggregatorMapping activeMapping = activeVendorAggregatorMappings.get(0);
			activeMapping.setActive(false);
			vendorAggregatorMappingDao.save(activeMapping);
		}
	}

	private void validateVendorAggregatorMapping(
			final VendorAggregatorMapping vendorAggregatorMapping) {
		Assert.notNull(vendorAggregatorMapping , "Vendor aggregator mapping required");
		Assert.notNull(vendorAggregatorMapping.getAggregator() , "Vendor aggregator mapping aggregator required");
		Assert.notNull(vendorAggregatorMapping.getVendor() , "Vendor aggregator mapping vendor required");
		if(vendorAggregatorMapping.getId() != null){
			Assert.notNull(vendorAggregatorMapping.getActive() , "Vendor aggregator mapping status required");
		}
		final VendorAggregatorMapping vendorAggregatorMappingByVendorAndAggregator = findVendorAggregatorMappingByVendorAndAggregator(vendorAggregatorMapping.getVendor().getId(), vendorAggregatorMapping.getAggregator().getId());
		if(vendorAggregatorMappingByVendorAndAggregator != null && !vendorAggregatorMappingByVendorAndAggregator.getId().equals(vendorAggregatorMapping.getId())){
			throw new GatewayApplicationException("Vendor aggregator mapping already exists for vendor " + vendorAggregatorMapping.getVendor().getName() + " and aggregator " + vendorAggregatorMapping.getAggregator().getName());
		}
	}

	public VendorAggregatorMapping updateVendorAggregatorMapping(
			VendorAggregatorMapping vendorAggregatorMapping) {
		validateVendorAggregatorMapping(vendorAggregatorMapping);
		disableExistingActiveMapping(vendorAggregatorMapping.getVendor().getId());
		return vendorAggregatorMappingDao.save(vendorAggregatorMapping);
	}

	@Transactional(readOnly = true)
	public VendorAggregatorMapping findVendorAggregatorMappingByVendorAggregatorMappingId(
			final long vendorAggregatorMappingId) {
		return vendorAggregatorMappingDao.findById(vendorAggregatorMappingId);
	}

	@Transactional(readOnly = true)
	public VendorAggregatorMapping findVendorAggregatorMappingByVendorAndAggregator(
			final long vendorId, final long aggregatorId) {
		return vendorAggregatorMappingDao.findByVendorAndAggregator(vendorId, aggregatorId);
	}

	@Transactional(readOnly = true)
	public List<VendorAggregatorMapping> findVendorAggregatorMappingByVendor(
			final long vendorId) {
		return vendorAggregatorMappingDao.findByVendor(vendorId);
	}

	@Transactional(readOnly = true)
	public List<VendorAggregatorMapping> findVendorAggregatorMappingByVendorAndStatus(
			final long vendorId, final boolean active) {
		return vendorAggregatorMappingDao.findByVendorAndStatus(vendorId, active);
	}

}
