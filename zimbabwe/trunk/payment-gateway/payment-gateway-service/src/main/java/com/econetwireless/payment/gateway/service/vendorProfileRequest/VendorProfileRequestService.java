package com.econetwireless.payment.gateway.service.vendorProfileRequest;

import java.util.List;

import com.econetwireless.payment.gateway.entities.VendorProfileRequest;
import com.econetwireless.payment.gateway.entities.VendorProfileRequestStatus;

public interface VendorProfileRequestService {

	VendorProfileRequest addVendorProfileRequest(VendorProfileRequest vendorProfileRequest);
	
	VendorProfileRequest updateVendorProfileRequest(VendorProfileRequest vendorProfileRequest);
	
	List<VendorProfileRequest> findVendorProfileRequestByApplicationAndStatus(long applicationId , VendorProfileRequestStatus status);
	
	List<VendorProfileRequest> findVendorProfileRequestsByApplicationAndName(long applicationId , String vendorName);
	
	VendorProfileRequest findVendorRequestProfileById(long vendorProfileRequestId);
	
}
