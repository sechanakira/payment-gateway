package com.econetwireless.payment.gateway.service.vendorProfileRequest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.dao.vendorProfileRequest.VendorProfileRequestDao;
import com.econetwireless.payment.gateway.entities.VendorProfileRequest;
import com.econetwireless.payment.gateway.entities.VendorProfileRequestStatus;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;

@Service
@Transactional(readOnly = true)
public class VendorProfileRequestServiceImpl implements VendorProfileRequestService{

	@Autowired
	private VendorProfileRequestDao vendorProfileRequestDao;
	
	@Override
	@Transactional(readOnly = false)
	public VendorProfileRequest addVendorProfileRequest(
			final VendorProfileRequest vendorProfileRequest) {
		validateVendorProfileRequest(vendorProfileRequest);
		vendorProfileRequest.setStatus(VendorProfileRequestStatus.PENDING);
		return vendorProfileRequestDao.save(vendorProfileRequest);
	}

	private void validateVendorProfileRequest(
			final VendorProfileRequest vendorProfileRequest) {
		Assert.notNull(vendorProfileRequest , "Vendor profile request required");
		Assert.hasText(vendorProfileRequest.getName() , "Vendor name required");
		Assert.notNull(vendorProfileRequest.getApplication() , "Vendor profile application required");
		Assert.notNull(vendorProfileRequest.getRequestedApis() , "Vendor profile request apis required");
		Assert.hasText(vendorProfileRequest.getAddedBy() , "Vendor pofile request added by required");
		Assert.hasText(vendorProfileRequest.getStatusUser() , "Vendor profile request status user required");
		Assert.notNull(vendorProfileRequest.getDateAdded() , "Vendor profile request date added required");
		Assert.notNull(vendorProfileRequest.getStatusDate() , "Vendor profile request status date required");
		if(vendorProfileRequest.getRequestedApis().isEmpty()){
			throw new GatewayApplicationException("Vendor profile requested apis required");
		}
		if(vendorProfileRequest.getId() != null){
			Assert.notNull(vendorProfileRequest.getStatus() , "Vendor profile request status required");
		}
		final List<VendorProfileRequest> requestsByApplicationAndName = findVendorProfileRequestsByApplicationAndName(vendorProfileRequest.getApplication().getId(), vendorProfileRequest.getName());
		for(VendorProfileRequest requestByApplicationAndName : requestsByApplicationAndName){
			if(requestByApplicationAndName.getStatus() != VendorProfileRequestStatus.REJECTED){
				throw new GatewayApplicationException("Vendor profile request for " + vendorProfileRequest.getName() + " already exists");
			}
		}
	}

	@Override
	public List<VendorProfileRequest> findVendorProfileRequestByApplicationAndStatus(
			final long applicationId, final VendorProfileRequestStatus status) {
		Assert.notNull(status , "Vendor profile request status required");
		return vendorProfileRequestDao.findByApplicationAndStatus(applicationId, status);
	}

	@Override
	public List<VendorProfileRequest> findVendorProfileRequestsByApplicationAndName(
			final long applicationId, final String vendorName) {
		Assert.hasText(vendorName , "Vendor name required");
		return vendorProfileRequestDao.findByApplicationAndName(applicationId, vendorName);
	}

	@Override
	@Transactional(readOnly = false)
	public VendorProfileRequest updateVendorProfileRequest(
			final VendorProfileRequest vendorProfileRequest) {
		validateVendorProfileRequest(vendorProfileRequest);
		return vendorProfileRequestDao.save(vendorProfileRequest);
	}

	@Override
	public VendorProfileRequest findVendorRequestProfileById(final long vendorProfileRequestId) {
		final VendorProfileRequest result = vendorProfileRequestDao.findById(vendorProfileRequestId);
		if(result != null){
			result.getRequestedApis().size();
		}
		return result;
	}

}
