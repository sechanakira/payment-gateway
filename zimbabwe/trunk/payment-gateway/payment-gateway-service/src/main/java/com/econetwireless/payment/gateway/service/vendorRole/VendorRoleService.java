package com.econetwireless.payment.gateway.service.vendorRole;

import java.util.List;

import com.econetwireless.payment.gateway.entities.VendorRole;

public interface VendorRoleService {

	VendorRole addVendorRole(VendorRole vendorRole , final List<Long> allowedTxnTypes);
	
	VendorRole updateVendorRole(VendorRole vendorRole , final List<Long> allowedTxnTypes);
	
	VendorRole findVendorRoleByVendorRoleId(long vendorRoleId);
	
	List<VendorRole> findVendorRolesByApplication(long applicationId);
	
	List<VendorRole> findVendorRolesByApplicationAndStatus(long applicationId , boolean active);
	
	VendorRole findVendorRoleByApplicationAndName(long applicationId , String vendorRoleName);
}
