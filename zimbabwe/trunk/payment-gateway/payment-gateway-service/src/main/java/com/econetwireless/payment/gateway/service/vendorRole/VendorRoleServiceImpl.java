package com.econetwireless.payment.gateway.service.vendorRole;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.dao.vendorRole.VendorRoleDao;
import com.econetwireless.payment.gateway.dao.vendorRoleAssignment.VendorRoleAssignmentDao;
import com.econetwireless.payment.gateway.dao.vendorRoleTranTypePermission.VendorRoleTranTypePermissionDao;
import com.econetwireless.payment.gateway.dao.vendorTranTypeMapping.VendorTranTypeMappingDao;
import com.econetwireless.payment.gateway.entities.Aggregator;
import com.econetwireless.payment.gateway.entities.TransactionType;
import com.econetwireless.payment.gateway.entities.Vendor;
import com.econetwireless.payment.gateway.entities.VendorRole;
import com.econetwireless.payment.gateway.entities.VendorRoleAssignment;
import com.econetwireless.payment.gateway.entities.VendorRoleTranTypePermission;
import com.econetwireless.payment.gateway.entities.VendorTranTypeMapping;
import com.econetwireless.payment.gateway.service.aggregator.AggregatorService;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;
import com.econetwireless.payment.gateway.service.transationType.TransactionTypeService;

@Service
@Transactional(readOnly = true)
public class VendorRoleServiceImpl implements VendorRoleService{

	@Autowired
	private VendorRoleDao vendorRoleDao;
	
	@Autowired
	private VendorRoleTranTypePermissionDao permissionDao;
	
	@Autowired
	private  TransactionTypeService transactionTypeService;
	
	@Autowired
	private VendorRoleAssignmentDao vendorRoleAssigmentDao;
	
	@Autowired
	private VendorTranTypeMappingDao vendorTranTypeMappingDao;
	
	@Autowired
	private AggregatorService aggregatorService;
	
	@Override
	@Transactional(readOnly = false)
	public VendorRole addVendorRole(final VendorRole vendorRole , final List<Long> allowedTxnTypes) {
		validateVendorRole(vendorRole);
		final Date auditDate = new Date();
		vendorRole.setActive(true);
		final VendorRole savedVendorRole = vendorRoleDao.save(vendorRole);
		final long[] transactionTypeIds = new long[allowedTxnTypes.size()];
		for(int i = 0 ; i < allowedTxnTypes.size() ; i++){
			transactionTypeIds[i] = allowedTxnTypes.get(i);
		}
		List<TransactionType> transactionTypes = transactionTypeService.findTransactionTypesByTransactionTypeIds(transactionTypeIds);
		for(TransactionType transactionType : transactionTypes){
			VendorRoleTranTypePermission permission = new VendorRoleTranTypePermission();
			permission.setActive(true);
			permission.setDateAdded(auditDate);
			permission.setTransactionType(transactionType);
			permission.setVendorRole(savedVendorRole);
			permissionDao.save(permission);
		}
		return savedVendorRole;
		
	}

	private void validateVendorRole(final VendorRole vendorRole) {
		Assert.notNull(vendorRole , "Vendor role required");
		Assert.hasText(vendorRole.getName() , "Vendor role name required");
		Assert.notNull(vendorRole.getApplication() , "Vendor role application required");
		Assert.hasText(vendorRole.getAddedBy() , "Vendor role added by required");
		Assert.notNull(vendorRole.getDateAdded() , "Vendor role date added required");
		if(vendorRole.getId() != null){
			Assert.notNull(vendorRole.getActive() , "Vendor role status required");
			Assert.hasText(vendorRole.getUpdatedBy() , "Vendor role updated by required");
			Assert.notNull(vendorRole.getDateUpdated() , "Vendor role date updated required");
		}
		final VendorRole vendorRoleByApplicationAndName = findVendorRoleByApplicationAndName(vendorRole.getApplication().getId(), vendorRole.getName());
		if(vendorRoleByApplicationAndName != null && !vendorRoleByApplicationAndName.getId().equals(vendorRole.getId())){
			throw new GatewayApplicationException("Vendor role " + vendorRole.getName() + " already exists for application " + vendorRole.getApplication().getName());
		}
	}

	@Override
	@Transactional
	public VendorRole updateVendorRole(final VendorRole vendorRole , final List<Long> allowedTxnTypes) {
		validateVendorRole(vendorRole);
		final Date dateAdded = new Date();
		final VendorRole savedVendorRole = vendorRoleDao.save(vendorRole);
		final long[] transactionTypeIds = new long[allowedTxnTypes.size()];
		for(int i = 0 ; i < allowedTxnTypes.size() ; i++){
			transactionTypeIds[i] = allowedTxnTypes.get(i);
		}
		final List<TransactionType> transactionTypes = transactionTypeService.findTransactionTypesByTransactionTypeIds(transactionTypeIds);
	    final List<VendorRoleTranTypePermission> existingPermissions = permissionDao.findByVendorRole(vendorRole.getId());
		for(TransactionType transactionType : transactionTypes){
			boolean existing = false;
			for(VendorRoleTranTypePermission vendorRoleTranTypePermission : existingPermissions){
				if(transactionType.getId().equals(vendorRoleTranTypePermission.getTransactionType().getId())){
					existing = true;
					if(!vendorRoleTranTypePermission.getActive()){
						vendorRoleTranTypePermission.setActive(true);
						permissionDao.save(vendorRoleTranTypePermission);
						cascadePermission(vendorRoleTranTypePermission , vendorRole.getActive());
						break;
					}
				}
			}
			if(!existing){
				VendorRoleTranTypePermission newPermission = new VendorRoleTranTypePermission();
				newPermission.setDateAdded(dateAdded);
				newPermission.setTransactionType(transactionType);
				newPermission.setVendorRole(savedVendorRole);
				newPermission.setActive(true);
				permissionDao.save(newPermission);
				cascadePermission(newPermission , vendorRole.getActive());
			}
		}
	    for(VendorRoleTranTypePermission vendorRoleTranTypePermission : existingPermissions){
	    	boolean revoked = true;
	    	for(TransactionType transactionType : transactionTypes){
	    		if(vendorRoleTranTypePermission.getTransactionType().getId().equals(transactionType.getId())){
	    			revoked = false;
	    		}
	    	}
	    	if(revoked){
	    		vendorRoleTranTypePermission.setActive(false);
	    		permissionDao.save(vendorRoleTranTypePermission);
	    		cascadePermission(vendorRoleTranTypePermission , vendorRole.getActive());
	    	}
	    }
	    return savedVendorRole;
	}

	private void cascadePermission(final VendorRoleTranTypePermission vendorRoleTranTypePermission, final boolean isActiveRole) {
		final List<VendorRoleAssignment> vendorRoleAssigments = vendorRoleAssigmentDao.findByVendorRoleAndStatus(vendorRoleTranTypePermission.getVendorRole().getId() , true);
		boolean allowed = vendorRoleTranTypePermission.getActive() && isActiveRole;
			for(VendorRoleAssignment vendorRoleAssignment : vendorRoleAssigments){
				if(!allowed){
				final List<VendorRoleAssignment> assignmentsByVendor = vendorRoleAssigmentDao.findByVendorAndApplication(vendorRoleAssignment.getVendor().getId(), vendorRoleAssignment.getRole().getApplication().getId());
				for(VendorRoleAssignment roleAssignment : assignmentsByVendor){
					if(roleAssignment.getActive() && !roleAssignment.getRole().getId().equals(vendorRoleTranTypePermission.getVendorRole().getId())){
						final VendorRoleTranTypePermission tranTypePermission = permissionDao.findByVendorRoleAndTranType(roleAssignment.getRole().getId(), vendorRoleTranTypePermission.getTransactionType().getId());
						if(tranTypePermission.getActive()){
							allowed = true;
							break;
						}
					}
				}
				}
				final Aggregator defaultAggregator = aggregatorService.findDefaultAggregator();
				Assert.notNull(defaultAggregator , "Default aggregator not set up");
				final VendorTranTypeMapping vendorTranTypeMapping = vendorTranTypeMappingDao.findByVendorAggregatorAndTranType(vendorRoleAssignment.getVendor().getId(), defaultAggregator.getId(), vendorRoleTranTypePermission.getTransactionType().getId());
				if(allowed){
					if(vendorTranTypeMapping != null){
						vendorTranTypeMapping.setActive(true);
					}
					else{
						final VendorTranTypeMapping newVendorTranTypeMapping = new VendorTranTypeMapping();
						newVendorTranTypeMapping.setActive(true);
						newVendorTranTypeMapping.setAggregator(defaultAggregator);
						newVendorTranTypeMapping.setHandlerClass(vendorRoleTranTypePermission.getTransactionType().getHandlerClass());
						newVendorTranTypeMapping.setVendor(vendorRoleAssignment.getVendor());
						newVendorTranTypeMapping.setTransactionType(vendorRoleTranTypePermission.getTransactionType());
						vendorTranTypeMappingDao.save(newVendorTranTypeMapping);
					}
				}
				else if(vendorTranTypeMapping != null && vendorTranTypeMapping.getActive()){
					vendorTranTypeMapping.setActive(false);
				}
			}
		}

	@Override
	public VendorRole findVendorRoleByVendorRoleId(final long vendorRoleId) {
		return vendorRoleDao.findById(vendorRoleId);
	}

	@Override
	public List<VendorRole> findVendorRolesByApplication(final long applicationId) {
		return vendorRoleDao.findByApplication(applicationId);
	}

	@Override
	public List<VendorRole> findVendorRolesByApplicationAndStatus(
			final long applicationId, final boolean active) {
		return vendorRoleDao.findByApplicationAndStatus(applicationId, active);
	}

	@Override
	public VendorRole findVendorRoleByApplicationAndName(final long applicationId,
			final String vendorRoleName) {
		Assert.hasText(vendorRoleName , "Vendor role name required");
		return vendorRoleDao.findByApplicationAndName(applicationId, vendorRoleName);
	}

}
