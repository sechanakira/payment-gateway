package com.econetwireless.payment.gateway.service.vendorTranTypeMapping;

import java.util.List;

import com.econetwireless.payment.gateway.entities.VendorTranTypeMapping;

public interface VendorTranTypeMappingService {

	VendorTranTypeMapping addVendorTranTypeMapping(VendorTranTypeMapping vendorTranTypeMapping);

	VendorTranTypeMapping updateVendorTranTypeMapping(VendorTranTypeMapping vendorTranTypeMapping);

	VendorTranTypeMapping findVendorTranTypeMappingByVendorTranTypeMappingId(long vendorTranTypeMappingId);

	VendorTranTypeMapping findVendorTranTypeMappingByVendorAggregatorTranTypeAndStatus(long vendorId, long aggregatorId,
			long tranTypeId, boolean active);

	VendorTranTypeMapping findVendorTranTypeMappingByVendorAggregatorAndTranType(long vendorId, long aggregatorId,
			long tranTypeId);

	List<VendorTranTypeMapping> findVendorTranTypeMappingsByVendor(long vendorId);

	List<VendorTranTypeMapping> findVendorTranTypeMappingsByVendorAndTranType(long vendorId, long tranTypeId);

	VendorTranTypeMapping findByMappedVendorCodeAggregatorTranTypeAndStatus(String mappedVendorCode, long aggregatorId,
			long tranTypeId, boolean active);
}
