package com.econetwireless.payment.gateway.service.vendorTranTypeMapping;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.econetwireless.payment.gateway.dao.vendorTranTypeMapping.VendorTranTypeMappingDao;
import com.econetwireless.payment.gateway.entities.VendorTranTypeMapping;
import com.econetwireless.payment.gateway.service.exception.GatewayApplicationException;

@Service
@Transactional
public class VendorTranTypeMappingServiceImpl implements VendorTranTypeMappingService {

	@Autowired
	private VendorTranTypeMappingDao vendorTranTypeMappingDao;

	@Override
	public VendorTranTypeMapping addVendorTranTypeMapping(final VendorTranTypeMapping vendorTranTypeMapping) {
		validateTranTypeMapping(vendorTranTypeMapping);
		vendorTranTypeMapping.setActive(true);
		return vendorTranTypeMappingDao.save(vendorTranTypeMapping);
	}

	private void validateTranTypeMapping(final VendorTranTypeMapping vendorTranTypeMapping) {
		Assert.notNull(vendorTranTypeMapping, "Vendor transaction type mapping required");
		Assert.notNull(vendorTranTypeMapping.getVendor(), "Vendor transation type mapping vendor required");
		Assert.notNull(vendorTranTypeMapping.getAggregator(), "Vendor transaction type mapping aggregator required");
		if (vendorTranTypeMapping.getId() != null) {
			Assert.notNull(vendorTranTypeMapping.getActive(), "Vendor transaction type mapping status required");

		}
		Assert.notNull(vendorTranTypeMapping.getTransactionType(),
				"Vendor transaction type mapping transaction type required");
		Assert.notNull(vendorTranTypeMapping.getHandlerClass(),
				"Vendor transaction type mapping handler class required");
		final VendorTranTypeMapping vendorTranTypeByAggregatorVendorAndTranType = findVendorTranTypeMappingByVendorAggregatorAndTranType(
				vendorTranTypeMapping.getVendor().getId(), vendorTranTypeMapping.getAggregator().getId(),
				vendorTranTypeMapping.getTransactionType().getId());
		if (vendorTranTypeByAggregatorVendorAndTranType != null
				&& !vendorTranTypeByAggregatorVendorAndTranType.getId().equals(vendorTranTypeMapping.getId())) {
			throw new GatewayApplicationException(
					"Vendor transaction type mapping for vendor " + vendorTranTypeMapping.getVendor().getName()
							+ ", transaction type " + vendorTranTypeMapping.getTransactionType().getName()
							+ " and aggregator " + vendorTranTypeMapping.getAggregator().getName() + " already exists");
		}
		// TODO validate handler class
	}

	@Override
	@Transactional(readOnly = true)
	public VendorTranTypeMapping findVendorTranTypeMappingByVendorTranTypeMappingId(
			final long vendorTranTypeMappingId) {
		return vendorTranTypeMappingDao.findById(vendorTranTypeMappingId);
	}

	@Override
	@Transactional(readOnly = true)
	public VendorTranTypeMapping findVendorTranTypeMappingByVendorAggregatorTranTypeAndStatus(final long vendorId,
			final long aggregatorId, final long tranTypeId, final boolean active) {
		return vendorTranTypeMappingDao.findByVendorAggregatorTranTypeAndStatus(vendorId, aggregatorId, tranTypeId,
				active);
	}

	@Override
	@Transactional(readOnly = true)
	public VendorTranTypeMapping findVendorTranTypeMappingByVendorAggregatorAndTranType(final long vendorId,
			final long aggregatorId, final long tranTypeId) {
		return vendorTranTypeMappingDao.findByVendorAggregatorAndTranType(vendorId, aggregatorId, tranTypeId);
	}

	@Override
	@Transactional(readOnly = true)
	public List<VendorTranTypeMapping> findVendorTranTypeMappingsByVendor(final long vendorId) {
		return vendorTranTypeMappingDao.findByVendor(vendorId);
	}

	@Override
	@Transactional(readOnly = true)
	public List<VendorTranTypeMapping> findVendorTranTypeMappingsByVendorAndTranType(final long vendorId,
			final long tranTypeId) {
		return vendorTranTypeMappingDao.findByVendorAndTranType(vendorId, tranTypeId);
	}

	@Override
	public VendorTranTypeMapping updateVendorTranTypeMapping(final VendorTranTypeMapping vendorTranTypeMapping) {
		validateTranTypeMapping(vendorTranTypeMapping);
		return vendorTranTypeMappingDao.save(vendorTranTypeMapping);
	}

	public VendorTranTypeMapping findByMappedVendorCodeAggregatorTranTypeAndStatus(String mappedVendorCode,
			long aggregatorId, long tranTypeId, boolean active) {
		return vendorTranTypeMappingDao.findByMappedVendorCodeAggregatorTranTypeAndStatus(mappedVendorCode,
				aggregatorId, tranTypeId, active);
	}

}
