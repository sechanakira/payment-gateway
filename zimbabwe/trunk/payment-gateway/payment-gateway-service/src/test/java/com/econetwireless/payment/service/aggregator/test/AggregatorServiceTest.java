package com.econetwireless.payment.service.aggregator.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.payment.gateway.entities.Aggregator;
import com.econetwireless.payment.gateway.service.aggregator.AggregatorService;
import com.econetwireless.payment.service.base.test.BaseGatewayTest;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class AggregatorServiceTest extends BaseGatewayTest{

	@Autowired
	private AggregatorService aggregatorService;
	
	@Test
	@Ignore
	public void shouldSaveAggregator(){
		final Aggregator aggregator = new Aggregator();
		aggregator.setName("Econet");
		aggregatorService.addAggregator(aggregator);
	}
	
	@Test
	@Ignore
	public void shouldFindAggregatorById(){
		final Aggregator aggregatorById = aggregatorService.findAggregatorByAggregatorId(1L);
		Assert.assertNotNull(aggregatorById);
	}
	
	@Test
	@Ignore
	public void shouldUpdateAggregator(){
		final Aggregator aggregator = aggregatorService.findAggregatorByAggregatorId(1L);
		aggregator.setName("ECONET");
		aggregatorService.updateAggregator(aggregator);
	}
	
	@Test
	@Ignore
	public void shouldFindAggregatorByName(){
		final Aggregator aggregatorByName = aggregatorService.findAggregatorByName("ECONET");
		Assert.assertNotNull(aggregatorByName);
	}
	
	@Test
	@Ignore
	public void shouldFindActiveAggregators(){
		final List<Aggregator> aggregators = aggregatorService.findAggregatorsByStatus(true);
		Assert.assertEquals(1, aggregators.size());
	}
	
	@Test
	@Ignore
	public void shouldFindAllAggreggators(){
		final List<Aggregator> allAggregators = aggregatorService.findAllAggregators();
		Assert.assertEquals(1, allAggregators.size());
	}
}
