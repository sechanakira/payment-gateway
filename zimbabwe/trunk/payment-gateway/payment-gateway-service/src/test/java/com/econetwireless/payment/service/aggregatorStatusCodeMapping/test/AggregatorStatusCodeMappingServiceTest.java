package com.econetwireless.payment.service.aggregatorStatusCodeMapping.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.payment.gateway.entities.Aggregator;
import com.econetwireless.payment.gateway.entities.AggregatorStatusCodeMapping;
import com.econetwireless.payment.gateway.entities.ApplicationStatusCode;
import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.service.aggregator.AggregatorService;
import com.econetwireless.payment.gateway.service.aggregatorStatusCodeMapping.AggregatorStatusCodeMappingService;
import com.econetwireless.payment.gateway.service.applicationStatusCode.ApplicationStatusCodeService;
import com.econetwireless.payment.service.base.test.BaseGatewayTest;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class AggregatorStatusCodeMappingServiceTest extends BaseGatewayTest{

	@Autowired
	private AggregatorStatusCodeMappingService aggregatorStatusCodeMappingService;
	
	@Autowired
	private AggregatorService aggregatorService;
	
	@Autowired
	private ApplicationStatusCodeService applicationStatusCodeService;
	
	@Test
	@Ignore
	public void shouldAddAggregatorStatusCodeMapping(){
		final AggregatorStatusCodeMapping aggregatorStatusCodeMapping = new AggregatorStatusCodeMapping();
		aggregatorStatusCodeMapping.setMappedCode("100");
		final Aggregator aggregator = aggregatorService.findAggregatorByAggregatorId(1L);
		aggregatorStatusCodeMapping.setAggregator(aggregator);
		final ApplicationStatusCode applicationStatusCode = applicationStatusCodeService.findApplicationStatusCodeById(4L);
		aggregatorStatusCodeMapping.setApplicationStatusCode(applicationStatusCode);
		aggregatorStatusCodeMappingService.addAggregatorStatusCodeMapping(aggregatorStatusCodeMapping);
	}
	
	
	@Test
	@Ignore
	public void shouldFindAggregatorStatusCodeByAggregatorStatusCodeId(){
		final AggregatorStatusCodeMapping aggregatorStatusCodeMapping = aggregatorStatusCodeMappingService.findAggregatorStatusCodeByAggregatorStatusCodeId(1L);
		Assert.assertNotNull(aggregatorStatusCodeMapping);
	}
	
	@Test
	@Ignore
    public void shouldFindAggregatorStatusCodesByAggregatorAndApplication(){
		final List<AggregatorStatusCodeMapping> aggregatorStatusCodeMappingsByAggregatorAndApp = aggregatorStatusCodeMappingService.findAggregatorStatusCodesByAggregatorAndApplication(1L, 1L);
		Assert.assertEquals(1, aggregatorStatusCodeMappingsByAggregatorAndApp.size());
	}
	
	@Test
	@Ignore
	public void shouldFindAggregatorStatusCodesByAggregatorApplicationMappedCodeAndDirection(){
		AggregatorStatusCodeMapping aggregatorStatusCodesByAggregatorApplicationAndCode = aggregatorStatusCodeMappingService.findAggregatorStatusCodeByAggregatorApplicationMappedCodeAndDirection(1L, 1L, "100" , Direction.INBOUND);
		Assert.assertNotNull(aggregatorStatusCodesByAggregatorApplicationAndCode);
		aggregatorStatusCodesByAggregatorApplicationAndCode = aggregatorStatusCodeMappingService.findAggregatorStatusCodeByAggregatorApplicationMappedCodeAndDirection(1L, 1L, "00" , Direction.INBOUND);
		Assert.assertNull(aggregatorStatusCodesByAggregatorApplicationAndCode);
	}
	
	
	@Test
	@Ignore
	public void shouldFindAggregatorStatusCodeByAggregatorAndApplicationStatusCode(){
		final AggregatorStatusCodeMapping aggregatorStatusCodeMappingByAggregatorAndApplicationStatusCode = aggregatorStatusCodeMappingService.findAggregatorStatusCodeByAggregatorAndApplicationStatusCode(1L, 3L);
		Assert.assertNotNull(aggregatorStatusCodeMappingByAggregatorAndApplicationStatusCode);
	}
}
