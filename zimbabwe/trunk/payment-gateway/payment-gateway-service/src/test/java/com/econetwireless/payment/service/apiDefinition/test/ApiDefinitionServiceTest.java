package com.econetwireless.payment.service.apiDefinition.test;

import org.junit.Ignore;
import org.junit.Test;
import org.omg.CORBA.PRIVATE_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.payment.gateway.entities.ApiDefinition;
import com.econetwireless.payment.gateway.entities.ApiType;
import com.econetwireless.payment.gateway.entities.Application;
import com.econetwireless.payment.gateway.service.apiDefinition.ApiDefinitionService;
import com.econetwireless.payment.gateway.service.application.ApplicationService;
import com.econetwireless.payment.service.base.test.BaseGatewayTest;

@Ignore
public class ApiDefinitionServiceTest extends BaseGatewayTest{

	@Autowired
	private ApiDefinitionService apiDefinitionService;
	
	@Autowired
	private ApplicationService applicationService;
	
	@Test
	public void shouldSaveApiDefinition(){
		final ApiDefinition apiDefinition = new ApiDefinition();
		apiDefinition.setType(ApiType.REST);
		final Application application = applicationService.findApplicationById(1L);
		apiDefinition.setApplication(application);
		apiDefinitionService.addApiDefinition(apiDefinition);
	}
}
