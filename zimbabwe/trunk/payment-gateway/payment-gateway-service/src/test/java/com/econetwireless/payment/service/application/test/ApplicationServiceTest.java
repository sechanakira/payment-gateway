package com.econetwireless.payment.service.application.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.econetwireless.payment.gateway.config.dao.DaoConfiguration;
import com.econetwireless.payment.gateway.config.service.ServiceConfiguration;
import com.econetwireless.payment.gateway.entities.Application;
import com.econetwireless.payment.gateway.service.application.ApplicationService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DaoConfiguration.class , ServiceConfiguration.class})
public class ApplicationServiceTest {

	@Autowired
	private ApplicationService applicationService;
	
	@Test
	@Ignore
	public void shouldSaveApplication(){
		final Application application = new Application();
		application.setCode("ECOCASH1");
		application.setName("ECOCASH");
		applicationService.addAplication(application);
	}
	
	@Test
	@Ignore
	public void shouldFindApplication(){
		final Application application = applicationService.findApplicationById(1L);
		Assert.assertNotNull(application);
	}
	
	@Test
	@Ignore
	public void shouldFindApplicationByCode(){
		final Application applicationByCode = applicationService.findApplicationByCode("ecocash");
		Assert.assertNotNull(applicationByCode);
	}
	
	@Test
	@Ignore
	public void shouldFindApplicationByName(){
		final Application applicationByName = applicationService.findApplicationByName("ecocash");
		Assert.assertNotNull(applicationByName);
	}
	
	@Test
	@Ignore
	public void shouldUpdateApplication(){
		final Application application = applicationService.findApplicationById(1L);
		application.setCode("ECC");
		applicationService.updateApplication(application);
	}
	
	@Test
	@Ignore
	public void shouldFindAllApplications(){
		final List<Application> applications = applicationService.findAllApplications();
		Assert.assertEquals(1, applications.size());
	}
	
	@Test
	@Ignore
	public void shouldFindActiveApplications(){
		final List<Application> applications = applicationService.findApplicationsByStatus(true);
		Assert.assertEquals(1, applications.size());
	}
}
