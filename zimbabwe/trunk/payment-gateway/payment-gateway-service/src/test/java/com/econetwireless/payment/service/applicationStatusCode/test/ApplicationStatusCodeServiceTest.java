package com.econetwireless.payment.service.applicationStatusCode.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.payment.gateway.entities.Application;
import com.econetwireless.payment.gateway.entities.ApplicationStatusCode;
import com.econetwireless.payment.gateway.entities.Direction;
import com.econetwireless.payment.gateway.service.application.ApplicationService;
import com.econetwireless.payment.gateway.service.applicationStatusCode.ApplicationStatusCodeService;
import com.econetwireless.payment.service.base.test.BaseGatewayTest;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class ApplicationStatusCodeServiceTest extends BaseGatewayTest{

	@Autowired
	private ApplicationStatusCodeService applicationStatusCodeService;
	
	@Autowired
	private ApplicationService applicationService;
	
	@Test
	@Ignore
	public void shouldAddApplicationStatusCode(){
		final ApplicationStatusCode applicationStatusCode = new ApplicationStatusCode();
		applicationStatusCode.setCode("200");
		applicationStatusCode.setDirection(Direction.OUTBOUND);
		applicationStatusCode.setDescription("Success");
		Application application = applicationService.findApplicationById(1L);
		applicationStatusCode.setApplication(application);
		applicationStatusCodeService.addApplicationStatusCode(applicationStatusCode);
	}
	
	@Test
	@Ignore
	public void shouldFindApplicationStatusCodeById(){
		final ApplicationStatusCode applicationStatusCode = applicationStatusCodeService.findApplicationStatusCodeById(3L);
		Assert.assertNotNull(applicationStatusCode);
	}
	
	@Test
	@Ignore
	public void shouldFindApplicationStatusCodeByApplicationDirectionAndCode(){
		ApplicationStatusCode applicationStatusCode = applicationStatusCodeService.findApplicationStatusCodeByApplicationIdDirectionAndCode(1L, Direction.BIDIRECTIONAL, "200");
		Assert.assertNotNull(applicationStatusCode);
	}
	
	@Test
	@Ignore
	public void shouldFindApplicationStatusCodesByApplication(){
		final List<ApplicationStatusCode> applicationStatusCodes = applicationStatusCodeService.findApplicationStatusCodeByApplicationId(1L);
		Assert.assertEquals(1, applicationStatusCodes.size());
	}
	
	@Test
	@Ignore
	public void shouldFindApplicationStatusCodeByApplicationStatusCodeId(){
		final ApplicationStatusCode applicationStatusCode = applicationStatusCodeService.findApplicationStatusCodeById(3L);
		Assert.assertNotNull(applicationStatusCode);
	}
	
}
