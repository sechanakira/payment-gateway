package com.econetwireless.payment.service.base.test;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.econetwireless.payment.gateway.config.dao.DaoConfiguration;
import com.econetwireless.payment.gateway.config.service.ServiceConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DaoConfiguration.class , ServiceConfiguration.class})
public abstract class BaseGatewayTest {

}
