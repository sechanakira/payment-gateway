package com.econetwireless.payment.service.currency.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.econetwireless.payment.gateway.entities.Currency;
import com.econetwireless.payment.gateway.service.currency.CurrencyService;
import com.econetwireless.payment.service.base.test.BaseGatewayTest;

public class CurrencyServiceTest extends BaseGatewayTest{

	@Autowired
	private CurrencyService currencyService;
	
	
	@Test
	@Ignore
	public void shouldSaveCurrency(){
		final Currency currency = new Currency();
		currency.setNumericCode("1233");
		currency.setAlphaCode("ZWD");
		currency.setCountry("ZIM");
		currencyService.addCurrency(currency);
	}
	
	@Test
	@Ignore
	public void shouldFindCurrencyById(){
		final Currency currencyById = currencyService.findCurrencyByCurrencyId(1L);
		Assert.assertNotNull(currencyById);
	}
	
	@Test
	@Ignore
	public void shouldFindCurrencyByNumericCode(){
		final Currency currencyByNumericCode = currencyService.findCurrencyByNumericCode("123");
		Assert.assertNotNull(currencyByNumericCode);
	}
	
	@Test
	@Ignore
	public void shouldFindCurrencyByAlphaCode(){
		final Currency currencyByAlphaCode = currencyService.findCurrencyByAlphaCode("ZWD");
		Assert.assertNotNull(currencyByAlphaCode);
	}
	
	@Test
	@Ignore
	public void shouldUpdateCurrency(){
		final Currency currency = currencyService.findCurrencyByCurrencyId(1L);
		currency.setNumericCode("1234");
		currencyService.updateCurrency(currency);
	}
	
	@Test
	@Ignore
	public void shouldFindAllCurrencies(){
		final List<Currency> allCurrencies = currencyService.findAllCurrencies();
		Assert.assertEquals(1, allCurrencies.size());
	}
	
	@Test
	@Ignore
	public void shouldFindActiveCurrencies(){
		final List<Currency> activeCurrencies = currencyService.findCurrenciesByStatus(true);
		Assert.assertEquals(1, activeCurrencies.size());
	}
}
