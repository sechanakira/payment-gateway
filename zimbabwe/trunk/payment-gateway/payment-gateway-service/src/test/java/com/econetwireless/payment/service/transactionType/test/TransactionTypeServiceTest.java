package com.econetwireless.payment.service.transactionType.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.payment.gateway.entities.Application;
import com.econetwireless.payment.gateway.entities.TransactionType;
import com.econetwireless.payment.gateway.service.application.ApplicationService;
import com.econetwireless.payment.gateway.service.transationType.TransactionTypeService;
import com.econetwireless.payment.service.base.test.BaseGatewayTest;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class TransactionTypeServiceTest extends BaseGatewayTest{

	@Autowired
	private TransactionTypeService transactionTypeService;
	
	@Autowired
	private ApplicationService applicationService;
	
	@Test
	@Ignore
	public void shouldAddTransactionType(){
		final TransactionType transactionType = new TransactionType();
		transactionType.setCode("01");
		transactionType.setName("Payment");
		Application application = applicationService.findApplicationById(1L);
		transactionType.setApplication(application);
		transactionTypeService.addTransactionType(transactionType);
	}
	
	@Test
	@Ignore
	public void shouldFindTransactionTypeById(){
		final TransactionType transactionTypeId = transactionTypeService.findTransactionTypeByTransactionTypeId(1L);
		Assert.assertNotNull(transactionTypeId);
	}
	
	@Test
	@Ignore
	public void shouldFindTransactionTypeByApplicationIdAndCode(){
		final TransactionType transactionTypeByAppAndCode = transactionTypeService.findTransactionTypeByApplicationIdAndCode(1L, "01");
		Assert.assertNotNull(transactionTypeByAppAndCode);
	}
	
	@Test
	@Ignore
	public void shouldFindTransactionTypeByApplicationIdAndName(){
		final TransactionType transactionTypeByAppAndName = transactionTypeService.findTransactionTypeByApplicationIdAndName(1L, "Payment");
		Assert.assertNotNull(transactionTypeByAppAndName);
	}
	
	@Test
	@Ignore
	public void shouldFindApplicationTransactionTypes(){
		final List<TransactionType> applicationTranTypes = transactionTypeService.findTransactionTypesByApplicationId(1L);
		Assert.assertEquals(1 , applicationTranTypes.size());
	}
	
	@Test
	@Ignore
	public void shouldFindActiveApplicationTransactionTypes(){
		final List<TransactionType> applicationTypes = transactionTypeService.findTransactionTypeByApplicationIdAndStatus(1L, true);
		Assert.assertEquals(1, applicationTypes.size());
	}
	
	@Test
	@Ignore
	public void shouldFindActiveTransactionTypes(){
		final List<TransactionType> activeTranTypes = transactionTypeService.findTransactionTypesByStatus(true);
		Assert.assertEquals(1, activeTranTypes.size());
	}
}
