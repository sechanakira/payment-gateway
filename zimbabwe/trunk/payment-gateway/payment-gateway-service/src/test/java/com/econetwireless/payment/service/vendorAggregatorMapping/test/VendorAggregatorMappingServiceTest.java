package com.econetwireless.payment.service.vendorAggregatorMapping.test;

import com.econetwireless.payment.gateway.entities.Aggregator;
import com.econetwireless.payment.gateway.entities.Vendor;
import com.econetwireless.payment.gateway.entities.VendorAggregatorMapping;
import com.econetwireless.payment.gateway.service.aggregator.AggregatorService;
import com.econetwireless.payment.gateway.service.vendor.VendorService;
import com.econetwireless.payment.gateway.service.vendorAggregatorMapping.VendorAggregatorMappingService;
import com.econetwireless.payment.service.base.test.BaseGatewayTest;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class VendorAggregatorMappingServiceTest extends BaseGatewayTest{

	@Autowired
	private VendorAggregatorMappingService vendorAggregatorMappingService;
	
	@Autowired
	private AggregatorService aggregatorService;
	
	@Autowired
	private VendorService vendorService;
	
	@Test
	@Ignore
	public void shouldAddVendorAggregatorMapping(){
		final VendorAggregatorMapping vendorAggregatorMapping = new VendorAggregatorMapping();
		final Aggregator aggregator = aggregatorService.findAggregatorByAggregatorId(1L);
		final Vendor vendor = vendorService.findVendorByVendorId(2L);
		vendorAggregatorMapping.setVendor(vendor);
		vendorAggregatorMapping.setAggregator(aggregator);
		vendorAggregatorMappingService.addVendorAggregatorMapping(vendorAggregatorMapping);
	}
	
	@Test
	@Ignore
	public void shouldFindVendorAggregatorMappingByVendorAndAggregator(){
		final VendorAggregatorMapping vendorAggregatorMapping = vendorAggregatorMappingService.findVendorAggregatorMappingByVendorAndAggregator(2L, 1L);
		Assert.assertNotNull(vendorAggregatorMapping);
	}
	
	@Test
	@Ignore
	public void shouldFindVendorAggregatorMappingByVendorAggregatorMappingId(){
		final VendorAggregatorMapping vendorAggregatorMappingById = vendorAggregatorMappingService.findVendorAggregatorMappingByVendorAggregatorMappingId(1L);
		Assert.assertNotNull(vendorAggregatorMappingById);
	}
	
	@Test
	@Ignore
	public void shouldFindVendorAggregatorMappingsByVendor(){
		final List<VendorAggregatorMapping> vendorAggregatorMappings = vendorAggregatorMappingService.findVendorAggregatorMappingByVendor(2L);
		Assert.assertEquals(1, vendorAggregatorMappings.size());
	}
	
	@Test
    @Ignore

    public void shouldFindVendorAggregatorMappingByVendorAndStatus(){
		final List<VendorAggregatorMapping> activeVendorAggregatorMappings = vendorAggregatorMappingService.findVendorAggregatorMappingByVendorAndStatus(2L, true);
		Assert.assertEquals(1, activeVendorAggregatorMappings.size());
	}
}
