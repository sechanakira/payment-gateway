package com.econetwireless.payment.service.vendorTranTypeMapping.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.payment.gateway.entities.Aggregator;
import com.econetwireless.payment.gateway.entities.TransactionType;
import com.econetwireless.payment.gateway.entities.Vendor;
import com.econetwireless.payment.gateway.entities.VendorTranTypeMapping;
import com.econetwireless.payment.gateway.service.aggregator.AggregatorService;
import com.econetwireless.payment.gateway.service.transationType.TransactionTypeService;
import com.econetwireless.payment.gateway.service.vendor.VendorService;
import com.econetwireless.payment.gateway.service.vendorTranTypeMapping.VendorTranTypeMappingService;
import com.econetwireless.payment.service.base.test.BaseGatewayTest;

public class VendorTranTypeMappingTest extends BaseGatewayTest {

	@Autowired
	private VendorTranTypeMappingService vendorTranTypeMappingService;

	@Autowired
	private VendorService vendorService;

	@Autowired
	private AggregatorService aggregatorService;

	@Autowired
	private TransactionTypeService transactionTypeService;

	@Test
	@Ignore
	public void shouldAddVendorTranTypeMapping() {
		final VendorTranTypeMapping vendorTranTypeMapping = new VendorTranTypeMapping();
		final Vendor vendor = vendorService.findVendorByVendorId(2L);
		vendorTranTypeMapping.setVendor(vendor);
		final Aggregator aggregator = aggregatorService.findAggregatorByAggregatorId(1L);
		vendorTranTypeMapping.setAggregator(aggregator);
		final TransactionType transactionType = transactionTypeService.findTransactionTypeByTransactionTypeId(1L);
		vendorTranTypeMapping.setTransactionType(transactionType);
		// vendorTranTypeMapping.setDirection(Direction.INBOUND);
		// vendorTranTypeMapping.setHandlerClass("java.lang.String");
		vendorTranTypeMappingService.addVendorTranTypeMapping(vendorTranTypeMapping);
	}

	@Test
	@Ignore
	public void shouldFindVendorTranTypeMappingByVendorAggregatorTranTypeDirectionAndStatus() {
		final VendorTranTypeMapping vendorTranTypeMapping = vendorTranTypeMappingService
				.findVendorTranTypeMappingByVendorAggregatorTranTypeAndStatus(2L, 1L, 1L, true);
		Assert.assertNotNull(vendorTranTypeMapping);
	}

	@Test
	@Ignore
	public void shouldFindVendorTranTypeMappingByVendorTranTypeMappingId() {
		final VendorTranTypeMapping vendorTranTypeMapping = vendorTranTypeMappingService
				.findVendorTranTypeMappingByVendorTranTypeMappingId(1L);
		Assert.assertNotNull(vendorTranTypeMapping);
	}

	@Test
	@Ignore
	public void shouldFindVendorTranTypeMappingByVendorAggregatorTranTypeAndDirection() {
		final VendorTranTypeMapping vendorTranTypeMapping = vendorTranTypeMappingService
				.findVendorTranTypeMappingByVendorAggregatorAndTranType(2L, 1L, 1L);
		Assert.assertNotNull(vendorTranTypeMapping);
	}

	@Test
	@Ignore
	public void shouldFindVendorTranTypeMappingByVendor() {
		final List<VendorTranTypeMapping> vendorTranTypeMappingsByVendor = vendorTranTypeMappingService
				.findVendorTranTypeMappingsByVendor(2L);
		Assert.assertEquals(1, vendorTranTypeMappingsByVendor.size());
	}

	@Test
	@Ignore
	public void shouldFindVendorTranTypeMappingByVendorAndTranType() {
		final List<VendorTranTypeMapping> vendorTranTypeMappings = vendorTranTypeMappingService
				.findVendorTranTypeMappingsByVendorAndTranType(2L, 1L);
		Assert.assertEquals(1, vendorTranTypeMappings.size());
	}
}
