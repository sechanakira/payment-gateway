package com.econetwireless.payment.gateway.vendor.impl.kwese.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by shingirai on 9/27/16.
 */
@Configuration
@ComponentScan(basePackages = {"com.econetwireless.payment.gateway.vendor.impl.kwese"})
public class ComponentScanConfig {
}
