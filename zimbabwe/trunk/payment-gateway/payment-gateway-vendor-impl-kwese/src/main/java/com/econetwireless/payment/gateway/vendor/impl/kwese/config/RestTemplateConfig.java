package com.econetwireless.payment.gateway.vendor.impl.kwese.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.net.InetSocketAddress;
import java.net.Proxy;

import static com.econetwireless.payment.gateway.vendor.impl.kwese.constants.ApplicationConstants.WebProxyConstants.*;

/**
 * Created by shingirai on 9/27/16.
 */
@Configuration
public class RestTemplateConfig {

    @Autowired
    private Environment environment;

    @Bean
    public RestTemplate restTemplate(){
        final SimpleClientHttpRequestFactory simpleClientHttpRequestFactory = new SimpleClientHttpRequestFactory();
        final Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(environment.getProperty(PROXY_HOST),
                        Integer.parseInt(environment.getProperty(PROXY_PORT))));
        simpleClientHttpRequestFactory.setProxy(proxy);
        simpleClientHttpRequestFactory.setConnectTimeout(CONNECT_TIMEOUT);
        simpleClientHttpRequestFactory.setReadTimeout(READ_TIMEOUT);
        return new RestTemplate(simpleClientHttpRequestFactory);
    }

}
