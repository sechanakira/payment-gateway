package com.econetwireless.payment.gateway.vendor.impl.kwese.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by shingirai on 10/3/16.
 */
@Configuration
@EnableScheduling
public class SchedulingConfig {

}
