package com.econetwireless.payment.gateway.vendor.impl.kwese.constants;

/**
 * Created by shingirai on 9/27/16.
 */
public class ApplicationConstants {

    private ApplicationConstants(){
    }

    public static class URLConstants{
        public static final String TOKEN_REQUEST_URL = "https://pr2.crm.com/crmapi/rest/v2/authentication/token/";
        public static final String CUSTOMER_LOOKUP_URL = "https://pr3.crm.com/crmapi/rest/v2/users/show";
        public static final String PAYMENT_URL = "https://pr3.crm.com/crmapi/rest/v2/payments/create";
        private URLConstants(){
        }
    }

    public static class ResponseConstants{
        public static final String TOKEN_REQUEST_SUCCESS_RESPONSE_CODE = "OK";
        private ResponseConstants(){
        }
    }

    public static class UserIdentifierConstants{
        public static final String USER_IDENTIFIER = "user_identifier=";
        public static final String ID = "id=";
        public static final String USER_NAME = "username=";
        private UserIdentifierConstants(){
        }
    }

    public static class WebProxyConstants{
        public static final String PROXY_HOST = "PROXY_HOST_IP";
        public static final String PROXY_PORT = "PROXY_PORT";
        public static final int CONNECT_TIMEOUT = 5000;
        public static final int READ_TIMEOUT = 18000;
        private WebProxyConstants(){
        }
    }

}
