package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.lookup;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by shingirai on 10/3/16.
 */
public class CustomerData implements Serializable {

	@JsonProperty("cti_phone_extension_number")
	private String ctiPhoneExtensionNumber;
	private String domain;
	private boolean active;
	@JsonProperty("udf_float_1")
	private String udfFloat1;
	@JsonProperty("udf_float_2")
	private String udfFloat2;
	@JsonProperty("udf_float_3")
	private String udfFloat3;
	@JsonProperty("udf_float_4")
	private String udfFloat4;
	private boolean developer;
	@JsonProperty("udf_string_1")
	private String udfString1;
	@JsonProperty("udf_string_2")
	private String udfString2;
	@JsonProperty("contact_information")
	private CustomerInformation customerInformation;

	public String getCtiPhoneExtensionNumber() {
		return ctiPhoneExtensionNumber;
	}

	public void setCtiPhoneExtensionNumber(String ctiPhoneExtensionNumber) {
		this.ctiPhoneExtensionNumber = ctiPhoneExtensionNumber;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getUdfFloat1() {
		return udfFloat1;
	}

	public void setUdfFloat1(String udfFloat1) {
		this.udfFloat1 = udfFloat1;
	}

	public String getUdfFloat2() {
		return udfFloat2;
	}

	public void setUdfFloat2(String udfFloat2) {
		this.udfFloat2 = udfFloat2;
	}

	public String getUdfFloat3() {
		return udfFloat3;
	}

	public void setUdfFloat3(String udfFloat3) {
		this.udfFloat3 = udfFloat3;
	}

	public String getUdfFloat4() {
		return udfFloat4;
	}

	public void setUdfFloat4(String udfFloat4) {
		this.udfFloat4 = udfFloat4;
	}

	public boolean isDeveloper() {
		return developer;
	}

	public void setDeveloper(boolean developer) {
		this.developer = developer;
	}

	public String getUdfString1() {
		return udfString1;
	}

	public void setUdfString1(String udfString1) {
		this.udfString1 = udfString1;
	}

	public String getUdfString2() {
		return udfString2;
	}

	public void setUdfString2(String udfString2) {
		this.udfString2 = udfString2;
	}

	public CustomerInformation getCustomerInformation() {
		return customerInformation;
	}

	public void setCustomerInformation(CustomerInformation customerInformation) {
		this.customerInformation = customerInformation;
	}

	@Override public String toString() {
		return "CustomerData{" + "ctiPhoneExtensionNumber='" + ctiPhoneExtensionNumber + '\'' + ", domain='" + domain + '\'' + ", active=" + active + ", udfFloat1='" + udfFloat1 + '\'' + ", udfFloat2='" + udfFloat2 + '\'' + ", udfFloat3='" + udfFloat3 + '\'' + ", udfFloat4='" + udfFloat4 + '\'' + ", developer=" + developer + ", udfString1='" + udfString1 + '\'' + ", udfString2='" + udfString2 + '\'' + ", customerInformation=" + customerInformation + '}';
	}
}
