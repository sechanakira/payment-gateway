package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.lookup;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shingirai on 10/3/16.
 */
public class CustomerInformation implements Serializable {
	/*
	 "udf_string_3": null,
        "udf_string_4": null,
        "super_user": true,
        "udf_string_5": null,
        "udf_string_6": null,
        "udf_string_7": null,
	 */

	@JsonProperty("last_name")
	private String lastName;
	@JsonProperty("middle_name")
	private String middleName;
	@JsonProperty("life_cycle_state")
	private String lifeCycleState;
	@JsonProperty("company_profile")
	private String companyProfile;
	private String title;
	@JsonProperty("first_name")
	private String firstName;
	private String type;
	private String name;
	private String id;
	@JsonProperty("company_name")
	private String companyName;
	@JsonProperty("demographics")
	private Demographics demographics;
	@JsonProperty("udf_string_3")
	private String udfString3;
	@JsonProperty("udf_string_4")
	private String udfString4;
	@JsonProperty("super_user")
	private boolean superUser;
	@JsonProperty("udf_string_5")
	private String udfString5;
	@JsonProperty("udf_string_6")
	private String udfString6;
	@JsonProperty("udf_string_7")
	private String udfString7;
	@JsonProperty("units_set")
	private List<UnitSet> unitSets;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLifeCycleState() {
		return lifeCycleState;
	}

	public void setLifeCycleState(String lifeCycleState) {
		this.lifeCycleState = lifeCycleState;
	}

	public String getCompanyProfile() {
		return companyProfile;
	}

	public void setCompanyProfile(String companyProfile) {
		this.companyProfile = companyProfile;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Demographics getDemographics() {
		return demographics;
	}

	public void setDemographics(Demographics demographics) {
		this.demographics = demographics;
	}

	public String getUdfString3() {
		return udfString3;
	}

	public void setUdfString3(String udfString3) {
		this.udfString3 = udfString3;
	}

	public String getUdfString4() {
		return udfString4;
	}

	public void setUdfString4(String udfString4) {
		this.udfString4 = udfString4;
	}

	public boolean isSuperUser() {
		return superUser;
	}

	public void setSuperUser(boolean superUser) {
		this.superUser = superUser;
	}

	public String getUdfString5() {
		return udfString5;
	}

	public void setUdfString5(String udfString5) {
		this.udfString5 = udfString5;
	}

	public String getUdfString6() {
		return udfString6;
	}

	public void setUdfString6(String udfString6) {
		this.udfString6 = udfString6;
	}

	public String getUdfString7() {
		return udfString7;
	}

	public void setUdfString7(String udfString7) {
		this.udfString7 = udfString7;
	}

	public List<UnitSet> getUnitSets() {
		return unitSets;
	}

	public void setUnitSets(List<UnitSet> unitSets) {
		this.unitSets = unitSets;
	}

	@Override public String toString() {
		return "CustomerInformation{" + "lastName='" + lastName + '\'' + ", middleName='" + middleName + '\'' + ", lifeCycleState='" + lifeCycleState + '\'' + ", companyProfile='" + companyProfile + '\'' + ", title='" + title + '\'' + ", firstName='" + firstName + '\'' + ", type='" + type + '\'' + ", name='" + name + '\'' + ", id='" + id + '\'' + ", companyName='" + companyName + '\'' + ", demographics=" + demographics + ", udfString3='" + udfString3 + '\'' + ", udfString4='" + udfString4 + '\'' + ", superUser=" + superUser + ", udfString5='" + udfString5 + '\'' + ", udfString6='" + udfString6 + '\'' + ", udfString7='" + udfString7 + '\'' + ", unitSets=" + unitSets + '}';
	}
}
