package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.lookup;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by shingirai on 10/3/16.
 */
public class CustomerLookupResponse implements Serializable {

	@JsonProperty("status")
	private CustomerLookupRequestStatus customerLookupRequestStatus;
	@JsonProperty("data")
	private CustomerData customerData;

	public CustomerLookupRequestStatus getCustomerLookupRequestStatus() {
		return customerLookupRequestStatus;
	}

	public void setCustomerLookupRequestStatus(CustomerLookupRequestStatus customerLookupRequestStatus) {
		this.customerLookupRequestStatus = customerLookupRequestStatus;
	}

	public CustomerData getCustomerData() {
		return customerData;
	}

	public void setCustomerData(CustomerData customerData) {
		this.customerData = customerData;
	}
}
