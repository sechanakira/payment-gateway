package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.lookup;

import java.io.Serializable;

/**
 * Created by shingirai on 10/3/16.
 */
public class DateOfBirth implements Serializable {

	private int month;
	private int day;
	private int year;

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override public String toString() {
		return "DateOfBirth{" + "month=" + month + ", day=" + day + ", year=" + year + '}';
	}
}
