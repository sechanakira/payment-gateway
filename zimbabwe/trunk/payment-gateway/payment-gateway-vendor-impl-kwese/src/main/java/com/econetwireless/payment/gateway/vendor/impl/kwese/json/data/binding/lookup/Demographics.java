package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.lookup;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by shingirai on 10/3/16.
 */
public class Demographics implements Serializable {

	private String gender;
	@JsonProperty("passport_issued_by_country")
	private String passportIssuedByCountry;
	@JsonProperty("social_security_number")
	private String socialSecurityNumber;
	@JsonProperty("passport_number")
	private String passPortNumber;
	@JsonProperty("industry_sector")
	private String industrySector;
	@JsonProperty("country_of_residence")
	private ResidenceCountry residenceCountry;
	@JsonProperty("name_day")
	private NameDay nameDay;
	@JsonProperty("date_of_birth")
	private DateOfBirth dateOfBirth;
	@JsonProperty("id_number")
	private String idNumber;
	private String industry;
	@JsonProperty("id_issued_by_country")
	private IDIssuedByCountry idIssuedByCountry;

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPassportIssuedByCountry() {
		return passportIssuedByCountry;
	}

	public void setPassportIssuedByCountry(String passportIssuedByCountry) {
		this.passportIssuedByCountry = passportIssuedByCountry;
	}

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public String getPassPortNumber() {
		return passPortNumber;
	}

	public void setPassPortNumber(String passPortNumber) {
		this.passPortNumber = passPortNumber;
	}

	public String getIndustrySector() {
		return industrySector;
	}

	public void setIndustrySector(String industrySector) {
		this.industrySector = industrySector;
	}

	public ResidenceCountry getResidenceCountry() {
		return residenceCountry;
	}

	public void setResidenceCountry(ResidenceCountry residenceCountry) {
		this.residenceCountry = residenceCountry;
	}

	public NameDay getNameDay() {
		return nameDay;
	}

	public void setNameDay(NameDay nameDay) {
		this.nameDay = nameDay;
	}

	public DateOfBirth getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(DateOfBirth dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public IDIssuedByCountry getIdIssuedByCountry() {
		return idIssuedByCountry;
	}

	public void setIdIssuedByCountry(IDIssuedByCountry idIssuedByCountry) {
		this.idIssuedByCountry = idIssuedByCountry;
	}

	@Override public String toString() {
		return "Demographics{" + "gender='" + gender + '\'' + ", passportIssuedByCountry='" + passportIssuedByCountry + '\'' + ", socialSecurityNumber='" + socialSecurityNumber + '\'' + ", passPortNumber='" + passPortNumber + '\'' + ", industrySector='" + industrySector + '\'' + ", residenceCountry=" + residenceCountry + ", nameDay=" + nameDay + ", dateOfBirth=" + dateOfBirth + ", idNumber='" + idNumber + '\'' + ", industry='" + industry + '\'' + ", idIssuedByCountry=" + idIssuedByCountry + '}';
	}
}
