package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.lookup;

import java.io.Serializable;

/**
 * Created by shingirai on 10/3/16.
 */
public class NameDay implements Serializable {

	private int month;
	private int day;

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	@Override public String toString() {
		return "NameDay{" + "month=" + month + ", day=" + day + '}';
	}
}
