package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.lookup;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by shingirai on 10/3/16.
 */
public class ResidenceCountry implements Serializable {

	@JsonProperty("three_character_code")
	private String threeCharacterCode;
	@JsonProperty("two_character_code")
	private String twoCharacterCode;
	private String id;
	private String name;
	@JsonProperty("alternative_code")
	private String alternativeCode;

	public String getThreeCharacterCode() {
		return threeCharacterCode;
	}

	public void setThreeCharacterCode(String threeCharacterCode) {
		this.threeCharacterCode = threeCharacterCode;
	}

	public String getTwoCharacterCode() {
		return twoCharacterCode;
	}

	public void setTwoCharacterCode(String twoCharacterCode) {
		this.twoCharacterCode = twoCharacterCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlternativeCode() {
		return alternativeCode;
	}

	public void setAlternativeCode(String alternativeCode) {
		this.alternativeCode = alternativeCode;
	}

	@Override public String toString() {
		return "ResidenceCountry{" + "threeCharacterCode='" + threeCharacterCode + '\'' + ", twoCharacterCode='" + twoCharacterCode + '\'' + ", id='" + id + '\'' + ", name='" + name + '\'' + ", alternativeCode='" + alternativeCode + '\'' + '}';
	}
}
