package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.lookup;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by shingirai on 10/3/16.
 */
public class Unit implements Serializable {

	@JsonProperty("community_name")
	private String communityName;
	private String id;
	private String name;
	@JsonProperty("group_name")
	private String groupName;
	private String description;
	@JsonProperty("alternative_code")
	private String alternativeCode;

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAlternativeCode() {
		return alternativeCode;
	}

	public void setAlternativeCode(String alternativeCode) {
		this.alternativeCode = alternativeCode;
	}
}
