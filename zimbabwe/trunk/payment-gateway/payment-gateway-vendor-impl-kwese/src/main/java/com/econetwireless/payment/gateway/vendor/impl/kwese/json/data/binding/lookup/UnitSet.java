package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.lookup;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by shingirai on 10/3/16.
 */
public class UnitSet implements Serializable {
	@JsonProperty("default")
	private boolean defaultState;
	private String id;
	@JsonProperty("unit")
	private Unit unit;
}
