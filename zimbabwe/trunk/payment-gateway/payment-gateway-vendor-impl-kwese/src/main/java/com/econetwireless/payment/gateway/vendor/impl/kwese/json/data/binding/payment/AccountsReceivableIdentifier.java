package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.payment;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by shingirai on 10/3/16.
 */
public class AccountsReceivableIdentifier implements Serializable {

	@JsonProperty("number")
	private String accountNumber;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@Override public String toString() {
		return "AccountsReceivableIdentifier{" + "accountNumber='" + accountNumber + '\'' + '}';
	}
}
