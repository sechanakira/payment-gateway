package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.payment;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by shingirai on 10/4/16.
 */
public class BillIdentifier implements Serializable {

	@JsonProperty("number")
	private String billNumber;

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	@Override public String toString() {
		return "BillIdentifier{" + "billNumber='" + billNumber + '\'' + '}';
	}
}
