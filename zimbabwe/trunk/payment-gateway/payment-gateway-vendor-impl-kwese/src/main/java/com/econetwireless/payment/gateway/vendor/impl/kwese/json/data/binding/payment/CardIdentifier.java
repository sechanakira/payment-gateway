package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.payment;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by shingirai on 10/4/16.
 */
public class CardIdentifier implements Serializable {

	@JsonProperty("payment_gateway_reference_number")
	private String paymentGatewayReferenceNumber;

	public String getPaymentGatewayReferenceNumber() {
		return paymentGatewayReferenceNumber;
	}

	public void setPaymentGatewayReferenceNumber(String paymentGatewayReferenceNumber) {
		this.paymentGatewayReferenceNumber = paymentGatewayReferenceNumber;
	}

	@Override public String toString() {
		return "CardIdentifier{" + "paymentGatewayReferenceNumber='" + paymentGatewayReferenceNumber + '\'' + '}';
	}
}
