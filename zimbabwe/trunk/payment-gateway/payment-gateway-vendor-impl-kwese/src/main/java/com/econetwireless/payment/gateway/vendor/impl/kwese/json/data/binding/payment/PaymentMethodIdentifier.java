package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.payment;

import java.io.Serializable;

/**
 * Created by shingirai on 10/4/16.
 */
public class PaymentMethodIdentifier implements Serializable {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override public String toString() {
		return "PaymentMethodIdentifier{" + "name='" + name + '\'' + '}';
	}
}
