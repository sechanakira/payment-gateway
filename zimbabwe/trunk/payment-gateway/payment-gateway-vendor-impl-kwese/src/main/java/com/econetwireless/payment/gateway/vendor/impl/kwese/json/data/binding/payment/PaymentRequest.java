package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.payment;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Created by shingirai on 10/3/16.
 */
public class PaymentRequest implements Serializable {

	private String token;
	@JsonProperty("accounts_receivable_identifier")
	private AccountsReceivableIdentifier accountsReceivableIdentifier;
	@JsonProperty("type_identifier")
	private TypeIdentifier typeIdentifier;
	@JsonProperty("life_cycle_state")
	private String lifeCycleState;
	@JsonProperty("payment_method_identifier")
	private PaymentMethodIdentifier paymentMethodIdentifier;
	@JsonProperty("card_identifier")
	private CardIdentifier cardIdentifier;
	@JsonProperty("payment_amount")
	private String paymentAmount;
	@JsonProperty("received_on")
	private Date dateReceived;
	@JsonProperty("received_by_user_identifier")
	private ReceiverUserIdentifier receiverUserIdentifier;
	@JsonProperty("received_by_unit_identifier")
	private ReceiverUnitIdentifier receiverUnitIdentifier;
	private String description;
	private String notes;
	@JsonProperty("udf_string_1")
	private String udfString1;
	@JsonProperty("udf_string_2")
	private String udfString2;
	@JsonProperty("udf_string_3")
	private String udfString3;
	@JsonProperty("udf_string_4")
	private String udfString4;
	@JsonProperty("udf_string_5")
	private String udfString5;
	@JsonProperty("udf_string_6")
	private String udfString6;
	@JsonProperty("udf_string_7")
	private String udfString7;
	@JsonProperty("udf_string_8")
	private String udfString8;
	@JsonProperty("udf_float_1")
	private float udfFloat1;
	@JsonProperty("udf_float_2")
	private float udfFloat2;
	@JsonProperty("udf_float_3")
	private float udfFloat3;
	@JsonProperty("udf_float_4")
	private float udfFloat4;
	@JsonProperty("udf_date_1")
	private Date udfDate1;
	@JsonProperty("udf_date_2")
	private Date udfDate2;
	@JsonProperty("udf_date_3")
	private Date udfDate3;
	@JsonProperty("udf_date_4")
	private Date udfDate4;
	@JsonProperty("bills_to_pay_set")
	private Set<BillIdentifier> billSet;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public AccountsReceivableIdentifier getAccountsReceivableIdentifier() {
		return accountsReceivableIdentifier;
	}

	public void setAccountsReceivableIdentifier(AccountsReceivableIdentifier accountsReceivableIdentifier) {
		this.accountsReceivableIdentifier = accountsReceivableIdentifier;
	}

	public TypeIdentifier getTypeIdentifier() {
		return typeIdentifier;
	}

	public void setTypeIdentifier(TypeIdentifier typeIdentifier) {
		this.typeIdentifier = typeIdentifier;
	}

	public String getLifeCycleState() {
		return lifeCycleState;
	}

	public void setLifeCycleState(String lifeCycleState) {
		this.lifeCycleState = lifeCycleState;
	}

	public PaymentMethodIdentifier getPaymentMethodIdentifier() {
		return paymentMethodIdentifier;
	}

	public void setPaymentMethodIdentifier(PaymentMethodIdentifier paymentMethodIdentifier) {
		this.paymentMethodIdentifier = paymentMethodIdentifier;
	}

	public CardIdentifier getCardIdentifier() {
		return cardIdentifier;
	}

	public void setCardIdentifier(CardIdentifier cardIdentifier) {
		this.cardIdentifier = cardIdentifier;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Date getDateReceived() {
		return dateReceived;
	}

	public void setDateReceived(Date dateReceived) {
		this.dateReceived = dateReceived;
	}

	public ReceiverUserIdentifier getReceiverUserIdentifier() {
		return receiverUserIdentifier;
	}

	public void setReceiverUserIdentifier(ReceiverUserIdentifier receiverUserIdentifier) {
		this.receiverUserIdentifier = receiverUserIdentifier;
	}

	public ReceiverUnitIdentifier getReceiverUnitIdentifier() {
		return receiverUnitIdentifier;
	}

	public void setReceiverUnitIdentifier(ReceiverUnitIdentifier receiverUnitIdentifier) {
		this.receiverUnitIdentifier = receiverUnitIdentifier;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getUdfString1() {
		return udfString1;
	}

	public void setUdfString1(String udfString1) {
		this.udfString1 = udfString1;
	}

	public String getUdfString2() {
		return udfString2;
	}

	public void setUdfString2(String udfString2) {
		this.udfString2 = udfString2;
	}

	public String getUdfString3() {
		return udfString3;
	}

	public void setUdfString3(String udfString3) {
		this.udfString3 = udfString3;
	}

	public String getUdfString4() {
		return udfString4;
	}

	public void setUdfString4(String udfString4) {
		this.udfString4 = udfString4;
	}

	public String getUdfString5() {
		return udfString5;
	}

	public void setUdfString5(String udfString5) {
		this.udfString5 = udfString5;
	}

	public String getUdfString6() {
		return udfString6;
	}

	public void setUdfString6(String udfString6) {
		this.udfString6 = udfString6;
	}

	public String getUdfString7() {
		return udfString7;
	}

	public void setUdfString7(String udfString7) {
		this.udfString7 = udfString7;
	}

	public String getUdfString8() {
		return udfString8;
	}

	public void setUdfString8(String udfString8) {
		this.udfString8 = udfString8;
	}

	public float getUdfFloat1() {
		return udfFloat1;
	}

	public void setUdfFloat1(float udfFloat1) {
		this.udfFloat1 = udfFloat1;
	}

	public float getUdfFloat2() {
		return udfFloat2;
	}

	public void setUdfFloat2(float udfFloat2) {
		this.udfFloat2 = udfFloat2;
	}

	public float getUdfFloat3() {
		return udfFloat3;
	}

	public void setUdfFloat3(float udfFloat3) {
		this.udfFloat3 = udfFloat3;
	}

	public float getUdfFloat4() {
		return udfFloat4;
	}

	public void setUdfFloat4(float udfFloat4) {
		this.udfFloat4 = udfFloat4;
	}

	public Date getUdfDate1() {
		return udfDate1;
	}

	public void setUdfDate1(Date udfDate1) {
		this.udfDate1 = udfDate1;
	}

	public Date getUdfDate2() {
		return udfDate2;
	}

	public void setUdfDate2(Date udfDate2) {
		this.udfDate2 = udfDate2;
	}

	public Date getUdfDate3() {
		return udfDate3;
	}

	public void setUdfDate3(Date udfDate3) {
		this.udfDate3 = udfDate3;
	}

	public Date getUdfDate4() {
		return udfDate4;
	}

	public void setUdfDate4(Date udfDate4) {
		this.udfDate4 = udfDate4;
	}

	public Set<BillIdentifier> getBillSet() {
		return billSet;
	}

	public void setBillSet(Set<BillIdentifier> billSet) {
		this.billSet = billSet;
	}

	@Override public String toString() {
		return "PaymentRequest{" + "token='" + token + '\'' + ", accountsReceivableIdentifier=" + accountsReceivableIdentifier + ", typeIdentifier=" + typeIdentifier + ", lifeCycleState='" + lifeCycleState + '\'' + ", paymentMethodIdentifier=" + paymentMethodIdentifier + ", cardIdentifier=" + cardIdentifier + ", paymentAmount='" + paymentAmount + '\'' + ", dateReceived=" + dateReceived + ", receiverUserIdentifier=" + receiverUserIdentifier + ", receiverUnitIdentifier=" + receiverUnitIdentifier + ", description='" + description + '\'' + ", notes='" + notes + '\'' + ", udfString1='" + udfString1 + '\'' + ", udfString2='" + udfString2 + '\'' + ", udfString3='" + udfString3 + '\'' + ", udfString4='" + udfString4 + '\'' + ", udfString5='" + udfString5 + '\'' + ", udfString6='" + udfString6 + '\'' + ", udfString7='" + udfString7 + '\'' + ", udfString8='" + udfString8 + '\'' + ", udfFloat1=" + udfFloat1 + ", udfFloat2=" + udfFloat2 + ", udfFloat3=" + udfFloat3 + ", udfFloat4=" + udfFloat4 + ", udfDate1=" + udfDate1 + ", udfDate2=" + udfDate2 + ", udfDate3=" + udfDate3 + ", udfDate4=" + udfDate4 + ", billSet=" + billSet + '}';
	}
}
