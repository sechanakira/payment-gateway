package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.payment;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by shingirai on 10/4/16.
 */
public class PaymentResponse implements Serializable {

	@JsonProperty("status")
	private PaymentResponseStatus paymentResponseStatus;
}
