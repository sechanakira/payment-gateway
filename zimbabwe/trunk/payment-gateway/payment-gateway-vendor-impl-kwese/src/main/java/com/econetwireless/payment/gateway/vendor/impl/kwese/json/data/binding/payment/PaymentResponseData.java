package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.payment;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by shingirai on 10/4/16.
 */
public class PaymentResponseData implements Serializable {

	@JsonProperty("payment_gateway_reference_number")
	private String paymentGatewayReferenceNumber;
	@JsonProperty("payment_amount")
	private double paymnetAmount;
	@JsonProperty("issued_on")
	private Date issueDate;
	@JsonProperty("processed_by_payment_gateway")
	private String paymentGateway;
	@JsonProperty("life_cycle_state")
	private String lifeCycleState;
	private String number;
	private String id;
	@JsonProperty("reference_number")
	private String referenceNumber;
	@JsonProperty("posted_on")
	private Date postDate;

	public String getPaymentGatewayReferenceNumber() {
		return paymentGatewayReferenceNumber;
	}

	public void setPaymentGatewayReferenceNumber(String paymentGatewayReferenceNumber) {
		this.paymentGatewayReferenceNumber = paymentGatewayReferenceNumber;
	}

	public double getPaymnetAmount() {
		return paymnetAmount;
	}

	public void setPaymnetAmount(double paymnetAmount) {
		this.paymnetAmount = paymnetAmount;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public String getLifeCycleState() {
		return lifeCycleState;
	}

	public void setLifeCycleState(String lifeCycleState) {
		this.lifeCycleState = lifeCycleState;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	@Override public String toString() {
		return "PaymentResponseData{" + "paymentGatewayReferenceNumber='" + paymentGatewayReferenceNumber + '\'' + ", paymnetAmount=" + paymnetAmount + ", issueDate=" + issueDate + ", paymentGateway='" + paymentGateway + '\'' + ", lifeCycleState='" + lifeCycleState + '\'' + ", number='" + number + '\'' + ", id='" + id + '\'' + ", referenceNumber='" + referenceNumber + '\'' + ", postDate=" + postDate + '}';
	}
}
