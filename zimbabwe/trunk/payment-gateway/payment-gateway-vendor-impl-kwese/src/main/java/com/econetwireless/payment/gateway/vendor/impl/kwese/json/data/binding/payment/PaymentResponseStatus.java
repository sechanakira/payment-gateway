package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.payment;

import java.io.Serializable;

/**
 * Created by shingirai on 10/4/16.
 */
public class PaymentResponseStatus implements Serializable {

	private String message;
	private String description;
	private String code;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override public String toString() {
		return "PaymentResponseStatus{" + "message='" + message + '\'' + ", description='" + description + '\'' + ", code='" + code + '\'' + '}';
	}
}
