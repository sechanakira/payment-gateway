package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.payment;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by shingirai on 10/4/16.
 */
public class ReceiverUserIdentifier implements Serializable {

	@JsonProperty("username")
	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override public String toString() {
		return "ReceiverUserIdentifier{" + "userName='" + userName + '\'' + '}';
	}
}
