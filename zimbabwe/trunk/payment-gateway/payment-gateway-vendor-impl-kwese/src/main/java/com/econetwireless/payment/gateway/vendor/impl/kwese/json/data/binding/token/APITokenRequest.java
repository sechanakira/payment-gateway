package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.token;

import java.io.Serializable;

/**
 * Created by shingirai on 9/27/16.
 */
public class APITokenRequest implements Serializable {

    private String username;
    private String password;
    private String organisation;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    @Override
    public String toString() {
        return "APITokenRequest{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", organisation='" + organisation + '\'' +
                '}';
    }
}
