package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.token;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by shingirai on 9/27/16.
 */
public class APITokenResponse implements Serializable {

    @JsonProperty("data")
    private TokenData tokenData;
    @JsonProperty("status")
    private APITokenResponseStatus apiTokenResponseStatus;

    public TokenData getTokenData() {
        return tokenData;
    }

    public void setTokenData(TokenData tokenData) {
        this.tokenData = tokenData;
    }

    public APITokenResponseStatus getApiTokenResponseStatus() {
        return apiTokenResponseStatus;
    }

    public void setApiTokenResponseStatus(APITokenResponseStatus apiTokenResponseStatus) {
        this.apiTokenResponseStatus = apiTokenResponseStatus;
    }

    @Override
    public String toString() {
        return "APITokenResponse{" +
                "tokenData=" + tokenData +
                ", apiTokenResponseStatus=" + apiTokenResponseStatus +
                '}';
    }

	/**
	 * Created by shingirai on 10/4/16.
	 */
	public static class TokenRequest implements Serializable {

		private String username;
		private String password;
		private String organisation;

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getOrganisation() {
			return organisation;
		}

		public void setOrganisation(String organisation) {
			this.organisation = organisation;
		}

		@Override public String toString() {
			return "TokenRequest{" + "username='" + username + '\'' + ", password='" + password + '\'' + ", organisation='" + organisation + '\'' + '}';
		}
	}
}
