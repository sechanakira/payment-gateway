package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.token;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by shingirai on 9/27/16.
 */
public class APITokenResponseStatus implements Serializable {

    @JsonProperty("code")
    private String status;
    private String description;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "APITokenResponseStatus{" +
                "status='" + status + '\'' +
                ", description='" + description + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
