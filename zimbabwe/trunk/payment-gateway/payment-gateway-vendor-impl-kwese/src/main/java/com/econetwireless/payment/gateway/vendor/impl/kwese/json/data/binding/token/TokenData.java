package com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.token;


import java.io.Serializable;

/**
 * Created by shingirai on 9/27/16.
 */
public class TokenData implements Serializable {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "TokenData{" +
                "token='" + token + '\'' +
                '}';
    }
}
