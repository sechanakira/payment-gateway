package com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by shingirai on 9/27/16.
 */
@Entity
@Table(name = "api_token")
public class APIToken implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "token")
    private String token;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
