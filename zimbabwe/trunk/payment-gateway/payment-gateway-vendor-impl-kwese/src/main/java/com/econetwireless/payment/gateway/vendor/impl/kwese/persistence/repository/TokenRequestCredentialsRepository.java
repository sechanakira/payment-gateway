package com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.repository;

import com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.model.TokenRequestCredentials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by shingirai on 9/27/16.
 */
@Repository
public interface TokenRequestCredentialsRepository extends JpaRepository<TokenRequestCredentials,Long> {
}
