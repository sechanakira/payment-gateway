package com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.service;

import com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.token.APITokenResponse;
import com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.model.APIToken;
import com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.model.TokenRequestCredentials;

/**
 * Created by shingirai on 9/27/16.
 */
public interface TokenService {
    TokenRequestCredentials getTokenRequestCredentials();
    void saveToken(APITokenResponse apiTokenResponse);
    APIToken getAPIToken();
}
