package com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.service;

import com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.token.APITokenResponse;
import com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.model.APIToken;
import com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.model.TokenRequestCredentials;
import com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.repository.APITokenRepository;
import com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.repository.TokenRequestCredentialsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by shingirai on 9/27/16.
 */
@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenRequestCredentialsRepository tokenRequestCredentialsRepository;

    @Autowired
    private APITokenRepository apiTokenRepository;

    @Override
    public TokenRequestCredentials getTokenRequestCredentials() {
        return tokenRequestCredentialsRepository.getOne(1L);
    }

    @Override
    public void saveToken(APITokenResponse apiTokenResponse) {
        Optional<APIToken> currentToken = Optional.of(getAPIToken());
        if(!currentToken.isPresent()){
            APIToken apiToken = new APIToken();
            apiToken.setToken(apiTokenResponse.getTokenData().getToken());
            apiTokenRepository.saveAndFlush(apiToken);
        }
        currentToken.ifPresent(apiToken-> apiToken.setToken(apiTokenResponse.getTokenData().getToken()));
    }

    @Override
    public APIToken getAPIToken() {
        return apiTokenRepository.getOne(1L);
    }
}
