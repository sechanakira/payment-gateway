package com.econetwireless.payment.gateway.vendor.impl.kwese.rest.client.lookup;

import com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.lookup.CustomerLookupResponse;

/**
 * Created by shingirai on 10/3/16.
 */
@FunctionalInterface
public interface CustomerLookupBean {
	CustomerLookupResponse findCustomer(String customerID);
}
