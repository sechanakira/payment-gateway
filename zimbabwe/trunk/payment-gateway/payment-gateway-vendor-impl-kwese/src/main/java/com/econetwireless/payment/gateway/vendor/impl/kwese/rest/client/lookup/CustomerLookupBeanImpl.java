package com.econetwireless.payment.gateway.vendor.impl.kwese.rest.client.lookup;

import com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.lookup.CustomerLookupResponse;
import com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import static com.econetwireless.payment.gateway.vendor.impl.kwese.constants.ApplicationConstants.URLConstants.CUSTOMER_LOOKUP_URL;
import static com.econetwireless.payment.gateway.vendor.impl.kwese.constants.ApplicationConstants.UserIdentifierConstants.ID;
import static com.econetwireless.payment.gateway.vendor.impl.kwese.constants.ApplicationConstants.UserIdentifierConstants.USER_IDENTIFIER;

/**
 * Created by shingirai on 10/3/16.
 */
@Component
public class CustomerLookupBeanImpl implements CustomerLookupBean {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private TokenService tokenService;

	private StringBuilder userIdentifier;

	@Override public CustomerLookupResponse findCustomer(String customerID) {
		userIdentifier.append(USER_IDENTIFIER + ID + customerID);
		ResponseEntity<CustomerLookupResponse> customerLookupResponseResponseEntity = restTemplate.getForEntity(CUSTOMER_LOOKUP_URL, CustomerLookupResponse.class,tokenService.getAPIToken().getToken(),userIdentifier);
		return customerLookupResponseResponseEntity.getBody();
	}
}
