package com.econetwireless.payment.gateway.vendor.impl.kwese.rest.client.payment;

import com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.payment.PaymentRequest;
import com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.payment.PaymentResponse;

/**
 * Created by shingirai on 10/4/16.
 */
@FunctionalInterface
public interface PaymentsBean {
	PaymentResponse createPayment(PaymentRequest paymentRequest);
}
