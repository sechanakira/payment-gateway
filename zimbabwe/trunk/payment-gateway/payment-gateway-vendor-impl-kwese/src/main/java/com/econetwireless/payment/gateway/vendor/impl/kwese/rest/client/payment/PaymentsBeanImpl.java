package com.econetwireless.payment.gateway.vendor.impl.kwese.rest.client.payment;

import com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.payment.PaymentRequest;
import com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.payment.PaymentResponse;
import com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import static com.econetwireless.payment.gateway.vendor.impl.kwese.constants.ApplicationConstants.URLConstants.PAYMENT_URL;
/**
 * Created by shingirai on 10/4/16.
 */
@Component
public class PaymentsBeanImpl implements PaymentsBean {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private TokenService tokenService;

	@Override public PaymentResponse createPayment(PaymentRequest paymentRequest) {
		ResponseEntity<PaymentResponse> paymentResponseResponseEntity = restTemplate.postForEntity(PAYMENT_URL,paymentRequest,PaymentResponse.class);
		return paymentResponseResponseEntity.getBody();
	}
}
