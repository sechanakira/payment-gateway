package com.econetwireless.payment.gateway.vendor.impl.kwese.rest.client.token;

import com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.token.APITokenResponse;
import com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Optional;

import static com.econetwireless.payment.gateway.vendor.impl.kwese.constants.ApplicationConstants.ResponseConstants.TOKEN_REQUEST_SUCCESS_RESPONSE_CODE;

/**
 * Created by shingirai on 9/27/16.
 */
@Component
@DependsOn({"tokenRequestBeanImpl","tokenServiceImpl"})
public class ScheduledTokenRetrievalBean {

    @Autowired
    private TokenRequestBean tokenRequestBean;

    @Autowired
    private TokenService tokenService;

    //@PostConstruct
    //@Scheduled(fixedDelay = 45*60*1000)
    public void getAPIToken(){
        APITokenResponse apiTokenResponse = tokenRequestBean.requestAccessToken();
        Optional<String> statusCode = Optional.of(apiTokenResponse.getApiTokenResponseStatus().getStatus());
        statusCode.ifPresent(status->{
            if(TOKEN_REQUEST_SUCCESS_RESPONSE_CODE.equals(status)){
                tokenService.saveToken(apiTokenResponse);
            }
        });
    }
}
