package com.econetwireless.payment.gateway.vendor.impl.kwese.rest.client.token;

import com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.token.APITokenResponse;

/**
 * Created by shingirai on 9/27/16.
 */
@FunctionalInterface
public interface TokenRequestBean {
    APITokenResponse requestAccessToken();
}
