package com.econetwireless.payment.gateway.vendor.impl.kwese.rest.client.token;

import com.econetwireless.payment.gateway.vendor.impl.kwese.json.data.binding.token.APITokenResponse;
import com.econetwireless.payment.gateway.vendor.impl.kwese.persistence.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import static com.econetwireless.payment.gateway.vendor.impl.kwese.constants.ApplicationConstants.URLConstants.TOKEN_REQUEST_URL;

/**
 * Created by shingirai on 9/27/16.
 */
@Component
public class TokenRequestBeanImpl implements TokenRequestBean {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private TokenService tokenService;

    @Override
    public APITokenResponse requestAccessToken() {
        ResponseEntity<APITokenResponse> apiTokenResponseResponseEntity = restTemplate.postForEntity(TOKEN_REQUEST_URL,tokenService.getTokenRequestCredentials(),APITokenResponse.class);
        return apiTokenResponseResponseEntity.getBody();
    }
}
