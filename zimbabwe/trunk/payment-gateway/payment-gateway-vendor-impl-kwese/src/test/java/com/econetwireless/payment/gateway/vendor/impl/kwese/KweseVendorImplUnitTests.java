package com.econetwireless.payment.gateway.vendor.impl.kwese;

import com.econetwireless.payment.gateway.vendor.impl.kwese.config.ComponentScanConfig;
import com.econetwireless.payment.gateway.vendor.impl.kwese.config.RestTemplateConfig;
import com.econetwireless.payment.gateway.vendor.impl.kwese.config.SchedulingConfig;
import com.econetwireless.payment.gateway.vendor.impl.kwese.config.SpringDataJPAConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by shingirai on 9/28/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={ComponentScanConfig.class, RestTemplateConfig.class, SchedulingConfig.class, SpringDataJPAConfig.class})
public class KweseVendorImplUnitTests {

    @Test
    public void contextLoads() {
    }
}
